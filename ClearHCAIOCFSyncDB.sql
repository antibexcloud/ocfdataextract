CREATE PROCEDURE dbo.spClearHDB
AS
--
delete BranchLineItem
delete InsurerLineItem

delete FacilityLicenseLineItem
delete RegistrationLineItem
delete ProviderLineItem
delete FacilityLineItem

delete GS9LineItemAR
delete OCF9AR


delete GS18LineItemSubmit
delete OCF18InjuryLineItemSubmit
delete SessionHeaderLineItemSubmit
delete SessionLineItemSubmit
delete OCF18Submit

delete GS18LineItemAR
delete GS21BLineItemAR
delete OCF18AR


delete GS21BLineItemSubmit
delete OCF21BInjuryLineItemSubmit
delete OCF21BSubmit

delete GS21BLineItemAR
delete OCF21BAR


delete OCF21CInjuryLineItemSubmit
delete OtherReimbursableLineItemSubmit
delete PAFReimbursableLineItemSubmit
delete RenderedGSLineItemSubmit
delete OCF21CSubmit

delete OtherReimbursableLineItemAR
delete PAFReimbursableLineItemAR
delete from OCF21CAR


delete OCF23InjuryLineItemSubmit
delete OtherGSLineItemSubmit
delete PAFLineItemSubmit
delete OCF23Submit

delete OtherGSLineItemAR
delete OCF23AR


delete AACNPart4LineItemSubm
delete AACNServicesLineItemSubm
delete AACNSubm

delete AACNPart4LineItemAR
delete AACNServicesLineItemAR
delete AACNAR
--
GO
