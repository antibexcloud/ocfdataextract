
set nocount on;
declare @clientId float, @caseIdBase nvarchar(105), @DOL datetime, @DateOfAccident datetime;
declare @tblSrc table (clientId float, caseIdBase nvarchar(105), DOL datetime, DateOfAccident datetime)
-- get data
insert @tblSrc (clientId, caseIdBase, DOL, DateOfAccident)
select T.client_id, T.CaseID, T.DOL, T.DateOfAccident
from (

select distinct Tcl.client_id,
       Tcs.CaseID, Tcs.DOL, Tcs.MVAInsurerHCAIID,
	   isnull(OCF18.CIDOA, OCF23.CIDOA) as DateOfAccident,
	   isnull(OCF18.MVAInsurerHCAIID, OCF23.MVAInsurerHCAIID) as docMVAInsurerHCAIID
  from Client_Case Tcs join
       client Tcl on Tcl.client_id = Tcs.client_id join
       PaperWork Tpw on Tpw.CaseID = Tcs.CaseID left join
	   OCF18 on OCF18.ReportID = Tpw.ReportID left join
	   OCF23 on OCF23.ReportID = Tpw.ReportID
  where Tcs.CaseType = 'MVA'
    and Tcs.DOL is not null
	and isnull(OCF18.CIDOA, OCF23.CIDOA) is not null
	and (Tpw.ReportType = 'OCF18' or Tpw.ReportType = 'OCF23' or Tpw.ReportType = 'FORM1')
	and Tcs.DOL <> isnull(OCF18.CIDOA, OCF23.CIDOA)
	and isnull(OCF18.CIDOA, OCF23.CIDOA) > '2019-01-01'
) T
  where T.DOL <> T.DateOfAccident
    and (ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)) > 30
	     or (ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)) > 10 and T.MVAInsurerHCAIID <> '' and T.MVAInsurerHCAIID <> T.docMVAInsurerHCAIID))
  order by ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)),
           T.client_id, T.CaseID, T.DOL, T.DateOfAccident

--*** disable triggers ***
IF exists (select * from sysobjects where name='ClientCaseChargesUpdateTriggerCSharp' and xtype='TA')
  alter table [dbo].ClientCaseCharges disable trigger ClientCaseChargesUpdateTriggerCSharp
IF exists (select * from sysobjects where name='ClientCaseChargesUpdateTrigger' and xtype='TR')
  alter table [dbo].ClientCaseCharges disable trigger ClientCaseChargesUpdateTrigger
IF exists (select * from sysobjects where name='PaperWorkUpdateTrigger' and xtype='TA')
  alter table [dbo].PaperWork disable trigger PaperWorkUpdateTrigger
IF exists (select * from sysobjects where name='InvoceGeneralInfoUpdateTrigger' and xtype='TA')
  alter table [dbo].InvoceGeneralInfo disable trigger InvoceGeneralInfoUpdateTrigger
IF exists (select * from sysobjects where name='PaymentInfoUpdateTrigger' and xtype='TA')
  alter table [dbo].PaymentInfo disable trigger PaymentInfoUpdateTrigger

--
declare @caseIdNew nvarchar(105), @caseRoot nvarchar(5), @idx int,
        @isOCF18 bit, @withEHC bit, @ReportID float;

declare @tblRepIds table (ReportID float, isOCF18 bit, withEHC bit)
declare @tblInvoiceIds table (InvoiceID float)
declare @tblPmtIds table (PaymentID float)

declare @EHC1InsName [nvarchar](100),
	@EHC1PlanMember [nvarchar](105),
	@EHC1PolicyNo [nvarchar](50),
	@EHC1IDCertNo [nvarchar](50),
	@EHC2InsName [nvarchar](100),
	@EHC2PlanMember [nvarchar](105),
	@EHC2PolicyNo [nvarchar](50),
	@EHC2IDCertNo [nvarchar](50),

	@CIClaimNo [nvarchar](50),
	@CIPolicyNo [nvarchar](50),

	@MVAInsName [nvarchar](100),
	@MVAAddress [nvarchar](150),
	@MVACity [nvarchar](50),
	@MVAProvince [nvarchar](50),
	@MVAPostalCode [nvarchar](10),
	@MVAAFirstName [nvarchar](50),
	@MVAALastName [nvarchar](50),
	@MVAATel [nvarchar](20),
	@MVAAExt [nvarchar](20),
	@MVAAFax [nvarchar](20),
	@MVAPHSame [smallint],
	@MVAPHFirstName [nvarchar](50),
	@MVAPHLastName [nvarchar](50),
	@MVAInsurerHCAIID [nvarchar](50),
	@MVABranchHCAIID [nvarchar](50);

-- open cursor
declare cursCharge cursor local for select clientId, caseIdBase, DOL, DateOfAccident from @tblSrc
open cursCharge
fetch next from cursCharge into @clientId, @caseIdBase, @DOL, @DateOfAccident;
while @@FETCH_STATUS = 0 begin
  set @caseRoot = SUBSTRING(@caseIdBase, 1, 4);
  set @idx = 1;

  -- **** clearing ****
  delete @tblRepIds
  delete @tblInvoiceIds
  delete @tblPmtIds
  
  -- *** get plans
  insert @tblRepIds (ReportID, isOCF18, withEHC)
  select Tpw.ReportID,
         case when OCF18.ReportID is not null then 1 else 0 end,
		 case when OCF18.qEHC = 'Yes' or OCF23.qEHC = 'Yes' then 1 else 0 end
    from PaperWork Tpw left join
	     OCF18 on OCF18.ReportID = Tpw.ReportID left join
	     OCF23 on OCF23.ReportID = Tpw.ReportID
	where Tpw.CaseID = @caseIdBase
	  and isnull(OCF18.CIDOA, OCF23.CIDOA) = @DateOfAccident

  -- *** get invoices
  insert @tblInvoiceIds (InvoiceID)
  select InvoiceID
    from InvoceGeneralInfo
	where CaseID = @caseIdBase and ReportID > 0 and ReportID in (select ReportID from @tblRepIds)

  -- *** get payments
  insert @tblPmtIds (PaymentID)
  select PaymentID
    from PaidInvoiceRelation
	where InvoiceID in (select InvoiceID from @tblInvoiceIds)

  -- *** get plan ***
  set @ReportID = 0;
  if exists (select ReportID from @tblRepIds where withEHC = 1)
    select top 1 @ReportID = ReportID, @isOCF18 = isOCF18 from @tblRepIds where withEHC = 1
  else if exists (select ReportID from @tblRepIds where isOCF18 = 1)
    select top 1 @ReportID = ReportID, @isOCF18 = isOCF18 from @tblRepIds where isOCF18 = 1
  else
    select top 1 @ReportID = ReportID, @isOCF18 = isOCF18 from @tblRepIds

  if @isOCF18 = 1
  begin
    SELECT @MVAInsurerHCAIID = [MVAInsurerHCAIID], @MVABranchHCAIID = [MVABranchHCAIID]
      ,@MVAInsName = [MVAInsName]
      ,@MVAAddress = [MVAAddress]
      ,@MVACity = [MVACity]
      ,@MVAProvince = [MVAProvince]
      ,@MVAPostalCode = [MVAPostalCode]
      ,@MVAAFirstName = [MVAAFirstName]
      ,@MVAALastName = [MVAALastName]
      ,@MVAATel = [MVAATel]
      ,@MVAAExt = [MVAAExt]
      ,@MVAAFax = [MVAAFax]
      ,@MVAPHSame = [MVAPHSame]
      ,@MVAPHFirstName = [MVAPHFirstName]
      ,@MVAPHLastName = [MVAPHLastName]

	  ,@CIClaimNo = CIClaimNo
	  ,@CIPolicyNo = CIPolicyNo

      ,@EHC1InsName = [EHC1InsName]
      ,@EHC1PlanMember = [EHC1PlanMember]
      ,@EHC1PolicyNo = [EHC1PolicyNo]
      ,@EHC1IDCertNo = [EHC1IDCertNo]

      ,@EHC2InsName = [EHC2InsName]
      ,@EHC2PlanMember = [EHC2PlanMember]
      ,@EHC2PolicyNo = [EHC2PolicyNo]
      ,@EHC2IDCertNo = [EHC2IDCertNo]
    FROM [dbo].[OCF18]
	where ReportID = @ReportID
  end else begin
    SELECT @MVAInsurerHCAIID = [MVAInsurerHCAIID], @MVABranchHCAIID = [MVABranchHCAIID]
      ,@MVAInsName = [MVAInsName]
      ,@MVAAddress = [MVAAddress]
      ,@MVACity = [MVACity]
      ,@MVAProvince = [MVAProvince]
      ,@MVAPostalCode = [MVAPostalCode]
      ,@MVAAFirstName = [MVAAFirstName]
      ,@MVAALastName = [MVAALastName]
      ,@MVAATel = [MVAATel]
      ,@MVAAExt = [MVAAExt]
      ,@MVAAFax = [MVAAFax]
      ,@MVAPHSame = [MVAPHSame]
      ,@MVAPHFirstName = [MVAPHFirstName]
      ,@MVAPHLastName = [MVAPHLastName]

	  ,@CIClaimNo = CIClaimNo
	  ,@CIPolicyNo = CIPolicyNo

      ,@EHC1InsName = [EHC1InsName]
      ,@EHC1PlanMember = [EHC1PlanMember]
      ,@EHC1PolicyNo = [EHC1PolicyNo]
      ,@EHC1IDCertNo = [EHC1IDCertNo]

      ,@EHC2InsName = [EHC2InsName]
      ,@EHC2PlanMember = [EHC2PlanMember]
      ,@EHC2PolicyNo = [EHC2PolicyNo]
      ,@EHC2IDCertNo = [EHC2IDCertNo]
    FROM [dbo].[OCF23]
	where ReportID = @ReportID
  end;

  -- I - make new CASE name
  while @idx < 100
  begin
    set @caseIdNew = @caseRoot + cast(@idx as nvarchar) + '_' + cast(cast(@clientId as int) as nvarchar);
	if not exists(select * from Client_Case where CaseID = @caseIdNew)
	  break;
	set @idx = @idx + 1;
  end;
  
  -- II - make new CASE
  select 'adding case ' + @caseIdNew
  insert dbo.[Client_Case] ([client_id],[CaseID],[CaseType],[OpenDate],[CaseStatus],[DOL]
      ,[MVAClaimNo]
      ,[MVAPolicyNo]
      ,[MVAPolicyHolder]

      ,[MVAFirstName]
      ,[MVALastName]

      ,[MVACompanyName]
      ,[MVAAddress]
      ,[MVACity]
      ,[MVAProvince]
      ,[MVAPostalCode]
      ,[MVACountry]

      ,[MVAInsurerHCAIID]
      ,[MVABranchHCAIID]

      ,[MVATPID]
      ,[MVAContactID]
      ,[MVATPBranchID]

      ,[MVAAdjCompanyName]
      ,[MVAAdjFirstName]
      ,[MVAAdjLastName]
      ,[MVAAdjTel]
      ,[MVAAdjFax]
      ,[MVAAdjExt]
      ,[MVAAdjTPID]
      ,[MVAAdjClaimNo]
      ,[MVAAdjEmail]
      ,[MVAAdjNote]

      ,[ReferralID]
      ,[RName]
      ,[FDRName]
      ,[FDRTel]
      ,[LegalRepID]
      ,[LegalRepCompanyName]
      ,[LegalRepName]
      ,[LegalRepPhone]
      ,[LegalRepFax]
      ,[LegalRepFileNo]
      ,[LegalRepEmail]

      ,[OverrideAppt]
      ,[FDEmail]
      ,[FDFax]
      ,[RecordCreationDate]

      ,[EHC1CompanyName]
      ,[EHC1Address]
      ,[EHC1City]
      ,[EHC1Province]
      ,[EHC1PostalCode]
      ,[EHC1Country]
      ,[EHC1Tel]
      ,[EHC1Fax]
      ,[EHC1IDCertNo]
      ,[EHC1PGroupNo]
      ,[EHC1PolicyHolder]
      ,[EHC1FirstName]
      ,[EHC1LastName]
      ,[EHC1TPID]
      ,[EHC1TPContactID]
      ,[EHC1DOB]
      ,[EHC1ClaimForms]

      ,[EHC2CompanyName]
      ,[EHC2Address]
      ,[EHC2City]
      ,[EHC2Province]
      ,[EHC2PostalCode]
      ,[EHC2Country]
      ,[EHC2Tel]
      ,[EHC2Fax]
      ,[EHC2IDCertNo]
      ,[EHC2PGroupNo]
      ,[EHC2PolicyHolder]
      ,[EHC2FirstName]
      ,[EHC2LastName]
      ,[EHC2TPID]
      ,[EHC2TPContactID]
      ,[EHC2DOB]
      ,[EHC2ClaimForms]
      )
  SELECT TOP (1000) @clientId, @caseIdNew, [CaseType], @DateOfAccident + 1.0, 1, @DateOfAccident
      ,@CIClaimNo
      ,@CIPolicyNo
      ,@MVAPHSame

      ,@MVAPHFirstName
      ,@MVAPHLastName

      ,@MVAInsName
      ,@MVAAddress
      ,@MVACity
      ,@MVAProvince
      ,@MVAPostalCode
      ,[MVACountry]

      ,@MVAInsurerHCAIID
      ,@MVABranchHCAIID

      ,case when @MVAInsurerHCAIID <> '' then
	      (select top 1 [ThirdPartyID] from [Third_Party] where [InsurerHCAIID] = @MVAInsurerHCAIID)
		else null end
		-- ?????
      ,case when @MVABranchHCAIID <> '' then
	      (select top 1 TPContactID from ThirdPartyContacts where BranchHCAIID = @MVABranchHCAIID)
		else null end
		-- ?????
      ,case when @MVABranchHCAIID <> '' then
	      (select top 1 TPContactID from ThirdPartyContacts where BranchHCAIID = @MVABranchHCAIID)
		else null end

      ,@MVAInsName
      ,@MVAAFirstName
      ,@MVAALastName
      ,@MVAATel
      ,@MVAAFax
      ,@MVAAExt
      ,[MVAAdjTPID]
      ,[MVAAdjClaimNo]
      ,[MVAAdjEmail]
      ,[MVAAdjNote]

      ,[ReferralID]
      ,[RName]
      ,[FDRName]
      ,[FDRTel]
      ,[LegalRepID]
      ,[LegalRepCompanyName]
      ,[LegalRepName]
      ,[LegalRepPhone]
      ,[LegalRepFax]
      ,[LegalRepFileNo]
      ,[LegalRepEmail]

      ,[OverrideAppt]
      ,[FDEmail]
      ,[FDFax]
      ,GETDATE()

      ,[EHC1CompanyName]
      ,[EHC1Address]
      ,[EHC1City]
      ,[EHC1Province]
      ,[EHC1PostalCode]
      ,[EHC1Country]
      ,[EHC1Tel]
      ,[EHC1Fax]
      ,[EHC1IDCertNo]
      ,[EHC1PGroupNo]
      ,[EHC1PolicyHolder]
      ,[EHC1FirstName]
      ,[EHC1LastName]
      ,[EHC1TPID]
      ,[EHC1TPContactID]
      ,[EHC1DOB]
      ,[EHC1ClaimForms]

      ,[EHC2CompanyName]
      ,[EHC2Address]
      ,[EHC2City]
      ,[EHC2Province]
      ,[EHC2PostalCode]
      ,[EHC2Country]
      ,[EHC2Tel]
      ,[EHC2Fax]
      ,[EHC2IDCertNo]
      ,[EHC2PGroupNo]
      ,[EHC2PolicyHolder]
      ,[EHC2FirstName]
      ,[EHC2LastName]
      ,[EHC2TPID]
      ,[EHC2TPContactID]
      ,[EHC2DOB]
      ,[EHC2ClaimForms]
  FROM [dbo].[Client_Case]
  where CaseID = @caseIdBase
  
  -- III - move data to new CASE
  update PaperWork set CaseID = @caseIdNew
    where CaseID = @caseIdBase and ReportID > 0 and ReportID in (select ReportID from @tblRepIds)
  update PaperWork_Log set CaseID = @caseIdNew
    where CaseID = @caseIdBase and ReportID > 0 and ReportID in (select ReportID from @tblRepIds)

  update InvoceGeneralInfo set CaseID = @caseIdNew
    where CaseID = @caseIdBase and ReportID > 0 and ReportID in (select ReportID from @tblRepIds)
  update InvoceGeneral_Log set CaseID = @caseIdNew
    where CaseID = @caseIdBase and ReportID > 0 and ReportID in (select ReportID from @tblRepIds)

  update ClientCaseCharges set CaseID = @caseIdNew
    where CaseID = @caseIdBase
	  and ((ReportID > 0 and ReportID in (select ReportID from @tblRepIds))
	        or
		   (InvoiceID > 0 and InvoiceID in (select InvoiceID from @tblInvoiceIds)))
  update ClientCaseCharges_Log set CaseID = @caseIdNew
    where CaseID = @caseIdBase
	  and ((ReportID > 0 and ReportID in (select ReportID from @tblRepIds))
	        or
		   (InvoiceID > 0 and InvoiceID in (select InvoiceID from @tblInvoiceIds)))

  update PaymentInfo set CaseID = @caseIdNew
    where CaseID = @caseIdBase and PaymentID in (select PaymentID from @tblPmtIds)
  update PaymentInfo_Log set CaseID = @caseIdNew
    where CaseID = @caseIdBase and PaymentID in (select PaymentID from @tblPmtIds)
  
  -- next
  fetch next from cursCharge into @clientId, @caseIdBase, @DOL, @DateOfAccident;
end;
close cursCharge; deallocate cursCharge;

--*** enable triggers
IF exists (select * from sysobjects where name='ClientCaseChargesUpdateTriggerCSharp' and xtype='TA')
  alter table [dbo].ClientCaseCharges enable trigger ClientCaseChargesUpdateTriggerCSharp
IF exists (select * from sysobjects where name='ClientCaseChargesUpdateTrigger' and xtype='TR')
  alter table [dbo].ClientCaseCharges enable trigger ClientCaseChargesUpdateTrigger
IF exists (select * from sysobjects where name='PaperWorkUpdateTrigger' and xtype='TA')
  alter table [dbo].PaperWork enable trigger PaperWorkUpdateTrigger
IF exists (select * from sysobjects where name='InvoceGeneralInfoUpdateTrigger' and xtype='TA')
  alter table [dbo].InvoceGeneralInfo enable trigger InvoceGeneralInfoUpdateTrigger
IF exists (select * from sysobjects where name='PaymentInfoUpdateTrigger' and xtype='TA')
  alter table [dbo].PaymentInfo enable trigger PaymentInfoUpdateTrigger

-- injuries
insert Case_Injuries ([CaseID],[Code],[CodeDescription],[iOrder],[dCode])
select DISTINCT P.CaseID, O.Code, O.CodeDescription, O.iOrder, 0
  from Paperwork P join
       OCFInjuries O on P.ReportID = O.ReportID
  where P.SentDate = (select MAX(P2.SentDate) from Paperwork P2 where P2.CaseID = P.CaseID and P2.ReportID in (select distinct ReportID from OCFInjuries))
    and not exists(select Tci.Code from Case_Injuries Tci where Tci.CaseID = P.CaseID and Tci.Code = O.Code)
  order by P.CaseID, O.iOrder
