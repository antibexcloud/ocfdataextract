select distinct ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)), *
from (

select distinct Tcl.first_name, Tcl.last_name,
       Tcs.CaseID, Tcs.DOL, Tcs.MVAInsurerHCAIID,
	   isnull(OCF18.CIDOA, OCF23.CIDOA) as DateOfAccident,
	   isnull(OCF18.MVAInsurerHCAIID, OCF23.MVAInsurerHCAIID) as docMVAInsurerHCAIID
  from Client_Case Tcs join
       client Tcl on Tcl.client_id = Tcs.client_id join
       PaperWork Tpw on Tpw.CaseID = Tcs.CaseID left join
	   OCF18 on OCF18.ReportID = Tpw.ReportID left join
	   OCF23 on OCF23.ReportID = Tpw.ReportID
  where Tcs.CaseType = 'MVA'
    and Tcs.DOL is not null
	and isnull(OCF18.CIDOA, OCF23.CIDOA) is not null
	and (Tpw.ReportType = 'OCF18' or Tpw.ReportType = 'OCF23' or Tpw.ReportType = 'FORM1')
	and Tcs.DOL <> isnull(OCF18.CIDOA, OCF23.CIDOA)
	and isnull(OCF18.CIDOA, OCF23.CIDOA) > '2019-01-01'
) T
  where T.DOL <> T.DateOfAccident
    and (ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)) > 30
	     or (ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)) > 10 and T.MVAInsurerHCAIID <> '' and T.MVAInsurerHCAIID <> T.docMVAInsurerHCAIID))
  order by ABS(DATEDIFF(d, T.DOL, T.DateOfAccident)),
           T.first_name, T.last_name, T.CaseID, T.DOL, T.DateOfAccident