delete Activity
GO
delete ActivityOnlineTags
GO
delete Authorized_Clients
GO
delete BatchGeneralInfo
GO
delete BenefitsSchedule
GO
delete Business_Center
GO
delete Business_Center_Arc
GO
delete CalendarTable
GO
delete Case_Injuries
GO
delete CasePlans
GO
delete CasePlansPreset
GO
delete CaseTracking
GO
delete ChargesPresetItems
GO
delete ChargesPresetList
GO
delete City
GO
delete Client
GO
delete Client_Case
GO
delete Client_EmergencyContact
GO
delete ClientCaseCharges
GO
delete ClientCaseCharges_Log
GO
delete ClientCaseEHCBatch
GO
delete ClientCaseEHCCoverage
GO
delete ClientCaseEHCCoverageBSDetails
GO
delete ClientCaseEHCCoverageBSDetailsCombo
GO
delete Comments
GO
delete Correspondence
GO
delete Country
GO
delete CustomLetter
GO
delete CustomLetterPreset
GO
delete Department
GO
delete DepartmentResourceRelation
GO

delete EClaimsBatch
GO
--EClaimsConstants
delete EClaimsEHCInvoice
GO
--EClaimsLicensingBody
delete EClaimsLog
GO
delete EClaimsPred
GO
--eClaimsPrivilege
--eClaimsPrivilegeToUserRoleRelation
delete eClaimsProviderRegistration
GO
delete eClaimsResponse
GO
delete eClaimsResponseCaused
GO
delete eClaimsResponseErrors
GO
delete eClaimsResponseItems
GO
--EClaimsServeCodeToProviderTypeRelation
--EClaimsSystemCodes
--eClaimsUserRole
delete eClaimsWSIBAdjudicationResult
GO
delete eClaimsWSIBAdjudicationResultReasonOf
GO
delete eClaimsWSIBAdjudicationResultReasonOfLines
GO
--eClaimsWSIBDeliveryLocationCode
--eClaimsWSIBExplanationCode
delete EClaimsWSIBInvoice
GO
delete EClaimsWSIBLog
GO
delete eClaimsWSIBPaymentAdvice
GO
delete eClaimsWSIBPaymentAdviceInvoiceRefereces
GO
--eClaimsWSIBPrivilege
--eClaimsWSIBPrivilegeToUserRoleRelation
delete eClaimsWSIBProviderRegistration
GO
--EClaimsWSIBServeCodeToProviderTypeRelation

delete EdtBatches
GO
delete EdtCharges
GO
delete EdtClaimFiles
GO
delete EdtClaimRemittances
GO
delete EdtClaims
GO
delete EdtMailBoxItems
GO
delete EdtOHIPAuditMessages
GO
delete EdtOhipProviders
GO
delete EdtRemittanceAdviceFileHeaders
GO
delete EdtRemittanceMessages
GO
delete EdtRemittanceTransactions
GO
delete EdtRepHeader
GO
delete EdtServiceRemittances
GO
delete EdtSubmitUidsDocument
GO
delete EdtUserMailBoxs
GO
delete EdtWSIBAuditMessages
GO
delete EdtWsibInvoiceAdjudicationRequests
GO
delete EdtWsibInvoiceAdjudicationRequestsLineItems
GO
delete EdtWsibInvoiceAdjudicationResults
GO
delete EdtWsibInvoiceAdjudicationResultsReasonOf
GO
delete EdtWSIBPaymentAdviceInvoiceReferences
GO
delete EdtWSIBPaymentAdvices
GO
delete EdtWsibProviders
GO
delete EdtWsibReasonOfAdjudicatedInvoiceLines
GO
delete EdtWsibReasonOfAdjudicatedInvoiceLinesExplanationCodes
GO
delete EdtWsibReasonOfAdjudicationExplanationCodes
GO

delete eGeneralLog
GO

delete EHCClaimForm
GO
delete EHCClaimFormExpenses
GO
delete EHCClaimFormOtherPolicy
GO
delete EHCClaimFormPatientsDependantsInfo
GO
delete EHCInvoice
GO

--EM_Broadcast
delete EM_BroadcastFiles
GO
--EM_BroadcastRecipients
--EM_DistributionList
--EM_DistributionListEmails
delete EM_EmailAttachments
GO
delete EM_EmailQueue
GO
delete EM_EmailQueue_Archive
GO
delete EM_Events
GO
delete EM_MailChimpInvalidItems
GO
delete EM_MailChimpSyncStatus
GO
--EM_Notification
--EM_Privilege
--EM_PrivilegeToUserRoleRelation
--EM_Settings
--EM_SettingsMailChimp
--EM_SmsSettings
--EM_Template
--EM_TemplateFiles
--EM_UserRole

delete ExternalFiles
GO
delete FaxComments
GO

delete FORM1
GO
delete FORM1_Parts123
GO

--FORM1_Parts123Preset
--FORM1Preset

delete HCAIErrorLog
GO
delete HCAIErrorLogItems
GO
delete HCAIStatusLog
GO

delete InterestChargeInvoice
GO
delete InvoceGeneral_Log
GO
delete InvoceGeneralInfo
GO
delete InvoiceInjuries
GO
delete InvoiceItemDetails
GO
delete InvoiceItemDistributePayment
GO
delete LegalRepresentatives
GO
--LogTriggersConfig
--MasterKeys
delete MVAInvoice
GO
delete MVAInvoiceGeneric
GO

delete OCF1
GO
delete OCF12
GO
delete OCF18
GO
--OCF18Presets
delete OCF19
GO
delete OCF2
GO
delete OCF22
GO
--OCF22Presets
delete OCF23
GO
--OCF23Presets
delete OCF24
GO
delete OCF3
GO
--OCF3Presets
delete OCF5
GO
delete OCF6
GO
delete OCF6Expenses
GO
delete OCF9
GO
delete OCF9LineItems
GO
delete OCFForm1
GO
delete OCFGoodsAndServices
GO
--OCFGoodsAndServicesPreset
delete OCFInjuries
GO

delete PaidInvoiceRelation
GO
delete PaperWork
GO
delete PaperWork_Log
GO
delete PaymentInfo
GO
delete PaymentInfo_Log
GO
--PaymentMethods
--PMTabColumnsSettings
--PMTabsCustomColumns

--Preference
--PresetList
--PresetListCols
--PresetOCFInjuries

delete PrivateInvoice
GO
delete Product
GO
delete Province
GO
--ReceiptItems
--ReceiptPmtDetails
--Receipts
--Referrals
delete RefundInfo
GO
delete RefundPaymentRelation
GO
delete RelatedClientCases
GO

--Reminders
--RemindersColumns
--ReminderTrigger

delete [Resource]
GO
delete Resource_Activity_Relation
GO
delete Resource_Schedule
GO

--RM_Category
--RM_CategoryNoAccess
--RM_Fields
--RM_Objects
--RM_Parameters
--RM_Report

delete Schedule_Book
GO
delete Schedule_Book_Log
GO
delete ScheduleBookCharges
GO
delete ScheduleBookRepeat
GO

--ServiceCodes
--ServiceCodesGroup
--ServiceLocator

delete SlipAndFallInvoice
GO

delete SOAPNotes
GO
delete SOAPNotesPlan
GO

--SOAPNotesPreset
--SOAPNotesPresetPlan

delete Staff
GO
--StaffTypes

delete Supplier
GO
delete Supplier_Provides
GO
delete SupplierProductRelation
GO
delete Suppliers
GO

--Tasks
--Tax
delete TempUsers
GO

--TrackingTypes
delete TriggerLog
GO
--TTCategories
--TTField
--TxCaseDocuments
--TxEditorPresets

delete Updates
GO

--UserDocumentsVisiblityPref
delete UserLoginDetails
GO
--UserPatientVisiblityPref

--Users
--UsersPresets
--UserToPatientNoAccessTbl

delete WaitingList
GO
--WaitingListSettings

GO
delete WSIB_MSKPOC
GO
delete WSIB_MSKPOC_ISR
GO
delete WSIB_PCMTBI_COS
GO
delete WSIB_PCMTBI_IAR
GO
delete WSIBALBICareAndOutcomesSummary
GO
delete WSIBALBIInitialAssessment
GO
delete WSIBFAF
GO
--WSIBFAFPreset
delete WSIBForm26
GO
delete WSIBForm8
GO
delete WSIBForm8PartD
GO
delete WSIBInitialAssessmentReport2610A
GO
delete WSIBInvoice
GO
delete WSIBInvoiceProducts
GO
delete WSIBPCLEICOutcomesSummary
GO
delete WSIBPCLEIInitialAssessmentReport
GO
delete WSIBPhysiotherapyAssessment
GO
delete WSIBSPCInitalAssessmentReport
GO
delete WSIBSPCOutcomesSummary
GO
delete WSIBSummaryReport2611A
GO
delete WSIBTreatmentExtensionRequest
GO
delete WSIBUEICOutcomesSummary
GO
delete WSIBUEIInitialAssessmentReport
GO

delete ThirdPartyTelFax
delete ThirdPartyContacts
delete Third_Party
GO