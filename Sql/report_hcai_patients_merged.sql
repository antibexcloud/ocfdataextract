declare @tblVIds table (id int)
insert @tblVIds (id)
select distinct [virtualId]
FROM [HCAIOCFSync_KitchenerPhysio].[dbo].[Patient]
  where [virtualId] is not null
--
declare @tblRes table (
	[Id] [int] NOT NULL,
	[IsVirtual] [bit] NOT NULL,
	[first_name] [nvarchar](50) NOT NULL,
	[last_name] [nvarchar](50) NOT NULL,
	[middle_name] [nvarchar](50) NULL,
	[gender] [nvarchar](1) NULL,
	[DOB] [datetime] NOT NULL,
	[HomePhone] [nvarchar](20) NULL,
	[HomeStreet] [nvarchar](150) NULL,
	[HomeCity] [nvarchar](50) NULL,
	[HomeZip] [nvarchar](10) NULL,
	[DOL] [datetime] NULL
);
--
declare @id int
declare curVirtuals cursor for select [Id] FROM @tblVIds
open curVirtuals;
--цикл
fetch next from curVirtuals into @id;
while @@FETCH_STATUS = 0 begin
  --
  insert @tblRes ([Id]
	  ,[IsVirtual]
      ,[first_name]
      ,[last_name]
      ,[middle_name]
      ,[gender]
      ,[DOB]
      ,[HomePhone]
      ,[HomeStreet]
      ,[HomeCity]
      ,[HomeZip]
      ,[DOL])
  SELECT [Id]
      ,1
      ,[first_name]
      ,[last_name]
      ,[middle_name]
      ,[gender]
      ,[DOB]
      ,[HomePhone]
      ,[HomeStreet]
      ,[HomeCity]
      ,[HomeZip]
      ,[DOL]
  FROM [HCAIOCFSync_KitchenerPhysio].[dbo].[Patient]
  where [Id] = @id
  --
  insert @tblRes ([Id]
      ,[IsVirtual]
      ,[first_name]
      ,[last_name]
      ,[middle_name]
      ,[gender]
      ,[DOB]
      ,[HomePhone]
      ,[HomeStreet]
      ,[HomeCity]
      ,[HomeZip]
      ,[DOL])
  SELECT [Id]
      ,0
      ,[first_name]
      ,[last_name]
      ,[middle_name]
      ,[gender]
      ,[DOB]
      ,[HomePhone]
      ,[HomeStreet]
      ,[HomeCity]
      ,[HomeZip]
      ,[DOL]
  FROM [HCAIOCFSync_KitchenerPhysio].[dbo].[Patient]
  where [virtualId] = @id
  order by id
  --
  fetch next from curVirtuals into @id;
end;
close curVirtuals; deallocate curVirtuals;
--
select * from @tblRes