declare @tableTypes table([StaffTypeID] [int] NOT NULL,
	[StaffType] [smallint] NULL,
	[Status] [smallint] NULL,
	[Occupation] [nvarchar](50) NULL,
	[ServiceType] [nvarchar](50) NULL,
	[EHCCode] [nvarchar](50) NULL,
	[MVACode] [nvarchar](50) NULL,
	[OHIPCode] [nvarchar](50) NULL,
	[WSIBCode] [nvarchar](50) NULL,
	[MVARate] [float] NULL,
	[eClaimsIsAvailable] [bit] NOT NULL,
	[eClaimsDisplayName] [nvarchar](200) NULL,
	[eClaimsDescription] [nvarchar](500) NULL,
	[eClaimsSystemCode] [nvarchar](18) NULL,
	[EClaimsLicensingBodyId] [int] NULL,
	[eClaimsWsibIsAvailable] [bit] NOT NULL,
	[eClaimsWsibDisplayName] [nvarchar](200) NULL,
	[eClaimsWsibDescription] [nvarchar](500) NULL);

insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (1, 1, 1, N'Chiropractor', N'Chiropractic', N'CHIRO', N'DC', N'', N'CHIRO', 0.00, 1, N'Chiropractic', N'Chiropractic', N'SCPTYPE', 1, 1, N'Chiropractic', N'Chiropractic');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (2, 1, 1, N'Physiotherapist', N'Physiotherapy', N'PHYSIO', N'PT', N'', N'PHYSIO', 0.00, 1, N'Physiotherapist', N'Physiotherapist', N'SCPTYPE', 4, 1, N'Physiotherapist', N'Physiotherapist');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (3, 1, 1, N'Massage Therapist', N'Massage Therapy', N'RMT', N'MT', N'', N'RMT', 0.00, 1, N'Massage Therapist', N'Massage Therapist', N'SCPTYPE', 2, 1, N'Massage Therapist', N'Massage Therapist');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (4, 0, 1, N'Office Administrator', N'Clerical', N'', N'', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (59, 1, 1, N'Dental Hygienist', N'Dental Hygienist', N'DH', N'DH', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (6, 1, 1, N'Dentist', N'Dentist', N'', N'DD', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (7, 1, 1, N'Family Practitioner/General Practitioner', N'Therapy', N'', N'GP', N'', N'GP', 0.00, 0, NULL, NULL, NULL, NULL, 1, N'Physician', N'Family Practitioner/General Practitioner');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (8, 1, 1, N'Nurse Practitioner', N'Nursing', N'', N'NP', N'', N'NP', 0.00, 0, NULL, NULL, NULL, NULL, 1, N'Nurse Practitioner', N'Nurse Practitioner');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (60, 1, 1, N'Compression Stockings', N'Compression Stockings', N'', N'', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (10, 1, 1, N'Optometrist', N'Optometry', N'OPT', N'OP', N'', N'OPT', 0.00, 1, N'Optometrist',
	   N'Optometrists are health service providers, who are legally qualified to examine patients'' eyes, diagnose diseases and disorders of the eye.',
	   N'SCPTYPE', 6, 1, N'Optometrist',
	   N'Optometrists are health service providers, who are legally qualified to examine patients'' eyes, diagnose diseases and disorders of the eye.');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (11, 1, 1, N'Occupational Therapist', N'Occupational Therapy', N'OT', N'OT', N'', N'OT', 0.00, 1, N'Occupational Therapist', N'Occupational therapists help people whose capabilities have been impaired by physical or mental illness or injury, emotional or developmental problems, or the aging process to improve their ability to function in everyday life. They assist people in caring for themselves, returning to work and resuming community activities.', N'SCPTYPE', 131, 1, N'Occupational Therapist', N'Occupational therapists help people whose capabilities have been impaired by physical or mental illness or injury, emotional or developmental problems, or the aging process to improve their ability to function in everyday life. They assist people in caring for themselves, returning to work and resuming community activities.');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (12, 1, 1, N'Podiatry/Chiropody', N'Podiatry/Chiropody', N'PO', N'PO', N'', N'PO', 0.00, 1, N'Podiatrist', N'Doctors of podiatric medicine are primary care practitioners who diagnose diseases, deformities and injuries of the human foot and communicate diagnoses to patients.', N'SCPTYPE', 10, 1, N'Podiatrist', N'Doctors of podiatric medicine are primary care practitioners who diagnose diseases, deformities and injuries of the human foot and communicate diagnoses to patients.');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (13, 1, 1, N'Psychologist', N'Psychology', N'PSYCH', N'PS', N'', N'PSYCH', 0.00, 1, N'Psychologist', N'Psychologists are health service providers who are legally qualified to assess, diagnose and treat psychological, emotional and behavioural disorders.', N'SCPTYPE', 11, 1, N'Psychologist', N'Psychologists are health service providers who are legally qualified to assess, diagnose and treat psychological, emotional and behavioural disorders.');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (15, 1, 1, N'Speech-Language Pathologist', N'Speech-Language Pathologist', N'', N'SL', N'', N'SLP', 0.00, 0, NULL, NULL, NULL, NULL, 1, N'Speech-Language Pathology', N'Speech-Language Pathology');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (16, 1, 0, N'Acupuncturist', N'Acupuncture', N'', N'AC', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (17, 1, 0, N'Kinesiologist', N'Kinesiology', N'', N'KN', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (18, 1, 1, N'Naturopath', N'Naturopathic', N'RND', N'NTR', N'', N'RND', 0.00, 1, N'Naturopath', N'Naturopath', N'SCPTYPE', 16, 1, N'Naturopath', N'Naturopath');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (19, 1, 1, N'Osteopath', N'Osteopathic', N'DO', N'OS', N'', N'DO', 0.00, 1, N'Osteopath', N'Doctor of Osteopathy', N'SCPTYPE', 15, 1, N'Osteopath', N'Doctor of Osteopathy');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (20, 0, 1, N'Front Desk', N'Front Desk', N'', N'', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (22, 1, 1, N'Driver', N'Transportation', N'', N'', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (21, 0, 1, N'Accounts Receivable', N'Accounts Receivable', N'', N'', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (23, 0, 1, N'Receptionist', N'Receptionist', N'', N'', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (24, 1, 1, N'Assistant Nurse', N'Assistant Nurse', N'', N'AN', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (25, 1, 1, N'Assistant Therapist', N'Assistant Therapist', N'', N'AT', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (26, 1, 1, N'Case Manager', N'Case Manager', N'', N'CM', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (65, 1, 1, N'Chiropodist', N'Chiropody', N'CPD', N'', N'', N'CPD', 0.00, 1, N'Chiropodist', N'', N'SCPTYPE', 10, 1, N'Chiropodist', N'Chiropodist');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (30, 1, 1, N'Dietician', N'Dietician', N'', N'DI', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (35, 1, 1, N'Occupational Therapy Assistant', N'Occupational Therapy Assistant', N'', N'OTA', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (36, 1, 1, N'Ophthalmologist', N'Ophthalmologist', N'OPHTH', N'OM', N'', N'OPHTH', 0.00, 1, N'Ophthalmologist', N'Ophthalmologist', N'SCPTYPE', NULL, 1, N'Ophthalmologist', N'Ophthalmologist');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (37, 1, 1, N'Optician', N'Optician', N'RO', N'OC', N'', N'RO', 0.00, 1, N'Optician', N'Optician', N'SCPTYPE', 5, 1, N'Optician', N'Optician');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (38, 1, 1, N'Orthotist/Prosthetist', N'Orthotist/Prosthetist', N'', N'OR', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (39, 1, 1, N'Other', N'Other', N'OPTICSPY', N'OTH', N'', N'OPTICSPY', 0.00, 1, N'Optical Supplier', N'Optical Supplier', N'SCPTYPE', NULL, 1, N'Optical Supplier', N'Optical Supplier');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (40, 1, 1, N'Other Medical/Surgical Practitioner', N'Other Medical/Surgical Practitioner', N'MD', N'MD', N'', N'MD', 0.00, 1, N'Medical Doctor', N'Medical Doctor is a person who is (or is studying to become) academically and legally qualified doctor of medicine.', N'SCPTYPE', NULL, 1, N'Medical Doctor', N'Medical Doctor is a person who is (or is studying to become) academically and legally qualified doctor of medicine.');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (41, 1, 1, N'Other Paramedical', N'Other Paramedical', N'', N'PX', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (43, 1, 1, N'Physiotherapy Assistant', N'Physiotherapy Assistant', N'', N'PHA', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (44, 1, 1, N'Psychiatrist', N'Psychiatrist', N'', N'PY', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (45, 1, 1, N'Psychological Associate', N'Psychological Associate', N'', N'PA', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (46, 1, 1, N'Psychometrist', N'Psychometrist', N'', N'PM', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (47, 1, 1, N'Psychotherapist', N'Psychotherapist', N'', N'PR', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (48, 1, 1, N'Recreation Therapist', N'Recreation Therapist', N'', N'RT', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (49, 1, 1, N'Rehabilitation Counselor /Therapist', N'Rehabilitation Counselor /Therapist', N'PRT', N'RH', N'', N'PRT', 0.00, 1, N'Physical Rehabilitation Therapist', N'Physical Rehabilitation Therapist', N'SCPTYPE', NULL, 1, N'Physical Rehabilitation Therapist', N'Physical Rehabilitation Therapist');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (51, 1, 1, N'Social service worker', N'Social service worker', N'', N'SS', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (52, 1, 1, N'Social worker', N'Social worker', N'SW', N'SW', N'', N'SW', 0.00, 1, N'Social worker', N'Social Workers are health service providers who are legally qualified to help individuals, families, groups, communities and organizations develop the skills and resources they need to enhance their social functioning and social environments.', N'SCPTYPE', 130, 1, N'Social Worker', N'Social Workers are health service providers who are legally qualified to help individuals, families, groups, communities and organizations develop the skills and resources they need to enhance their social functioning and social environments.');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (53, 1, 1, N'Vocational Rehabilitation Counselor', N'Vocational Rehabilitation Counselor', N'', N'VR', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (54, 1, 1, N'Registered Nurse', N'Registered Nurse', N'', N'RN', N'', N'', NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (56, 1, 1, N'Traditional Chinese Medicine Practitioner', N'Traditional Chinese Medicine', N'', N'TCM', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (57, 1, 1, N'Acupuncturist (Regulated)', N'Acupuncture', N'RAC', N'ACR', N'', N'RAC', 0.00, 1, N'Acupuncturist', N'Acupuncturist', N'SCPTYPE', 8, 1, N'Acupuncturist', N'Acupuncturist');
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (58, 1, 1, N'Kinesiologist (Regulated)', N'Kinesiology', N'', N'KNR', N'', N'', 0.00, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (66, 1, 1, N'Registered Dietitian', N'Registered Dietitian', N'RD', N'RD', N'', NULL, 0.00, 1, N'Registered Dietitian', N'Dietitians are health service providers who are legally qualified to develop, implement and evaluate food and nutrition strategies to promote health and treat disease; manage food service systems; and develop and deliver related programs and policies.', N'SCPTYPE', NULL, 0, NULL, NULL);
insert into @tableTypes ([StaffTypeID], [StaffType], [Status], [Occupation], [ServiceType], [EHCCode], [MVACode], [OHIPCode], [WSIBCode], [MVARate], [eClaimsIsAvailable], [eClaimsDisplayName], [eClaimsDescription], [eClaimsSystemCode], [EClaimsLicensingBodyId], [eClaimsWsibIsAvailable], [eClaimsWsibDisplayName], [eClaimsWsibDescription])
     values (67, 1, 1, N'Speech Therapist', N'Speech Therapist', N'ST', N'ST', N'', NULL, 0.00, 1, N'Speech Therapist', N'Speech Therapists, Speech and Language Therapists (SLTs), or Speech-Language Pathologists (SLPs) are allied health professionals. Speech therapists hold certificates to practice following a degree course and clinical training. Additional specialization can occur in neurological disorders, child language, or vocal habilitation (vocology).', N'SCPTYPE', NULL, 0, NULL, NULL);

--
declare @StaffTypeID int,
	@StaffType smallint,
	@Status smallint,
	@Occupation nvarchar(50),
	@ServiceType nvarchar(50),
	@EHCCode nvarchar(50),
	@MVACode nvarchar(50),
	@OHIPCode nvarchar(50),
	@WSIBCode nvarchar(50),
	@MVARate float,
	@eClaimsIsAvailable bit,
	@eClaimsDisplayName nvarchar(200),
	@eClaimsDescription nvarchar(500),
	@eClaimsSystemCode nvarchar(18),
	@EClaimsLicensingBodyId int,
	@eClaimsWsibIsAvailable bit,
	@eClaimsWsibDisplayName nvarchar(200),
	@eClaimsWsibDescription nvarchar(500);
--
select @StaffTypeID = max(StaffTypeID) + 1 from [StaffTypes]
--
declare curTypes cursor for select StaffType, [Status], Occupation,
    ServiceType, EHCCode, MVACode, OHIPCode, WSIBCode, MVARate,
    eClaimsIsAvailable, eClaimsDisplayName, eClaimsDescription, eClaimsSystemCode,
    EClaimsLicensingBodyId, eClaimsWsibIsAvailable, eClaimsWsibDisplayName, eClaimsWsibDescription
	from @tableTypes
open curTypes;
--
fetch next from curTypes into @StaffType, @Status, @Occupation,
    @ServiceType, @EHCCode, @MVACode, @OHIPCode, @WSIBCode, @MVARate,
    @eClaimsIsAvailable, @eClaimsDisplayName, @eClaimsDescription, @eClaimsSystemCode,
    @EClaimsLicensingBodyId, @eClaimsWsibIsAvailable, @eClaimsWsibDisplayName, @eClaimsWsibDescription;

while @@FETCH_STATUS = 0 begin
  if isnull(@MVACode,'') <> '' and not exists(select StaffTypeID from [StaffTypes] where MVACode = @MVACode and StaffType = @StaffType)
  begin
    insert [StaffTypes] (StaffTypeID, StaffType, [Status], Occupation,
    ServiceType, EHCCode, MVACode, OHIPCode, WSIBCode, MVARate,
    eClaimsIsAvailable, eClaimsDisplayName, eClaimsDescription, eClaimsSystemCode,
    EClaimsLicensingBodyId, eClaimsWsibIsAvailable, eClaimsWsibDisplayName, eClaimsWsibDescription)
	  values (@StaffTypeID, @StaffType, @Status, @Occupation,
    @ServiceType, @EHCCode, @MVACode, @OHIPCode, @WSIBCode, @MVARate,
    @eClaimsIsAvailable, @eClaimsDisplayName, @eClaimsDescription, @eClaimsSystemCode,
    @EClaimsLicensingBodyId, @eClaimsWsibIsAvailable, @eClaimsWsibDisplayName, @eClaimsWsibDescription)
	--
	set @StaffTypeID = @StaffTypeID + 1;
  end;
  --
  fetch next from curTypes into @StaffType, @Status, @Occupation,
    @ServiceType, @EHCCode, @MVACode, @OHIPCode, @WSIBCode, @MVARate,
    @eClaimsIsAvailable, @eClaimsDisplayName, @eClaimsDescription, @eClaimsSystemCode,
    @EClaimsLicensingBodyId, @eClaimsWsibIsAvailable, @eClaimsWsibDisplayName, @eClaimsWsibDescription;
end;
close curTypes; deallocate curTypes;