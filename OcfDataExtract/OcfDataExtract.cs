﻿using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using System.Threading;
using OcfDataExtract.Extract;

namespace OcfDataExtract
{
    using System.Data.SqlClient;

    public partial class OcfDataExtract : Form
    {
        private OcfDataExtractWorker _worker;
        private DataExtractConfig _config;

        public string Status
        {
            get => _status;
            set
            {
                _status = value;
                try
                {
                    labelStatus.BeginInvoke((Action) (() => labelStatus.Text = Status));
                }
                catch{}
            }
        }

        public int TotalDaysToProcess
        {
            get => _totalDaysToProcess;
            set
            {
                _totalDaysToProcess = value;
                try
                {
                    progressBarStatus.BeginInvoke((Action)(() => progressBarStatus.Maximum = TotalDaysToProcess));
                }
                catch { }
            }
        }

        private readonly StringBuilder _messages = new();
        private readonly BackgroundWorker _bgWorker = new();
        private int _totalDaysToProcess;
        private string _status;

        public OcfDataExtract()
        {
            InitializeComponent();
            _bgWorker.WorkerReportsProgress = true;
            _bgWorker.WorkerSupportsCancellation = true;
            _bgWorker.ProgressChanged += BgWorkerProgressChanged;
            _bgWorker.DoWork += BgWorkerDoWork;
            _bgWorker.RunWorkerCompleted += BgWorkerRunWorkerCompleted;
        }

        private void BgWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            btnUpdate.Enabled = true;
            btnInitialize.Enabled = true;
            btnExtractDocument.Enabled = true;
            btnStop.Enabled = false;

            progressBarStatus.Value = 0;
            if (!_worker.IsExtract)
                Status = "Done.";

            const string fileName = "ocf_extract.log";
            if (File.Exists(fileName)) File.Delete(fileName);
            File.WriteAllText(fileName, _messages.ToString());
        }

        private void BgWorkerProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is null)
            {
                progressBarStatus.Value = e.ProgressPercentage < progressBarStatus.Maximum
                    ? e.ProgressPercentage
                    : progressBarStatus.Maximum;
                var msg = "Processing day " + e.ProgressPercentage + " of " + TotalDaysToProcess;
                labelStatus.Text = msg;
            }
            else
            {
                //message arrived
                var s = (string)e.UserState;
                _messages.AppendLine(s);
            }
        }

        private void OcfDataExtract_Load(object sender, EventArgs e)
        {
            // config
            _config = DataExtractConfig.Load();
            if (_config is null)
            {
                _config = CreateDefaultConfig();
                DataExtractConfig.Save(_config);
            }

            textBoxLogin.Text = _config.Login;
            textBoxPassword.Text = _config.Pass;
            textBoxNumberOfDays.Text = $"{(int)_config.TimeInterval.TotalDays}";
            checkBoxUpdateFacility.Checked = _config.UpdateFacility;
            checkBoxUpdateInsurers.Checked = _config.UpdateInsurers;
            textBoxDBName.Text = _config.DbName;
            textBoxServerName.Text = _config.ServerName;
            UpdateConnectionString();

            // worker
            _worker = new OcfDataExtractWorker(this, _config);
            _worker.NewMessageEvent += WriteNewLogMessage;
            _worker.Progress += Progress;

            _worker.AppPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            _worker.XmlPath = Path.Combine(_worker.AppPath, "ReceivedXml");
        }

        private void OcfDataExtract_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (_worker is null) return;
            _worker.Dispose();
            _worker = null;
        }

        private void Progress(int obj)
        {
            _bgWorker.ReportProgress(obj, null);
        }

        private void WriteNewLogMessage(string obj)
        {
            if (_worker.IsExtract)
            {
                Status = obj;
                Thread.Sleep(2000);
            }
            else
            {
                _bgWorker.ReportProgress(0, obj);
            }
        }

        private DataExtractConfig CreateDefaultConfig()
        {
            return new DataExtractConfig
            {
                DbName = "HCAIOCFSync",
                ServerName = @"SERGHOME\SQL2014",
                InitRun = false,
                Login = "antibex10",
                Pass = "aNXq2Ki6",
                TimeInterval = new TimeSpan(14, 0, 0, 0),
                UpdateFacility = true,
                UpdateInsurers = true
            };
        }

        private void btnInitialize_Click(object sender, EventArgs e)
        {
            if (!ValidateGui()) return;
            
            var from = new DateTime(2015, 1, 1);
            var to = DateTime.Now;

            if (MessageBox.Show(@"Are you sure you want to initialize?", @"Warning", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;

            TotalDaysToProcess = (int)Math.Ceiling((to - @from).TotalDays);
            progressBarStatus.Value = 0;

            _config.UpdateFacility = true;
            _config.UpdateInsurers = true;
            _config.TimeInterval = to - @from;
            _worker.Config = _config;
            _worker.CancellationPending = false;

            btnUpdate.Enabled = false;
            btnInitialize.Enabled = false;
            btnStop.Enabled = true;
            btnExtractDocument.Enabled = false;

            _worker.IsExtract = false;
            _worker.IsInitialize = true;
            _bgWorker.RunWorkerAsync();
        }
        
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!ValidateGui()) return;
            if (MessageBox.Show(@"Are you sure you want to update?", @"Warning", MessageBoxButtons.YesNo) != DialogResult.Yes)
                return;

            TotalDaysToProcess = (int)Math.Ceiling(_config.TimeInterval.TotalDays);
            progressBarStatus.Value = 0;
            _worker.CancellationPending = false;
                
            btnUpdate.Enabled = false;
            btnInitialize.Enabled = false;
            btnStop.Enabled = true;
            btnExtractDocument.Enabled = false;

            _worker.IsExtract = false;
            _worker.IsInitialize = false;
            _bgWorker.RunWorkerAsync();
        }

        private void BgWorkerDoWork(object sender, DoWorkEventArgs e)
        {
            if (_worker.IsExtract)
            {
                _worker.DocumentProcess();
            }
            else
            {
                _worker.ExtractDocsUpdate();
            }
        }

        private bool ValidateGui()
        {
            if (string.IsNullOrWhiteSpace(textBoxDBName.Text))
            {
                MessageBox.Show(@"Please enter database name to proceed.", @"Invalid Database Name");
                return false;
            }
            if (string.IsNullOrWhiteSpace(textBoxServerName.Text))
            {
                MessageBox.Show(@"Please enter server name to proceed.", @"Invalid Server Name");
                return false;
            }
            if (string.IsNullOrWhiteSpace(textBoxLogin.Text))
            {
                MessageBox.Show(@"Please enter PMS HCAI login to proceed.", @"Invalid PMS HCAI Login");
                return false;
            }
            if (string.IsNullOrWhiteSpace(textBoxPassword.Text))
            {
                MessageBox.Show(@"Please enter PMS HCAI password to proceed.", @"Invalid PMS HCAI Password");
                return false;
            }
            if (string.IsNullOrWhiteSpace(textBoxNumberOfDays.Text) || !textBoxNumberOfDays.Text.IsNumeric())
            {
                MessageBox.Show(@"Please enter valid numeric date range to proceed.", @"Invalid Date Range");
                return false;
            }

            _config.Login = textBoxLogin.Text;
            _config.Pass = textBoxPassword.Text;
            _config.DbName = textBoxDBName.Text;
            _config.ServerName = textBoxServerName.Text;
            _config.TimeInterval = new TimeSpan(Convert.ToInt32(textBoxNumberOfDays.Text), 0, 0, 0);
            _config.UpdateFacility = checkBoxUpdateFacility.Checked;
            _config.UpdateInsurers = checkBoxUpdateInsurers.Checked;

            DataExtractConfig.Save(_config);

            UpdateConnectionString();
            _worker.ConnectionString = tbConnectionString.Text;
            _worker.IsXmlSave = chbSaveXml.Checked;

            _worker.HcaiDocumentNumber = textBoxHcaiDocNumber.Text;
            _worker.OcfType = comboBoxOcfType.Text;

            return true;
        }
        
        private void btnStop_Click(object sender, EventArgs e)
        {
            _worker.CancellationPending = true;
        }

        private void UpdateConnectionString()
        {
            tbConnectionString.Text = $@"server={textBoxServerName.Text};Initial Catalog={textBoxDBName.Text};" +
                                      $@" User ID={textBoxUserName.Text}; Password={textBoxPassw.Text}";
        }

        private void InputChanged(object sender, EventArgs e)
        {
            UpdateConnectionString();
        }
        
        private void TestConnectionClick(object sender, EventArgs e)
        {
            var originalButtonText = btnTestConnection.Text;
            btnTestConnection.Text = @"Please wait...";
            btnTestConnection.Enabled = false;
            Update();
            MessageBox.Show(TestSqlConnection() ? "Success" : "Failed");
            btnTestConnection.Text = originalButtonText;
            btnTestConnection.Enabled = true;
        }

        private bool TestSqlConnection()
        {
            try
            {
                using var sqlConn = new SqlConnection(tbConnectionString.Text);
                sqlConn.Open();
                sqlConn.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!ValidateGui()) return;
            if (MessageBox.Show(@"Are you sure you want to clear?", @"Warning", MessageBoxButtons.YesNo) != DialogResult.Yes) return;

            try
            {
                _worker.ClearDb();
                MessageBox.Show(@"Database is clearing!", @"Information", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Error!", MessageBoxButtons.OK);
            }
        }

        private void btnExtractDocument_Click(object sender, EventArgs e)
        {
            if (!ValidateGui()) return;

            progressBarStatus.Value = 0;
            _worker.CancellationPending = false;

            btnUpdate.Enabled = false;
            btnInitialize.Enabled = false;
            btnStop.Enabled = false;
            btnExtractDocument.Enabled = false;

            _worker.IsExtract = true;

            _bgWorker.RunWorkerAsync();
        }
    }
}
