﻿namespace OcfDataExtract
{
    partial class OcfDataExtract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OcfDataExtract));
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.tabMain = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnExtractDocument = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxOcfType = new System.Windows.Forms.ComboBox();
            this.textBoxHcaiDocNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.chbSaveXml = new System.Windows.Forms.CheckBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnInitialize = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.progressBarStatus = new System.Windows.Forms.ProgressBar();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.innerTabControl = new System.Windows.Forms.TabControl();
            this.tabGeneral = new System.Windows.Forms.TabPage();
            this.checkBoxUpdateInsurers = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdateFacility = new System.Windows.Forms.CheckBox();
            this.labelNumbeOfDays = new System.Windows.Forms.Label();
            this.textBoxNumberOfDays = new System.Windows.Forms.TextBox();
            this.tabSystem = new System.Windows.Forms.TabPage();
            this.tbConnectionString = new System.Windows.Forms.TextBox();
            this.lblConnString = new System.Windows.Forms.Label();
            this.btnTestConnection = new System.Windows.Forms.Button();
            this.textBoxPassw = new System.Windows.Forms.TextBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.textBoxUserName = new System.Windows.Forms.TextBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.textBoxDBName = new System.Windows.Forms.TextBox();
            this.labelDBName = new System.Windows.Forms.Label();
            this.textBoxServerName = new System.Windows.Forms.TextBox();
            this.labelServerName = new System.Windows.Forms.Label();
            this.groupBoxLogingInfo = new System.Windows.Forms.GroupBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxLogin = new System.Windows.Forms.TextBox();
            this.labelLogin = new System.Windows.Forms.Label();
            this.mainTabControl.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.innerTabControl.SuspendLayout();
            this.tabGeneral.SuspendLayout();
            this.tabSystem.SuspendLayout();
            this.groupBoxLogingInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTabControl
            // 
            this.mainTabControl.Controls.Add(this.tabMain);
            this.mainTabControl.Controls.Add(this.tabSettings);
            this.mainTabControl.Location = new System.Drawing.Point(5, 2);
            this.mainTabControl.Margin = new System.Windows.Forms.Padding(4);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(937, 378);
            this.mainTabControl.TabIndex = 0;
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.groupBox1);
            this.tabMain.Controls.Add(this.button1);
            this.tabMain.Controls.Add(this.chbSaveXml);
            this.tabMain.Controls.Add(this.btnStop);
            this.tabMain.Controls.Add(this.btnUpdate);
            this.tabMain.Controls.Add(this.btnInitialize);
            this.tabMain.Controls.Add(this.labelStatus);
            this.tabMain.Controls.Add(this.progressBarStatus);
            this.tabMain.Location = new System.Drawing.Point(4, 25);
            this.tabMain.Margin = new System.Windows.Forms.Padding(4);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(4);
            this.tabMain.Size = new System.Drawing.Size(929, 349);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Main";
            this.tabMain.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnExtractDocument);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxOcfType);
            this.groupBox1.Controls.Add(this.textBoxHcaiDocNumber);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(9, 80);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(465, 92);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Extract test";
            // 
            // btnExtractDocument
            // 
            this.btnExtractDocument.Location = new System.Drawing.Point(333, 55);
            this.btnExtractDocument.Name = "btnExtractDocument";
            this.btnExtractDocument.Size = new System.Drawing.Size(106, 27);
            this.btnExtractDocument.TabIndex = 18;
            this.btnExtractDocument.Text = "Extract";
            this.btnExtractDocument.UseVisualStyleBackColor = true;
            this.btnExtractDocument.Click += new System.EventHandler(this.btnExtractDocument_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(102, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "OCF type:";
            // 
            // comboBoxOcfType
            // 
            this.comboBoxOcfType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxOcfType.FormattingEnabled = true;
            this.comboBoxOcfType.Items.AddRange(new object[] {
            "AACN",
            "OCF18",
            "OCF23",
            "OCF21B",
            "OCF21C",
            "ACSI"});
            this.comboBoxOcfType.Location = new System.Drawing.Point(191, 57);
            this.comboBoxOcfType.Name = "comboBoxOcfType";
            this.comboBoxOcfType.Size = new System.Drawing.Size(125, 24);
            this.comboBoxOcfType.TabIndex = 16;
            // 
            // textBoxHcaiDocNumber
            // 
            this.textBoxHcaiDocNumber.Location = new System.Drawing.Point(191, 21);
            this.textBoxHcaiDocNumber.Name = "textBoxHcaiDocNumber";
            this.textBoxHcaiDocNumber.Size = new System.Drawing.Size(248, 22);
            this.textBoxHcaiDocNumber.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(165, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "HCAI Document Number:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(383, 245);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 8;
            this.button1.Text = "Clear DB";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chbSaveXml
            // 
            this.chbSaveXml.AutoSize = true;
            this.chbSaveXml.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.chbSaveXml.Location = new System.Drawing.Point(383, 313);
            this.chbSaveXml.Margin = new System.Windows.Forms.Padding(4);
            this.chbSaveXml.Name = "chbSaveXml";
            this.chbSaveXml.Size = new System.Drawing.Size(172, 24);
            this.chbSaveXml.TabIndex = 7;
            this.chbSaveXml.Text = "Save received files";
            this.chbSaveXml.UseVisualStyleBackColor = true;
            // 
            // btnStop
            // 
            this.btnStop.Enabled = false;
            this.btnStop.Location = new System.Drawing.Point(820, 4);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(100, 28);
            this.btnStop.TabIndex = 5;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Visible = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(813, 310);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 28);
            this.btnUpdate.TabIndex = 3;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnInitialize
            // 
            this.btnInitialize.Location = new System.Drawing.Point(8, 310);
            this.btnInitialize.Margin = new System.Windows.Forms.Padding(4);
            this.btnInitialize.Name = "btnInitialize";
            this.btnInitialize.Size = new System.Drawing.Size(100, 28);
            this.btnInitialize.TabIndex = 2;
            this.btnInitialize.Text = "Initialize";
            this.btnInitialize.UseVisualStyleBackColor = true;
            this.btnInitialize.Click += new System.EventHandler(this.btnInitialize_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(8, 12);
            this.labelStatus.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(49, 17);
            this.labelStatus.TabIndex = 1;
            this.labelStatus.Text = "Ready";
            // 
            // progressBarStatus
            // 
            this.progressBarStatus.Location = new System.Drawing.Point(8, 35);
            this.progressBarStatus.Margin = new System.Windows.Forms.Padding(4);
            this.progressBarStatus.Name = "progressBarStatus";
            this.progressBarStatus.Size = new System.Drawing.Size(911, 28);
            this.progressBarStatus.TabIndex = 0;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.innerTabControl);
            this.tabSettings.Location = new System.Drawing.Point(4, 25);
            this.tabSettings.Margin = new System.Windows.Forms.Padding(4);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(4);
            this.tabSettings.Size = new System.Drawing.Size(929, 349);
            this.tabSettings.TabIndex = 1;
            this.tabSettings.Text = "Settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // innerTabControl
            // 
            this.innerTabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.innerTabControl.Controls.Add(this.tabGeneral);
            this.innerTabControl.Controls.Add(this.tabSystem);
            this.innerTabControl.Location = new System.Drawing.Point(35, 22);
            this.innerTabControl.Margin = new System.Windows.Forms.Padding(4);
            this.innerTabControl.Name = "innerTabControl";
            this.innerTabControl.SelectedIndex = 0;
            this.innerTabControl.Size = new System.Drawing.Size(884, 315);
            this.innerTabControl.TabIndex = 0;
            // 
            // tabGeneral
            // 
            this.tabGeneral.Controls.Add(this.checkBoxUpdateInsurers);
            this.tabGeneral.Controls.Add(this.checkBoxUpdateFacility);
            this.tabGeneral.Controls.Add(this.labelNumbeOfDays);
            this.tabGeneral.Controls.Add(this.textBoxNumberOfDays);
            this.tabGeneral.Location = new System.Drawing.Point(4, 4);
            this.tabGeneral.Margin = new System.Windows.Forms.Padding(4);
            this.tabGeneral.Name = "tabGeneral";
            this.tabGeneral.Padding = new System.Windows.Forms.Padding(4);
            this.tabGeneral.Size = new System.Drawing.Size(876, 286);
            this.tabGeneral.TabIndex = 0;
            this.tabGeneral.Text = "General";
            this.tabGeneral.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateInsurers
            // 
            this.checkBoxUpdateInsurers.AutoSize = true;
            this.checkBoxUpdateInsurers.Checked = true;
            this.checkBoxUpdateInsurers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUpdateInsurers.Location = new System.Drawing.Point(27, 91);
            this.checkBoxUpdateInsurers.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxUpdateInsurers.Name = "checkBoxUpdateInsurers";
            this.checkBoxUpdateInsurers.Size = new System.Drawing.Size(131, 21);
            this.checkBoxUpdateInsurers.TabIndex = 3;
            this.checkBoxUpdateInsurers.Text = "Update Insurers";
            this.checkBoxUpdateInsurers.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateFacility
            // 
            this.checkBoxUpdateFacility.AutoSize = true;
            this.checkBoxUpdateFacility.Checked = true;
            this.checkBoxUpdateFacility.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxUpdateFacility.Location = new System.Drawing.Point(27, 62);
            this.checkBoxUpdateFacility.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxUpdateFacility.Name = "checkBoxUpdateFacility";
            this.checkBoxUpdateFacility.Size = new System.Drawing.Size(200, 21);
            this.checkBoxUpdateFacility.TabIndex = 2;
            this.checkBoxUpdateFacility.Text = "Update Facility && Providers";
            this.checkBoxUpdateFacility.UseVisualStyleBackColor = true;
            // 
            // labelNumbeOfDays
            // 
            this.labelNumbeOfDays.AutoSize = true;
            this.labelNumbeOfDays.Location = new System.Drawing.Point(23, 11);
            this.labelNumbeOfDays.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNumbeOfDays.Name = "labelNumbeOfDays";
            this.labelNumbeOfDays.Size = new System.Drawing.Size(144, 17);
            this.labelNumbeOfDays.TabIndex = 1;
            this.labelNumbeOfDays.Text = "Update Range (days)";
            // 
            // textBoxNumberOfDays
            // 
            this.textBoxNumberOfDays.Location = new System.Drawing.Point(195, 7);
            this.textBoxNumberOfDays.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxNumberOfDays.Name = "textBoxNumberOfDays";
            this.textBoxNumberOfDays.Size = new System.Drawing.Size(103, 22);
            this.textBoxNumberOfDays.TabIndex = 0;
            this.textBoxNumberOfDays.Text = "14";
            // 
            // tabSystem
            // 
            this.tabSystem.Controls.Add(this.tbConnectionString);
            this.tabSystem.Controls.Add(this.lblConnString);
            this.tabSystem.Controls.Add(this.btnTestConnection);
            this.tabSystem.Controls.Add(this.textBoxPassw);
            this.tabSystem.Controls.Add(this.lblPassword);
            this.tabSystem.Controls.Add(this.textBoxUserName);
            this.tabSystem.Controls.Add(this.lblUser);
            this.tabSystem.Controls.Add(this.textBoxDBName);
            this.tabSystem.Controls.Add(this.labelDBName);
            this.tabSystem.Controls.Add(this.textBoxServerName);
            this.tabSystem.Controls.Add(this.labelServerName);
            this.tabSystem.Controls.Add(this.groupBoxLogingInfo);
            this.tabSystem.Location = new System.Drawing.Point(4, 4);
            this.tabSystem.Margin = new System.Windows.Forms.Padding(4);
            this.tabSystem.Name = "tabSystem";
            this.tabSystem.Padding = new System.Windows.Forms.Padding(4);
            this.tabSystem.Size = new System.Drawing.Size(876, 286);
            this.tabSystem.TabIndex = 1;
            this.tabSystem.Text = "System";
            this.tabSystem.UseVisualStyleBackColor = true;
            // 
            // tbConnectionString
            // 
            this.tbConnectionString.Location = new System.Drawing.Point(147, 231);
            this.tbConnectionString.Margin = new System.Windows.Forms.Padding(4);
            this.tbConnectionString.Name = "tbConnectionString";
            this.tbConnectionString.ReadOnly = true;
            this.tbConnectionString.Size = new System.Drawing.Size(547, 22);
            this.tbConnectionString.TabIndex = 14;
            // 
            // lblConnString
            // 
            this.lblConnString.AutoSize = true;
            this.lblConnString.Location = new System.Drawing.Point(20, 235);
            this.lblConnString.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblConnString.Name = "lblConnString";
            this.lblConnString.Size = new System.Drawing.Size(118, 17);
            this.lblConnString.TabIndex = 13;
            this.lblConnString.Text = "Connection string";
            // 
            // btnTestConnection
            // 
            this.btnTestConnection.Location = new System.Drawing.Point(716, 247);
            this.btnTestConnection.Margin = new System.Windows.Forms.Padding(4);
            this.btnTestConnection.Name = "btnTestConnection";
            this.btnTestConnection.Size = new System.Drawing.Size(149, 28);
            this.btnTestConnection.TabIndex = 12;
            this.btnTestConnection.Text = "Test connection";
            this.btnTestConnection.UseVisualStyleBackColor = true;
            this.btnTestConnection.Click += new System.EventHandler(this.TestConnectionClick);
            // 
            // textBoxPassw
            // 
            this.textBoxPassw.Location = new System.Drawing.Point(147, 199);
            this.textBoxPassw.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPassw.Name = "textBoxPassw";
            this.textBoxPassw.PasswordChar = '*';
            this.textBoxPassw.Size = new System.Drawing.Size(248, 22);
            this.textBoxPassw.TabIndex = 11;
            this.textBoxPassw.Text = "passc0de";
            this.textBoxPassw.TextChanged += new System.EventHandler(this.InputChanged);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Location = new System.Drawing.Point(68, 203);
            this.lblPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(69, 17);
            this.lblPassword.TabIndex = 10;
            this.lblPassword.Text = "Password";
            // 
            // textBoxUserName
            // 
            this.textBoxUserName.Location = new System.Drawing.Point(147, 167);
            this.textBoxUserName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxUserName.Name = "textBoxUserName";
            this.textBoxUserName.Size = new System.Drawing.Size(248, 22);
            this.textBoxUserName.TabIndex = 9;
            this.textBoxUserName.Text = "uouser";
            this.textBoxUserName.TextChanged += new System.EventHandler(this.InputChanged);
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(95, 171);
            this.lblUser.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(38, 17);
            this.lblUser.TabIndex = 8;
            this.lblUser.Text = "User";
            // 
            // textBoxDBName
            // 
            this.textBoxDBName.Location = new System.Drawing.Point(147, 135);
            this.textBoxDBName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxDBName.Name = "textBoxDBName";
            this.textBoxDBName.Size = new System.Drawing.Size(248, 22);
            this.textBoxDBName.TabIndex = 7;
            this.textBoxDBName.Text = "HCAIOCFSync";
            this.textBoxDBName.TextChanged += new System.EventHandler(this.InputChanged);
            // 
            // labelDBName
            // 
            this.labelDBName.AutoSize = true;
            this.labelDBName.Location = new System.Drawing.Point(21, 139);
            this.labelDBName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDBName.Name = "labelDBName";
            this.labelDBName.Size = new System.Drawing.Size(110, 17);
            this.labelDBName.TabIndex = 6;
            this.labelDBName.Text = "Database Name";
            // 
            // textBoxServerName
            // 
            this.textBoxServerName.Location = new System.Drawing.Point(147, 103);
            this.textBoxServerName.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxServerName.Name = "textBoxServerName";
            this.textBoxServerName.Size = new System.Drawing.Size(248, 22);
            this.textBoxServerName.TabIndex = 5;
            this.textBoxServerName.Text = "localhost\\anxsqlserver";
            this.textBoxServerName.TextChanged += new System.EventHandler(this.InputChanged);
            // 
            // labelServerName
            // 
            this.labelServerName.AutoSize = true;
            this.labelServerName.Location = new System.Drawing.Point(41, 107);
            this.labelServerName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelServerName.Name = "labelServerName";
            this.labelServerName.Size = new System.Drawing.Size(91, 17);
            this.labelServerName.TabIndex = 4;
            this.labelServerName.Text = "Server Name";
            // 
            // groupBoxLogingInfo
            // 
            this.groupBoxLogingInfo.Controls.Add(this.textBoxPassword);
            this.groupBoxLogingInfo.Controls.Add(this.labelPassword);
            this.groupBoxLogingInfo.Controls.Add(this.textBoxLogin);
            this.groupBoxLogingInfo.Controls.Add(this.labelLogin);
            this.groupBoxLogingInfo.Location = new System.Drawing.Point(8, 7);
            this.groupBoxLogingInfo.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxLogingInfo.Name = "groupBoxLogingInfo";
            this.groupBoxLogingInfo.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxLogingInfo.Size = new System.Drawing.Size(388, 89);
            this.groupBoxLogingInfo.TabIndex = 0;
            this.groupBoxLogingInfo.TabStop = false;
            this.groupBoxLogingInfo.Text = "HCAI PMS Login Info";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(139, 55);
            this.textBoxPassword.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(132, 22);
            this.textBoxPassword.TabIndex = 3;
            this.textBoxPassword.Text = "qNVCfH7e";
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(17, 55);
            this.labelPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(69, 17);
            this.labelPassword.TabIndex = 2;
            this.labelPassword.Text = "Password";
            // 
            // textBoxLogin
            // 
            this.textBoxLogin.Location = new System.Drawing.Point(139, 23);
            this.textBoxLogin.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxLogin.Name = "textBoxLogin";
            this.textBoxLogin.Size = new System.Drawing.Size(132, 22);
            this.textBoxLogin.TabIndex = 1;
            this.textBoxLogin.Text = "Apex1";
            // 
            // labelLogin
            // 
            this.labelLogin.AutoSize = true;
            this.labelLogin.Location = new System.Drawing.Point(44, 32);
            this.labelLogin.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelLogin.Name = "labelLogin";
            this.labelLogin.Size = new System.Drawing.Size(43, 17);
            this.labelLogin.TabIndex = 0;
            this.labelLogin.Text = "Login";
            // 
            // OcfDataExtract
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 395);
            this.Controls.Add(this.mainTabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OcfDataExtract";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OcfDataExtract";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.OcfDataExtract_FormClosed);
            this.Load += new System.EventHandler(this.OcfDataExtract_Load);
            this.mainTabControl.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabMain.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.innerTabControl.ResumeLayout(false);
            this.tabGeneral.ResumeLayout(false);
            this.tabGeneral.PerformLayout();
            this.tabSystem.ResumeLayout(false);
            this.tabSystem.PerformLayout();
            this.groupBoxLogingInfo.ResumeLayout(false);
            this.groupBoxLogingInfo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage tabMain;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.ProgressBar progressBarStatus;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.TabControl innerTabControl;
        private System.Windows.Forms.TabPage tabGeneral;
        private System.Windows.Forms.TabPage tabSystem;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnInitialize;
        private System.Windows.Forms.CheckBox checkBoxUpdateInsurers;
        private System.Windows.Forms.CheckBox checkBoxUpdateFacility;
        private System.Windows.Forms.Label labelNumbeOfDays;
        private System.Windows.Forms.TextBox textBoxNumberOfDays;
        private System.Windows.Forms.TextBox textBoxDBName;
        private System.Windows.Forms.Label labelDBName;
        private System.Windows.Forms.TextBox textBoxServerName;
        private System.Windows.Forms.Label labelServerName;
        private System.Windows.Forms.GroupBox groupBoxLogingInfo;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.TextBox textBoxLogin;
        private System.Windows.Forms.Label labelLogin;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox textBoxUserName;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.TextBox tbConnectionString;
        private System.Windows.Forms.Label lblConnString;
        private System.Windows.Forms.Button btnTestConnection;
        private System.Windows.Forms.TextBox textBoxPassw;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.CheckBox chbSaveXml;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnExtractDocument;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxOcfType;
        private System.Windows.Forms.TextBox textBoxHcaiDocNumber;
        private System.Windows.Forms.Label label1;
    }
}

