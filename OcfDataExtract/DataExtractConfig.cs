﻿using System;
using System.IO;
using System.Xml.Serialization;
using System.ComponentModel;

namespace OcfDataExtract
{
    [Serializable]
    public class DataExtractConfig
    {
        private const string Filename = @"DataExtract.config";

        public string ServerName { get; set; }
        public string DbName { get; set; }
        public bool InitRun { get; set; }
        public bool UpdateFacility { get; set; }
        public bool UpdateInsurers { get; set; }
        public string Login { get; set; }
        public string Pass { get; set; }

        [XmlIgnore]
        public TimeSpan TimeInterval { get; set; }

        // Pretend property for serialization
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
        [XmlElement("TimeInterval")]
        public string TimeSinceLastEventTicks
        {
            get => TimeInterval.ToString();
            set => TimeInterval = TimeSpan.Parse(value);
        }

        public static bool Save(DataExtractConfig config)
        {
            try
            {
                using (var file = new FileStream(Filename, FileMode.Create))
                {
                    var xml = new XmlSerializer(typeof(DataExtractConfig));
                    xml.Serialize(file, config);
                }
                return true;
            }
            catch(Exception e)
            {
                //Debuger.WriteToFile("Failed to save DataExtractConfig. Error: " + e.Message, 100);
                return false;
            }
        }

        public static DataExtractConfig Load()
        {
            DataExtractConfig res = null;

            if (File.Exists(Filename))
            {
                try
                {
                    using (var file = new FileStream(Filename, FileMode.Open))
                    {
                        var xml = new XmlSerializer(typeof(DataExtractConfig));
                        res = (DataExtractConfig)xml.Deserialize(file);
                    }
                }
                catch (Exception e)
                {
                    //Debuger.WriteToFile("Failed to load " + Filename + ". Error: " + e.Message, 100);
                }
            }
            return res;
        }
    }
}