﻿using System;
using System.Linq;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractAcsi(IDataItem doc)
        {
            var edOcfAcsiKeys = new PMSToolkit.DataObjects.ACSIDataExtractResponseKeys();
            var edOcfAcsiLiKeys = new PMSToolkit.DataObjects.ACSIDataExtractResponseLineItemKeys();

            var documentNumber = doc.GetValue(edOcfAcsiKeys.HCAI_Document_Number);
            var documentStatus = doc.GetValue(edOcfAcsiKeys.Document_Status);
            var ocfAr = _dbContext.ACSIARs
                .Include("GSACSILineItemARs")
                .FirstOrDefault(ar => ar.HCAI_Document_Number == documentNumber);

            var rowSubmitted = _dbContext.ACSISubmits
                .Include("GSACSILineItemSubmits")
                .FirstOrDefault(d => d.HCAI_Document_Number == documentNumber);
            var isExists = rowSubmitted != null;

            var docApproved = ocfAr?.ACSI_InsurerTotals_SubTotal_Approved ?? 0;
            var itemsApproved = ocfAr?.GSACSILineItemARs
                .Select(t => t.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost)
                .Sum() ?? 0;

            if (!isExists)
            {
                NewMessageEvent($"Extracting doc with type 'ACSI' & number = '{documentNumber}' ");
            }
            else if (documentStatus == ocfAr?.Document_Status && Math.Abs(docApproved - itemsApproved) > 0.01m)
            {
                return false;
            }
            else
            {
                NewMessageEvent($"Updating doc with type 'ACSI' & number = '{documentNumber}' from status '{ocfAr?.Document_Status}' to status '{documentStatus}'");
            }

            rowSubmitted ??= _dbContext.ACSISubmits.Add(new ACSISubmit { HCAI_Document_Number = documentNumber, });

            #region sumbit doc fields

            // **** header ****
            rowSubmitted.PMSFields_PMSSoftware = doc.GetValue(edOcfAcsiKeys.PMSFields_PMSSoftware);
            rowSubmitted.PMSFields_PMSVersion = doc.GetValue(edOcfAcsiKeys.PMSFields_PMSVersion);
            rowSubmitted.PMSFields_PMSDocumentKey = doc.GetValue(edOcfAcsiKeys.PMSFields_PMSDocumentKey);
            rowSubmitted.PMSFields_PMSPatientKey = doc.GetValue(edOcfAcsiKeys.PMSFields_PMSPatientKey);

            rowSubmitted.AttachmentsBeingSent = doc.GetValue(edOcfAcsiKeys.AttachmentsBeingSent).ToBool();
            rowSubmitted.Header_ClaimNumber = doc.GetValue(edOcfAcsiKeys.Header_ClaimNumber);
            rowSubmitted.Header_PolicyNumber = doc.GetValue(edOcfAcsiKeys.Header_PolicyNumber);
            rowSubmitted.Header_DateOfAccident = doc.GetValue(edOcfAcsiKeys.Header_DateOfAccident).ToHcaiDate();

            rowSubmitted.Applicant_Name_FirstName = doc.GetValue(edOcfAcsiKeys.Applicant_Name_FirstName);
            rowSubmitted.Applicant_Name_MiddleName = doc.GetValue(edOcfAcsiKeys.Applicant_Name_MiddleName);
            rowSubmitted.Applicant_Name_LastName = doc.GetValue(edOcfAcsiKeys.Applicant_Name_LastName);
            rowSubmitted.Applicant_DateOfBirth = doc.GetValue(edOcfAcsiKeys.Applicant_DateOfBirth).ToHcaiDate();
            rowSubmitted.Applicant_Gender = doc.GetValue(edOcfAcsiKeys.Applicant_Gender).ToGender();
            rowSubmitted.Applicant_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcfAcsiKeys.Applicant_TelephoneNumber));
            rowSubmitted.Applicant_TelephoneExtension = doc.GetValue(edOcfAcsiKeys.Applicant_TelephoneExtension);
            rowSubmitted.Applicant_Address_StreetAddress1 = doc.GetValue(edOcfAcsiKeys.Applicant_Address_StreetAddress1);
            rowSubmitted.Applicant_Address_StreetAddress2 = doc.GetValue(edOcfAcsiKeys.Applicant_Address_StreetAddress2);
            rowSubmitted.Applicant_Address_City = doc.GetValue(edOcfAcsiKeys.Applicant_Address_City);
            rowSubmitted.Applicant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcfAcsiKeys.Applicant_Address_Province));
            rowSubmitted.Applicant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcfAcsiKeys.Applicant_Address_PostalCode));

            rowSubmitted.Insurer_IBCInsurerID = doc.GetValue(edOcfAcsiKeys.Insurer_IBCInsurerID);
            rowSubmitted.Insurer_IBCBranchID = doc.GetValue(edOcfAcsiKeys.Insurer_IBCBranchID);
            rowSubmitted.Insurer_Adjuster_Name_FirstName = doc.GetValue(edOcfAcsiKeys.Insurer_Adjuster_Name_FirstName);
            rowSubmitted.Insurer_Adjuster_Name_LastName = doc.GetValue(edOcfAcsiKeys.Insurer_Adjuster_Name_LastName);
            rowSubmitted.Insurer_Adjuster_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcfAcsiKeys.Insurer_Adjuster_TelephoneNumber));
            rowSubmitted.Insurer_Adjuster_TelephoneExtension = doc.GetValue(edOcfAcsiKeys.Insurer_Adjuster_TelephoneExtension);
            rowSubmitted.Insurer_Adjuster_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcfAcsiKeys.Insurer_Adjuster_FaxNumber));
            rowSubmitted.Insurer_PolicyHolder_IsSameAsApplicant = doc.GetValue(edOcfAcsiKeys.Insurer_PolicyHolder_IsSameAsApplicant).ToBool();
            rowSubmitted.Insurer_PolicyHolder_Name_FirstName = doc.GetValue(edOcfAcsiKeys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmitted.Insurer_PolicyHolder_Name_LastName = doc.GetValue(edOcfAcsiKeys.Insurer_PolicyHolder_Name_LastName);

            rowSubmitted.ACSI_InvoiceInformation_InvoiceNumber = doc.GetValue(edOcfAcsiKeys.ACSI_InvoiceInformation_InvoiceNumber);
            rowSubmitted.ACSI_InvoiceInformation_FirstInvoice = doc.GetValue(edOcfAcsiKeys.ACSI_InvoiceInformation_FirstInvoice).ToBool();
            rowSubmitted.ACSI_InvoiceInformation_LastInvoice = doc.GetValue(edOcfAcsiKeys.ACSI_InvoiceInformation_LastInvoice).ToBool();

            rowSubmitted.ACSI_PreviouslyApprovedGoodsAndServices_Type = doc.GetValue(edOcfAcsiKeys.ACSI_PreviouslyApprovedGoodsAndServices_Type);
            rowSubmitted.ACSI_PreviouslyApprovedGoodsAndServices_PlanDate =
                doc.GetValue(edOcfAcsiKeys.ACSI_PreviouslyApprovedGoodsAndServices_PlanDate).ToHcaiDateNullable();
            rowSubmitted.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber = doc.GetValue(edOcfAcsiKeys.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber);

            rowSubmitted.ACSI_Payee_FacilityRegistryID = doc.GetValue(edOcfAcsiKeys.ACSI_Payee_FacilityRegistryID).ToInt32();
            rowSubmitted.ACSI_Payee_FacilityIsPayee = doc.GetValue(edOcfAcsiKeys.ACSI_Payee_FacilityIsPayee).ToBool();
            rowSubmitted.ACSI_Payee_MakeChequePayableTo = doc.GetValue(edOcfAcsiKeys.ACSI_Payee_MakeChequePayableTo);

            rowSubmitted.ACSI_OtherInsurance_IsThereOtherInsuranceCoverage =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_IsThereOtherInsuranceCoverage).ToBool();

            rowSubmitted.ACSI_OtherInsurance_MOHAvailable =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_MOHAvailable).ToEnumIntParseNullable<FlagWithNotApplicable>();

            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer1_ID = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer1_Name = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);

            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer2_ID = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer2_Name = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmitted.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);

            rowSubmitted.ACSI_OtherInsuranceAmounts_MOH_OtherService_Proposed =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOH_OtherService_Proposed).ToDecimalNullable();

            rowSubmitted.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Proposed =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Proposed).ToDecimalNullable();

            rowSubmitted.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Proposed =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Proposed).ToDecimalNullable();

            rowSubmitted.ACSI_OtherInsuranceAmounts_OtherServiceType = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_OtherServiceType);

            rowSubmitted.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Proposed =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Proposed).ToDecimalNullable();

            rowSubmitted.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Proposed =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Proposed).ToDecimalNullable();

            rowSubmitted.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Proposed =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Proposed).ToDecimalNullable();

            rowSubmitted.ACSI_OtherInsuranceAmounts_IsAmountRefused =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_IsAmountRefused).ToBoolNullable();
            rowSubmitted.ACSI_OtherInsuranceAmounts_Debits_OtherServiceType = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Debits_OtherServiceType);

            rowSubmitted.ACSI_AccountActivity_PriorBalance =
                doc.GetValue(edOcfAcsiKeys.ACSI_AccountActivity_PriorBalance).ToDecimalNullable();
            rowSubmitted.ACSI_AccountActivity_PaymentReceivedFromAutoInsurer =
                doc.GetValue(edOcfAcsiKeys.ACSI_AccountActivity_PaymentReceivedFromAutoInsurer).ToDecimalNullable();
            rowSubmitted.ACSI_AccountActivity_OverdueAmount =
                doc.GetValue(edOcfAcsiKeys.ACSI_AccountActivity_OverdueAmount).ToDecimalNullable();

            rowSubmitted.ACSI_InsurerTotals_OtherInsurers_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_OtherInsurers_Proposed).ToDecimal();
            rowSubmitted.ACSI_InsurerTotals_MOH_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_MOH_Proposed).ToDecimal();
            rowSubmitted.ACSI_InsurerTotals_AutoInsurerTotal_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_AutoInsurerTotal_Proposed).ToDecimal();
            rowSubmitted.ACSI_InsurerTotals_GST_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_GST_Proposed).ToDecimal();
            rowSubmitted.ACSI_InsurerTotals_PST_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_PST_Proposed).ToDecimal();
            rowSubmitted.ACSI_InsurerTotals_SubTotal_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_SubTotal_Proposed).ToDecimal();
            rowSubmitted.ACSI_InsurerTotals_Interest_Proposed = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_Interest_Proposed).ToDecimal();

            rowSubmitted.ACSI_OtherInformation = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInformation);
            rowSubmitted.AdditionalComments = doc.GetValue(edOcfAcsiKeys.AdditionalComments);
            rowSubmitted.OCFVersion = doc.GetValue(edOcfAcsiKeys.OCFVersion).ToInt32();

            //GSACSILineItemSubmit
            var gsList = doc.getList(LineItemType.DE_GSACSILineItem_Estimated);
            if (gsList?.Count > 0)
            {
                var iOrder = 1;
                NewMessageEvent($"  ACSILineItemSubmit[{gsList.Count}]");
                for (var i = 0; i < gsList.Count; i++)
                {
                    var tmpItem = (IDataItem)gsList[i];
                    var itemCode = tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Code);

                    var key = tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                        key = tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber);
                    if (string.IsNullOrEmpty(key))
                        key = $"{iOrder++}";

                    var gs21BLineItemSubmitRow = rowSubmitted.GSACSILineItemSubmits
                        .FirstOrDefault(t => t.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey == key
                            && t.ACSI_ReimbursableGoodsAndServices_Items_Item_Code == itemCode)
                        ?? _dbContext.GSACSILineItemSubmits.Add(new GSACSILineItemSubmit
                        {
                            HCAI_Document_Number = documentNumber,
                            ACSI_ReimbursableGoodsAndServices_Items_Item_Code = itemCode,
                            ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey = key,
                        });

                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Attribute =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Attribute);
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID)
                            .ToInt32();
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Quantity =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Quantity).ToDecimal();
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Measure =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Measure);
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_DateOfService =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_DateOfService)
                            .ToHcaiDate();

                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_GST =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_GST).ToBool();
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PST =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PST).ToBool();
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                            .ToDecimal();
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber)
                            .ToInt32Nullable();
                    gs21BLineItemSubmitRow.ACSI_ReimbursableGoodsAndServices_Items_Item_Description =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Description);
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            // Adjuster Response
            ocfAr ??= _dbContext.ACSIARs.Add(new ACSIAR { HCAI_Document_Number = documentNumber });

            #region approved doc fields

            // header
            ocfAr.PMSFields_PMSDocumentKey = doc.GetValue(edOcfAcsiKeys.PMSFields_PMSDocumentKey);
            ocfAr.Submission_Date = doc.GetValue(edOcfAcsiKeys.Submission_Date).ToHcaiDateTime();
            ocfAr.Submission_Source = doc.GetValue(edOcfAcsiKeys.Submission_Source);
            ocfAr.Adjuster_Response_Date = doc.GetValue(edOcfAcsiKeys.Adjuster_Response_Date).ToHcaiDateTime();
            ocfAr.In_Dispute = doc.GetValue(edOcfAcsiKeys.In_Dispute).ToBool();
            ocfAr.Document_Type = doc.GetValue(edOcfAcsiKeys.Document_Type);
            ocfAr.Document_Status = documentStatus;
            ocfAr.Attachments_Received_Date = doc.GetValue(edOcfAcsiKeys.Attachments_Received_Date).ToHcaiDateNullable();

            ocfAr.Claimant_First_Name = doc.GetValue(edOcfAcsiKeys.Claimant_First_Name);
            ocfAr.Claimant_Middle_Name = doc.GetValue(edOcfAcsiKeys.Claimant_Middle_Name);
            ocfAr.Claimant_Last_Name = doc.GetValue(edOcfAcsiKeys.Claimant_Last_Name);
            ocfAr.Claimant_Date_Of_Birth = doc.GetValue(edOcfAcsiKeys.Claimant_Date_Of_Birth).ToHcaiDate();
            ocfAr.Claimant_Gender = doc.GetValue(edOcfAcsiKeys.Claimant_Gender).ToGender();
            ocfAr.Claimant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcfAcsiKeys.Claimant_Telephone));
            ocfAr.Claimant_Extension = doc.GetValue(edOcfAcsiKeys.Claimant_Extension);
            ocfAr.Claimant_Address1 = doc.GetValue(edOcfAcsiKeys.Claimant_Address1);
            ocfAr.Claimant_Address2 = doc.GetValue(edOcfAcsiKeys.Claimant_Address2);
            ocfAr.Claimant_City = doc.GetValue(edOcfAcsiKeys.Claimant_City);
            ocfAr.Claimant_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcfAcsiKeys.Claimant_Province));
            ocfAr.Claimant_Postal_Code = ConvertHelper.FormatPostalCode(doc.GetValue(edOcfAcsiKeys.Claimant_Postal_Code));

            ocfAr.Insurer_IBCInsurerID = doc.GetValue(edOcfAcsiKeys.Insurer_IBCInsurerID);
            ocfAr.Insurer_IBCBranchID = doc.GetValue(edOcfAcsiKeys.Insurer_IBCBranchID);
            ocfAr.Status_Code = doc.GetValue(edOcfAcsiKeys.Status_Code);
            ocfAr.Messages = doc.GetValue(edOcfAcsiKeys.Messages);

            //
            ocfAr.ACSI_ReimbursableGoodsAndServices_AdjusterResponseExplanation = doc.GetValue(edOcfAcsiKeys.ACSI_ReimbursableGoodsAndServices_AdjusterResponseExplanation);

            ocfAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_LineCost =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCode =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_LineCost =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_LineCost =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_LineCost =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_LineCost =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_LineCost =
                doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_OtherInsuranceAmounts_AdjusterResponseExplanation = doc.GetValue(edOcfAcsiKeys.ACSI_OtherInsuranceAmounts_AdjusterResponseExplanation);
            ocfAr.ACSI_InsurerSignature_ClaimFormReceived = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerSignature_ClaimFormReceived).ToBoolNullable();
            ocfAr.ACSI_InsurerSignature_ClaimFormReceivedDate = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerSignature_ClaimFormReceivedDate).ToHcaiDateNullable();

            ocfAr.ACSI_InsurerTotals_MOH_Approved = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_MOH_Approved).ToDecimal();
            ocfAr.ACSI_InsurerTotals_OtherInsurers_Approved = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_OtherInsurers_Approved).ToDecimal();
            ocfAr.ACSI_InsurerTotals_AutoInsurerTotal_Approved = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_AutoInsurerTotal_Approved).ToDecimal();

            ocfAr.ACSI_InsurerTotals_GST_Approved_LineCost = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_GST_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_InsurerTotals_PST_Approved_LineCost = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_PST_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.ACSI_InsurerTotals_SubTotal_Approved = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_SubTotal_Approved).ToDecimal();
            ocfAr.ACSI_InsurerTotals_Interest_Approved_LineCost = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_Interest_Approved_LineCost).ToDecimal();
            ocfAr.ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
            ocfAr.ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocfAr.ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcfAcsiKeys.ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocfAr.SigningAdjuster_FirstName = doc.GetValue(edOcfAcsiKeys.SigningAdjuster_FirstName);
            ocfAr.SigningAdjuster_LastName = doc.GetValue(edOcfAcsiKeys.SigningAdjuster_LastName);
            ocfAr.ApprovedByOnPDF = doc.GetValue(edOcfAcsiKeys.ApprovedByOnPDF);
            ocfAr.Archival_Status = doc.GetValue(edOcfAcsiKeys.Archival_Status);

            //GSACSILineItemAR
            var arList = doc.getList(LineItemType.DE_GSACSILineItem_Approved);
            if (arList?.Count > 0)
            {
                var iOrder = 0;
                var equelLists = rowSubmitted.GSACSILineItemSubmits.Count == arList.Count;
                NewMessageEvent($"  GSACSILineItemAR[{arList.Count}]");
                for (var i = 0; i < arList.Count; i++)
                {
                    var tmpItem = (IDataItem)arList[i];
                    ++iOrder;
                    var lineCost = tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost).ToDecimal();

                    var key = tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                    {
                        if (equelLists)
                            key = $"{iOrder}";
                        else
                        {
                            try
                            {
                                var findSubm = rowSubmitted.GSACSILineItemSubmits
                                    .SingleOrDefault(s => s.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost == lineCost);
                                if (findSubm != null)
                                    key = findSubm.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey;
                                else
                                    key = $"{iOrder}";
                            }
                            catch
                            {
                                key = $"{iOrder}";
                            }
                        }
                    }

                    var gsAcsiLineItemAr = ocfAr.GSACSILineItemARs
                        .FirstOrDefault(t => t.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == key)
                        ?? _dbContext.GSACSILineItemARs.Add(new GSACSILineItemAR
                        {
                            HCAI_Document_Number = documentNumber,
                            ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey = key,
                        });

                    gsAcsiLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost = lineCost;

                    gsAcsiLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_GST =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_GST).ToBool();
                    gsAcsiLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PST =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PST).ToBool();

                    gsAcsiLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    gsAcsiLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    gsAcsiLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        tmpItem.GetValue(edOcfAcsiLiKeys.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            return true;
        }
    }
}
