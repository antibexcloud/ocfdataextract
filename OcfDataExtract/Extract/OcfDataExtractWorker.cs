﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Utils;
using OcfDataExtract.Model;
using PMSToolkit;
using PMSToolkit.DataObjects;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker : IDisposable
    {
        #region private

        private readonly Toolkit _pmsTk = new();
        private HCAIOCFSyncEntities _dbContext;
        private readonly OcfDataExtract _owner;

        #endregion

        #region Properties

        public string ConnectionString { get; set; }
        public DataExtractConfig Config { get; set; }
        public bool CancellationPending;
        public bool IsInitialize { get; set; }

        public string AppPath { get; set; }
        public string XmlPath { get; set; }
        public bool IsXmlSave { get; set; }

        public event Action<string> NewMessageEvent = delegate { };
        public event Action<int> Progress = delegate { };

        public string HcaiDocumentNumber { get; set; }
        public string OcfType { get; set; }
        public bool IsExtract { get; set; }

        #endregion

        public OcfDataExtractWorker(OcfDataExtract owner, DataExtractConfig config)
        {
            _owner = owner;
            Config = config;
        }

        public void Dispose()
        {
            _dbContext?.Dispose();
            _dbContext = null;
        }

        private void InitDbContext()
        {
            _dbContext?.Dispose();
            _dbContext = HCAIOCFSyncEntities.OpenFromSqlConnection(ConnectionString);
            _dbContext.UpdateMetadata();
        }

        #region Extractors

        public void ClearDb()
        {
            InitDbContext();
            _dbContext.spClearHDB();
        }

        public void ExtractDocsUpdate()
        {
            InitDbContext();

            var currentDate = DateTime.Now.Date.AddDays(1);
            var startDate = currentDate - Config.TimeInterval;
            var endDate = currentDate;

            if (Config.UpdateFacility || !_dbContext.ProviderLineItems.Any())
            {
                _owner.Status = @"ExtractFacilityInfo...";
                if (!ExtractFacilityInfo())
                {
                    MessageBox.Show($@"{nameof(ExtractFacilityInfo)} bad", @"Error!", MessageBoxButtons.OK);
                    return;
                }
            }

            if (Config.UpdateInsurers || !_dbContext.InsurerLineItems.Any())
            {
                _owner.Status = $"{nameof(ExtractInsurersInfo)}...";
                if (!ExtractInsurersInfo())
                {
                    MessageBox.Show($@"{nameof(ExtractInsurersInfo)} bad", @"Error!", MessageBoxButtons.OK);
                    return;
                }
            }

            // try find start
            if (IsInitialize && _dbContext.FacilityLineItems.Any())
            {
                startDate = _dbContext.FacilityLineItems.Select(f => f.Facility_Start_Date).Min().ToLocalTime().Date;
                _owner.TotalDaysToProcess = (int)Math.Ceiling((endDate - startDate).TotalDays);
            }

            _owner.Status = @"Start...";
            ExtractDocumentsDayByDay(startDate, endDate);
        }

        public void DocumentProcess()
        {
            InitDbContext();

            _owner.Status = @"Start...";

            var hcaiNumber = this.HcaiDocumentNumber;
            var ocfType = this.OcfType;
            var date = HcaiNum2Date(hcaiNumber);

            var res = ExtractSingleFullDocument(hcaiNumber, ocfType, date, out _);
            if (res)
                _owner.Status = @"Extracted successfully";
        }

        private void ExtractDocumentsDayByDay(DateTime start, DateTime end)
        {
            var date = start;
            var day = 0;

            var addedCount = 0;
            while (date < end && !CancellationPending)
            {
                Progress(++day);
                var res = ExtractFullDocuments(date, date.AddDays(1), ref addedCount);
                if (!res) break;

                // next
                date = date.AddDays(2);
                Progress(++day);

                // reset DB cache
                if (addedCount >= 20)
                {
                    addedCount = 0;
                    InitDbContext();
                }
            }
        }

        private bool ExtractFullDocuments(DateTime from, DateTime to, ref int addedCount)
        {
            var result = GetActivityList(from, to);
            if (result is null) return true;

            foreach (var doc in result)
            {
                var res = ExtractSingleFullDocument(doc, out var added);
                if (!res)
                    return false;
                if (added)
                    ++addedCount;
            }
            return true;
        }

        private bool ExtractSingleFullDocument(ActivityLineItem activityDoc, out bool added)
        {
            var date = activityDoc.Submission_Date.ToHcaiDateTime().Date;
            var hcaiNumber = activityDoc.HCAI_Document_Number;
            var ocfType = activityDoc.Document_Type;

            return ExtractSingleFullDocument(hcaiNumber, ocfType, date, out added);
        }

        private bool ExtractSingleFullDocument(string hcaiNumber, string ocfType, DateTime date, out bool added)
        {
            added = false;

            if (string.IsNullOrEmpty(ocfType))
            {
                NewMessageEvent("Error: ocfType is null or empty");
                return !this.IsExtract;
            }
            if (string.IsNullOrEmpty(hcaiNumber))
            {
                NewMessageEvent("Error: hcai number is null or empty");
                return !this.IsExtract;
            }

            if (!Enum.TryParse(ocfType, true, out DocType docType))
            {
                NewMessageEvent($"Error: unknown document type '{ocfType}'");
                return !this.IsExtract;
            }

            var res = _pmsTk.dataExtract(Config.Login, Config.Pass, hcaiNumber, docType);
            if (res is null)
            {
                if (this.IsExtract)
                    _owner.Status = @"No document in response!";
                return !this.IsExtract;
            }

            try
            {
                if (IsXmlSave && this.IsExtract)
                {
                    var fileName = $"{ocfType}-{hcaiNumber}.xml";
                    var fullPath = Path.Combine(XmlPath, $"{date:yyyy-MM-dd}");
                    var filePath = Path.Combine(fullPath, fileName);

                    SaveAsXml(res, filePath);
                }

                switch (docType)
                {
                    case DocType.AACN:
                        added = ExtractForm1(res);
                        break;
                    case DocType.OCF18:
                        added = ExtractOcf18(res);
                        break;
                    case DocType.OCF21B:
                        added = ExtractOcf21B(res);
                        break;
                    case DocType.OCF21C:
                        added = ExtractOcf21C(res);
                        break;
                    case DocType.OCF23:
                        added = ExtractOcf23(res);
                        break;
                    case DocType.ACSI:
                        added = ExtractAcsi(res);
                        break;

                    default:
                        break;
                }

                if (IsXmlSave && added && !this.IsExtract)
                {
                    var fileName = $"{docType}-{hcaiNumber}.xml";
                    var fullPath = Path.Combine(XmlPath, $"{date:yyyy-MM-dd}");
                    var filePath = Path.Combine(fullPath, fileName);

                    SaveAsXml(res, filePath);
                }

                // OCF 9
                if (added)
                    ExtractOcf9(res, docType, hcaiNumber);

                return true;
            }
            catch (Exception ex)
            {
                NewMessageEvent("Error: " + ex.ExtendedEfMessage());
                return false;
            }
        }

        #endregion

        private ActivityLineItem[] GetActivityList(DateTime from, DateTime to)
        {
            try
            {
                var res = new List<ActivityLineItem>();

                var actLst = _pmsTk.activityList(Config.Login, Config.Pass, from, to);
                if (actLst is null)
                {
                    NewMessageEvent(Environment.NewLine + $"*** {nameof(GetActivityList)}({@from:yyyy-MM-dd}, {to:yyyy-MM-dd})");
                    PopulateErrorMessages();
                    return null;
                }

                var lst = actLst.getList(LineItemType.ActivityLineItem);
                if (lst is null)
                {
                    NewMessageEvent(Environment.NewLine + $"*** {nameof(GetActivityList)}({@from:yyyy-MM-dd}, {to:yyyy-MM-dd})");
                    PopulateErrorMessages();
                    return null;
                }

                var actKeys = new ActivityLineItemKeys();
                for (var i = 0; i < lst.Count; i++ )
                {
                    var item = (IDataItem)lst[i];
                    var act = new ActivityLineItem
                    {
                        HCAI_Document_Number = item.getValue(actKeys.HCAI_Document_Number),
                        PMS_Document_Key = item.getValue(actKeys.PMSFields_PMSDocumentKey),
                        PMS_Patient_Key = item.getValue(actKeys.PMSFields_PMSPatientKey),
                        Submission_Source = item.getValue(actKeys.Submission_Source),
                        Submission_Date = item.getValue(actKeys.Submission_Date),
                        Document_Status = item.getValue(actKeys.Document_Status),
                        Document_Type = item.getValue(actKeys.Document_Type),
                    };
                    res.Add(act);
                }

                var count = res.Count;
                if (count > 0)
                    NewMessageEvent(Environment.NewLine + $"*** {nameof(GetActivityList)}({@from:yyyy-MM-dd}, {to:yyyy-MM-dd}): {count} documents.");
                return res.OrderBy(a => a.Submission_Date).ToArray();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ExtendedEfMessage(), $@"{nameof(GetActivityList)} Error!", MessageBoxButtons.OK);
                return null;
            }
        }

        #region Service

        private static DateTime HcaiNum2Date(string number)
        {
            try
            {
                var year = (DateTime.Now.Year / 100) * 100 + int.Parse(number.Substring(0, 2));
                var month = int.Parse(number.Substring(2, 2));
                var day = int.Parse(number.Substring(4, 2));

                return new DateTime(year, month, day);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void PopulateErrorMessages()
        {
            var err = _pmsTk.getError();
            if (err is null) return;

            var errorType = err.getErrorType();
            var errorDescription = err.getErrorDescription();
            if (errorType == "109")
                errorDescription = "Authentication Error: Check login and password";

            var errorList = !string.IsNullOrEmpty(errorType) || !string.IsNullOrEmpty(errorDescription)
                ? errorType + ":?:" + errorDescription
                : string.Empty;

            var errList = err.getErrorList();
            if (errList.Count > 0)
            {
                if (!string.IsNullOrEmpty(errorList))
                    errorList += Environment.NewLine;

                // Common Errors
                foreach (var t in errList)
                {
                    var li = (PMSToolkit.ToolkitError.ErrorDescription)t;
                    var tmp = li.getKey().Replace("/", " ");
                    tmp = tmp + "; " + li.getDescription();
                    errorList += tmp + Environment.NewLine;
                }
            }

            if (!string.IsNullOrEmpty(errorList))
                NewMessageEvent($"Error: {errorList}");
        }

        private void SaveAsXml(IDataItem res, string filePath)
        {
            var fullPath = Path.GetDirectoryName(filePath);
            try
            {
                // check dir
                if (!Directory.Exists(fullPath)) Directory.CreateDirectory(fullPath);
                // get
                var data = (XmlDataItem)res;
                var text = data.Xmn.InnerXml;
                // parse
                var doc = XDocument.Parse(text);

                // save
                var exist = File.Exists(filePath);
                if (exist)
                {
                    var ext = Path.GetExtension(filePath);
                    var pureFileName = Path.GetFileNameWithoutExtension(filePath);

                    // find last version
                    var lastFilePath = filePath;
                    int idx;
                    for (idx = 1; idx < 10; idx++)
                    {
                        if (File.Exists(Path.Combine(fullPath, $"{pureFileName}_{idx}{ext}")))
                        {
                            lastFilePath = Path.Combine(fullPath, $"{pureFileName}_{idx}{ext}");
                            continue;
                        }
                        break;
                    }

                    // comparing
                    var oldDoc = XDocument.Load(lastFilePath);
                    if (HashXElement(oldDoc.Root) != HashXElement(doc.Root))
                    {
                        idx = 1;
                        while (File.Exists(filePath))
                        {
                            filePath = Path.Combine(fullPath, $"{pureFileName}_{idx++}{ext}");
                        }
                        doc.Save(filePath);
                        NewMessageEvent($"  file - saved new version: '{filePath}'");
                    }
                }
                else
                {
                    doc.Save(filePath);
                }
            }
            catch (Exception ex)
            {
                NewMessageEvent($"Exception doc.Save({filePath}): {ex.Message}");
            }
        }

        private static int HashXElement(XElement elem)
        {
            var hash = 23;

            foreach (var attribute in elem.Attributes())
            {
                var attributeHash = 23;
                attributeHash = attributeHash * 37 + attribute.Name.GetHashCode();
                attributeHash = attributeHash * 37 + attribute.Value.GetHashCode();
                hash = hash ^ attributeHash;
            }

            foreach (var subElem in elem.Descendants())
            {
                hash = hash * 37 + HashXElement(subElem);
            }

            hash = hash * 37 + elem.Value.GetHashCode();

            return hash;
        }

        #endregion
    }
}