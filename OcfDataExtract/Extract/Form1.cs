﻿using System.Linq;
using HcaiSync.Common.EfData;
using PMSToolkit;
using PMSToolkit.DataObjects;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractForm1(IDataItem doc)
        {
            var form1AdjKeys = new AACNDataExtractResponseKeys();
            var form1ServicesLineItemKeys = new AACNDataExtractResponseLineItemKeys();

            var documentNumber = doc.GetValue(form1AdjKeys.DocumentNumber);
            if (string.IsNullOrEmpty(documentNumber))
            {
                NewMessageEvent("Extracting doc with type 'FORM1' - NO number!");
                return false;
            }

            var documentStatus = doc.GetValue(form1AdjKeys.DocumentState);
            var form1ARrow = _dbContext.AACNARs
                .Include("AACNServicesLineItemARs").Include("AACNPart4LineItemAR")
                .FirstOrDefault(d => d.DocumentNumber == documentNumber);

            var rowSubmitted = _dbContext.AACNSubms
                .Include("AACNServicesLineItemSubms").Include("AACNPart4LineItemSubm")
                .FirstOrDefault(d => d.DocumentNumber == documentNumber);
            var isExists = rowSubmitted != null;
            if (!isExists)
            {
                NewMessageEvent($"Extracting doc with type 'FORM1' & number = '{documentNumber}' ");
            }
            else if (documentStatus == form1ARrow?.DocumentState)
            {
                return false;
            }
            else
            {
                NewMessageEvent($"Updating doc with type 'FORM1' & number = '{documentNumber}' from status '{form1ARrow?.DocumentState}' to status '{documentStatus}'");
            }

            rowSubmitted ??= _dbContext.AACNSubms.Add(new AACNSubm { DocumentNumber = documentNumber });

            #region sumbit doc fields

            // ***** header *****
            rowSubmitted.PMSFields_PMSSoftware = doc.GetValue(form1AdjKeys.PMSFields_PMSSoftware);
            rowSubmitted.PMSFields_PMSVersion = doc.GetValue(form1AdjKeys.PMSFields_PMSVersion);
            rowSubmitted.PMSFields_PMSDocumentKey = doc.GetValue(form1AdjKeys.PMSFields_PMSDocumentKey);
            rowSubmitted.PMSFields_PMSPatientKey = doc.GetValue(form1AdjKeys.PMSFields_PMSPatientKey);

            rowSubmitted.AttachmentsBeingSent = doc.GetValue(form1AdjKeys.AttachmentsBeingSent).ToBool();
            rowSubmitted.AttachmentCount = doc.GetValue(form1AdjKeys.AttachmentCount).ToInt32Nullable();
            rowSubmitted.AdditionalComments = doc.GetValue(form1AdjKeys.AdditionalComments);

            rowSubmitted.Header_ClaimNumber = doc.GetValue(form1AdjKeys.Header_ClaimNumber);
            rowSubmitted.Header_PolicyNumber = doc.GetValue(form1AdjKeys.Header_PolicyNumber);
            rowSubmitted.Header_DateOfAccident = doc.GetValue(form1AdjKeys.Header_DateOfAccident).ToHcaiDate();
            // Applicant
            rowSubmitted.Applicant_Name_FirstName = doc.GetValue(form1AdjKeys.Applicant_Name_FirstName);
            rowSubmitted.Applicant_Name_MiddleName = doc.GetValue(form1AdjKeys.Applicant_Name_MiddleName);
            rowSubmitted.Applicant_Name_LastName = doc.GetValue(form1AdjKeys.Applicant_Name_LastName);
            rowSubmitted.Applicant_DateOfBirth = doc.GetValue(form1AdjKeys.Applicant_DateOfBirth).ToHcaiDate();
            rowSubmitted.Applicant_Gender = doc.GetValue(form1AdjKeys.Applicant_Gender).ToGender();
            rowSubmitted.Applicant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(form1AdjKeys.Applicant_Telephone));
            rowSubmitted.Applicant_Extension = doc.GetValue(form1AdjKeys.Applicant_Extension);
            rowSubmitted.Applicant_Address_StreetAddress1 = doc.GetValue(form1AdjKeys.Applicant_Address_StreetAddress1);
            rowSubmitted.Applicant_Address_StreetAddress2 = doc.GetValue(form1AdjKeys.Applicant_Address_StreetAddress2);
            rowSubmitted.Applicant_Address_City = doc.GetValue(form1AdjKeys.Applicant_Address_City);
            rowSubmitted.Applicant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(form1AdjKeys.Applicant_Address_Province));
            rowSubmitted.Applicant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(form1AdjKeys.Applicant_Address_PostalCode));

            rowSubmitted.AssessmentDate = doc.GetValue(form1AdjKeys.AssessmentDate).ToHcaiDate();
            rowSubmitted.FirstAssessment = doc.GetValue(form1AdjKeys.FirstAssessment).ToInt32();
            rowSubmitted.LastAssessmentDate = doc.GetValue(form1AdjKeys.LastAssessmentDate).ToHcaiDateNullable();
            rowSubmitted.CurrentMonthlyAllowance = doc.GetValue(form1AdjKeys.CurrentMonthlyAllowance).ToDecimalNullable();
            rowSubmitted.InsurerExamination = doc.GetValue(form1AdjKeys.InsurerExamination).ToBoolNullable();

            rowSubmitted.Assessor_FacilityRegistryID = doc.GetValue(form1AdjKeys.Assessor_FacilityRegistryID).ToInt32();
            rowSubmitted.Assessor_ProviderRegistryID = doc.GetValue(form1AdjKeys.Assessor_ProviderRegistryID).ToInt32();
            rowSubmitted.Assessor_Occupation = doc.GetValue(form1AdjKeys.Assessor_Occupation);
            rowSubmitted.Assessor_Email = doc.GetValue(form1AdjKeys.Assessor_Email);
            rowSubmitted.Assessor_IsSignatureOnFile = doc.GetValue(form1AdjKeys.Assessor_IsSignatureOnFile).ToBool();
            rowSubmitted.Assessor_DateSigned = doc.GetValue(form1AdjKeys.Assessor_DateSigned).ToHcaiDate();

            // Insurer
            rowSubmitted.Insurer_IBCInsurerID = doc.GetValue(form1AdjKeys.Insurer_IBCInsurerID);
            rowSubmitted.Insurer_IBCBranchID = doc.GetValue(form1AdjKeys.Insurer_IBCBranchID);

            rowSubmitted.Insurer_PolicyHolder_IsSameAsApplicant = doc.GetValue(form1AdjKeys.Insurer_PolicyHolder_IsSameAsApplicant).ToBool();
            rowSubmitted.Insurer_PolicyHolder_Name_FirstName = doc.GetValue(form1AdjKeys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmitted.Insurer_PolicyHolder_Name_LastName = doc.GetValue(form1AdjKeys.Insurer_PolicyHolder_Name_LastName);

            rowSubmitted.FormVersion = doc.GetValue(form1AdjKeys.FormVersion).ToInt32();

            // AACNServicesLineItemSubm-s - ok & good!
            //"/hcai:*/hcai:AACN/hcai:AttendantCareServices/hcai:Item"
            var liList = doc.getList(LineItemType.AACNServicesLineItem);
            if (liList?.Count > 0)
            {
                NewMessageEvent($"  AACNServicesLineItemSubm[{liList.Count}]");
                for (var i = 0; i < liList.Count; i++)
                {
                    var tmpItem = (IDataItem)liList[i];
                    var acsItemId = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_ACSItemID);
                    var minutes = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Assessed_Minutes).ToDecimal();
                    var timesPerWeek = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Assessed_TimesPerWeek).ToDecimal();
                    
                    // skip if all zero
                    if (minutes == 0 && timesPerWeek == 0)
                    {
                        var approvedMinutes = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_Minutes).ToDecimal();
                        var approvedTimesPerWeek = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_TimesPerWeek).ToDecimal();
                        var approvedTotalMinutes = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_TotalMinutes).ToDecimal();

                        if (approvedMinutes == 0 && approvedTimesPerWeek == 0 && approvedTotalMinutes == 0)
                            continue;
                    }

                    var lineItemSubm = rowSubmitted.AACNServicesLineItemSubms
                        .FirstOrDefault(t => t.AttendantCareServices_Item_ACSItemID == acsItemId)
                        ?? _dbContext.AACNServicesLineItemSubms.Add(new AACNServicesLineItemSubm
                        {
                            AACNSubm = rowSubmitted,
                            DocumentNumber = documentNumber,
                            AttendantCareServices_Item_ACSItemID = acsItemId,
                        });

                    lineItemSubm.AttendantCareServices_Item_ACSItemID = acsItemId;
                    lineItemSubm.AttendantCareServices_Item_PMSLineItemKey = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_PMSLineItemKey);
                    lineItemSubm.AttendantCareServices_Item_Assessed_Minutes = minutes;
                    lineItemSubm.AttendantCareServices_Item_Assessed_TimesPerWeek = timesPerWeek;
                }
            }

            // AACNPart4LineItemSubm-s - ok & good!
            //"/hcai:*/hcai:AACN/hcai:Costs/hcai:Approved/hcai:Part"
            liList = doc.getList(LineItemType.AACNAssessedPart);
            if (liList?.Count > 0)
            {
                var form1Part4LineItemSubmKeys = new AACNDataExtractResponseLineItemKeys();
                NewMessageEvent($"  AACNPart4LineItemSubm[{liList.Count}]");
                for (var i = 0; i < liList.Count; i++)
                {
                    var tmpItem = (IDataItem)liList[i];
                    var acsItemId = tmpItem.GetValue(form1Part4LineItemSubmKeys.Costs_Assessed_Part_ACSItemID);

                    var item = rowSubmitted.AACNPart4LineItemSubm
                        .FirstOrDefault(t => t.Costs_Assessed_Part_ACSItemID == acsItemId)
                        ?? _dbContext.AACNPart4LineItemSubm.Add(new AACNPart4LineItemSubm
                        {
                            AACNSubm = rowSubmitted,
                            DocumentNumber = documentNumber,
                            Costs_Assessed_Part_ACSItemID = acsItemId
                        });

                    item.Costs_Assessed_Part_HourlyRate =
                        tmpItem.GetValue(form1Part4LineItemSubmKeys.Costs_Assessed_Part_HourlyRate).ToDecimal();
                    item.Costs_Assessed_Part_WeeklyHours =
                        tmpItem.GetValue(form1Part4LineItemSubmKeys.Costs_Assessed_Part_WeeklyHours).ToDecimal();
                    item.Costs_Assessed_Part_MonthlyHours =
                        tmpItem.GetValue(form1Part4LineItemSubmKeys.Costs_Assessed_Part_MonthlyHours).ToDecimal();
                    item.Costs_Assessed_Part_Benefit =
                        tmpItem.GetValue(form1Part4LineItemSubmKeys.Costs_Assessed_Part_Benefit).ToDecimal();
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            // ****** Adjuster Response *****
            form1ARrow ??= _dbContext.AACNARs.Add(new AACNAR { DocumentNumber = documentNumber });

            #region approved doc fields

            // ***** header ****
            form1ARrow.Document_Type = doc.GetValue(form1AdjKeys.Document_Type);

            form1ARrow.PMSFields_PMSDocumentKey = doc.GetValue(form1AdjKeys.PMSFields_PMSDocumentKey);
            form1ARrow.Insurer_IBCInsurerID = doc.GetValue(form1AdjKeys.Insurer_IBCInsurerID);
            form1ARrow.Insurer_IBCBranchID = doc.GetValue(form1AdjKeys.Insurer_IBCBranchID);
            form1ARrow.DocumentVersion = doc.GetValue(form1AdjKeys.DocumentVersion).ToInt32();
            form1ARrow.SubmissionTime = doc.GetValue(form1AdjKeys.SubmissionTime).ToHcaiDateTime();
            form1ARrow.SubmissionSource = doc.GetValue(form1AdjKeys.SubmissionSource);
            form1ARrow.FacilityVersion = doc.GetValue(form1AdjKeys.FacilityVersion).ToInt32();
            form1ARrow.InsurerVersion = doc.GetValue(form1AdjKeys.InsurerVersion).ToInt32();
            form1ARrow.BranchVersion = doc.GetValue(form1AdjKeys.BranchVersion).ToInt32();
            form1ARrow.DocumentState = documentStatus;
            form1ARrow.InDispute = doc.GetValue(form1AdjKeys.InDispute).ToBool();
            form1ARrow.VersionDate = doc.GetValue(form1AdjKeys.VersionDate).ToHcaiDateTime();
            form1ARrow.AttachmentsReceivedDate = doc.GetValue(form1AdjKeys.AttachmentsReceivedDate).ToHcaiDateNullable();

            form1ARrow.SigningAdjusterName_FirstName = doc.GetValue(form1AdjKeys.SigningAdjusterName_FirstName);
            form1ARrow.SigningAdjusterName_LastName = doc.GetValue(form1AdjKeys.SigningAdjusterName_LastName);
            form1ARrow.NameOfAdjusterOnPDF = doc.GetValue(form1AdjKeys.NameOfAdjusterOnPDF);
            form1ARrow.AdjusterResponseTime = doc.GetValue(form1AdjKeys.AdjusterResponseTime).ToHcaiDateTime();
            form1ARrow.ArchivalStatus = doc.GetValue(form1AdjKeys.ArchivalStatus);

            form1ARrow.Claimant_Name_FirstName = doc.GetValue(form1AdjKeys.Claimant_Name_FirstName);
            form1ARrow.Claimant_Name_MiddleName = doc.GetValue(form1AdjKeys.Claimant_Name_MiddleName);
            form1ARrow.Claimant_Name_LastName = doc.GetValue(form1AdjKeys.Claimant_Name_LastName);
            form1ARrow.Claimant_DateOfBirth = doc.GetValue(form1AdjKeys.Claimant_DateOfBirth).ToHcaiDate();
            form1ARrow.Claimant_Gender = doc.GetValue(form1AdjKeys.Claimant_Gender).ToGender();
            form1ARrow.Claimant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(form1AdjKeys.Claimant_Telephone));
            form1ARrow.Claimant_Extension = doc.GetValue(form1AdjKeys.Claimant_Extension);
            form1ARrow.Claimant_Address_StreetAddress1 = doc.GetValue(form1AdjKeys.Claimant_Address_StreetAddress1);
            form1ARrow.Claimant_Address_StreetAddress2 = doc.GetValue(form1AdjKeys.Claimant_Address_StreetAddress2);
            form1ARrow.Claimant_Address_City = doc.GetValue(form1AdjKeys.Claimant_Address_City);
            form1ARrow.Claimant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(form1AdjKeys.Claimant_Address_Province));
            form1ARrow.Claimant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(form1AdjKeys.Claimant_Address_PostalCode));

            form1ARrow.EOB_EOBAdditionalComments = doc.GetValue(form1AdjKeys.EOB_EOBAdditionalComments);

            form1ARrow.Costs_Approved_CalculatedBenefit = doc.GetValue(form1AdjKeys.Costs_Approved_CalculatedBenefit).ToDecimal();
            form1ARrow.Costs_Approved_Benefit = doc.GetValue(form1AdjKeys.Costs_Approved_Benefit).ToDecimal();

            form1ARrow.Costs_Approved_ReasonCode = doc.GetValue(form1AdjKeys.Costs_Approved_ReasonCode);
            form1ARrow.Costs_Approved_ReasonDescription = doc.GetValue(form1AdjKeys.Costs_Approved_ReasonDescription);
            form1ARrow.Costs_Approved_AdjusterResponseExplanation = doc.GetValue(form1AdjKeys.Costs_Approved_AdjusterResponseExplanation);

            // AACNServicesLineItemAR-s - ok & good!
            //"/hcai:*/hcai:AACN/hcai:AttendantCareServices/hcai:Item"
            liList = doc.getList(LineItemType.AACNServicesLineItem);
            if (liList?.Count > 0)
            {
                NewMessageEvent($"  AACNServicesLineItemAR[{liList.Count}]");
                for (var i = 0; i < liList.Count; i++)
                {
                    var tmpItem = (IDataItem)liList[i];
                    var acsItemId = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_ACSItemID);

                    // skip if no in submit
                    var findSubmit = rowSubmitted.AACNServicesLineItemSubms
                        .FirstOrDefault(t => t.AttendantCareServices_Item_ACSItemID == acsItemId);
                    if (findSubmit is null) continue;

                    var lineService = form1ARrow.AACNServicesLineItemARs
                        .FirstOrDefault(t => t.AttendantCareServices_Item_ACSItemID == acsItemId)
                        ?? _dbContext.AACNServicesLineItemARs.Add(new AACNServicesLineItemAR
                        {
                            AACNAR = form1ARrow,
                            DocumentNumber = documentNumber,
                            AttendantCareServices_Item_ACSItemID = acsItemId
                        });

                    // fill
                    lineService.AttendantCareServices_Item_PMSLineItemKey =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_PMSLineItemKey);
                    lineService.AttendantCareServices_Item_Description =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Description);

                    lineService.AttendantCareServices_Item_Approved_Minutes =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_Minutes).ToDecimal();
                    lineService.AttendantCareServices_Item_Approved_TimesPerWeek =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_TimesPerWeek).ToDecimal();
                    lineService.AttendantCareServices_Item_Approved_TotalMinutes =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_TotalMinutes).ToDecimal();

                    lineService.AttendantCareServices_Item_Approved_ReasonCode =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_ReasonCode);
                    lineService.AttendantCareServices_Item_Approved_ReasonDescription =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_ReasonDescription);

                    lineService.AttendantCareServices_Item_Approved_IsItemDeclined =
                        tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_Approved_IsItemDeclined)
                            .ToBoolNullable();
                }
            }

            // AACNPart4LineItemAR-s - ok & good!
            //"/hcai:*/hcai:AACN/hcai:Costs/hcai:Approved/hcai:Part"
            liList = doc.getList(LineItemType.AACNApprovedPart);
            if (liList?.Count > 0)
            {
                NewMessageEvent($"  AACNPart4LineItemAR[{liList.Count}]");
                for (var i = 0; i < liList.Count; i++)
                {
                    var tmpItem = (IDataItem)liList[i];
                    var acsItemId = tmpItem.GetValue(form1ServicesLineItemKeys.AttendantCareServices_Item_ACSItemID);

                    var linePart4 = form1ARrow.AACNPart4LineItemAR
                        .FirstOrDefault(t => t.Costs_Approved_Part_ACSItemID == acsItemId)
                        ?? _dbContext.AACNPart4LineItemAR.Add(new AACNPart4LineItemAR
                        {
                            AACNAR = form1ARrow,
                            DocumentNumber = documentNumber,
                            Costs_Approved_Part_ACSItemID = acsItemId,
                        });

                    // fill
                    linePart4.Costs_Approved_Part_WeeklyHours =
                        tmpItem.GetValue(form1ServicesLineItemKeys.Costs_Approved_Part_WeeklyHours).ToDecimal();
                    linePart4.Costs_Approved_Part_MonthlyHours =
                        tmpItem.GetValue(form1ServicesLineItemKeys.Costs_Approved_Part_MonthlyHours).ToDecimal();
                    linePart4.Costs_Approved_Part_HourlyRate =
                        tmpItem.GetValue(form1ServicesLineItemKeys.Costs_Approved_Part_HourlyRate).ToDecimalNullable();
                    linePart4.Costs_Approved_Part_Benefit =
                        tmpItem.GetValue(form1ServicesLineItemKeys.Costs_Approved_Part_Benefit).ToDecimal();
                    linePart4.Costs_Approved_Part_ReasonCode =
                        tmpItem.GetValue(form1ServicesLineItemKeys.Costs_Approved_Part_ReasonCode);
                    linePart4.Costs_Approved_Part_ReasonDescription =
                        tmpItem.GetValue(form1ServicesLineItemKeys.Costs_Approved_Part_ReasonDescription);
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            return true;
        }
    }
}
