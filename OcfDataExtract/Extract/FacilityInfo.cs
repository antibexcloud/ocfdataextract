﻿using System;
using System.IO;
using System.Linq;
using HcaiSync.Common.EfData;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractFacilityInfo()
        {
            NewMessageEvent("*** Extracting Facility Info");

            var facility = _pmsTk.facilityInfo(Config.Login, Config.Pass);
            if (facility is null)
            {
                PopulateErrorMessages();
                return false;
            }

            if (IsXmlSave)
            {
                var filePath = Path.Combine(XmlPath, "FacilityInfo.xml");
                SaveAsXml(facility, filePath);
            }

            var facList = facility.getList(LineItemType.FacilityLineItem);
            if (facList is null)
            {
                PopulateErrorMessages();
                return false;
            }

            if (facList.Count == 0)
                facList = facility.getList(LineItemType.FacilityLineItem);
            if (facList is null)
            {
                PopulateErrorMessages();
                return false;
            }

            var facilityKeys = new PMSToolkit.DataObjects.FacilityKeys();
            var providerKeys = new PMSToolkit.DataObjects.ProviderLineItemKeys();
            var regKeys = new PMSToolkit.DataObjects.RegistrationLineItemKeys();
            var licKeys = new PMSToolkit.DataObjects.FacilityLicenseLineItemKeys();

            for (var k = 0; k < facList.Count; k++)
            {
                var fcl = (IDataItem)facList[k];
                var id = Convert.ToInt32(fcl.getValue(facilityKeys.HCAI_Facility_Registry_ID));

                var f = _dbContext.FacilityLineItems
                            .Include("ProviderLineItems")
                            .FirstOrDefault(fc => fc.HCAI_Facility_Registry_ID == id)
                        ?? _dbContext.FacilityLineItems.Add(new FacilityLineItem
                        {
                            HCAI_Facility_Registry_ID = id,
                            HcaiLogin = Config.Login,
                            HcaiPassword = Config.Pass
                        });

                f.Facility_Name = fcl.getValue(facilityKeys.Facility_Name);
                f.HcaiCheckPayableTo = fcl.getValue("hcai:MakeChequePayableText");
                f.Billing_Address_Line_1 = fcl.getValue(facilityKeys.Billing_Address_Line_1);
                f.Billing_Address_Line_2 = fcl.getValue(facilityKeys.Billing_Address_Line_2);
                f.Billing_City = fcl.getValue(facilityKeys.Billing_City);
                f.Billing_Province = ConvertHelper.FormatProvince(fcl.getValue(facilityKeys.Billing_Province));
                f.Billing_Postal_Code = ConvertHelper.FormatPostalCode(fcl.getValue(facilityKeys.Billing_Postal_Code));

                // Service
                f.SameServiceAddress = fcl.getValue(facilityKeys.SameServiceAddress).ToBool();
                f.Service_Address_Line_1 = fcl.getValue(facilityKeys.Service_Address_Line_1);
                f.Service_Address_Line_2 = fcl.getValue(facilityKeys.Service_Address_Line_2);
                f.Service_City = fcl.getValue(facilityKeys.Service_City);
                f.Service_Province = ConvertHelper.FormatProvince(fcl.getValue(facilityKeys.Service_Province));
                f.Service_Postal_Code = ConvertHelper.FormatPostalCode(fcl.getValue(facilityKeys.Service_Postal_Code));
                
                f.AISI_Facility_Number = fcl.getValue(facilityKeys.AISI_Facility_Number);
                f.Telephone_Number = fcl.getValue(facilityKeys.Telephone_Number);
                f.Telephone_Extension = fcl.getValue("hcai:Extension");
                f.Fax_Number = fcl.getValue(facilityKeys.Fax_Number);

                f.Authorizing_Officer_First_Name = fcl.getValue(facilityKeys.Authorizing_Officer_First_Name);
                f.Authorizing_Officer_Last_Name = fcl.getValue(facilityKeys.Authorizing_Officer_Last_Name);
                f.Authorizing_Officer_Title = fcl.getValue(facilityKeys.Authorizing_Officer_Title);
                f.Authorizing_Officer_Email = fcl.getValue("hcai:AuthorizingOfficerEmail");

                f.Lock_Payable_Flag = fcl.getValue(facilityKeys.Lock_Payable_Flag).ToBoolNullable() ?? false;

                f.Facility_Start_Date = fcl.getValue(facilityKeys.Facility_Start_Date).ToHcaiDateTime();
                f.Facility_End_Date = fcl.getValue(facilityKeys.Facility_End_Date).ToHcaiDateTimeNullable();

                // not documented ))
                f.CorporationNumber = fcl.getValue("hcai:CorporationNumber").ToInt32Nullable();

                f.Contact1_First_Name = fcl.getValue("hcai:Contact1Name/hcai:FirstName");
                f.Contact1_Last_Name = fcl.getValue("hcai:Contact1Name/hcai:LastName");
                f.Contact1_First_Title = fcl.getValue("hcai:Contact1Title");
                f.Contact1_First_Telephone = ConvertHelper.PhoneFromHcai(fcl.getValue("hcai:Contact1Telephone"));
                f.Contact1_First_Extension = fcl.getValue("hcai:Contact1Extension");
                f.Contact1_First_Email = fcl.getValue("hcai:Contact1Email");

                f.Contact2_First_Name = fcl.getValue("hcai:Contact2Name/hcai:FirstName");
                f.Contact2_Last_Name = fcl.getValue("hcai:Contact2Name/hcai:LastName");
                f.Contact2_First_Title = fcl.getValue("hcai:Contact2Title");
                f.Contact2_First_Telephone = ConvertHelper.PhoneFromHcai(fcl.getValue("hcai:Contact2Telephone"));
                f.Contact2_First_Extension = fcl.getValue("hcai:Contact2Extension");
                f.Contact2_First_Email = fcl.getValue("hcai:Contact2Email");

                // Providers
                var providerList = fcl.getList(LineItemType.ProviderLineItem);
                if (providerList != null)
                    for (var i = 0; i < providerList.Count; i++)
                    {
                        var provider = (IDataItem)providerList[i];
                        var idProvider = Convert.ToInt32(provider.getValue(providerKeys.HCAI_Provider_Registry_ID));
                        var pr = f.ProviderLineItems
                            .FirstOrDefault(p => p.HCAI_Provider_Registry_ID == idProvider) ??
                            _dbContext.ProviderLineItems.Add(new ProviderLineItem
                            {
                                FacilityLineItem = f,
                                HCAI_Facility_Registry_ID = f.HCAI_Facility_Registry_ID,
                                HCAI_Provider_Registry_ID = idProvider,
                            });

                        pr.Provider_First_Name = provider.getValue(providerKeys.Provider_First_Name);
                        pr.Provider_Last_Name = provider.getValue(providerKeys.Provider_Last_Name);
                        pr.Provider_Start_Date = provider.getValue(providerKeys.Provider_Start_Date).ToHcaiDate();
                        pr.Provider_End_Date = provider.getValue(providerKeys.Provider_End_Date).ToHcaiDateNullable();
                        pr.Provider_Status = provider.getValue(providerKeys.Provider_Status);

                        // ****
                        var providerRegList = provider.getList(LineItemType.RegistrationLineItem);
                        if (providerRegList != null)
                            for (var j = 0; j < providerRegList.Count; j++)
                            {
                                var provReg = (IDataItem)providerRegList[j];
                                var numReg = provReg.getValue(regKeys.Registration_Number);

                                var prr = pr.RegistrationLineItems
                                    .FirstOrDefault(prof => prof.Registration_Number == numReg) ??
                                    _dbContext.RegistrationLineItems.Add(new RegistrationLineItem
                                    {
                                        ProviderLineItem = pr,
                                        HCAI_Provider_Registry_ID = pr.HCAI_Provider_Registry_ID,
                                        Registration_Number = numReg,
                                    });

                                prr.Profession = provReg.getValue(regKeys.Profession);
                                prr.HCAI_Provider_Registry_ID = pr.HCAI_Provider_Registry_ID;
                            }
                    }

                // licenses
                var licList = fcl.getList(LineItemType.FacilityLicenseLineItem);
                if (licList != null)
                    for (var i = 0; i < licList.Count; i++)
                    {
                        var fl = (IDataItem) licList[i];
                        var numLic = fl.getValue(licKeys.License_Number);

                        var lic = f.FacilityLicenseLineItems
                            .FirstOrDefault(l => l.License_Number == numLic) ??
                                  _dbContext.FacilityLicenseLineItems.Add(new FacilityLicenseLineItem
                                  {
                                      FacilityLineItem = f,
                                      HCAI_Facility_Registry_ID = f.HCAI_Facility_Registry_ID,
                                      License_Number = numLic,
                                  });

                        lic.License_Status = fl.getValue(licKeys.License_Status);
                        lic.License_End_Date = fl.getValue(licKeys.License_End_Date).ToHcaiDateNullable();
                        lic.License_Last_Modified_Date = fl.getValue(licKeys.License_Last_Modified_Date).ToHcaiDate();
                    }

                // save
                _dbContext.SaveChangesWithDetect();
            }

            NewMessageEvent($"  facilities = {facList.Count}");
            return true;
        }
    }
}
