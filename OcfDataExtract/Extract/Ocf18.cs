﻿using System;
using System.Linq;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractOcf18(IDataItem doc)
        {
            var edOcf18Keys = new PMSToolkit.DataObjects.OCF18DataExtractResponseKeys();
            var edOcf18LisKeys = new PMSToolkit.DataObjects.OCF18DataExtractResponseLineItemKeys();

            var documentNumber = doc.GetValue(edOcf18Keys.HCAI_Document_Number);
            if (string.IsNullOrEmpty(documentNumber))
            {
                NewMessageEvent("Extracting doc with type 'OCF18' - NO number!");
                return false;
            }

            var documentStatus = doc.GetValue(edOcf18Keys.Document_Status);
            var ocf18ARrow = _dbContext.OCF18AR
                .Include("GS18LineItemAR").Include("SessionHeaderLineItemARs")
                .FirstOrDefault(ar => ar.HCAI_Document_Number == documentNumber);

            var rowSubmitted = _dbContext.OCF18Submit
                .Include("GS18LineItemSubmit").Include("SessionHeaderLineItemSubmits")
                .Include("SessionLineItemSubmits").Include("OCF18InjuryLineItemSubmit")
                .FirstOrDefault(d => d.HCAI_Document_Number == documentNumber);
            var isExists = rowSubmitted != null;

            var docApproved = ocf18ARrow?.OCF18_InsurerTotals_SubTotal_Approved ?? 0;
            var itemsApproved = ocf18ARrow?.GS18LineItemAR
                .Select(t => t.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost)
                .Sum() ?? 0;

            if (!isExists)
            {
                NewMessageEvent($"Extracting doc with type 'OCF18' & number = '{documentNumber}'");
            }
            else if (documentStatus == ocf18ARrow?.Document_Status && Math.Abs(docApproved - itemsApproved) > 0.01m)
            {
                return false;
            }
            else
            {
                NewMessageEvent($"Updating doc with type 'OCF18' & number = '{documentNumber}' from status '{ocf18ARrow?.Document_Status}' to status '{documentStatus}'");
            }

            rowSubmitted ??= _dbContext.OCF18Submit.Add(new OCF18Submit { HCAI_Document_Number = documentNumber });

            #region sumbit doc fields

            // ***** header ***
            rowSubmitted.PMSFields_PMSSoftware = doc.GetValue(edOcf18Keys.PMSFields_PMSSoftware);
            rowSubmitted.PMSFields_PMSVersion = doc.GetValue(edOcf18Keys.PMSFields_PMSVersion);
            rowSubmitted.PMSFields_PMSDocumentKey = doc.GetValue(edOcf18Keys.PMSFields_PMSDocumentKey);
            rowSubmitted.PMSFields_PMSPatientKey = doc.GetValue(edOcf18Keys.PMSFields_PMSPatientKey);
            rowSubmitted.AttachmentsBeingSent = doc.GetValue(edOcf18Keys.AttachmentsBeingSent).ToBool();
            rowSubmitted.Header_ClaimNumber = doc.GetValue(edOcf18Keys.Header_ClaimNumber);
            rowSubmitted.Header_PolicyNumber = doc.GetValue(edOcf18Keys.Header_PolicyNumber);
            rowSubmitted.Header_DateOfAccident = doc.GetValue(edOcf18Keys.Header_DateOfAccident).ToHcaiDate();
            rowSubmitted.OCF18_Header_PlanNumber = doc.GetValue(edOcf18Keys.OCF18_Header_PlanNumber);

            rowSubmitted.Applicant_Name_FirstName = doc.GetValue(edOcf18Keys.Applicant_Name_FirstName);
            rowSubmitted.Applicant_Name_MiddleName = doc.GetValue(edOcf18Keys.Applicant_Name_MiddleName);
            rowSubmitted.Applicant_Name_LastName = doc.GetValue(edOcf18Keys.Applicant_Name_LastName);
            rowSubmitted.Applicant_DateOfBirth = doc.GetValue(edOcf18Keys.Applicant_DateOfBirth).ToHcaiDate();
            rowSubmitted.Applicant_Gender = doc.GetValue(edOcf18Keys.Applicant_Gender).ToGender();
            rowSubmitted.Applicant_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf18Keys.Applicant_TelephoneNumber));
            rowSubmitted.Applicant_TelephoneExtension = doc.GetValue(edOcf18Keys.Applicant_TelephoneExtension);
            rowSubmitted.Applicant_Address_StreetAddress1 = doc.GetValue(edOcf18Keys.Applicant_Address_StreetAddress1);
            rowSubmitted.Applicant_Address_StreetAddress2 = doc.GetValue(edOcf18Keys.Applicant_Address_StreetAddress2);
            rowSubmitted.Applicant_Address_City = doc.GetValue(edOcf18Keys.Applicant_Address_City);
            rowSubmitted.Applicant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf18Keys.Applicant_Address_Province));
            rowSubmitted.Applicant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf18Keys.Applicant_Address_PostalCode));

            rowSubmitted.Insurer_IBCInsurerID = doc.GetValue(edOcf18Keys.Insurer_IBCInsurerID);
            rowSubmitted.Insurer_IBCBranchID = doc.GetValue(edOcf18Keys.Insurer_IBCBranchID);

            //
            rowSubmitted.Insurer_Adjuster_Name_FirstName = doc.GetValue(edOcf18Keys.Insurer_Adjuster_Name_FirstName);
            rowSubmitted.Insurer_Adjuster_Name_LastName = doc.GetValue(edOcf18Keys.Insurer_Adjuster_Name_LastName);
            rowSubmitted.Insurer_Adjuster_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf18Keys.Insurer_Adjuster_TelephoneNumber));
            rowSubmitted.Insurer_Adjuster_TelephoneExtension = doc.GetValue(edOcf18Keys.Insurer_Adjuster_TelephoneExtension);
            rowSubmitted.Insurer_Adjuster_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf18Keys.Insurer_Adjuster_FaxNumber));
            rowSubmitted.Insurer_PolicyHolder_IsSameAsApplicant =
                doc.GetValue(edOcf18Keys.Insurer_PolicyHolder_IsSameAsApplicant).ToBool();
            rowSubmitted.Insurer_PolicyHolder_Name_FirstName = doc.GetValue(edOcf18Keys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmitted.Insurer_PolicyHolder_Name_LastName = doc.GetValue(edOcf18Keys.Insurer_PolicyHolder_Name_LastName);

            rowSubmitted.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage =
                doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage).ToBoolNullable();
            rowSubmitted.OCF18_OtherInsurance_MOHAvailable =
                doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_MOHAvailable).ToEnumIntParseNullable<FlagWithNotApplicable>();

            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);

            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmitted.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.GetValue(edOcf18Keys.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);

            rowSubmitted.OCF18_HealthPractitioner_FacilityRegistryID =
                doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_FacilityRegistryID).ToInt32Nullable();
            rowSubmitted.OCF18_HealthPractitioner_ProviderRegistryID =
                doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_ProviderRegistryID).ToInt32Nullable();
            rowSubmitted.OCF18_HealthPractitioner_Occupation = doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_Occupation);
            rowSubmitted.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists =
                doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists).ToBoolNullable();
            rowSubmitted.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails =
                doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails);
            rowSubmitted.OCF18_HealthPractitioner_IsSignatureOnFile =
                doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_IsSignatureOnFile).ToBoolNullable();
            rowSubmitted.OCF18_HealthPractitioner_DateSigned =
                doc.GetValue(edOcf18Keys.OCF18_HealthPractitioner_DateSigned).ToHcaiDateNullable();

            rowSubmitted.OCF18_IsOtherHealthPractitioner = doc.GetValue(edOcf18Keys.OCF18_IsOtherHealthPractitioner).ToBoolNullable();
            if (rowSubmitted.OCF18_IsOtherHealthPractitioner == true)
            {
                rowSubmitted.OCF18_OtherHealthPractitioner_Name_FirstName = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Name_FirstName);
                rowSubmitted.OCF18_OtherHealthPractitioner_Name_LastName = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Name_LastName);
                rowSubmitted.OCF18_OtherHealthPractitioner_FacilityName = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_FacilityName);
                rowSubmitted.OCF18_OtherHealthPractitioner_AISIFacilityNumber = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_AISIFacilityNumber);
                rowSubmitted.OCF18_OtherHealthPractitioner_FacilityRegistryID =
                    doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_FacilityRegistryID).ToInt32Nullable();
                rowSubmitted.OCF18_OtherHealthPractitioner_LicenseNumber = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_LicenseNumber);
                rowSubmitted.OCF18_OtherHealthPractitioner_Address_StreetAddress1 = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress1);
                rowSubmitted.OCF18_OtherHealthPractitioner_Address_StreetAddress2 = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_StreetAddress2);
                rowSubmitted.OCF18_OtherHealthPractitioner_Address_City = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_City);
                rowSubmitted.OCF18_OtherHealthPractitioner_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_Province));
                rowSubmitted.OCF18_OtherHealthPractitioner_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Address_PostalCode));
                rowSubmitted.OCF18_OtherHealthPractitioner_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_TelephoneNumber));
                rowSubmitted.OCF18_OtherHealthPractitioner_TelephoneExtension = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_TelephoneExtension);
                rowSubmitted.OCF18_OtherHealthPractitioner_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_FaxNumber));
                rowSubmitted.OCF18_OtherHealthPractitioner_Email = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Email);
                rowSubmitted.OCF18_OtherHealthPractitioner_ProviderRegistryNumber = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_ProviderRegistryNumber);
                rowSubmitted.OCF18_OtherHealthPractitioner_Occupation = doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_Occupation);
                rowSubmitted.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists =
                    doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictExists).ToBoolNullable();
                rowSubmitted.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails =
                    doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_ConflictOfInterest_ConflictDetails);
                rowSubmitted.OCF18_OtherHealthPractitioner_IsSignatureOnFile =
                    doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_IsSignatureOnFile).ToBoolNullable();
                rowSubmitted.OCF18_OtherHealthPractitioner_DateSigned =
                    doc.GetValue(edOcf18Keys.OCF18_OtherHealthPractitioner_DateSigned).ToHcaiDateNullable();
            }

            rowSubmitted.OCF18_IsRHPSameAsHealthPractitioner = doc.GetValue(edOcf18Keys.OCF18_IsRHPSameAsHealthPractitioner).ToBool();
            rowSubmitted.OCF18_RegulatedHealthProfessional_FacilityRegistryID =
                doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_FacilityRegistryID).ToInt32Nullable();
            rowSubmitted.OCF18_RegulatedHealthProfessional_ProviderRegistryID =
                doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_ProviderRegistryID).ToInt32Nullable();
            rowSubmitted.OCF18_RegulatedHealthProfessional_Occupation = doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_Occupation);
            rowSubmitted.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists =
                doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists).ToBoolNullable();
            rowSubmitted.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails =
                doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails);
            rowSubmitted.OCF18_RegulatedHealthProfessional_IsSignatureOnFile =
                doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_IsSignatureOnFile).ToBoolNullable();
            rowSubmitted.OCF18_RegulatedHealthProfessional_DateSigned =
                doc.GetValue(edOcf18Keys.OCF18_RegulatedHealthProfessional_DateSigned).ToHcaiDateNullable();

            rowSubmitted.OCF18_PriorAndConcurrentConditions_PriorCondition_Response =
                doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Response)
                    .ToEnumIntParse<FlagWithUnknownEnum>();
            rowSubmitted.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation = doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation);

            rowSubmitted.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response =
                doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response)
                    .ToEnumIntParse<FlagWithUnknownEnum>();
            rowSubmitted.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation = doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);

            rowSubmitted.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response =
                doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response)
                    .ToEnumIntParse<FlagWithUnknownEnum>();
            rowSubmitted.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation = doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation);

            rowSubmitted.OCF18_PriorAndConcurrentConditions_IsPAF_Response =
                doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Response)
                    .ToEnumIntParse<FlagWithNotApplicable>();
            rowSubmitted.OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance =
                doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Circumstance);
            rowSubmitted.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation = doc.GetValue(edOcf18Keys.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation);

            rowSubmitted.OCF18_ActivityLimitations_ToEmployment_Response =
                doc.GetValue(edOcf18Keys.OCF18_ActivityLimitations_ToEmployment_Response)
                    .ToEnumIntParse<FlagEmploymentEnum>();
            rowSubmitted.OCF18_ActivityLimitations_ToNormalLife_Response =
                doc.GetValue(edOcf18Keys.OCF18_ActivityLimitations_ToNormalLife_Response)
                    .ToEnumIntParse<FlagWithUnknownEnum>();
            rowSubmitted.OCF18_ActivityLimitations_ImpactOnAbilities = doc.GetValue(edOcf18Keys.OCF18_ActivityLimitations_ImpactOnAbilities);

            rowSubmitted.OCF18_ActivityLimitations_ModifiedEmployment_Response =
                doc.GetValue(edOcf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Response)
                    .ToEnumIntParse<FlagEmploymentEnum>();
            rowSubmitted.OCF18_ActivityLimitations_ModifiedEmployment_Explanation = doc.GetValue(edOcf18Keys.OCF18_ActivityLimitations_ModifiedEmployment_Explanation);

            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription);
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other).ToBoolNullable();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription);
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation);
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact);

            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response).ToBool();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation
                = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation);
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response).ToBool();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation);
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response).ToBool();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation);
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response =
                doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response).ToBool();
            rowSubmitted.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation = doc.GetValue(edOcf18Keys.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation);

            //
            rowSubmitted.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration =
                doc.GetValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration).ToInt32();
            rowSubmitted.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits =
                doc.GetValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits).ToInt32Nullable();
            rowSubmitted.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments =
                doc.GetValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments);
            rowSubmitted.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile =
                doc.GetValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_ApplicantInitialsOnFile).ToBool();

            rowSubmitted.OCF18_InsurerTotals_MOH_Proposed = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Proposed).ToDecimal();
            rowSubmitted.OCF18_InsurerTotals_OtherInsurers_Proposed = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Proposed).ToDecimal();
            rowSubmitted.OCF18_InsurerTotals_AutoInsurerTotal_Proposed = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Proposed).ToDecimal();
            rowSubmitted.OCF18_InsurerTotals_GST_Proposed = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_GST_Proposed).ToDecimal();
            rowSubmitted.OCF18_InsurerTotals_PST_Proposed = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_PST_Proposed).ToDecimal();
            rowSubmitted.OCF18_InsurerTotals_SubTotal_Proposed = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_SubTotal_Proposed).ToDecimal();

            rowSubmitted.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer =
                doc.GetValue(edOcf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer).ToBoolNullable();
            rowSubmitted.OCF18_ApplicantSignature_IsApplicantSignatureOnFile =
                doc.GetValue(edOcf18Keys.OCF18_ApplicantSignature_IsApplicantSignatureOnFile).ToBool();
            rowSubmitted.OCF18_ApplicantSignature_SigningApplicant_FirstName = doc.GetValue(edOcf18Keys.OCF18_ApplicantSignature_SigningApplicant_FirstName);
            rowSubmitted.OCF18_ApplicantSignature_SigningApplicant_LastName = doc.GetValue(edOcf18Keys.OCF18_ApplicantSignature_SigningApplicant_LastName);
            rowSubmitted.OCF18_ApplicantSignature_SigningDate = doc.GetValue(edOcf18Keys.OCF18_ApplicantSignature_SigningDate).ToHcaiDateNullable();

            rowSubmitted.AdditionalComments = doc.GetValue(edOcf18Keys.AdditionalComments);
            rowSubmitted.OCFVersion = doc.GetValue(edOcf18Keys.OCFVersion).ToInt32();

            //OCF18 Session Header - no in xml ((
            //"hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component"
            var shList = doc.getList(LineItemType.DE_GS18SessionHeaderLineItem_Estimated);
            if (shList?.Count > 0)
            {
                var iOrder = 1;
                NewMessageEvent($"  SessionHeaderLineItemSubmit[{shList.Count}]");
                try
                {
                    for (var i = 0; i < shList.Count; i++)
                    {
                        var tmpItem = (IDataItem)shList[i];

                        var itemCode = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code);

                        var key = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                        if (string.IsNullOrEmpty(key))
                            key = $"{iOrder++}";

                        var sessionHeaderRow = rowSubmitted
                                                   .SessionHeaderLineItemSubmits
                                                   .FirstOrDefault(h =>
                                                       h.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey == key
                                                       && h.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code == itemCode)
                                               ?? _dbContext.SessionHeaderLineItemSubmits.Add(
                                                   new SessionHeaderLineItemSubmit
                                                   {
                                                       //OCF18Submit = rowSubmited,
                                                       HCAI_Document_Number = documentNumber,
                                                       OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Code = itemCode,
                                                       OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey = key,
                                                   });

                        sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Quantity =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity)
                                .ToDecimal();
                        sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Measure =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure);
                        sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost)
                                .ToDecimal();
                        sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_Count)
                                .ToInt32();
                        sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_TotalLineCost)
                                .ToDecimal();
                        sessionHeaderRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_ReferenceNumber =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_ReferenceNumber)
                                .ToInt32Nullable();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Unable to get DE_GS18SessionHeaderLineItem_Estimated line; HCAI Number='{documentNumber}': " + ex.Message);
                }
            }

            //OCF18 Session Line Item - no in xml ((
            //"hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='SessionData']]/hl7:component"
            var snList = doc.getList(LineItemType.DE_GS18SessionLineItem_Estimated);
            if (snList?.Count > 0)
            {
                var iOrder = 1;
                NewMessageEvent($"  SessionLineItemSubmit[{snList.Count}]");
                try
                {
                    for (var i = 0; i < snList.Count; i++)
                    {
                        var tmpItem = (IDataItem)snList[i];

                        var itemCode = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code);

                        var key = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey);
                        if (string.IsNullOrEmpty(key))
                            key = $"{iOrder++}";

                        var sessionLineItemSubmit =
                            rowSubmitted.SessionLineItemSubmits
                                .FirstOrDefault(s =>
                                    s.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey == key
                                    && s.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code == itemCode)
                            ?? _dbContext.SessionLineItemSubmits.Add(new SessionLineItemSubmit
                            {
                                //OCF18Submit = rowSubmited,
                                HCAI_Document_Number = documentNumber,
                                OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Code = itemCode,
                                OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_PMSGSKey = key,
                            });

                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Attribute);
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_ProviderRegistryID)
                                .ToInt32();
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_ProviderReference_Occupation);
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Quantity)
                                .ToDecimal();
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Measure);
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_GST)
                                .ToBool();
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_PST)
                                .ToBool();
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Estimated_LineCost)
                                .ToDecimal();
                        sessionLineItemSubmit.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Description =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_SessionData_Items_Item_Description);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(@"Unable to get DE_GS18SessionLineItem_Estimated line: " + ex.Message);
                }
            }

            //GS18LineItemSubmit - ok & good!
            //"hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='NonSessionGoodsAndServices']]/hl7:component"
            var gsList = doc.getList(LineItemType.DE_GS18LineItem_Estimated);
            if (gsList.Count > 0)
                try
                {
                    NewMessageEvent($"  GS18LineItems[{gsList.Count}]");
                    for (var i = 0; i < gsList.Count; i++)
                    {
                        var tmpItem = (IDataItem)gsList[i];
                        var itemCode = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code);

                        var pmsKey = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                        var refNumber = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ReferenceNumber);

                        if (string.IsNullOrEmpty(pmsKey))
                            pmsKey = refNumber;
                        if (string.IsNullOrEmpty(pmsKey))
                            throw new Exception($"No PMS key for {documentNumber}!");

                        var gs18LineItemSubmitRow =
                            rowSubmitted.GS18LineItemSubmit
                                .FirstOrDefault(t =>
                                    t.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey == pmsKey
                                    && t.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code == itemCode)
                            ?? _dbContext.GS18LineItemSubmit.Add(new GS18LineItemSubmit
                            {
                                HCAI_Document_Number = documentNumber,
                                OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code = itemCode,
                                OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey = pmsKey,
                            });

                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ReferenceNumber =
                            refNumber.ToInt32Nullable();

                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute);
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID)
                                .ToInt32();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation);
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity)
                                .ToDecimal();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure);
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST)
                                .ToBool();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST)
                                .ToBool();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost)
                                .ToDecimal();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count)
                                .ToInt32();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost)
                                .ToDecimal();
                        gs18LineItemSubmitRow.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Description =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Description);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(@"Unable to get DE_GS18LineItem_Estimated line:" + ex.Message);
                }

            //OCF18InjuryLineItemSubmit - ok & good!
            //"/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:InjuriesAndSequelae/hcai:Injury"
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList?.Count > 0)
                try
                {
                    NewMessageEvent($"  OCF18InjuryLineItemSubmit[{injList.Count}]");
                    for (var i = 0; i < injList.Count; i++)
                    {
                        var tmpItem = (IDataItem)injList[i];
                        var code = tmpItem.GetValue(edOcf18LisKeys.OCF18_InjuriesAndSequelae_Injury_Code);

                        var injury = rowSubmitted.OCF18InjuryLineItemSubmit
                                 .FirstOrDefault(d => d.OCF18_InjuriesAndSequelae_Injury_Code == code)
                             ?? _dbContext.OCF18InjuryLineItemSubmit.Add(new OCF18InjuryLineItemSubmit
                             {
                                 HCAI_Document_Number = documentNumber,
                                 OCF18_InjuriesAndSequelae_Injury_Code = code,
                             });
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(@"Unable to get DE_GS18SessionHeaderLineItem_Approved line:" + ex.Message);
                }

            #endregion

            _dbContext.SaveChangesWithDetect();

            // Adjuster Response
            ocf18ARrow ??= _dbContext.OCF18AR.Add(new OCF18AR { HCAI_Document_Number = documentNumber });

            #region approved doc fields

            // ***** header *****
            ocf18ARrow.Document_Type = doc.GetValue(edOcf18Keys.Document_Type);
            ocf18ARrow.Document_Status = documentStatus;
            ocf18ARrow.PMSFields_PMSDocumentKey = doc.GetValue(edOcf18Keys.PMSFields_PMSDocumentKey);
            ocf18ARrow.Submission_Date = doc.GetValue(edOcf18Keys.Submission_Date).ToHcaiDateTime();
            ocf18ARrow.Submission_Source = doc.GetValue(edOcf18Keys.Submission_Source);
            ocf18ARrow.Adjuster_Response_Date = doc.GetValue(edOcf18Keys.Adjuster_Response_Date).ToHcaiDateTime();
            ocf18ARrow.In_Dispute = doc.GetValue(edOcf18Keys.In_Dispute).ToBool();
            ocf18ARrow.Attachments_Received_Date = doc.GetValue(edOcf18Keys.Attachments_Received_Date).ToHcaiDateNullable();

            ocf18ARrow.Claimant_First_Name = doc.GetValue(edOcf18Keys.Claimant_First_Name);
            ocf18ARrow.Claimant_Middle_Name = doc.GetValue(edOcf18Keys.Claimant_Middle_Name);
            ocf18ARrow.Claimant_Last_Name = doc.GetValue(edOcf18Keys.Claimant_Last_Name);
            ocf18ARrow.Claimant_Date_Of_Birth = doc.GetValue(edOcf18Keys.Claimant_Date_Of_Birth).ToHcaiDate();
            ocf18ARrow.Claimant_Gender = doc.GetValue(edOcf18Keys.Claimant_Gender).ToGender();
            ocf18ARrow.Claimant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf18Keys.Claimant_Telephone));
            ocf18ARrow.Claimant_Extension = doc.GetValue(edOcf18Keys.Claimant_Extension);
            ocf18ARrow.Claimant_Address1 = doc.GetValue(edOcf18Keys.Claimant_Address1);
            ocf18ARrow.Claimant_Address2 = doc.GetValue(edOcf18Keys.Claimant_Address2);
            ocf18ARrow.Claimant_City = doc.GetValue(edOcf18Keys.Claimant_City);
            ocf18ARrow.Claimant_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf18Keys.Claimant_Province));
            ocf18ARrow.Claimant_Postal_Code = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf18Keys.Claimant_Postal_Code));

            ocf18ARrow.Insurer_IBCInsurerID = doc.GetValue(edOcf18Keys.Insurer_IBCInsurerID);
            ocf18ARrow.Insurer_IBCBranchID = doc.GetValue(edOcf18Keys.Insurer_IBCBranchID);

            ocf18ARrow.Status_Code = doc.GetValue(edOcf18Keys.Status_Code);
            ocf18ARrow.Messages = doc.GetValue(edOcf18Keys.Messages);
            ocf18ARrow.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation = doc.GetValue(edOcf18Keys.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation);

            //
            ocf18ARrow.OCF18_InsurerTotals_AutoInsurerTotal_Approved =
                doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_AutoInsurerTotal_Approved).ToDecimal();
            ocf18ARrow.OCF18_InsurerTotals_GST_Approved_LineCost =
                doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_LineCost).ToDecimal();
            ocf18ARrow.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            ocf18ARrow.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_PST_Approved_LineCost =
                doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_LineCost).ToDecimal();
            ocf18ARrow.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            ocf18ARrow.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost =
                doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost).ToDecimal();
            ocf18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode);
            ocf18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_MOH_Approved_LineCost =
                doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_LineCost).ToDecimal();
            ocf18ARrow.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode);
            ocf18ARrow.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf18ARrow.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            // TO CHECK
            ocf18ARrow.OCF18_InsurerTotals_SubTotal_Approved = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_SubTotal_Approved).ToDecimal();
            ocf18ARrow.OCF18_InsurerTotals_TotalCount_Approved = doc.GetValue(edOcf18Keys.OCF18_InsurerTotals_TotalCount_Approved).ToInt32();
            ocf18ARrow.OCF18_InsurerSignature_ApplicantSignatureWaived = doc.GetValue(edOcf18Keys.OCF18_InsurerSignature_ApplicantSignatureWaived).ToBool();
            ocf18ARrow.SigningAdjuster_FirstName = doc.GetValue(edOcf18Keys.SigningAdjuster_FirstName);
            ocf18ARrow.SigningAdjuster_LastName = doc.GetValue(edOcf18Keys.SigningAdjuster_LastName);
            ocf18ARrow.NameOfAdjusterOnPDF = doc.GetValue(edOcf18Keys.NameOfAdjusterOnPDF);
            ocf18ARrow.Archival_Status = doc.GetValue(edOcf18Keys.Archival_Status);

            //SessionHeaderLineItemAR - no in XML (((
            //"/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='SessionGoodsAndServices']]/hl7:component"
            var aRshList = doc.getList(LineItemType.DE_GS18SessionHeaderLineItem_Approved);
            if (aRshList?.Count > 0)
                try
                {
                    var equalLists = rowSubmitted.SessionHeaderLineItemSubmits.Count == aRshList.Count;
                    var iOrder = 0;
                    NewMessageEvent($"  SessionHeaderLineItemAR[{aRshList.Count}]");
                    for (var i = 0; i < aRshList.Count; i++)
                    {
                        var tmpItem = (IDataItem)aRshList[i];

                        var lineCost = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost)
                            .ToDecimal();

                        ++iOrder;
                        var key = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                        if (string.IsNullOrEmpty(key))
                        {
                            if (equalLists)
                                key = $"{iOrder}";
                            else
                            {
                                try
                                {
                                    var findSubm = rowSubmitted.SessionHeaderLineItemSubmits
                                        .SingleOrDefault(s => s.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_LineCost == lineCost);
                                    if (findSubm != null)
                                        key = findSubm.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Estimated_PMSGSKey;
                                    else
                                        key = $"{iOrder}";
                                }
                                catch
                                {
                                    key = $"{iOrder}";
                                }
                            }
                        }

                        var sessionHeaderLineItemArRow = ocf18ARrow.SessionHeaderLineItemARs
                             .FirstOrDefault(r =>
                                 r.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey == key)
                         ?? _dbContext.SessionHeaderLineItemARs.Add(
                             new SessionHeaderLineItemAR
                             {
                                 HCAI_Document_Number = documentNumber,
                                 OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_PMSGSKey = key,
                             });

                        sessionHeaderLineItemArRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_LineCost = lineCost;

                        sessionHeaderLineItemArRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_Count)
                                .ToInt32();
                        sessionHeaderLineItemArRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_TotalLineCost)
                                .ToDecimal();
                        sessionHeaderLineItemArRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                        sessionHeaderLineItemArRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                        sessionHeaderLineItemArRow.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_SessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(@"Unable to get DE_GS18SessionHeaderLineItem_Approved line:" + ex.Message);
                }

            //GS18LineItemAR - ok!
            //"/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ProposedGoodsAndServices']]/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='NonSessionGoodsAndServices']]/hl7:component"
            var arList = doc.getList(LineItemType.DE_GS18LineItem_Approved);
            if (arList.Count > 0)
                try
                {
                    if (arList.Count != gsList.Count) NewMessageEvent($"  GS18LineItemAR[{arList.Count}] !!!");

                    for (var i = 0; i < arList.Count; i++)
                    {
                        var tmpItem = (IDataItem)arList[i];
                        var lineCost = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost)
                            .ToDecimal();

                        var key = tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey);
                        if (string.IsNullOrEmpty(key))
                        {
                            var submItem = (IDataItem)gsList[i];
                            var pmsKey = submItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                            var refNumber = submItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ReferenceNumber);
                            key = !string.IsNullOrEmpty(pmsKey) ? pmsKey : refNumber;
                        }

                        var gs18LineItemAr = ocf18ARrow.GS18LineItemAR
                            .FirstOrDefault(r => r.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey == key)
                            ?? _dbContext.GS18LineItemAR.Add(new GS18LineItemAR
                            {
                                HCAI_Document_Number = documentNumber,
                                OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey = key,
                            });

                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost = lineCost;

                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST).ToBool();
                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST).ToBool();

                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count)
                                .ToInt32();
                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost)
                                .ToDecimal();

                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                        gs18LineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                            tmpItem.GetValue(edOcf18LisKeys.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(@"Unable to get DE_GS18LineItem_Approved line:" + ex.Message);
                }

            #endregion

            _dbContext.SaveChangesWithDetect();
            return true;
        }
    }
}
