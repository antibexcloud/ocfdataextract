﻿using System;
using System.Linq;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractOcf21B(IDataItem doc)
        {
            var edOcf21BKeys = new PMSToolkit.DataObjects.OCF21BDataExtractResponseKeys();
            var edOcf21BLisKeys = new PMSToolkit.DataObjects.OCF21BDataExtractResponseLineItemKeys();

            var documentNumber = doc.GetValue(edOcf21BKeys.HCAI_Document_Number);
            if (string.IsNullOrEmpty(documentNumber))
            {
                NewMessageEvent("Extracting doc with type 'OCF21B' - NO number!");
                return false;
            }

            var documentStatus = doc.GetValue(edOcf21BKeys.Document_Status);
            var ocf21Bar = _dbContext.OCF21BAR
                .Include("GS21BLineItemAR")
                .FirstOrDefault(ar => ar.HCAI_Document_Number == documentNumber);

            var rowSubmitted = _dbContext.OCF21BSubmit
                .Include("GS21BLineItemSubmit").Include("OCF21BInjuryLineItemSubmit")
                .FirstOrDefault(d => d.HCAI_Document_Number == documentNumber);
            var isExists = rowSubmitted != null;

            var docApproved = ocf21Bar?.OCF21B_InsurerTotals_SubTotal_Approved ?? 0;
            var itemsApproved = ocf21Bar?.GS21BLineItemAR
                .Select(t => t.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost)
                .Sum() ?? 0;

            if (!isExists)
            {
                NewMessageEvent($"Extracting doc with type 'OCF21B' & number = '{documentNumber}' ");
            }
            else if (documentStatus == ocf21Bar?.Document_Status && Math.Abs(docApproved - itemsApproved) > 0.01m)
            {
                return false;
            }
            else
            {
                NewMessageEvent($"Updating doc with type 'OCF21B' & number = '{documentNumber}' from status '{ocf21Bar?.Document_Status}' to status '{documentStatus}'");
            }

            rowSubmitted ??= _dbContext.OCF21BSubmit.Add(new OCF21BSubmit { HCAI_Document_Number = documentNumber, });

            #region sumbit doc fields

            // **** header ****
            rowSubmitted.PMSFields_PMSSoftware = doc.GetValue(edOcf21BKeys.PMSFields_PMSSoftware);
            rowSubmitted.PMSFields_PMSVersion = doc.GetValue(edOcf21BKeys.PMSFields_PMSVersion);
            rowSubmitted.PMSFields_PMSDocumentKey = doc.GetValue(edOcf21BKeys.PMSFields_PMSDocumentKey);
            rowSubmitted.PMSFields_PMSPatientKey = doc.GetValue(edOcf21BKeys.PMSFields_PMSPatientKey);

            rowSubmitted.AttachmentsBeingSent = doc.GetValue(edOcf21BKeys.AttachmentsBeingSent).ToBool();
            rowSubmitted.Header_ClaimNumber = doc.GetValue(edOcf21BKeys.Header_ClaimNumber);
            rowSubmitted.Header_PolicyNumber = doc.GetValue(edOcf21BKeys.Header_PolicyNumber);
            rowSubmitted.Header_DateOfAccident = doc.GetValue(edOcf21BKeys.Header_DateOfAccident).ToHcaiDate();

            rowSubmitted.Applicant_Name_FirstName = doc.GetValue(edOcf21BKeys.Applicant_Name_FirstName);
            rowSubmitted.Applicant_Name_MiddleName = doc.GetValue(edOcf21BKeys.Applicant_Name_MiddleName);
            rowSubmitted.Applicant_Name_LastName = doc.GetValue(edOcf21BKeys.Applicant_Name_LastName);
            rowSubmitted.Applicant_DateOfBirth = doc.GetValue(edOcf21BKeys.Applicant_DateOfBirth).ToHcaiDate();
            rowSubmitted.Applicant_Gender = doc.GetValue(edOcf21BKeys.Applicant_Gender).ToGender();
            rowSubmitted.Applicant_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21BKeys.Applicant_TelephoneNumber));
            rowSubmitted.Applicant_TelephoneExtension = doc.GetValue(edOcf21BKeys.Applicant_TelephoneExtension);
            rowSubmitted.Applicant_Address_StreetAddress1 = doc.GetValue(edOcf21BKeys.Applicant_Address_StreetAddress1);
            rowSubmitted.Applicant_Address_StreetAddress2 = doc.GetValue(edOcf21BKeys.Applicant_Address_StreetAddress2);
            rowSubmitted.Applicant_Address_City = doc.GetValue(edOcf21BKeys.Applicant_Address_City);
            rowSubmitted.Applicant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf21BKeys.Applicant_Address_Province));
            rowSubmitted.Applicant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf21BKeys.Applicant_Address_PostalCode));

            rowSubmitted.Insurer_IBCInsurerID = doc.GetValue(edOcf21BKeys.Insurer_IBCInsurerID);
            rowSubmitted.Insurer_IBCBranchID = doc.GetValue(edOcf21BKeys.Insurer_IBCBranchID);
            rowSubmitted.Insurer_Adjuster_Name_FirstName = doc.GetValue(edOcf21BKeys.Insurer_Adjuster_Name_FirstName);
            rowSubmitted.Insurer_Adjuster_Name_LastName = doc.GetValue(edOcf21BKeys.Insurer_Adjuster_Name_LastName);
            rowSubmitted.Insurer_Adjuster_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21BKeys.Insurer_Adjuster_TelephoneNumber));
            rowSubmitted.Insurer_Adjuster_TelephoneExtension = doc.GetValue(edOcf21BKeys.Insurer_Adjuster_TelephoneExtension);
            rowSubmitted.Insurer_Adjuster_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21BKeys.Insurer_Adjuster_FaxNumber));
            rowSubmitted.Insurer_PolicyHolder_IsSameAsApplicant = doc.GetValue(edOcf21BKeys.Insurer_PolicyHolder_IsSameAsApplicant).ToBool();
            rowSubmitted.Insurer_PolicyHolder_Name_FirstName = doc.GetValue(edOcf21BKeys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmitted.Insurer_PolicyHolder_Name_LastName = doc.GetValue(edOcf21BKeys.Insurer_PolicyHolder_Name_LastName);

            rowSubmitted.OCF21B_InvoiceInformation_InvoiceNumber = doc.GetValue(edOcf21BKeys.OCF21B_InvoiceInformation_InvoiceNumber);
            rowSubmitted.OCF21B_InvoiceInformation_FirstInvoice = doc.GetValue(edOcf21BKeys.OCF21B_InvoiceInformation_FirstInvoice).ToBool();
            rowSubmitted.OCF21B_InvoiceInformation_LastInvoice = doc.GetValue(edOcf21BKeys.OCF21B_InvoiceInformation_LastInvoice).ToBool();

            rowSubmitted.OCF21B_PreviouslyApprovedGoodsAndServices_Type = doc.GetValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_Type);
            rowSubmitted.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate =
                doc.GetValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanDate).ToHcaiDateNullable();
            rowSubmitted.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber = doc.GetValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber);
            rowSubmitted.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved =
                doc.GetValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_AmountApproved).ToDecimalNullable();
            rowSubmitted.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled =
                doc.GetValue(edOcf21BKeys.OCF21B_PreviouslyApprovedGoodsAndServices_PreviouslyBilled).ToDecimalNullable();

            rowSubmitted.OCF21B_Payee_FacilityRegistryID = doc.GetValue(edOcf21BKeys.OCF21B_Payee_FacilityRegistryID).ToInt32();
            rowSubmitted.OCF21B_Payee_FacilityIsPayee = doc.GetValue(edOcf21BKeys.OCF21B_Payee_FacilityIsPayee).ToBool();
            rowSubmitted.OCF21B_Payee_MakeChequePayableTo = doc.GetValue(edOcf21BKeys.OCF21B_Payee_MakeChequePayableTo);

            rowSubmitted.OCF21B_Payee_ConflictOfInterests_ConflictExists =
                doc.GetValue(edOcf21BKeys.OCF21B_Payee_ConflictOfInterests_ConflictExists).ToBoolNullable();
            rowSubmitted.OCF21B_Payee_ConflictOfInterests_ConflictDetails = doc.GetValue(edOcf21BKeys.OCF21B_Payee_ConflictOfInterests_ConflictDetails);

            rowSubmitted.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_IsThereOtherInsuranceCoverage).ToBool();

            rowSubmitted.OCF21B_OtherInsurance_MOHAvailable =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_MOHAvailable).ToEnumIntParseNullable<FlagWithNotApplicable>();

            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);

            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmitted.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);

            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOH_Chiropractic =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOH_OtherService =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21B_OtherInsuranceAmounts_OtherServiceType = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_OtherServiceType);

            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21B_OtherInsuranceAmounts_IsAmountRefused =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_IsAmountRefused).ToBoolNullable();
            rowSubmitted.OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Debits_OtherServiceType);

            rowSubmitted.OCF21B_AccountActivity_PriorBalance =
                doc.GetValue(edOcf21BKeys.OCF21B_AccountActivity_PriorBalance).ToDecimalNullable();
            rowSubmitted.OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer =
                doc.GetValue(edOcf21BKeys.OCF21B_AccountActivity_PaymentReceivedFromAutoInsurer).ToDecimalNullable();
            rowSubmitted.OCF21B_AccountActivity_OverdueAmount =
                doc.GetValue(edOcf21BKeys.OCF21B_AccountActivity_OverdueAmount).ToDecimalNullable();

            rowSubmitted.OCF21B_InsurerTotals_OtherInsurers_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_OtherInsurers_Proposed).ToDecimal();
            rowSubmitted.OCF21B_InsurerTotals_MOH_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_MOH_Proposed).ToDecimal();
            rowSubmitted.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Proposed).ToDecimal();
            rowSubmitted.OCF21B_InsurerTotals_GST_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Proposed).ToDecimal();
            rowSubmitted.OCF21B_InsurerTotals_PST_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Proposed).ToDecimal();
            rowSubmitted.OCF21B_InsurerTotals_SubTotal_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_SubTotal_Proposed).ToDecimal();
            rowSubmitted.OCF21B_InsurerTotals_Interest_Proposed = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Proposed).ToDecimal();

            rowSubmitted.OCF21B_OtherInformation = doc.GetValue(edOcf21BKeys.OCF21B_OtherInformation);
            rowSubmitted.AdditionalComments = doc.GetValue(edOcf21BKeys.AdditionalComments);
            rowSubmitted.OCFVersion = doc.GetValue(edOcf21BKeys.OCFVersion).ToInt32();

            //GS21BLineItemSubmit - ok & good!
            //"hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:component"
            var gsList = doc.getList(LineItemType.DE_GS21BLineItem_Estimated);
            if (gsList.Count > 0)
            {
                NewMessageEvent($"  GS21BLineItems[{gsList.Count}]");
                for (var i = 0; i < gsList.Count; i++)
                {
                    var tmpItem = (IDataItem)gsList[i];
                    var itemCode = tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code);

                    var key = tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    var referenceNumber = tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber);

                    if (string.IsNullOrEmpty(key))
                        key = referenceNumber;
                    if (string.IsNullOrEmpty(key))
                        throw new Exception($"No PMS key for {documentNumber}!");

                    var gs21BLineItemSubmitRow = rowSubmitted.GS21BLineItemSubmit
                        .FirstOrDefault(t => t.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey == key
                            && t.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code == itemCode)
                        ?? _dbContext.GS21BLineItemSubmit.Add(new GS21BLineItemSubmit
                    {
                        HCAI_Document_Number = documentNumber,
                        OCF21B_ReimbursableGoodsAndServices_Items_Item_Code = itemCode,
                        OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey = key,
                    });

                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/hl7:qualifier/@code"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute);
                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:performer/hl7:healthCareProvider/hl7:id/@extension"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID)
                            .ToInt32();
                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:performer/hl7:healthCareProvider/hl7:code/@code"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    //"hl7:invoiceElementDetail/hl7:unitQuantity/hl7:numerator/@value"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity).ToDecimal();
                    //"hl7:invoiceElementDetail/hl7:unitQuantity/hl7:numerator/@unit"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure);
                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:effectiveTime/@value"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_DateOfService)
                            .ToHcaiDate();

                    //"hl7:invoiceElementDetail/hl7:fstInd/@value"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST).ToBool();
                    //"hl7:invoiceElementDetail/hl7:pstInd/@value"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST).ToBool();
                    //"hl7:invoiceElementDetail/hl7:unitPriceAmt/hl7:numerator/@value"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                            .ToDecimal();
                    //"hl7:sequenceNumber/@value"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber =
                        referenceNumber.ToInt32Nullable();
                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/@displayName"
                    gs21BLineItemSubmitRow.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description);
                }
            }

            //OCF21BInjuryLineItemSubmit - ok & good!
            //"/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:InjuriesAndSequelae/hcai:Injury"
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList?.Count > 0)
            {
                NewMessageEvent($"  OCF21BInjuryLineItemSubmit[{injList.Count}]");
                for (var i = 0; i < injList.Count; i++)
                {
                    var tmpItem = (IDataItem)injList[i];
                    var code = tmpItem.GetValue(edOcf21BLisKeys.OCF21B_InjuriesAndSequelae_Injury_Code);

                    var injury = rowSubmitted.OCF21BInjuryLineItemSubmit
                        .FirstOrDefault(c => c.OCF21B_InjuriesAndSequelae_Injury_Code == code)
                        ?? _dbContext.OCF21BInjuryLineItemSubmit.Add(new OCF21BInjuryLineItemSubmit
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF21B_InjuriesAndSequelae_Injury_Code = code,
                        });
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            // Adjuster Response
            ocf21Bar ??= _dbContext.OCF21BAR.Add(new OCF21BAR { HCAI_Document_Number = documentNumber });
            
            #region approved doc fields

            // header
            ocf21Bar.PMSFields_PMSDocumentKey = doc.GetValue(edOcf21BKeys.PMSFields_PMSDocumentKey);
            ocf21Bar.Submission_Date = doc.GetValue(edOcf21BKeys.Submission_Date).ToHcaiDateTime();
            ocf21Bar.Submission_Source = doc.GetValue(edOcf21BKeys.Submission_Source);
            ocf21Bar.Adjuster_Response_Date = doc.GetValue(edOcf21BKeys.Adjuster_Response_Date).ToHcaiDateTime();
            ocf21Bar.In_Dispute = doc.GetValue(edOcf21BKeys.In_Dispute).ToBool();
            ocf21Bar.Document_Type = doc.GetValue(edOcf21BKeys.Document_Type);
            ocf21Bar.Document_Status = documentStatus;
            ocf21Bar.Attachments_Received_Date = doc.GetValue(edOcf21BKeys.Attachments_Received_Date).ToHcaiDateNullable();

            ocf21Bar.Claimant_First_Name = doc.GetValue(edOcf21BKeys.Claimant_First_Name);
            ocf21Bar.Claimant_Middle_Name = doc.GetValue(edOcf21BKeys.Claimant_Middle_Name);
            ocf21Bar.Claimant_Last_Name = doc.GetValue(edOcf21BKeys.Claimant_Last_Name);
            ocf21Bar.Claimant_Date_Of_Birth = doc.GetValue(edOcf21BKeys.Claimant_Date_Of_Birth).ToHcaiDate();
            ocf21Bar.Claimant_Gender = doc.GetValue(edOcf21BKeys.Claimant_Gender).ToGender();
            ocf21Bar.Claimant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21BKeys.Claimant_Telephone));
            ocf21Bar.Claimant_Extension = doc.GetValue(edOcf21BKeys.Claimant_Extension);
            ocf21Bar.Claimant_Address1 = doc.GetValue(edOcf21BKeys.Claimant_Address1);
            ocf21Bar.Claimant_Address2 = doc.GetValue(edOcf21BKeys.Claimant_Address2);
            ocf21Bar.Claimant_City = doc.GetValue(edOcf21BKeys.Claimant_City);
            ocf21Bar.Claimant_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf21BKeys.Claimant_Province));
            ocf21Bar.Claimant_Postal_Code = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf21BKeys.Claimant_Postal_Code));

            ocf21Bar.Insurer_IBCInsurerID = doc.GetValue(edOcf21BKeys.Insurer_IBCInsurerID);
            ocf21Bar.Insurer_IBCBranchID = doc.GetValue(edOcf21BKeys.Insurer_IBCBranchID);
            ocf21Bar.Status_Code = doc.GetValue(edOcf21BKeys.Status_Code);
            ocf21Bar.Messages = doc.GetValue(edOcf21BKeys.Messages);

            //
            ocf21Bar.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation = doc.GetValue(edOcf21BKeys.OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation);

            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost =
                doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation = doc.GetValue(edOcf21BKeys.OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation);
            ocf21Bar.OCF21B_InsurerSignature_ClaimFormReceived = doc.GetValue(edOcf21BKeys.OCF21B_InsurerSignature_ClaimFormReceived).ToBoolNullable();
            ocf21Bar.OCF21B_InsurerSignature_ClaimFormReceivedDate = doc.GetValue(edOcf21BKeys.OCF21B_InsurerSignature_ClaimFormReceivedDate).ToHcaiDateNullable();

            ocf21Bar.OCF21B_InsurerTotals_MOH_Approved = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_MOH_Approved).ToDecimal();
            ocf21Bar.OCF21B_InsurerTotals_OtherInsurers_Approved = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_OtherInsurers_Approved).ToDecimal();
            ocf21Bar.OCF21B_InsurerTotals_AutoInsurerTotal_Approved = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_AutoInsurerTotal_Approved).ToDecimal();

            ocf21Bar.OCF21B_InsurerTotals_GST_Approved_LineCost = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_InsurerTotals_PST_Approved_LineCost = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.OCF21B_InsurerTotals_SubTotal_Approved = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_SubTotal_Approved).ToDecimal();
            ocf21Bar.OCF21B_InsurerTotals_Interest_Approved_LineCost = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_LineCost).ToDecimal();
            ocf21Bar.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Bar.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Bar.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21BKeys.OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Bar.SigningAdjuster_FirstName = doc.GetValue(edOcf21BKeys.SigningAdjuster_FirstName);
            ocf21Bar.SigningAdjuster_LastName = doc.GetValue(edOcf21BKeys.SigningAdjuster_LastName);
            ocf21Bar.ApprovedByOnPDF = doc.GetValue(edOcf21BKeys.ApprovedByOnPDF);
            ocf21Bar.Archival_Status = doc.GetValue(edOcf21BKeys.Archival_Status);

            //GS21BLineItemAR - bad: PMSGSKey may be empty (((
            //"/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='ReimbursableGoodsAndServices']]/hl7:component"
            var arList = doc.getList(LineItemType.DE_GS21BLineItem_Approved);
            if (arList.Count > 0)
            {
                if (arList.Count != gsList.Count) NewMessageEvent($"  GS21BLineItemAR[{arList.Count}] !!!");
                for (var i = 0; i < arList.Count; i++)
                {
                    var tmpItem = (IDataItem)arList[i];

                    var key = tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                    {
                        var submItem = (IDataItem)gsList[i];
                        var pmsKey = submItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                        var refNumber = submItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber);
                        key = !string.IsNullOrEmpty(pmsKey) ? pmsKey : refNumber;
                    }

                    var gs21BLineItemAr = ocf21Bar.GS21BLineItemAR
                        .FirstOrDefault(t => t.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == key)
                        ?? _dbContext.GS21BLineItemAR.Add(new GS21BLineItemAR
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey = key,
                        });

                    gs21BLineItemAr.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost).ToDecimal();

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:fstInd/@value"
                    gs21BLineItemAr.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_GST).ToBool();
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:pstInd/@value"
                    gs21BLineItemAr.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PST).ToBool();
                    
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value"
                    gs21BLineItemAr.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value"
                    gs21BLineItemAr.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value"
                    gs21BLineItemAr.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        tmpItem.GetValue(edOcf21BLisKeys.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            return true;
        }
    }
}
