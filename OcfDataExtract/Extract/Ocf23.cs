﻿using System;
using System.Linq;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractOcf23(IDataItem doc)
        {
            var edOcf23Keys = new PMSToolkit.DataObjects.OCF23DataExtractResponseKeys();
            var edOcf23LisKeys = new PMSToolkit.DataObjects.OCF23DataExtractResponseLineItemKeys();

            var documentNumber = doc.GetValue(edOcf23Keys.HCAI_Document_Number);
            if (string.IsNullOrEmpty(documentNumber))
            {
                NewMessageEvent("Extracting doc with type 'OCF23' - NO number!");
                return false;
            }

            var documentStatus = doc.GetValue(edOcf23Keys.Document_Status);
            var ocf23ARrow = _dbContext.OCF23AR.Include("OtherGSLineItemARs")
                .FirstOrDefault(d => d.HCAI_Document_Number == documentNumber);

            var rowSubmitted = _dbContext.OCF23Submit
                .Include("OtherGSLineItemSubmits").Include("PAFLineItemSubmits").Include("OCF23InjuryLineItemSubmit")
                .FirstOrDefault(d => d.HCAI_Document_Number == documentNumber);
            var isExists = rowSubmitted != null;

            var docApproved = ocf23ARrow?.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved ?? 0;
            var itemsApproved = ocf23ARrow?.OtherGSLineItemARs
                .Select(t => t.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost)
                .Sum() ?? 0;

            if (!isExists)
            {
                NewMessageEvent($"Extracting doc with type 'OCF23' & number = '{documentNumber}' ");
            }
            else if (documentStatus == ocf23ARrow?.Document_Status && Math.Abs(docApproved - itemsApproved) > 0.01m)
            {
                return false;
            }
            else
            {
                NewMessageEvent($"Updating doc with type 'OCF23' & number = '{documentNumber}' from status '{ocf23ARrow?.Document_Status}' to status '{documentStatus}'");
            }

            rowSubmitted ??= _dbContext.OCF23Submit.Add(new OCF23Submit { HCAI_Document_Number = documentNumber });

            #region sumbit doc fields

            // ***** header *****
            rowSubmitted.PMSFields_PMSSoftware = doc.GetValue(edOcf23Keys.PMSFields_PMSSoftware);
            rowSubmitted.PMSFields_PMSVersion = doc.GetValue(edOcf23Keys.PMSFields_PMSVersion);
            rowSubmitted.PMSFields_PMSDocumentKey = doc.GetValue(edOcf23Keys.PMSFields_PMSDocumentKey);
            rowSubmitted.PMSFields_PMSPatientKey = doc.GetValue(edOcf23Keys.PMSFields_PMSPatientKey);
            rowSubmitted.AttachmentsBeingSent = doc.GetValue(edOcf23Keys.AttachmentsBeingSent).ToBool();
            rowSubmitted.Header_ClaimNumber = doc.GetValue(edOcf23Keys.Header_ClaimNumber);
            rowSubmitted.Header_PolicyNumber = doc.GetValue(edOcf23Keys.Header_PolicyNumber);
            rowSubmitted.Header_DateOfAccident = doc.GetValue(edOcf23Keys.Header_DateOfAccident).ToHcaiDate();
            // Applicant
            rowSubmitted.Applicant_Name_FirstName = doc.GetValue(edOcf23Keys.Applicant_Name_FirstName);
            rowSubmitted.Applicant_Name_MiddleName = doc.GetValue(edOcf23Keys.Applicant_Name_MiddleName);
            rowSubmitted.Applicant_Name_LastName = doc.GetValue(edOcf23Keys.Applicant_Name_LastName);
            rowSubmitted.Applicant_DateOfBirth = doc.GetValue(edOcf23Keys.Applicant_DateOfBirth).ToHcaiDate();
            rowSubmitted.Applicant_Gender = doc.GetValue(edOcf23Keys.Applicant_Gender).ToGender();
            rowSubmitted.Applicant_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf23Keys.Applicant_TelephoneNumber));
            rowSubmitted.Applicant_TelephoneExtension = doc.GetValue(edOcf23Keys.Applicant_TelephoneExtension);
            rowSubmitted.Applicant_Address_StreetAddress1 = doc.GetValue(edOcf23Keys.Applicant_Address_StreetAddress1);
            rowSubmitted.Applicant_Address_StreetAddress2 = doc.GetValue(edOcf23Keys.Applicant_Address_StreetAddress2);
            rowSubmitted.Applicant_Address_City = doc.GetValue(edOcf23Keys.Applicant_Address_City);
            rowSubmitted.Applicant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf23Keys.Applicant_Address_Province));
            rowSubmitted.Applicant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf23Keys.Applicant_Address_PostalCode));
            // Insurer
            rowSubmitted.Insurer_IBCInsurerID = doc.GetValue(edOcf23Keys.Insurer_IBCInsurerID);
            rowSubmitted.Insurer_IBCBranchID = doc.GetValue(edOcf23Keys.Insurer_IBCBranchID);
            rowSubmitted.Insurer_Adjuster_Name_FirstName = doc.GetValue(edOcf23Keys.Insurer_Adjuster_Name_FirstName);
            rowSubmitted.Insurer_Adjuster_Name_LastName = doc.GetValue(edOcf23Keys.Insurer_Adjuster_Name_LastName);
            rowSubmitted.Insurer_Adjuster_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf23Keys.Insurer_Adjuster_TelephoneNumber));
            rowSubmitted.Insurer_Adjuster_TelephoneExtension = doc.GetValue(edOcf23Keys.Insurer_Adjuster_TelephoneExtension);
            rowSubmitted.Insurer_Adjuster_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf23Keys.Insurer_Adjuster_FaxNumber));
            rowSubmitted.Insurer_PolicyHolder_IsSameAsApplicant = doc.GetValue(edOcf23Keys.Insurer_PolicyHolder_IsSameAsApplicant).ToBool();
            rowSubmitted.Insurer_PolicyHolder_Name_FirstName = doc.GetValue(edOcf23Keys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmitted.Insurer_PolicyHolder_Name_LastName = doc.GetValue(edOcf23Keys.Insurer_PolicyHolder_Name_LastName);

            // OtherInsurance
            rowSubmitted.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage =
                doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage)
                    .ToEnumIntParseNullable<FlagWithNotApplicable>();
            rowSubmitted.OCF23_OtherInsurance_MOHAvailable = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_MOHAvailable)
                .ToEnumIntParseNullable<FlagWithNotApplicable>();

            rowSubmitted.OCF23_DirectPaymentAssignment_Response = doc.GetValue(edOcf23Keys.OCF23_DirectPaymentAssignment_Response).ToBool();

            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName);

            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName);
            rowSubmitted.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName = doc.GetValue(edOcf23Keys.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName);

            // HealthPractitioner
            rowSubmitted.OCF23_HealthPractitioner_FacilityRegistryID = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_FacilityRegistryID).ToInt32();
            rowSubmitted.OCF23_HealthPractitioner_ProviderRegistryID = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_ProviderRegistryID).ToInt32();
            rowSubmitted.OCF23_HealthPractitioner_Occupation = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_Occupation);
            rowSubmitted.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists).ToBool();
            rowSubmitted.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails);
            rowSubmitted.OCF23_HealthPractitioner_IsSignatureOnFile = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_IsSignatureOnFile).ToBool();
            rowSubmitted.OCF23_HealthPractitioner_DateSigned = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_DateSigned).ToHcaiDate();
            rowSubmitted.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner).ToBool();

            rowSubmitted.OCF23_HealthPractitioner_ProviderFirstName = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_ProviderFirstName);
            rowSubmitted.OCF23_HealthPractitioner_ProviderLastName = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_ProviderLastName);
            rowSubmitted.OCF23_HealthPractitioner_FacilityName = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_FacilityName);
            rowSubmitted.OCF23_HealthPractitioner_AISIFacilityNumber = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_AISIFacilityNumber);
            rowSubmitted.OCF23_HealthPractitioner_Address1 = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_Address1);
            rowSubmitted.OCF23_HealthPractitioner_Address2 = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_Address2);
            rowSubmitted.OCF23_HealthPractitioner_City = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_City);
            rowSubmitted.OCF23_HealthPractitioner_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_Province));
            rowSubmitted.OCF23_HealthPractitioner_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_PostalCode));
            rowSubmitted.OCF23_HealthPractitioner_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_TelephoneNumber));
            rowSubmitted.OCF23_HealthPractitioner_TelephoneExtension = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_TelephoneExtension);
            rowSubmitted.OCF23_HealthPractitioner_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_FaxNumber));
            rowSubmitted.OCF23_HealthPractitioner_Email = doc.GetValue(edOcf23Keys.OCF23_HealthPractitioner_Email);

            // PriorAndConcurrentConditions
            rowSubmitted.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident = doc.GetValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident).ToBool();
            rowSubmitted.OCF23_PriorAndConcurrentConditions_PriorCondition_Response =
                doc.GetValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Response)
                    .ToEnumIntParse<FlagWithUnknownEnum>();
            rowSubmitted.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation = doc.GetValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation);
            rowSubmitted.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response =
                doc.GetValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response)
                    .ToEnumIntParseNullable<FlagWithUnknownEnum>();
            rowSubmitted.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation = doc.GetValue(edOcf23Keys.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation);

            rowSubmitted.OCF23_BarriersToRecovery_Response = doc.GetValue(edOcf23Keys.OCF23_BarriersToRecovery_Response).ToBool();
            rowSubmitted.OCF23_BarriersToRecovery_Explanation = doc.GetValue(edOcf23Keys.OCF23_BarriersToRecovery_Explanation);
            rowSubmitted.OCF23_PAFPreApproveServices_PAF_PAFType = doc.GetValue(edOcf23Keys.OCF23_PAFPreApproveServices_PAF_PAFType);
            rowSubmitted.OCF23_PAFPreApproveServices_PAF_EstimatedFee =
                doc.GetValue(edOcf23Keys.OCF23_PAFPreApproveServices_PAF_EstimatedFee).ToDecimal();
            rowSubmitted.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description = doc.GetValue(edOcf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description);
            rowSubmitted.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee =
                doc.GetValue(edOcf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee).ToDecimalNullable();
            rowSubmitted.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey = doc.GetValue(edOcf23Keys.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_PMSGSKey);
            rowSubmitted.OCF23_InsurerTotals_AutoInsurerTotal_Proposed =
                doc.GetValue(edOcf23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Proposed).ToDecimal();
            rowSubmitted.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed =
                doc.GetValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed).ToDecimal();
            rowSubmitted.OCF23_InsurerTotals_SubTotalPreApproved_Proposed =
                doc.GetValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Proposed).ToDecimal();
            rowSubmitted.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer =
                doc.GetValue(edOcf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer).ToBool();
            rowSubmitted.OCF23_ApplicantSignature_IsApplicantSignatureOnFile = doc.GetValue(edOcf23Keys.OCF23_ApplicantSignature_IsApplicantSignatureOnFile).ToBool();
            rowSubmitted.OCF23_ApplicantSignature_SigningApplicant_FirstName = doc.GetValue(edOcf23Keys.OCF23_ApplicantSignature_SigningApplicant_FirstName);
            rowSubmitted.OCF23_ApplicantSignature_SigningApplicant_LastName = doc.GetValue(edOcf23Keys.OCF23_ApplicantSignature_SigningApplicant_LastName);
            rowSubmitted.OCF23_ApplicantSignature_SigningDate = doc.GetValue(edOcf23Keys.OCF23_ApplicantSignature_SigningDate).ToHcaiDateNullable();
            rowSubmitted.OCF23_OtherGoodsAndServices_GoodsAndServicesComments = doc.GetValue(edOcf23Keys.OCF23_OtherGoodsAndServices_GoodsAndServicesComments);
            rowSubmitted.AdditionalComments = doc.GetValue(edOcf23Keys.AdditionalComments);
            rowSubmitted.OCFVersion = doc.GetValue(edOcf23Keys.OCFVersion).ToInt32();

            //OtherGSLineItemSubmit - ok & good!
            //"/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:component"
            var oList = doc.getList(LineItemType.DE_GS23OtherGSLineItem_Estimated);
            if (oList.Count > 0)
            {
                NewMessageEvent($"  OtherGSLineItemEstimated[{oList.Count}]");
                for (var i = 0; i < oList.Count; i++)
                {
                    var tmpItem = (IDataItem)oList[i];

                    var code = tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Code);

                    var key = tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    var refNumber = tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber);
                    if (string.IsNullOrEmpty(key))
                        key = refNumber;
                    if (string.IsNullOrEmpty(key))
                        throw new Exception($"No PMS key for {documentNumber}!");

                    var otherGsLineItemSubmitRow = rowSubmitted.OtherGSLineItemSubmits
                        .FirstOrDefault(t => t.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey == key
                            && t.OCF23_OtherGoodsAndServices_Items_Item_Code == code)
                        ?? _dbContext.OtherGSLineItemSubmits.Add(new OtherGSLineItemSubmit
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF23_OtherGoodsAndServices_Items_Item_Code = code,
                            OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey = key,
                        });

                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_ReferenceNumber = refNumber.ToInt32Nullable();
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Attribute =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Attribute);
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID).ToInt32();
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Quantity =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Quantity).ToDecimal();
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Measure =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Measure);
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost).ToDecimal();
                    otherGsLineItemSubmitRow.OCF23_OtherGoodsAndServices_Items_Item_Description =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Description);
                }
            }

            //PAFLineItemSubmit - ok & good!
            //"/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:OCF23/hcai:PAFPreApproveServices/hcai:OtherPreApprovedServices/hcai:Item"
            var pList = doc.getList(LineItemType.DE_GS23PAFLineItem);
            if (pList.Count > 0)
            {
                NewMessageEvent($"  PAFLineItemSubmit[{pList.Count}]");
                for (var i = 0; i < pList.Count; i++)
                {
                    var tmpItem = (IDataItem)pList[i];
                    var code = tmpItem.GetValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code);
                    var description = tmpItem.GetValue("hcai:Description");

                    var key = tmpItem.GetValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var pafLineItemSubmitRow = rowSubmitted.PAFLineItemSubmits
                        .FirstOrDefault(t => t.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey == key
                            && t.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code == code)
                        ?? _dbContext.PAFLineItemSubmits.Add(new PAFLineItemSubmit
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code = code,
                            OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey = key,
                        });

                    pafLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute);
                    pafLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee)
                            .ToDecimal();
                    pafLineItemSubmitRow.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Description = description;
                }
            }

            //OCF23InjuryLineItemSubmit - ok & good!
            //"/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:InjuriesAndSequelae/hcai:Injury"
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList.Count > 0)
            {
                NewMessageEvent($"  OCF23InjuryLineItemSubmit[{injList.Count}]");
                for (var i = 0; i < injList.Count; i++)
                {
                    var tmpItem = (IDataItem)injList[i];
                    var code = tmpItem.GetValue(edOcf23LisKeys.OCF23_InjuriesAndSequelae_Injury_Code);

                    var injury = rowSubmitted.OCF23InjuryLineItemSubmit
                        .FirstOrDefault(c => c.OCF23_InjuriesAndSequelae_Injury_Code == code)
                    ?? _dbContext.OCF23InjuryLineItemSubmit.Add(new OCF23InjuryLineItemSubmit
                    {
                        HCAI_Document_Number = documentNumber,
                        OCF23_InjuriesAndSequelae_Injury_Code = code,
                    });
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            // Adjuster Response
            ocf23ARrow ??= _dbContext.OCF23AR.Add(new OCF23AR { HCAI_Document_Number = documentNumber });

            #region approved doc fields

            // ***** header ****
            ocf23ARrow.PMSFields_PMSDocumentKey = doc.GetValue(edOcf23Keys.PMSFields_PMSDocumentKey);
            ocf23ARrow.Submission_Date = doc.GetValue(edOcf23Keys.Submission_Date).ToHcaiDateTime();
            ocf23ARrow.Submission_Source = doc.GetValue(edOcf23Keys.Submission_Source);
            ocf23ARrow.Adjuster_Response_Date = doc.GetValue(edOcf23Keys.Adjuster_Response_Date).ToHcaiDateTime();
            ocf23ARrow.In_Dispute = doc.GetValue(edOcf23Keys.In_Dispute).ToBool();
            ocf23ARrow.Document_Type = doc.GetValue(edOcf23Keys.Document_Type);
            ocf23ARrow.Document_Status = documentStatus;
            ocf23ARrow.Attachments_Received_Date = doc.GetValue(edOcf23Keys.Attachments_Received_Date).ToHcaiDateNullable();

            ocf23ARrow.Claimant_First_Name = doc.GetValue(edOcf23Keys.Claimant_First_Name);
            ocf23ARrow.Claimant_Middle_Name = doc.GetValue(edOcf23Keys.Claimant_Middle_Name);
            ocf23ARrow.Claimant_Last_Name = doc.GetValue(edOcf23Keys.Claimant_Last_Name);
            ocf23ARrow.Claimant_Date_Of_Birth = doc.GetValue(edOcf23Keys.Claimant_Date_Of_Birth).ToHcaiDate();
            ocf23ARrow.Claimant_Gender = doc.GetValue(edOcf23Keys.Claimant_Gender).ToGender();
            ocf23ARrow.Claimant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf23Keys.Claimant_Telephone));
            ocf23ARrow.Claimant_Extension = doc.GetValue(edOcf23Keys.Claimant_Extension);
            ocf23ARrow.Claimant_Address1 = doc.GetValue(edOcf23Keys.Claimant_Address1);
            ocf23ARrow.Claimant_Address2 = doc.GetValue(edOcf23Keys.Claimant_Address2);
            ocf23ARrow.Claimant_City = doc.GetValue(edOcf23Keys.Claimant_City);
            ocf23ARrow.Claimant_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf23Keys.Claimant_Province));
            ocf23ARrow.Claimant_Postal_Code = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf23Keys.Claimant_Postal_Code));
            ocf23ARrow.Insurer_IBCInsurerID = doc.GetValue(edOcf23Keys.Insurer_IBCInsurerID);
            ocf23ARrow.Insurer_IBCBranchID = doc.GetValue(edOcf23Keys.Insurer_IBCBranchID);
            ocf23ARrow.Status_Code = doc.GetValue(edOcf23Keys.Status_Code);
            ocf23ARrow.Messages = doc.GetValue(edOcf23Keys.Messages);

            //
            ocf23ARrow.OCF23_InsurerTotals_AutoInsurerTotal_Approved =
                doc.GetValue(edOcf23Keys.OCF23_InsurerTotals_AutoInsurerTotal_Approved).ToDecimal();
            ocf23ARrow.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved =
                doc.GetValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved).ToDecimal();
            ocf23ARrow.OCF23_InsurerTotals_SubTotalPreApproved_Approved =
                doc.GetValue(edOcf23Keys.OCF23_InsurerTotals_SubTotalPreApproved_Approved).ToDecimal();

            ocf23ARrow.OCF23_InsurerSignature_ApplicantSignatureWaived =
                doc.GetValue(edOcf23Keys.OCF23_InsurerSignature_ApplicantSignatureWaived).ToBoolNullable();
            ocf23ARrow.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices =
                doc.GetValue(edOcf23Keys.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices).ToBoolNullable();
            ocf23ARrow.OCF23_InsurerSignature_PolicyInForceConfirmation =
                doc.GetValue(edOcf23Keys.OCF23_InsurerSignature_PolicyInForceConfirmation).ToBoolNullable();

            ocf23ARrow.SigningAdjuster_FirstName = doc.GetValue(edOcf23Keys.SigningAdjuster_FirstName);
            ocf23ARrow.SigningAdjuster_LastName = doc.GetValue(edOcf23Keys.SigningAdjuster_LastName);
            ocf23ARrow.NameOfAdjusterOnPDF = doc.GetValue(edOcf23Keys.NameOfAdjusterOnPDF);
            ocf23ARrow.Archival_Status = doc.GetValue(edOcf23Keys.Archival_Status);
            ocf23ARrow.OCF23_OtherGoodsAndServices_AdjusterResponseExplanation =
                doc.GetValue(edOcf23Keys.OCF23_OtherGoodsAndServices_AdjusterResponseExplanation);

            //OtherGSLineItemAR - ok & good!
            //"/hl7:QUCR_IN350104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherGoodsAndServices']]/hl7:component"
            var aRoList = doc.getList(LineItemType.DE_GS23OtherGSLineItem_Approved);
            if (aRoList.Count > 0)
            {
                if (oList.Count != aRoList.Count)
                    NewMessageEvent($"  OtherGSLineItemApproved[{aRoList.Count}] !!!");
                for (var i = 0; i < aRoList.Count; i++)
                {
                    var tmpItem = (IDataItem) aRoList[i];

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:unitPriceAmt/hl7:numerator/@value"
                    var lineCost = tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost).ToDecimal();

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:id/@extension"
                    var key = tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var otherGsLineItemAr = ocf23ARrow.OtherGSLineItemARs
                        .FirstOrDefault(t => t.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey == key)
                        ?? _dbContext.OtherGSLineItemARs.Add(new OtherGSLineItemAR
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey = key,
                        });

                    otherGsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost = lineCost;

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value"
                    otherGsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value"
                    otherGsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value"
                    otherGsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        tmpItem.GetValue(edOcf23LisKeys.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            return true;
        }
    }
}
