﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using HcaiSync.Common.EfData;
using PMSToolkit;
using PMSToolkit.DataObjects;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractOcf21C(IDataItem doc)
        {
            var edOcf21CKeys = new OCF21CDataExtractResponseKeys();
            var edOcf21CLisKeys = new OCF21CDataExtractResponseLineItemKeys();

            var documentNumber = doc.GetValue(edOcf21CKeys.HCAI_Document_Number);
            if (string.IsNullOrEmpty(documentNumber))
            {
                NewMessageEvent("Extracting doc with type 'OCF21C' - NO number!");
                return false;
            }

            var documentStatus = doc.GetValue(edOcf21CKeys.Document_Status);
            var ocf21Car = _dbContext.OCF21CAR
                .Include("OtherReimbursableLineItemARs").Include("PAFReimbursableLineItemARs")
                .FirstOrDefault(ar => ar.HCAI_Document_Number == documentNumber);

            var rowSubmitted = _dbContext.OCF21CSubmit
                .Include("RenderedGSLineItemSubmits").Include("OCF21CInjuryLineItemSubmit")
                .Include("PAFReimbursableLineItemSubmits").Include("OtherReimbursableLineItemSubmits")
                .FirstOrDefault(d => d.HCAI_Document_Number == documentNumber);
            var isExists = rowSubmitted != null;

            var docApproved = ocf21Car?.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved ?? 0;
            var itemsApproved = ocf21Car?.OtherReimbursableLineItemARs
                .Select(t => t.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost)
                .Sum() ?? 0;

            if (!isExists)
            {
                NewMessageEvent($"Extracting doc with type 'OCF21C' & number = '{documentNumber}' ");
            }
            else if (documentStatus == ocf21Car?.Document_Status && Math.Abs(docApproved - itemsApproved) > 0.01m)
            {
                return false;
            }
            else
            {
                NewMessageEvent($"Updating doc with type 'OCF21C' & number = '{documentNumber}' from status '{ocf21Car?.Document_Status}' to status '{documentStatus}'");
            }

            rowSubmitted ??= _dbContext.OCF21CSubmit.Add(new OCF21CSubmit { HCAI_Document_Number = documentNumber });
            
            #region sumbit doc fields

            // **** header ****
            rowSubmitted.PMSFields_PMSSoftware = doc.GetValue(edOcf21CKeys.PMSFields_PMSSoftware);
            rowSubmitted.PMSFields_PMSVersion = doc.GetValue(edOcf21CKeys.PMSFields_PMSVersion);
            rowSubmitted.PMSFields_PMSDocumentKey = doc.GetValue(edOcf21CKeys.PMSFields_PMSDocumentKey);
            rowSubmitted.PMSFields_PMSPatientKey = doc.GetValue(edOcf21CKeys.PMSFields_PMSPatientKey);

            rowSubmitted.AttachmentsBeingSent = doc.GetValue(edOcf21CKeys.AttachmentsBeingSent).ToBool();
            //'WRONG
            rowSubmitted.Header_ClaimNumber = doc.GetValue(edOcf21CKeys.Header_ClaimNumber);
            rowSubmitted.Header_PolicyNumber = doc.GetValue(edOcf21CKeys.Header_PolicyNumber);
            rowSubmitted.Header_DateOfAccident = doc.GetValue(edOcf21CKeys.Header_DateOfAccident).ToHcaiDate();

            rowSubmitted.Applicant_Name_FirstName = doc.GetValue(edOcf21CKeys.Applicant_Name_FirstName);
            rowSubmitted.Applicant_Name_MiddleName = doc.GetValue(edOcf21CKeys.Applicant_Name_MiddleName);
            rowSubmitted.Applicant_Name_LastName = doc.GetValue(edOcf21CKeys.Applicant_Name_LastName);
            rowSubmitted.Applicant_DateOfBirth = doc.GetValue(edOcf21CKeys.Applicant_DateOfBirth).ToHcaiDate();
            rowSubmitted.Applicant_Gender = doc.GetValue(edOcf21CKeys.Applicant_Gender).ToGender();
            rowSubmitted.Applicant_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21CKeys.Applicant_TelephoneNumber));
            rowSubmitted.Applicant_TelephoneExtension = doc.GetValue(edOcf21CKeys.Applicant_TelephoneExtension);
            rowSubmitted.Applicant_Address_StreetAddress1 = doc.GetValue(edOcf21CKeys.Applicant_Address_StreetAddress1);
            rowSubmitted.Applicant_Address_StreetAddress2 = doc.GetValue(edOcf21CKeys.Applicant_Address_StreetAddress2);
            rowSubmitted.Applicant_Address_City = doc.GetValue(edOcf21CKeys.Applicant_Address_City);
            rowSubmitted.Applicant_Address_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf21CKeys.Applicant_Address_Province));
            rowSubmitted.Applicant_Address_PostalCode = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf21CKeys.Applicant_Address_PostalCode));

            rowSubmitted.Insurer_IBCInsurerID = doc.GetValue(edOcf21CKeys.Insurer_IBCInsurerID);
            rowSubmitted.Insurer_IBCBranchID = doc.GetValue(edOcf21CKeys.Insurer_IBCBranchID);

            rowSubmitted.Insurer_Adjuster_Name_FirstName = doc.GetValue(edOcf21CKeys.Insurer_Adjuster_Name_FirstName);
            rowSubmitted.Insurer_Adjuster_Name_LastName = doc.GetValue(edOcf21CKeys.Insurer_Adjuster_Name_LastName);
            rowSubmitted.Insurer_Adjuster_TelephoneNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21CKeys.Insurer_Adjuster_TelephoneNumber));
            rowSubmitted.Insurer_Adjuster_TelephoneExtension = doc.GetValue(edOcf21CKeys.Insurer_Adjuster_TelephoneExtension);
            rowSubmitted.Insurer_Adjuster_FaxNumber = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21CKeys.Insurer_Adjuster_FaxNumber));

            rowSubmitted.Insurer_PolicyHolder_IsSameAsApplicant = doc.GetValue(edOcf21CKeys.Insurer_PolicyHolder_IsSameAsApplicant).ToBool();
            rowSubmitted.Insurer_PolicyHolder_Name_FirstName = doc.GetValue(edOcf21CKeys.Insurer_PolicyHolder_Name_FirstName);
            rowSubmitted.Insurer_PolicyHolder_Name_LastName = doc.GetValue(edOcf21CKeys.Insurer_PolicyHolder_Name_LastName);

            //
            rowSubmitted.OCF21C_InvoiceInformation_InvoiceNumber = doc.GetValue(edOcf21CKeys.OCF21C_InvoiceInformation_InvoiceNumber);
            rowSubmitted.OCF21C_InvoiceInformation_FirstInvoice = doc.GetValue(edOcf21CKeys.OCF21C_InvoiceInformation_FirstInvoice).ToBool();
            rowSubmitted.OCF21C_InvoiceInformation_LastInvoice = doc.GetValue(edOcf21CKeys.OCF21C_InvoiceInformation_LastInvoice).ToBool();

            rowSubmitted.OCF21C_Payee_FacilityRegistryID = doc.GetValue(edOcf21CKeys.OCF21C_Payee_FacilityRegistryID).ToInt32();
            rowSubmitted.OCF21C_Payee_FacilityIsPayee = doc.GetValue(edOcf21CKeys.OCF21C_Payee_FacilityIsPayee).ToBool();
            rowSubmitted.OCF21C_Payee_MakeChequePayableTo = doc.GetValue(edOcf21CKeys.OCF21C_Payee_MakeChequePayableTo);

            rowSubmitted.OCF21C_Payee_ConflictOfInterests_ConflictExists =
                doc.GetValue(edOcf21CKeys.OCF21C_Payee_ConflictOfInterests_ConflictExists).ToBoolNullable();
            rowSubmitted.OCF21C_Payee_ConflictOfInterests_ConflictDetails = doc.GetValue(edOcf21CKeys.OCF21C_Payee_ConflictOfInterests_ConflictDetails);
            rowSubmitted.OCF21C_PAFReimbursableFees_PAFType = doc.GetValue(edOcf21CKeys.OCF21C_PAFReimbursableFees_PAFType);

            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOH_OtherService =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOH_Total_Proposed =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21C_OtherInsuranceAmounts_OtherServiceType = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_OtherServiceType);

            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOHDebits_Chiropractic =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOHDebits_Physiotherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOHDebits_MassageTherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOHDebits_OtherService =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Proposed =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Chiropractic =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1Debits_OtherService =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Chiropractic =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Chiropractic).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2Debits_OtherService =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_OtherService).ToDecimalNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed).ToDecimalNullable();

            rowSubmitted.OCF21C_OtherInsuranceAmounts_IsAmountRefused =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_IsAmountRefused).ToBoolNullable();
            rowSubmitted.OCF21C_OtherInsuranceAmounts_Debits_OtherServiceType = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Debits_OtherServiceType);

            rowSubmitted.OCF21C_AccountActivity_PriorBalance =
                doc.GetValue(edOcf21CKeys.OCF21C_AccountActivity_PriorBalance).ToDecimalNullable();
            rowSubmitted.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer =
                doc.GetValue(edOcf21CKeys.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer).ToDecimalNullable();
            rowSubmitted.OCF21C_AccountActivity_OverdueAmount =
                doc.GetValue(edOcf21CKeys.OCF21C_AccountActivity_OverdueAmount).ToDecimalNullable();

            rowSubmitted.OCF21C_InsurerTotals_OtherInsurers_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_MOH_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_MOH_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_GST_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_PST_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_Interest_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed).ToDecimal();
            rowSubmitted.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed).ToDecimal();

            rowSubmitted.OCF21C_OtherInformation = doc.GetValue(edOcf21CKeys.OCF21C_OtherInformation);
            rowSubmitted.AdditionalComments = doc.GetValue(edOcf21CKeys.AdditionalComments);

            rowSubmitted.OCF21C_PreviouslyApprovedGoodsAndServices_Type = doc.GetValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_Type);
            rowSubmitted.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate =
                doc.GetValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate).ToHcaiDateNullable();
            rowSubmitted.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber = doc.GetValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber);
            rowSubmitted.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved =
                doc.GetValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved).ToDecimalNullable();
            rowSubmitted.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled =
                doc.GetValue(edOcf21CKeys.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled).ToDecimalNullable();

            rowSubmitted.OCFVersion = doc.GetValue(edOcf21CKeys.OCFVersion).ToInt32();

            //RenderedGSLineItemSubmit - ok & good!
            //"hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='RenderedGoodsAndServices']]/hl7:component"
            var rnList = doc.getList(LineItemType.DE_GS21CRenderedGSLineItem);
            if (rnList.Count > 0)
            {
                NewMessageEvent($"  RenderedGSLineItemSubmit[{rnList.Count}]");
                for (var i = 0; i < rnList.Count; i++)
                {
                    var tmpItem = (IDataItem)rnList[i];
                    var itemCode = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Code);

                    //"hl7:invoiceElementDetail/hl7:id/@extension"
                    var key = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey);
                    var referenceNumber = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ReferenceNumber);
                    if (string.IsNullOrEmpty(key))
                        key = referenceNumber;
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var renderedGsLineItemSubmit = rowSubmitted.RenderedGSLineItemSubmits
                        .FirstOrDefault(t => t.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey == key
                            && t.OCF21C_RenderedGoodsAndServices_Items_Item_Code == itemCode)
                        ?? _dbContext.RenderedGSLineItemSubmits.Add(new RenderedGSLineItemSubmit
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF21C_RenderedGoodsAndServices_Items_Item_Code = itemCode,
                            OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey = key,
                        });

                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/hl7:qualifier/@code"
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute);
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID)
                            .ToInt32();
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity)
                            .ToDecimal();
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Measure =
                        (tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Measure));
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService)
                            .ToHcaiDate();
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ReferenceNumber =
                        referenceNumber.ToInt32Nullable();
                    renderedGsLineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Description =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_RenderedGoodsAndServices_Items_Item_Description);
                }
            }

            //OCF21CInjuryLineItemSubmit - ok & good!
            //"/hl7:*/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:pertinentInformation1/hl7:healthDocumentAttachment/hl7:value/hl7:inlineData/hcai:*/hcai:InjuriesAndSequelae/hcai:Injury"
            var injList = doc.getList(LineItemType.InjuryLineItem);
            if (injList.Count > 0)
            {
                NewMessageEvent($"  OCF21CInjuryLineItemSubmit[{injList.Count}]");
                for (var i = 0; i < injList.Count; i++)
                {
                    var tmpItem = (IDataItem)injList[i];
                    var code = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_InjuriesAndSequelae_Injury_Code);

                    var injury = rowSubmitted.OCF21CInjuryLineItemSubmit
                        .FirstOrDefault(c => c.OCF21C_InjuriesAndSequelae_Injury_Code == code)
                        ?? _dbContext.OCF21CInjuryLineItemSubmit.Add(new OCF21CInjuryLineItemSubmit
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF21C_InjuriesAndSequelae_Injury_Code = code,
                        });
                }
            }

            //OtherReimbursableLineItemSubmit - ok & good!
            //"hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='OtherReimbursableGoodsAndServices']]/hl7:component"
            var orList = doc.getList(LineItemType.DE_GS21COtherReimbursableLineItem_Estimated);
            if (orList.Count > 0)
            {
                NewMessageEvent($"  OtherReimbursableLineItemSubmit[{orList.Count}]");
                for (var i = 0; i < orList.Count; i++)
                {
                    var tmpItem = (IDataItem)orList[i];

                    var itemCode = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code);
                    var key = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                    var referenceNumber = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ReferenceNumber);
                    if (string.IsNullOrEmpty(key))
                        key = referenceNumber;
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var otherReimbursableLineItemSubmit = rowSubmitted.OtherReimbursableLineItemSubmits
                        .FirstOrDefault(t => t.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey == key
                            && t.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code == itemCode)
                        ?? _dbContext.OtherReimbursableLineItemSubmits.Add(
                        new OtherReimbursableLineItemSubmit
                            {
                                HCAI_Document_Number = documentNumber,
                                OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code = itemCode,
                                OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey = key,
                            });

                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute);
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID)
                            .ToInt32();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation);
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity)
                            .ToDecimal();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure);
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService)
                            .ToHcaiDate();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST)
                            .ToBool();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST)
                            .ToBool();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                            .ToDecimal();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ReferenceNumber =
                        referenceNumber.ToInt32Nullable();
                    otherReimbursableLineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description);
                }
            }

            //PAFReimbursableLineItemSubmit - ok & good!
            //"hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:reference2/hl7:invoiceElementGroup/hl7:component/hl7:invoiceElementGroup[child::hl7:id[@extension='PAFReimbursableFees']]/hl7:component"
            var pafList = doc.getList(LineItemType.DE_GS21CPAFReimbursableLineItem_Estimated);
            if (pafList.Count > 0)
            {
                NewMessageEvent($"  OtherReimbursableLineItemSubmit[{pafList.Count}]");
                for (var i = 0; i < pafList.Count; i++)
                {
                    var tmpItem = (IDataItem)pafList[i];

                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/@code"
                    var itemCode = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Code);

                    //"hl7:invoiceElementDetail/hl7:id/@extension" - may be empty ((((
                    var key = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var pafReimbursableLineItemSubmit = rowSubmitted.PAFReimbursableLineItemSubmits
                        .FirstOrDefault(t => t.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey == key
                            && t.OCF21C_PAFReimbursableFees_Items_Item_Code == itemCode)
                        ?? _dbContext.PAFReimbursableLineItemSubmits.Add(
                            new PAFReimbursableLineItemSubmit
                            {
                                HCAI_Document_Number = documentNumber,
                                OCF21C_PAFReimbursableFees_Items_Item_Code = itemCode,
                                OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey = key,
                            });

                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID)
                            .ToInt32();
                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation);

                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID =
                        tmpItem.GetValue("hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:secondaryPerformer/hl7:healthCareProvider/hl7:id/@extension")
                            .ToInt32Nullable();
                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation =
                        tmpItem.GetValue("hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:secondaryPerformer/hl7:healthCareProvider/hl7:code/@code");

                    //"hl7:invoiceElementDetail/hl7:reasonOf/hl7:billableClinicalService/hl7:code/hl7:qualifier/@code"
                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Attribute =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Attribute);
                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost)
                            .ToDecimal();
                    pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService)
                            .ToHcaiDate();

                    // secondary providers
                    var pafSecPpList = GetSecondaryProviders(tmpItem);
                    if (!pafSecPpList.Any()) continue;

                    for (var j = 0; j < pafSecPpList.Count; j++)
                    {
                        var pafSecPp = pafSecPpList[j];
                        var regId = pafSecPp.regId;
                        var occupation = pafSecPp.code;

                        switch (j)
                        {
                            case 0:
                                pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID = regId;
                                pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation = occupation;
                                break;
                            case 1:
                                pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_ProviderRegistryID = regId;
                                pafReimbursableLineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_Occupation = occupation;
                                break;
                        }
                    }
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            // Adjuster Response
            ocf21Car ??= _dbContext.OCF21CAR.Add(new OCF21CAR { HCAI_Document_Number = documentNumber });

            #region approved doc fields

            // **** header ****
            ocf21Car.PMSFields_PMSDocumentKey = doc.GetValue(edOcf21CKeys.PMSFields_PMSDocumentKey);
            ocf21Car.Submission_Date = doc.GetValue(edOcf21CKeys.Submission_Date).ToHcaiDateTime();
            ocf21Car.Submission_Source = doc.GetValue(edOcf21CKeys.Submission_Source);
            ocf21Car.Adjuster_Response_Date = doc.GetValue(edOcf21CKeys.Adjuster_Response_Date).ToHcaiDateTime();
            ocf21Car.In_Dispute = doc.GetValue(edOcf21CKeys.In_Dispute).ToBool();
            ocf21Car.Document_Type = doc.GetValue(edOcf21CKeys.Document_Type);
            ocf21Car.Document_Status = documentStatus;
            ocf21Car.Attachments_Received_Date = doc.GetValue(edOcf21CKeys.Attachments_Received_Date).ToHcaiDateNullable();

            ocf21Car.Claimant_First_Name = doc.GetValue(edOcf21CKeys.Claimant_First_Name);
            ocf21Car.Claimant_Middle_Name = doc.GetValue(edOcf21CKeys.Claimant_Middle_Name);
            ocf21Car.Claimant_Last_Name = doc.GetValue(edOcf21CKeys.Claimant_Last_Name);
            ocf21Car.Claimant_Date_Of_Birth = doc.GetValue(edOcf21CKeys.Claimant_Date_Of_Birth).ToHcaiDate();
            ocf21Car.Claimant_Gender = doc.GetValue(edOcf21CKeys.Claimant_Gender).ToGender();
            ocf21Car.Claimant_Telephone = ConvertHelper.PhoneFromHcai(doc.GetValue(edOcf21CKeys.Claimant_Telephone));
            ocf21Car.Claimant_Extension = doc.GetValue(edOcf21CKeys.Claimant_Extension);
            ocf21Car.Claimant_Address1 = doc.GetValue(edOcf21CKeys.Claimant_Address1);
            ocf21Car.Claimant_Address2 = doc.GetValue(edOcf21CKeys.Claimant_Address2);
            ocf21Car.Claimant_City = doc.GetValue(edOcf21CKeys.Claimant_City);
            ocf21Car.Claimant_Province = ConvertHelper.FormatProvince(doc.GetValue(edOcf21CKeys.Claimant_Province));
            ocf21Car.Claimant_Postal_Code = ConvertHelper.FormatPostalCode(doc.GetValue(edOcf21CKeys.Claimant_Postal_Code));

            ocf21Car.Insurer_IBCInsurerID = doc.GetValue(edOcf21CKeys.Insurer_IBCInsurerID);
            ocf21Car.Insurer_IBCBranchID = doc.GetValue(edOcf21CKeys.Insurer_IBCBranchID);

            ocf21Car.Status_Code = doc.GetValue(edOcf21CKeys.Status_Code);
            ocf21Car.Messages = doc.GetValue(edOcf21CKeys.Messages);

            //
            ocf21Car.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation = doc.GetValue(edOcf21CKeys.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation);

            ocf21Car.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            //
            var otherInsuranceAmountsMohTotalApprovedLineCost =
                doc.getValue(DocumentParseMyKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost);
            if (string.IsNullOrEmpty(otherInsuranceAmountsMohTotalApprovedLineCost))
                otherInsuranceAmountsMohTotalApprovedLineCost = doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost);
            ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost = otherInsuranceAmountsMohTotalApprovedLineCost.ToDecimal();

            ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode =
                doc.getValue(DocumentParseMyKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);
            if (string.IsNullOrEmpty(ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode))
                ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode =
                    doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode);

            ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                doc.getValue(DocumentParseMyKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            if (string.IsNullOrEmpty(ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc))
                ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                    doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);

            ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                doc.getValue(DocumentParseMyKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
            if (string.IsNullOrEmpty(ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc))
                ocf21Car.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                    doc.getValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            //
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation = doc.GetValue(edOcf21CKeys.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation);
            ocf21Car.OCF21C_InsurerSignature_ClaimFormReceived = doc.GetValue(edOcf21CKeys.OCF21C_InsurerSignature_ClaimFormReceived).ToBoolNullable();
            ocf21Car.OCF21C_InsurerSignature_ClaimFormReceivedDate = doc.GetValue(edOcf21CKeys.OCF21C_InsurerSignature_ClaimFormReceivedDate).ToHcaiDate();

            ocf21Car.OCF21C_InsurerTotals_MOH_Approved = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_MOH_Approved).ToDecimal();
            ocf21Car.OCF21C_InsurerTotals_OtherInsurers_Approved = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_OtherInsurers_Approved).ToDecimal();
            ocf21Car.OCF21C_InsurerTotals_AutoInsurerTotal_Approved = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_AutoInsurerTotal_Approved).ToDecimal();

            ocf21Car.OCF21C_InsurerTotals_GST_Approved_LineCost = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_InsurerTotals_PST_Approved_LineCost = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_InsurerTotals_Interest_Approved_LineCost = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_LineCost).ToDecimal();
            ocf21Car.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode);
            ocf21Car.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc);
            ocf21Car.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc);

            ocf21Car.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved).ToDecimal();
            ocf21Car.OCF21C_InsurerTotals_SubTotalPreApproved_Approved = doc.GetValue(edOcf21CKeys.OCF21C_InsurerTotals_SubTotalPreApproved_Approved).ToDecimal();

            ocf21Car.SigningAdjuster_FirstName = doc.GetValue(edOcf21CKeys.SigningAdjuster_FirstName);
            ocf21Car.SigningAdjuster_LastName = doc.GetValue(edOcf21CKeys.SigningAdjuster_LastName);
            ocf21Car.ApprovedByOnPDF = doc.GetValue(edOcf21CKeys.ApprovedByOnPDF);
            ocf21Car.Archival_Status = doc.GetValue(edOcf21CKeys.Archival_Status);

            //OtherReimbursableLineItemSubmit = no in xml (
            //"/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='OtherReimbursableGoodsAndServices']]/hl7:component"
            var orArList = doc.getList(LineItemType.DE_GS21COtherReimbursableLineItem_Approved);
            if (orArList.Count > 0)
            {
                if (orArList.Count != orList.Count)
                    NewMessageEvent($"  OtherReimbursableLineItemApproved[{orArList.Count}] !!!");

                for (var i = 0; i < orArList.Count; i++)
                {
                    var tmpItem = (IDataItem)orArList[i];
                    var lineCost = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost).ToDecimal();

                    var key = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var otherReimbursableLineItemAr = ocf21Car.OtherReimbursableLineItemARs
                        .FirstOrDefault(r => r.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == key)
                        ?? _dbContext.OtherReimbursableLineItemARs.Add(new OtherReimbursableLineItemAR
                        {
                            HCAI_Document_Number = documentNumber,
                            OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey = key,
                        });

                    otherReimbursableLineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost = lineCost;

                    otherReimbursableLineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST).ToBool();
                    otherReimbursableLineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST).ToBool();

                    otherReimbursableLineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    otherReimbursableLineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    otherReimbursableLineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                }
            }

            //PAFReimbursableLineItemAR - ok & good!
            //"/hl7:QUCR_IN650104UV02/hl7:controlActProcess/hl7:subject/hl7:paymentIntent/hl7:reasonOf/hl7:adjudicatedInvoiceElementGroup/hl7:component/hl7:adjudicatedInvoiceElementGroup[child::hl7:id[@extension='PAFReimbursableFees']]/hl7:component"
            var pafArList = doc.getList(LineItemType.DE_GS21CPAFReimbursableLineItem_Approved);
            if (pafArList.Count > 0)
            {
                if (pafArList.Count != pafList.Count)
                    NewMessageEvent($"  PAFReimbursableLineItemApproved[{pafArList.Count}] !!!");
                for (var i = 0; i < pafArList.Count; i++)
                {
                    var tmpItem = (IDataItem)pafArList[i];

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:unitPriceAmt/hl7:numerator/@value"
                    var lineCost = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost).ToDecimal();

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:id/@extension" - may be empty ((
                    var key = tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey);
                    if (string.IsNullOrEmpty(key))
                        key = $"{i + 1}";

                    var pafReimbursableLineItemAr = ocf21Car.PAFReimbursableLineItemARs
                        .FirstOrDefault(t => t.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey == key)
                        ?? _dbContext.PAFReimbursableLineItemARs.Add(
                            new PAFReimbursableLineItemAR
                            {
                                HCAI_Document_Number = documentNumber,
                                OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey = key,
                            });

                    pafReimbursableLineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost = lineCost;

                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation/hl7:adjudicationResultReason/hl7:value"
                    pafReimbursableLineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode);
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value"
                    pafReimbursableLineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc);
                    //"hl7:adjudicatedInvoiceElementDetail/hl7:outcomeOf/hl7:adjudicationResult/hl7:pertinentInformation[2]/hl7:adjudicationResultInformation/hl7:value"
                    pafReimbursableLineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc =
                        tmpItem.GetValue(edOcf21CLisKeys.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc);
                }
            }

            #endregion

            _dbContext.SaveChangesWithDetect();

            return true;
        }

        private static List<(int regId, string code)> GetSecondaryProviders(IDataItem item)
        {
            var res = new List<(int regId, string code)>();

            try
            {
                var data = (XmlDataItem)item;
                var doc = XDocument.Parse(data.Xmn.InnerXml);

                var billableClinicalService = doc.Root.ElementByLocalName("reasonOf").ElementByLocalName("billableClinicalService");
                var secondaryPerformers = billableClinicalService.ElementsByLocalName("secondaryPerformer");
                foreach (var secondaryPerformer in secondaryPerformers)
                {
                    var healthCareProvide = secondaryPerformer.ElementByLocalName("healthCareProvider");

                    var sRegId = healthCareProvide.ElementByLocalName("id").AttributeByLocalName("extension").Value;
                    var regId = !string.IsNullOrEmpty(sRegId) && int.TryParse(sRegId, out var tmp)
                        ? tmp
                        : 0;
                    var code = healthCareProvide.ElementByLocalName("code").AttributeByLocalName("code").Value;

                    if (regId > 0 && !string.IsNullOrEmpty(code))
                        res.Add((regId, code));
                }
            }
            catch (Exception ex)
            {
                //ignore
            }

            return res;
        }
    }
}
