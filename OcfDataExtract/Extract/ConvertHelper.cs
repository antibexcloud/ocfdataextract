﻿using System;
using System.Globalization;
using HcaiSync.Common.Enums;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public static class ConvertHelper
    {
        public static string GetValue(this IDataItem doc, string key)
        {
            return doc.getValue(key)?.Trim() ?? string.Empty;
        }

        #region UI service

        public static bool IsNumeric(this string n)
        {
            if (string.IsNullOrWhiteSpace(n)) return false;

            try
            {
                return double.TryParse(n, out _);
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Bools

        public static bool ToBool(this string val)
        {
            return !string.IsNullOrEmpty(val) && (val.ToLower() == "true" || val.ToLower() == "yes");
        }

        public static bool? ToBoolNullable(this string val)
        {
            return !string.IsNullOrEmpty(val)
                ? val.ToLower() == "true" || val.ToLower() == "yes"
                : (bool?)null;
        }

        #endregion

        #region Enums

        public static T ToEnumElemParse<T>(this string code)
            where T : struct
        {
            if (string.IsNullOrEmpty(code)) return default(T);
            return Enum.TryParse<T>(code, true, out var res) ? res : default(T);
        }

        public static T? ToEnumElemParseNullable<T>(this string code)
            where T : struct
        {
            if (string.IsNullOrEmpty(code)) return null;
                        return Enum.TryParse<T>(code, true, out var res) ? (T?)res : null;
        }

        public static int ToEnumIntParse<T>(this string code)
            where T : struct
        {
            if (string.IsNullOrEmpty(code)) return 0;

            if (Enum.TryParse<T>(code, true, out _))
                return (int) Enum.Parse(typeof(T), code);
            return 0;
        }

        public static int? ToEnumIntParseNullable<T>(this string code)
            where T : struct
        {
            if (string.IsNullOrEmpty(code)) return null;

            if (Enum.TryParse<T>(code, true, out _))
                return (int)Enum.Parse(typeof(T), code);
            return null;
        }

        #endregion

        #region Numeric

        public static int ToInt32(this string val)
        {
            if (string.IsNullOrEmpty(val)) return 0;
            return int.TryParse(val, out var res) ? res : 0;
        }

        public static int? ToInt32Nullable(this string val)
        {
            if (string.IsNullOrEmpty(val)) return null;
            return int.TryParse(val, out var res) ? res : (int?)null;
        }

        public static uint ToUInt(this string val)
        {
            if (string.IsNullOrEmpty(val)) return 0;
            return uint.TryParse(val, out var res) ? res : 0;
        }

        public static uint? ToUIntNullable(this string val)
        {
            if (string.IsNullOrEmpty(val)) return null;
            return uint.TryParse(val, out var res) ? res : (uint?)null;
        }

        public static long ToLong(this string val)
        {
            if (string.IsNullOrEmpty(val)) return 0L;
            return long.TryParse(val, out var res) ? res : 0;
        }

        public static long? ToLongNullable(this string val)
        {
            if (string.IsNullOrEmpty(val)) return null;
            return long.TryParse(val, out var res) ? res : (long?)null;
        }

        public static decimal ToDecimal(this string val)
        {
            if (string.IsNullOrEmpty(val)) return 0.0m;

            var text = val.Replace("$", "").Replace(" ", "").Replace(',', '.');
            try
            {
                return decimal.Parse(text, CultureInfo.InvariantCulture);
            }
            catch
            {
                return 0.0m;
            }
        }

        public static decimal? ToDecimalNullable(this string val)
        {
            if (string.IsNullOrEmpty(val)) return null;

            var text = val.Replace("$", "").Replace(" ", "").Replace(',', '.');
            try
            {
                return decimal.Parse(text, CultureInfo.InvariantCulture);
            }
            catch
            {
                return null;
            }
        }

        public static double ToDouble(this string val)
        {
            if (string.IsNullOrEmpty(val)) return 0.0;

            var text = val.Replace("$", "").Replace(" ", "").Replace(',', '.');
            try
            {
                return double.Parse(text, CultureInfo.InvariantCulture);
            }
            catch
            {
                return 0.0;
            }
        }

        public static double? ToDoubleNullable(this string val)
        {
            if (string.IsNullOrEmpty(val)) return null;

            var text = val.Replace("$", "").Replace(" ", "").Replace(',', '.');
            try
            {
                return double.Parse(text, CultureInfo.InvariantCulture);
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region DateTimes

        private static DateTime? HcaiDate(string val)
        {
            if (string.IsNullOrEmpty(val)) return null;

            var formats = new[] { "yyyy-MM-dd" };
            if (DateTime.TryParseExact(val, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out var res) ||
                DateTime.TryParse(val, out res))
                return DateTime.SpecifyKind(res, DateTimeKind.Local);
            return null;
        }

        private static DateTime? HcaiDateTime(string val)
        {
            if (string.IsNullOrEmpty(val)) return null;

            var formats = new[] { "yyyyMMddHHmmss.ffzzz", "yyyy-MM-ddTHH:mm:ss.ffZ", "yyy-MM-ddTHH:mm:ss.ff%K" };
            if (DateTime.TryParseExact(val, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out var res) ||
                DateTime.TryParse(val, out res))
            {
                if (res.Kind != DateTimeKind.Unspecified)
                    res = res.Kind == DateTimeKind.Local ? res : res.ToLocalTime();
                else
                    res = DateTime.SpecifyKind(res, DateTimeKind.Local);
                return res;
            }
            return null;
        }

        public static DateTime ToHcaiDate(this string val)
        {
            var res = HcaiDate(val);
            return res ?? new DateTime(1990, 1, 1);
        }

        public static DateTime? ToHcaiDateNullable(this string val)
        {
            var res = HcaiDate(val);
            return res;
        }

        public static DateTime ToHcaiDateTime(this string val)
        {
            var res = HcaiDateTime(val);
            return res ?? new DateTime(1990, 1, 1);
        }

        public static DateTime? ToHcaiDateTimeNullable(this string val)
        {
            var res = HcaiDateTime(val);
            return res;
        }

        #endregion

        #region Gender

        public static string ToGender(this string val)
        {
            var text = val;
            if (!string.IsNullOrEmpty(text) && text.Length > 1)
                text = text.Substring(0, 1);
            var res = text.ToEnumElemParseNullable<GenderEnum>();
            return res != null
                ? res.ToString()
                : GenderEnum.M.ToString();
        }

        public static string ToGenderNullable(this string val)
        {
            var text = val;
            if (!string.IsNullOrEmpty(text) && text.Length > 1)
                text = text.Substring(0, 1);
            var res = text.ToEnumElemParseNullable<GenderEnum>();
            return res != null
                ? res.ToString()
                : string.Empty;
        }

        #endregion

        public static string PhoneFromHcai(string phone)
        {
            phone = phone?.Trim() ?? string.Empty;
            if (string.IsNullOrWhiteSpace(phone)) return string.Empty;

            if (phone.Length < 10) return phone;
            if (!long.TryParse(phone, out _)) return phone;

            var result = string.Empty;
            if (phone.Length > 10)
            {
                result = phone.Substring(0, phone.Length - 10) + " ";
                phone = phone.Substring(phone.Length - 10, 10);
            }
            result += $"{phone.Substring(0, 3)}-{phone.Substring(3, 3)}-{phone.Substring(6, 4)}";
            return result;
        }

        public static string MyLeft(string text, int length)
        {
            if (length <= 0 || string.IsNullOrEmpty(text))
                return string.Empty;
            if (text.Length <= length)
                return text;
            return text.Substring(0, length);
        }

        public static string FormatProvince(string province)
        {
            if (string.IsNullOrWhiteSpace(province)) return string.Empty;

            return province.Length < 2
                ? province
                : MyLeft(province, 2).ToUpper();
        }

        public static string FormatPostalCode(string postalCode)
        {
            try
            {
                var tPostalCode = postalCode?.Trim() ?? string.Empty;
                tPostalCode = tPostalCode.Replace(" ", "");
                tPostalCode = tPostalCode.ToUpper();

                if (tPostalCode.Length == 5)
                {
                    if (IsNumeric(tPostalCode))
                        return tPostalCode;
                }
                if (tPostalCode.Length < 6) return string.Empty;

                var arr = tPostalCode.ToCharArray(0, tPostalCode.Length);

                if ((arr[0] >= 65 && arr[0] <= 90)
                    && (IsNumeric(arr[1].ToString()))
                    && (arr[2] >= 65 && arr[2] <= 90)
                    && (IsNumeric(arr[3].ToString())) && (IsNumeric(arr[5].ToString()))
                    && (arr[4] >= 65 && arr[4] <= 90))
                {
                    return arr[0].ToString() + arr[1] + arr[2] + " " + arr[3] + arr[4] + arr[5];
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
