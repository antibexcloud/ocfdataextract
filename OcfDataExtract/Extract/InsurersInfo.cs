﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using HcaiSync.Common.EfData;
using PMSToolkit;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private bool ExtractInsurersInfo()
        {
            var insKeys = new PMSToolkit.DataObjects.InsurerLineItemKeys();
            var branchKey = new PMSToolkit.DataObjects.BranchLineItemKeys();

            NewMessageEvent("*** Extracting Insurer list");

            var insurers = _pmsTk.insurerList(Config.Login, Config.Pass);
            if (insurers is null)
            {
                PopulateErrorMessages();
                return false;
            }

            if (IsXmlSave)
            {
                var filePath = Path.Combine(XmlPath, "InsurersInfo.xml");
                SaveAsXml(insurers, filePath);
            }

            var insList = insurers.getList(LineItemType.InsurerLineItem);
            if (insList is null)
            {
                PopulateErrorMessages();
                return false;
            }

            // ***** insurers *****
            for (var i = 0; i < insList.Count; i++)
            {
                var insurer = (IDataItem)insList[i];
                var idInsurer = insurer.getValue(insKeys.Insurer_ID);

                var ins = _dbContext.InsurerLineItems.Include("BranchLineItems")
                              .FirstOrDefault(c => c.Insurer_ID == idInsurer) ??
                          _dbContext.InsurerLineItems.Add(new InsurerLineItem
                          {
                              Insurer_ID = idInsurer,
                              BranchLineItems = new List<BranchLineItem>(),
                          });

                ins.Insurer_ID = insurer.getValue(insKeys.Insurer_ID);
                ins.Insurer_Name = insurer.getValue(insKeys.Insurer_Name);
                ins.Insurer_Status = insurer.getValue(insKeys.Insurer_Status);

                // ***** branches *****
                var branchList = insurer.getList(LineItemType.BranchLineItem);
                if (branchList != null)
                    for (var j = 0; j < branchList.Count; j++)
                    {
                        var branch = (IDataItem)branchList[j];
                        var idBranch = branch.getValue(branchKey.Branch_ID);

                        var ib = ins.BranchLineItems
                            .FirstOrDefault(b => b.Branch_ID == idBranch) ??
                            _dbContext.BranchLineItems.Add(new BranchLineItem
                            {
                                InsurerLineItem = ins,
                                Insurer_ID = ins.Insurer_ID,
                                Branch_ID = idBranch,
                            });

                        ib.Branch_Name = branch.getValue(branchKey.Branch_Name);
                        ib.Branch_Status = branch.getValue(branchKey.Branch_Status);
                    }

                // save
                _dbContext.SaveChangesWithDetect();
            }

            NewMessageEvent($"  insurers = {insList.Count}");
            return true;
        }
    }
}
