﻿using System.Collections.Generic;
using System.Linq;
using HcaiSync.Common.EfData;
using PMSToolkit;
using PMSToolkit.DataObjects;

namespace OcfDataExtract.Extract
{
    public partial class OcfDataExtractWorker
    {
        private void ExtractOcf9(IDataItem doc, DocType docType, string papaHcaiDocumentNumber)
        {
            // check
            switch (docType)
            {
                case DocType.OCF18:
                    if (_dbContext.OCF18AR.FirstOrDefault(d => d.HCAI_Document_Number == papaHcaiDocumentNumber) is null)
                    {
                        NewMessageEvent("DataExtract OCF18's ocf9 (ExtractOcf9): " + papaHcaiDocumentNumber);
                        return;
                    }
                    break;

                case DocType.OCF23:
                    if (_dbContext.OCF23AR.FirstOrDefault(d => d.HCAI_Document_Number == papaHcaiDocumentNumber) is null)
                    {
                        NewMessageEvent("DataExtract OCF23's ocf9(ExtractOcf9): " + papaHcaiDocumentNumber);
                        return;
                    }
                    break;

                case DocType.OCF21B:
                    if (_dbContext.OCF21BAR.FirstOrDefault(d => d.HCAI_Document_Number == papaHcaiDocumentNumber) is null)
                    {
                        NewMessageEvent("DataExtract OCF21B's ocf9(ExtractOcf9): " + papaHcaiDocumentNumber);
                        return;
                    }
                    break;

                case DocType.OCF21C:
                    if (_dbContext.OCF21CAR.FirstOrDefault(d => d.HCAI_Document_Number == papaHcaiDocumentNumber) is null)
                    {
                        NewMessageEvent("DataExtract OCF21C's ocf9(ExtractOcf9): " + papaHcaiDocumentNumber);
                        return;
                    }
                    break;

                case DocType.AACN:
                    if (_dbContext.AACNARs.FirstOrDefault(d => d.DocumentNumber == papaHcaiDocumentNumber) is null)
                    {
                        NewMessageEvent("DataExtract AACN's ocf9(ExtractOcf9): " + papaHcaiDocumentNumber);
                        return;
                    }
                    break;

                case DocType.ACSI:
                    if (_dbContext.ACSIARs.FirstOrDefault(d => d.HCAI_Document_Number == papaHcaiDocumentNumber) is null)
                    {
                        NewMessageEvent("DataExtract ACSI's ocf9(ExtractOcf9): " + papaHcaiDocumentNumber);
                        return;
                    }
                    break;

                default:
                    break;
            }

            var isForm1 = docType == DocType.AACN;
            var isAcsi = docType == DocType.ACSI;

            if (isForm1)
            {
                var form1Ocf9Keys = new AACNEOBDataExtractResponseKeys();
                var form1Keys = new AACNDataExtractResponseKeys();

                var documentNumber = doc.getValue(form1Ocf9Keys.EOB_EOBDocumentNumber);
                if (string.IsNullOrEmpty(documentNumber)) return;

                var rowSubmitted = _dbContext.OCF9AR.Include("GS9LineItemAR")
                    .FirstOrDefault(d => d.OCF9_HCAI_Document_Number == documentNumber);
                var isExists = rowSubmitted != null;
                if (!isExists)
                {
                    NewMessageEvent($"Extracting OCF9 for papaHcaiDocumentNumber='{papaHcaiDocumentNumber}' &" +
                                    $" with documentNumber='{documentNumber}' ");
                }
                else
                {
                    NewMessageEvent($"Updating OCF9 for papaHcaiDocumentNumber='{papaHcaiDocumentNumber}' &" +
                                    $" with documentNumber='{documentNumber}' ");
                }

                rowSubmitted ??= _dbContext.OCF9AR.Add(new OCF9AR
                {
                    OCF9_HCAI_Document_Number = documentNumber,
                    HCAI_Document_Number = papaHcaiDocumentNumber
                });

                rowSubmitted.OCF9_HCAI_Document_Number = documentNumber;
                rowSubmitted.OCFVersion = doc.getValue(form1Ocf9Keys.EOB_OCFVersion).ToInt32();
                rowSubmitted.OCF9_Header_DateRevised = doc.getValue(form1Ocf9Keys.EOB_DateRevised).ToHcaiDate();

                rowSubmitted.AdditionalComments = doc.getValue(form1Ocf9Keys.EOB_EOBAdditionalComments);
                if (string.IsNullOrEmpty(rowSubmitted.AdditionalComments))
                {
                    rowSubmitted.AdditionalComments = doc.getValue(form1Keys.EOB_EOBAdditionalComments);
                }
            }
            else if (isAcsi)
            {
                var acsiKeys = new ACSIEOBDataExtractResponseKeys();
                var acsiLiKeys = new ACSIEOBDataExtractResponseLineItemKeys();

                var documentNumber = doc.getValue(acsiKeys.HCAI_Document_Number);
                if (string.IsNullOrEmpty(documentNumber) || string.IsNullOrEmpty(doc.getValue(acsiKeys.OCFVersion))) return;

                var rowSubmitted = _dbContext.OCF9AR.Include("GS9LineItemAR")
                    .FirstOrDefault(d => d.OCF9_HCAI_Document_Number == documentNumber);
                var isExists = rowSubmitted != null;
                if (!isExists)
                {
                    NewMessageEvent($"Extracting OCF9 for papaHcaiDocumentNumber='{papaHcaiDocumentNumber}' &" +
                                    $" with documentNumber='{documentNumber}' ");
                }
                else
                {
                    NewMessageEvent($"Updating OCF9 for papaHcaiDocumentNumber='{papaHcaiDocumentNumber}' &" +
                                    $" with documentNumber='{documentNumber}' ");
                }

                rowSubmitted ??= _dbContext.OCF9AR.Add(new OCF9AR
                {
                    OCF9_HCAI_Document_Number = documentNumber,
                    HCAI_Document_Number = papaHcaiDocumentNumber
                });

                rowSubmitted.OCF9_HCAI_Document_Number = documentNumber;
                rowSubmitted.OCFVersion = doc.getValue(acsiKeys.OCFVersion).ToInt32();
                rowSubmitted.OCF9_Header_DateRevised = doc.getValue(acsiKeys.ACSIEOB_Header_DateRevised).ToHcaiDate();
                rowSubmitted.AttachmentsBeingSent = doc.getValue(acsiKeys.AttachmentsBeingSent).ToBool();

                rowSubmitted.AdditionalComments = doc.getValue(acsiKeys.AdditionalComments);

                var liList2 = doc.getList(LineItemType.GSACSIEOBLineItem);
                if (liList2 != null)
                {
                    rowSubmitted.GS9LineItemAR = new List<GS9LineItemAR>();
                    for (var i = 0; i < liList2.Count; i++)
                    {
                        var tmpItem = (IDataItem)liList2[i];
                        var li = new GS9LineItemAR
                        {
                            OCF9_HCAI_Document_Number = rowSubmitted.OCF9_HCAI_Document_Number,
                            OCF9_GoodsAndServices_Items_Item_ReferenceNumber = tmpItem.getValue(acsiLiKeys.ACSIEOB_GoodsAndServices_Items_Item_ReferenceNumber),
                            OCF9_GoodsAndServices_Items_Item_InterestPayable = tmpItem.getValue(acsiLiKeys.ACSIEOB_GoodsAndServices_Items_Item_InterestPayable)
                                .ToDecimalNullable(),
                            OCF9_GoodsAndServices_Items_Item_Code = tmpItem.getValue(acsiLiKeys.ACSIEOB_GoodsAndServices_Items_Item_Code),
                            OCF9AR = rowSubmitted,
                        };

                        rowSubmitted.GS9LineItemAR.Add(li);
                    }
                }
            }
            else
            {
                var ocf9Keys = new OCF9DataExtractResponseKeys();
                var ocf9LiKeys = new OCF9DataExtractResponseLineItemKeys();

                var documentNumber = doc.getValue(ocf9Keys.HCAI_Document_Number);
                if (string.IsNullOrEmpty(documentNumber)) return;

                var rowSubmitted = _dbContext.OCF9AR.Include("GS9LineItemAR")
                    .FirstOrDefault(d => d.OCF9_HCAI_Document_Number == documentNumber);
                var isExists = rowSubmitted != null;
                if (!isExists)
                {
                    NewMessageEvent($"Extracting OCF9 for papaHcaiDocumentNumber='{papaHcaiDocumentNumber}' &" +
                                    $" with documentNumber='{documentNumber}' ");
                }
                else
                {
                    NewMessageEvent($"Updating OCF9 for papaHcaiDocumentNumber='{papaHcaiDocumentNumber}' &" +
                                    $" with documentNumber='{documentNumber}' ");
                }

                rowSubmitted ??= _dbContext.OCF9AR.Add(new OCF9AR
                {
                    OCF9_HCAI_Document_Number = documentNumber,
                    HCAI_Document_Number = papaHcaiDocumentNumber
                });

                rowSubmitted.OCF9_HCAI_Document_Number = documentNumber;
                rowSubmitted.OCF9_Header_DateRevised = doc.GetValue(ocf9Keys.OCF9_Header_DateRevised).ToHcaiDate();
                rowSubmitted.AdditionalComments = doc.GetValue(ocf9Keys.AdditionalComments);
                rowSubmitted.OCFVersion = doc.GetValue(ocf9Keys.OCFVersion).ToInt32();
                rowSubmitted.AttachmentsBeingSent = doc.GetValue(ocf9Keys.AttachmentsBeingSent).ToBool();

                // items
                var oList = doc.getList(LineItemType.GS9LineItem);
                if (oList?.Count > 0)
                {
                    NewMessageEvent($"  GS9LineItemAR[{oList.Count}]");
                    for (var i = 0; i < oList.Count; i++)
                    {
                        var tmpItem = (IDataItem)oList[i];
                        var itemCode = tmpItem.GetValue(ocf9LiKeys.OCF9_GoodsAndServices_Items_Item_Code);

                        var gs9LineItemAr = rowSubmitted.GS9LineItemAR
                                .FirstOrDefault(g => g.OCF9_GoodsAndServices_Items_Item_Code == itemCode)
                         ?? _dbContext.GS9LineItemAR.Add(new GS9LineItemAR
                         {
                             OCF9AR = rowSubmitted,
                             OCF9_HCAI_Document_Number = documentNumber,
                             OCF9_GoodsAndServices_Items_Item_Code = itemCode,
                         });

                        gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_InterestPayable =
                            tmpItem.GetValue(ocf9LiKeys.OCF9_GoodsAndServices_Items_Item_InterestPayable).ToDecimalNullable();
                        gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_ReferenceNumber =
                            tmpItem.GetValue(ocf9LiKeys.OCF9_GoodsAndServices_Items_Item_ReferenceNumber);
                    }
                }
            }

            // save
            _dbContext.SaveChangesWithDetect();
        }
    }
}
