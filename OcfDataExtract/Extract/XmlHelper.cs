﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using static System.StringComparison;

namespace OcfDataExtract.Extract
{
    public static class XmlHelper
    {
        public static XElement ElementByLocalName(this XElement element, string name, bool isStrong = true)
        {
            if (element is null) throw new ArgumentNullException(nameof(element));
            if (name is null) throw new ArgumentNullException(nameof(name));

            var result = element.Elements()
                .FirstOrDefault(e => name.Equals(e.Name.LocalName, CurrentCultureIgnoreCase));
            if (result is null && isStrong)
                throw new Exception($"Element '{element.Name}' don`t contain node '{name}'");
            return result;
        }

        public static IEnumerable<XElement> ElementsByLocalName(this XElement element, string name)
        {
            if (element is null) throw new ArgumentNullException(nameof(element));
            if (name is null) throw new ArgumentNullException(nameof(name));

            return element.Elements()
                .Where(e => name.Equals(e.Name.LocalName, CurrentCultureIgnoreCase))
                .ToArray();
        }

        public static XAttribute AttributeByLocalName(this XElement element, string name, bool isStrong = true)
        {
            if (element is null) throw new ArgumentNullException(nameof(element));
            if (name is null) throw new ArgumentNullException(nameof(name));

            var result = element.Attributes()
                .FirstOrDefault(e => name.Equals(e.Name.LocalName, CurrentCultureIgnoreCase));
            if (result is null && isStrong)
                throw new Exception($"Element '{element.Name}' don`t contain attribute '{name}'");
            return result;
        }
    }
}
