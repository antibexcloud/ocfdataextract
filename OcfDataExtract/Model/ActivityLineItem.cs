﻿namespace OcfDataExtract.Model
{
    public class ActivityLineItem
    {
        public string HCAI_Document_Number { get; set; }

        public string PMS_Document_Key{ get; set; }
        public string PMS_Patient_Key{ get; set; }

        public string Submission_Source{ get; set; }
        public string Submission_Date{ get; set; }

        public string Document_Status{ get; set; }
        public string Document_Type { get; set; }
    }
}
