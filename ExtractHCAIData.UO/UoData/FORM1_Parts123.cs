//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtractHcaiData.Uo.UoData
{
    using System;
    using System.Collections.Generic;
    
    public partial class FORM1_Parts123
    {
        public double ReportID { get; set; }
        public byte gPartNo { get; set; }
        public int gRowOrder { get; set; }
        public int NumberOfMin { get; set; }
        public int TxWeeks { get; set; }
        public string ACSItemID { get; set; }
        public Nullable<double> AttendantCareServices_Item_Approved_Minutes { get; set; }
        public Nullable<double> AttendantCareServices_Item_Approved_TimesPerWeek { get; set; }
        public Nullable<double> AttendantCareServices_Item_Approved_TotalMinutes { get; set; }
        public string AttendantCareServices_Item_Approved_ReasonCode { get; set; }
        public string AttendantCareServices_Item_Approved_ReasonDescription { get; set; }
        public Nullable<byte> AttendantCareServices_Item_Approved_IsItemDeclined { get; set; }
    }
}
