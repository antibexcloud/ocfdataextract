﻿if (SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OCF23' AND TABLE_SCHEMA='dbo' AND COLUMN_NAME = 'PAFItem1Description') <> 200
  alter table [dbo].[OCF23] alter column [PAFItem1Description] nvarchar(200)
GO
if (SELECT CHARACTER_MAXIMUM_LENGTH FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OCF23' AND TABLE_SCHEMA='dbo' AND COLUMN_NAME = 'PAFItem2Description') <> 200
  alter table [dbo].[OCF23] alter column [PAFItem2Description] nvarchar(200)
GO
