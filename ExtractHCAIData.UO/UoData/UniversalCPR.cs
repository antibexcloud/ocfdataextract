﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;

namespace ExtractHcaiData.Uo.UoData
{
    public partial class UniversalCPR
    {
        private const bool AutoDetectChanges = false;

        public bool Inited { get; }
        public string Name { get; private set; }

        private UniversalCPR(string nameOrConnectionString) : base(nameOrConnectionString)
        {
            try
            {
                Configuration.AutoDetectChangesEnabled = AutoDetectChanges;
                Inited = true;
            }
            catch (Exception ex)
            {
                Inited = false;
                throw;
            }
        }

        private static UniversalCPR Open(string serverName, string dbName, bool integratedSecurity, string login, string password)
        {
            var entityBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder
            {
                ProviderConnectionString = $"data source={serverName};initial catalog={dbName};MultipleActiveResultSets=True;App=EntityFramework;",
                Provider = "System.Data.SqlClient",
                Metadata = @"res://*/UoData.UoModel.csdl|res://*/UoData.UoModel.ssdl|res://*/UoData.UoModel.msl"
            };

            entityBuilder.ProviderConnectionString +=
                integratedSecurity
                    ? "Integrated Security=True;Persist Security Info=False"
                    : $"Persist security info=True;User id={login};Password={password};";

            var connectionString = entityBuilder.ConnectionString;

            return new UniversalCPR(connectionString) {Name = dbName};
        }

        public static UniversalCPR OpenFromSqlConnection(string connectString)
        {
            try
            {
                // prepare data
                var builder = new SqlConnectionStringBuilder(connectString);

                var name = builder.InitialCatalog;
                var serverName = builder.DataSource;
                var login = builder.UserID;
                var password = builder.Password;
                var integratedSecurity = builder.IntegratedSecurity;

                // open
                return Open(serverName, name, integratedSecurity, login, password);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SaveChangesWithDetect(DbContextTransaction transaction = null)
        {
            if (!AutoDetectChanges)
            {
                ChangeTracker.DetectChanges();
            }

            SaveChanges();
            if (transaction != null)
            {
                transaction.Commit();
                transaction.Dispose();
            }
        }

        public void CancelChanges(DbContextTransaction transaction = null)
        {
            if (transaction != null)
            {
                transaction.Rollback();
                transaction.Dispose();
            }

            if (!AutoDetectChanges)
            {
                ChangeTracker.DetectChanges();
            }

            foreach (var entry in this.ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    // Under the covers, changing the state of an entity from  
                    // Modified to Unchanged first sets the values of all  
                    // properties to the original values that were read from  
                    // the database when it was queried, and then marks the  
                    // entity as Unchanged. This will also reject changes to  
                    // FK relationships since the original value of the FK  
                    // will be restored. 
                    case EntityState.Modified:
                        entry.State = EntityState.Unchanged;
                        break;

                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;

                    // If the EntityState is the Deleted, reload the date from the database.   
                    case EntityState.Deleted:
                        entry.Reload();
                        break;

                    default: break;
                }
            }
        }
    }
}
