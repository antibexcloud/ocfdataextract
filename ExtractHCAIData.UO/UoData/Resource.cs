//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtractHcaiData.Uo.UoData
{
    using System;
    using System.Collections.Generic;
    
    public partial class Resource
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Resource()
        {
            this.Resource_Activity_Relation = new HashSet<Resource_Activity_Relation>();
        }
    
        public string resource_id { get; set; }
        public Nullable<int> staff_id { get; set; }
        public string staff_name { get; set; }
        public Nullable<short> status { get; set; }
        public string type { get; set; }
        public string resource_name { get; set; }
        public Nullable<int> max_occupancy { get; set; }
        public string ColRegType1 { get; set; }
        public Nullable<short> ColRegRegulated1 { get; set; }
        public string ColRegNo1 { get; set; }
        public string ColRegHrRate1 { get; set; }
        public string ColRegServiceType1 { get; set; }
        public string ColRegSignature1 { get; set; }
        public string ColRegType2 { get; set; }
        public Nullable<short> ColRegRegulated2 { get; set; }
        public string ColRegNo2 { get; set; }
        public string ColRegHrRate2 { get; set; }
        public string ColRegServiceType2 { get; set; }
        public string ColRegSignature2 { get; set; }
        public string ColRegType3 { get; set; }
        public Nullable<short> ColRegRegulated3 { get; set; }
        public string ColRegNo3 { get; set; }
        public string ColRegHrRate3 { get; set; }
        public string ColRegServiceType3 { get; set; }
        public string ColRegSignature3 { get; set; }
        public string wsibBillingNo { get; set; }
        public string wsibProviderID { get; set; }
        public string ohipPractitionerNo { get; set; }
        public string ohipCode { get; set; }
        public string ohipLocation { get; set; }
        public string ServiceType { get; set; }
        public string HCAIRegNo { get; set; }
        public byte chkCellWidth { get; set; }
        public double txtCellWidth { get; set; }
        public int Id { get; set; }
        public byte AllowToOnline { get; set; }
        public int OB { get; set; }
        public bool TimeUtilization { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Resource_Activity_Relation> Resource_Activity_Relation { get; set; }
        public virtual Staff Staff { get; set; }
    }
}
