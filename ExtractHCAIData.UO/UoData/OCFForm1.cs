//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtractHcaiData.Uo.UoData
{
    using System;
    using System.Collections.Generic;
    
    public partial class OCFForm1
    {
        public double ReportID { get; set; }
        public string CIAddress { get; set; }
        public string CIClaimNo { get; set; }
        public string CIPolicyNo { get; set; }
        public string CIDOL { get; set; }
        public string AssDate { get; set; }
        public Nullable<short> AssFirst { get; set; }
        public string AssDOL { get; set; }
        public string AssMonthlyAllowance { get; set; }
        public string IIName { get; set; }
        public string IIAddress { get; set; }
        public string IIPolicyHolder { get; set; }
        public string HPName { get; set; }
        public string HPID { get; set; }
        public string HPFacility { get; set; }
        public string HPAddress { get; set; }
        public string HPTel { get; set; }
        public string RHPName { get; set; }
        public string RHPID { get; set; }
        public string RHPOccupation { get; set; }
        public string RHPCollegeRegNo { get; set; }
        public string RHPFacility { get; set; }
        public string RHPAISINumber { get; set; }
        public string RHPAddress { get; set; }
        public string RHPTel { get; set; }
        public string RHPFax { get; set; }
        public string Part1Row1Time { get; set; }
        public string Part1Row1PerWeek { get; set; }
        public string Part1Row2Time { get; set; }
        public string Part1Row2PerWeek { get; set; }
        public string Part1Row3Time { get; set; }
        public string Part1Row3PerWeek { get; set; }
        public string Part1Row4Time { get; set; }
        public string Part1Row4PerWeek { get; set; }
        public string Part1Row5Time { get; set; }
        public string Part1Row5PerWeek { get; set; }
        public string Part1Row6Time { get; set; }
        public string Part1Row6PerWeek { get; set; }
        public string Part1Row7Time { get; set; }
        public string Part1Row7PerWeek { get; set; }
        public string Part1Row8Time { get; set; }
        public string Part1Row8PerWeek { get; set; }
        public string Part1Row9Time { get; set; }
        public string Part1Row9PerWeek { get; set; }
        public string Part1Row10Time { get; set; }
        public string Part1Row10PerWeek { get; set; }
        public string Part1Row11Time { get; set; }
        public string Part1Row11PerWeek { get; set; }
        public string Part1Row12Time { get; set; }
        public string Part1Row12PerWeek { get; set; }
        public string Part1Row13Time { get; set; }
        public string Part1Row13PerWeek { get; set; }
        public string Part1Row14Time { get; set; }
        public string Part1Row14PerWeek { get; set; }
        public string Part1Row15Time { get; set; }
        public string Part1Row15PerWeek { get; set; }
        public string Part1Row16Time { get; set; }
        public string Part1Row16PerWeek { get; set; }
        public string Part1Row17Time { get; set; }
        public string Part1Row17PerWeek { get; set; }
        public string Part1Row18Time { get; set; }
        public string Part1Row18PerWeek { get; set; }
        public string Part1Row19Time { get; set; }
        public string Part1Row19PerWeek { get; set; }
        public string Part1Row20Time { get; set; }
        public string Part1Row20PerWeek { get; set; }
        public string Part1Row21Time { get; set; }
        public string Part1Row21PerWeek { get; set; }
        public string Part1Row22Time { get; set; }
        public string Part1Row22PerWeek { get; set; }
        public string Part1Row23Time { get; set; }
        public string Part1Row23PerWeek { get; set; }
        public string Part1Row24Time { get; set; }
        public string Part1Row24PerWeek { get; set; }
        public string Part2Row1Time { get; set; }
        public string Part2Row1PerWeek { get; set; }
        public string Part2Row2Time { get; set; }
        public string Part2Row2PerWeek { get; set; }
        public string Part2Row3Time { get; set; }
        public string Part2Row3PerWeek { get; set; }
        public string Part2Row4Time { get; set; }
        public string Part2Row4PerWeek { get; set; }
        public string Part2Row5Time { get; set; }
        public string Part2Row5PerWeek { get; set; }
        public string Part2Row6Time { get; set; }
        public string Part2Row6PerWeek { get; set; }
        public string Part2Row7Time { get; set; }
        public string Part2Row7PerWeek { get; set; }
        public string Part2Row8Time { get; set; }
        public string Part2Row8PerWeek { get; set; }
        public string Part2Row9Time { get; set; }
        public string Part2Row9PerWeek { get; set; }
        public string Part2Row10Time { get; set; }
        public string Part2Row10PerWeek { get; set; }
        public string Part3Row1TimeP1 { get; set; }
        public string Part3Row1PerWeekP1 { get; set; }
        public string Part3Row2TimeP1 { get; set; }
        public string Part3Row2PerWeekP1 { get; set; }
        public string Part3Row3TimeP1 { get; set; }
        public string Part3Row3PerWeekP1 { get; set; }
        public string Part3Row4TimeP1 { get; set; }
        public string Part3Row4PerWeekP1 { get; set; }
        public string Part3Row5TimeP1 { get; set; }
        public string Part3Row5PerWeekP1 { get; set; }
        public string Part3Row6TimeP1 { get; set; }
        public string Part3Row6PerWeekP1 { get; set; }
        public string Part3Row7TimeP1 { get; set; }
        public string Part3Row7PerWeekP1 { get; set; }
        public string Part3Row8TimeP1 { get; set; }
        public string Part3Row8PerWeekP1 { get; set; }
        public string Part3Row9TimeP1 { get; set; }
        public string Part3Row9PerWeekP1 { get; set; }
        public string Part3Row10TimeP1 { get; set; }
        public string Part3Row10PerWeekP1 { get; set; }
        public string Part3Row11TimeP1 { get; set; }
        public string Part3Row11PerWeekP1 { get; set; }
        public string Part3Row12TimeP1 { get; set; }
        public string Part3Row12PerWeekP1 { get; set; }
        public string Part3Row13TimeP1 { get; set; }
        public string Part3Row13PerWeekP1 { get; set; }
        public string Part3Row14TimeP1 { get; set; }
        public string Part3Row14PerWeekP1 { get; set; }
        public string Part3Row15TimeP1 { get; set; }
        public string Part3Row15PerWeekP1 { get; set; }
        public string Part3Row16TimeP1 { get; set; }
        public string Part3Row16PerWeekP1 { get; set; }
        public string Part3Row17TimeP1 { get; set; }
        public string Part3Row17PerWeekP1 { get; set; }
        public string Part3Row18TimeP1 { get; set; }
        public string Part3Row18PerWeekP1 { get; set; }
        public string Part3Row19TimeP1 { get; set; }
        public string Part3Row19PerWeekP1 { get; set; }
        public string Part3Row20TimeP1 { get; set; }
        public string Part3Row20PerWeekP1 { get; set; }
        public string Part3Row21TimeP1 { get; set; }
        public string Part3Row21PerWeekP1 { get; set; }
        public string Part3Row22TimeP1 { get; set; }
        public string Part3Row22PerWeekP1 { get; set; }
        public string Part3Row23TimeP1 { get; set; }
        public string Part3Row23PerWeekP1 { get; set; }
        public string Part3Row24TimeP1 { get; set; }
        public string Part3Row24PerWeekP1 { get; set; }
        public string Part3Row25TimeP1 { get; set; }
        public string Part3Row25PerWeekP1 { get; set; }
        public string Part3Row26TimeP1 { get; set; }
        public string Part3Row26PerWeekP1 { get; set; }
        public string Part3Row27TimeP1 { get; set; }
        public string Part3Row27PerWeekP1 { get; set; }
        public string Part3Row28TimeP1 { get; set; }
        public string Part3Row28PerWeekP1 { get; set; }
        public string Part3Row1TimeP2 { get; set; }
        public string Part3Row1PerWeekP2 { get; set; }
        public string Part3Row2TimeP2 { get; set; }
        public string Part3Row2PerWeekP2 { get; set; }
        public string Part3Row3TimeP2 { get; set; }
        public string Part3Row3PerWeekP2 { get; set; }
        public string Part3Row4TimeP2 { get; set; }
        public string Part3Row4PerWeekP2 { get; set; }
        public string Part3Row5TimeP2 { get; set; }
        public string Part3Row5PerWeekP2 { get; set; }
        public string Part3Row6TimeP2 { get; set; }
        public string Part3Row6PerWeekP2 { get; set; }
        public string Part3Row7TimeP2 { get; set; }
        public string Part3Row7PerWeekP2 { get; set; }
        public string Part3Row8TimeP2 { get; set; }
        public string Part3Row8PerWeekP2 { get; set; }
        public string Part3Row9TimeP2 { get; set; }
        public string Part3Row9PerWeekP2 { get; set; }
        public string Part3Row10TimeP2 { get; set; }
        public string Part3Row10PerWeekP2 { get; set; }
        public string Part3Row11TimeP2 { get; set; }
        public string Part3Row11PerWeekP2 { get; set; }
        public string Part3Row12TimeP2 { get; set; }
        public string Part3Row12PerWeekP2 { get; set; }
        public string Part3Row13TimeP2 { get; set; }
        public string Part3Row13PerWeekP2 { get; set; }
        public string Part3Row14TimeP2 { get; set; }
        public string Part3Row14PerWeekP2 { get; set; }
        public string Part3Row15TimeP2 { get; set; }
        public string Part3Row15PerWeekP2 { get; set; }
        public string Part3Row16TimeP2 { get; set; }
        public string Part3Row16PerWeekP2 { get; set; }
        public string Part3Row17TimeP2 { get; set; }
        public string Part3Row17PerWeekP2 { get; set; }
        public string Part3Row18TimeP2 { get; set; }
        public string Part3Row18PerWeekP2 { get; set; }
        public string Part3Row19TimeP2 { get; set; }
        public string Part3Row19PerWeekP2 { get; set; }
        public string Part3Row20TimeP2 { get; set; }
        public string Part3Row20PerWeekP2 { get; set; }
        public string Part3Row21TimeP2 { get; set; }
        public string Part3Row21PerWeekP2 { get; set; }
        public string Part3Row22TimeP2 { get; set; }
        public string Part3Row22PerWeekP2 { get; set; }
        public string Part3Row23TimeP2 { get; set; }
        public string Part3Row23PerWeekP2 { get; set; }
        public string Part3Row24TimeP2 { get; set; }
        public string Part3Row24PerWeekP2 { get; set; }
        public string Part3Row25TimeP2 { get; set; }
        public string Part3Row25PerWeekP2 { get; set; }
        public string Part3Row26TimeP2 { get; set; }
        public string Part3Row26PerWeekP2 { get; set; }
    
        public virtual PaperWork PaperWork { get; set; }
    }
}
