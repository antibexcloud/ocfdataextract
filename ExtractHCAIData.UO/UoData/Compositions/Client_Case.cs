﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExtractHcaiData.Uo.UoData
{
    public partial class Client_Case
    {
        [NotMapped]
        public string PureCaseId
        {
            get
            {
                return CaseID.Contains("_")
                    ? CaseID.Split(new[] { "_" }, StringSplitOptions.None)[0]
                    : CaseID;
            }
        }
    }
}