//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExtractHcaiData.Uo.UoData
{
    using System;
    using System.Collections.Generic;
    
    public partial class ClientCaseCharges_Log
    {
        public decimal RecordID { get; set; }
        public System.DateTime RecordDateTime { get; set; }
        public decimal ChargeID { get; set; }
        public string CaseID { get; set; }
        public Nullable<System.DateTime> ChargeDate { get; set; }
        public string ItemID { get; set; }
        public string ItemDescription { get; set; }
        public Nullable<int> ItemQty { get; set; }
        public Nullable<double> ItemRate { get; set; }
        public Nullable<double> GST { get; set; }
        public Nullable<double> PST { get; set; }
        public string ItemServiceCode { get; set; }
        public string ItemMeasure { get; set; }
        public string ItemProviderID { get; set; }
        public string ItemAttribute { get; set; }
        public Nullable<double> InvoiceID { get; set; }
        public Nullable<double> ReportID { get; set; }
        public Nullable<short> BilledToStatus { get; set; }
        public string ChargeType { get; set; }
        public Nullable<double> BookID { get; set; }
        public Nullable<decimal> BlockID { get; set; }
        public string OHIPDiagCode { get; set; }
        public string OHIPRefDrNumber { get; set; }
        public Nullable<byte> OHIPManualReview { get; set; }
        public string OHIPStatus { get; set; }
        public Nullable<double> OHIPPaidAmt { get; set; }
        public Nullable<System.DateTime> OHIPFileDate { get; set; }
        public Nullable<int> OHIPaccountingNumber { get; set; }
        public Nullable<System.DateTime> OHIPAccountDate { get; set; }
        public string OHIPFileID { get; set; }
        public Nullable<System.DateTime> LastModifiedDate { get; set; }
        public string UserName { get; set; }
        public string ComputerName { get; set; }
        public string EventType { get; set; }
    }
}
