﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;
using ExtractHcaiData.Uo.Dialogs;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Sql.Config;
using ExtractHcaiData.Uo.ThreadWork;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using ExtractHcaiData.Uo.Work;
using HcaiSync.Common.Sql;

namespace ExtractHcaiData.Uo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Commands

        private static RoutedUICommand CmdSrcSetConn { get; } = new("SrcSetConn", "CmdSrcSetConn", typeof(MainWindow));

        private static RoutedUICommand CmdDstSetConn { get; } = new("DstSetConn", "CmdDstSetConn", typeof(MainWindow));

        private static RoutedUICommand CmdSrcCheckDbConn { get; } = new("Check", "CmdSrcCheckConn", typeof(MainWindow));

        private static RoutedUICommand CmdDstCheckConn { get; } = new("Check", "CmdDstCheckConn", typeof(MainWindow));

        private static RoutedUICommand CmdTouchUps { get; } = new("UO TouchUps", "CmdTouchups", typeof(MainWindow));

        private static RoutedUICommand CmdModifySync { get; } = new("Modify HCAI Data Before Transfer", "CmdModifySync", typeof(MainWindow));

        private static RoutedUICommand CmdAnalyzeSync { get; } = new("Analyze HCAI Data", "CmdAnalyzeSync", typeof(MainWindow));

        private static RoutedUICommand CmdTrBusinessCenter { get; } = new("1. Transfer Business Center", "CmdTrBusinessCenter", typeof(MainWindow));

        private static RoutedUICommand CmdTrProviders { get; } = new("2. Transfer Providers", "CmdTrProviders", typeof(MainWindow));

        private static RoutedUICommand CmdTrInsurers { get; } = new("3. Transfer Insurers", "CmdTrInsurers", typeof(MainWindow));

        private static RoutedUICommand CmdTrActivities { get; } = new("4. Transfer Activities", "CmdTrActivities", typeof(MainWindow));

        private static RoutedUICommand CmdTrClients { get; } = new("5. Transfer Patients && Case", "CmdTrClients", typeof(MainWindow));

        private static RoutedUICommand CmdTrOcf18 { get; } = new("Transfer OCF18", "CmdTrOcf18", typeof(MainWindow));

        private static RoutedUICommand CmdTrOcf23 { get; } = new("Transfer OCF23", "CmdTrOcf23", typeof(MainWindow));

        private static RoutedUICommand CmdTrOcf21B { get; } = new("Transfer OCF21B", "CmdTrOcf21B", typeof(MainWindow));

        private static RoutedUICommand CmdTrOcf21C { get; } = new("Transfer OCF21C", "CmdTrOcf21C", typeof(MainWindow));

        private static RoutedUICommand CmdTrForm1 { get; } = new("Transfer Form1", "CmdTrForm1", typeof(MainWindow));

        private static RoutedUICommand CmdTrAcsi { get; } = new("Transfer ACSI", "CmdTrAcsi", typeof(MainWindow));

        private static RoutedUICommand CmdTransferAll { get; } = new("TRANSFER ALL", "CmdTransferAll", typeof(MainWindow));

        private void SetCommands()
        {
            CommandBindings.Add(new CommandBinding(CmdSrcSetConn, CmdSrcSetConnOnExecute, CmdSrcSetConnOnCanExecute));
            SrcSetupDbConnButton.Command = CmdSrcSetConn;
            CommandBindings.Add(new CommandBinding(CmdSrcCheckDbConn, CmdSrcCheckDbConnOnExecute, CmdSrcCheckDbConnOnCanExecute));
            SrcCheckDbConnButton.Command = CmdSrcCheckDbConn;

            CommandBindings.Add(new CommandBinding(CmdDstSetConn, CmdDstSetConnOnExecute, CmdDstSetConnOnCanExecute));
            DstSetupConnButton.Command = CmdDstSetConn;
            CommandBindings.Add(new CommandBinding(CmdDstCheckConn, CmdDstCheckConnOnExecute, CmdDstCheckConnOnCanExecute));
            DstCheckConnButton.Command = CmdDstCheckConn;

            //
            CommandBindings.Add(new CommandBinding(CmdAnalyzeSync, CmdAnalyzeSyncOnExecute, CmdTransferCommonOnCanExecute));
            AnalizeDataButton.Command = CmdAnalyzeSync;
            CommandBindings.Add(new CommandBinding(CmdModifySync, CmdModifySyncOnExecute, CmdSyncOnCanExecute));
            ModifyDataButton.Command = CmdModifySync;
            CommandBindings.Add(new CommandBinding(CmdTouchUps, CmdTouchupsOnExecute, CmdTargetOnCanExecute));
            TouchupsButton.Command = CmdTouchUps;

            //
            CommandBindings.Add(new CommandBinding(CmdTrBusinessCenter, CmdTrBusinessCenterOnExecute, CmdTransferCommonOnCanExecute));
            TrBusinessCenterButton.Command = CmdTrBusinessCenter;
            CommandBindings.Add(new CommandBinding(CmdTrProviders, CmdTrProvidersOnExecute, CmdTransferProvidersOnCanExecute));
            TrProvidersButton.Command = CmdTrProviders;
            CommandBindings.Add(new CommandBinding(CmdTrInsurers, CmdTrInsurersOnExecute, CmdTransferInsurersOnCanExecute));
            TrInsurersButton.Command = CmdTrInsurers;
            CommandBindings.Add(new CommandBinding(CmdTrActivities, CmdTrActivitiesOnExecute, CmdTransferActivitiesOnCanExecute));
            TrActivitiesButton.Command = CmdTrActivities;
            CommandBindings.Add(new CommandBinding(CmdTrClients, CmdTrClientsOnExecute, CmdTransferClientsOnCanExecute));
            TrClientsButton.Command = CmdTrClients;

            //
            CommandBindings.Add(new CommandBinding(CmdTrOcf18, CmdTrOcf18OnExecute, CmdTransferDoc18OnCanExecute));
            TrOcf18Button.Command = CmdTrOcf18;
            CommandBindings.Add(new CommandBinding(CmdTrOcf23, CmdTrOcf23OnExecute, CmdTransferDoc23OnCanExecute));
            TrOcf23Button.Command = CmdTrOcf23;
            CommandBindings.Add(new CommandBinding(CmdTrOcf21B, CmdTrOcf21BOnExecute, CmdTransferDoc21bOnCanExecute));
            TrOcf21BButton.Command = CmdTrOcf21B;
            CommandBindings.Add(new CommandBinding(CmdTrOcf21C, CmdTrOcf21COnExecute, CmdTransferDoc21cOnCanExecute));
            TrOcf21CButton.Command = CmdTrOcf21C;
            CommandBindings.Add(new CommandBinding(CmdTrForm1, CmdTrForm1OnExecute, CmdTransferDocForm1OnCanExecute));
            TrForm1Button.Command = CmdTrForm1;
            CommandBindings.Add(new CommandBinding(CmdTrAcsi, CmdTrAcsiOnExecute, CmdTransferDocAcsiOnCanExecute));
            TrAcsiButton.Command = CmdTrAcsi;

            CommandBindings.Add(new CommandBinding(CmdTransferAll, CmdTransferAllOnExecute, CmdTransferDocAllOnCanExecute));
            TransferAllButton.Command = CmdTransferAll;
        }

        #endregion

        #region private data

        private bool _inited;

        #endregion

        #region Properies

        public static ConfigurationMy Configuration;

        public MainVm VmMain { get; }

        public TransferWork _transfer;

        public bool IsCreatePayments { get; private set; }

        #endregion

        #region Life circle

        public MainWindow()
        {
            InitializeComponent();
            ModalDialog.SetParent(MainGrid);

            Configuration = new ConfigurationMy(typeof(MainWindow).Assembly);
            VmMain = new MainVm(this);
            Logger.Vm = VmMain;

            Height = SystemParameters.WorkArea.Height * 0.9;
            Width = SystemParameters.WorkArea.Width * 0.85;

            VmMain.ProgressVisibility = Visibility.Collapsed;
        }

        private void MainWindowOnLoaded(object sender, RoutedEventArgs e)
        {
            if (_inited) return;
            _inited = true;

            VmMain.Init();
            DataContext = VmMain;
            VmMain.SendChangedAll();

            SetCommands();
            CommandsRefresh();
            SetLogMax();

            ThreadManager.ExecuteAction(null);
        }

        private void MainWindowOnClosing(object sender, CancelEventArgs e)
        {
            if (VmMain.InProcess)
            {
                e.Cancel = true;
                return;
            }

            VmMain.SaveConfig();

            VmMain.SyncDb.Close();
            VmMain.TargetDb.Close();
            Logger.Close();
        }

        private void MainWindowOnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetLogMax();
        }

        public void CommandsRefresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        private void SetLogMax()
        {
            var lines = (int)(LogTextBox.ActualHeight / 14.5) - 1;
            VmMain.MaxMsgLines = lines;
        }

        #endregion

        #region Service

        private void ProgressShow()
        {
            WaitingBox.Visibility = Visibility.Visible;
            CommandsRefresh();
        }

        public void ProgressHide()
        {
            WaitingBox.Visibility = Visibility.Collapsed;
            CommandsRefresh();
        }

        private void ButtonCancelOnClick(object sender, RoutedEventArgs e)
        {
            VmMain.IsCanceled = true;
        }

        public bool CheckConnection(string connString, bool silent = false)
        {
            if (string.IsNullOrEmpty(connString)) return false;
            if (!AbstractDataBaseConnection.CheckConnection(connString, out var error))
            {
                if (!silent)
                    ModalDialog.ShowHandlerMessage(error);
                return false;
            }
            return true;
        }

        private void MainWaitCanvasOnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var vis = (bool)e.NewValue;

            var res = this.Resources["WaitStoryboard"];
            var storyboard = res as Storyboard;
            if (storyboard is null) return;

            if (vis)
                storyboard.Begin();
            else
                storyboard.Remove();
        }

        #endregion

        #region CanExecute

        private bool BaseReady => IsLoaded && VmMain != null;

        private void CmdSrcSetConnOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && (!VmMain.SyncDb.Inited || !VmMain.TargetDb.Inited);
        }

        private void CmdDstSetConnOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && (!VmMain.SyncDb.Inited || !VmMain.TargetDb.Inited);
        }


        private void CmdSrcCheckDbConnOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && !string.IsNullOrEmpty(VmMain.DbSyncConnectionString)
                           && (!VmMain.SyncDb.Inited || !VmMain.TargetDb.Inited);
        }

        private void CmdDstCheckConnOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && !string.IsNullOrEmpty(VmMain.DbTargetConnectionString)
                           && (!VmMain.SyncDb.Inited || !VmMain.TargetDb.Inited);
        }


        private void CmdSyncOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && (!VmMain.SyncDb.ReadyActivities || !VmMain.SyncDb.ReadyClients);
        }

        private void CmdTargetOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsTargetConnectReady
                           && (Configuration.AvralMode
                               || (VmMain.SyncDb.ReadyOcf18
                                   && VmMain.SyncDb.ReadyOcf23
                                   && VmMain.SyncDb.ReadyOcf21b
                                   && VmMain.SyncDb.ReadyOcf21c
                                   && VmMain.SyncDb.ReadyForm1
                                   && VmMain.SyncDb.ReadyAcsi))
                           && (!VmMain.TargetDb.ReadyOcf18PlanNo || !VmMain.TargetDb.ReadyOcf23PlanNo);
        }


        private void CmdTransferCommonOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady && VmMain.IsTargetConnectReady;
        }

        private void CmdTransferProvidersOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && !VmMain.SyncDb.ReadyProviders;
        }

        private void CmdTransferInsurersOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && !VmMain.SyncDb.ReadyInsurers;
        }

        private void CmdTransferActivitiesOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && !VmMain.SyncDb.ReadyActivities;
        }

        private void CmdTransferClientsOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyInsurers
                           && !VmMain.SyncDb.ReadyClients;
        }

        private void CmdTransferDoc18OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyProviders
                           && VmMain.SyncDb.ReadyInsurers
                           && VmMain.SyncDb.ReadyActivities
                           && VmMain.SyncDb.ReadyClients
                           && !VmMain.SyncDb.ReadyOcf18;
        }

        private void CmdTransferDoc23OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyProviders
                           && VmMain.SyncDb.ReadyInsurers
                           && VmMain.SyncDb.ReadyActivities
                           && VmMain.SyncDb.ReadyClients
                           && !VmMain.SyncDb.ReadyOcf23;
        }

        private void CmdTransferDoc21bOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyProviders
                           && VmMain.SyncDb.ReadyInsurers
                           && VmMain.SyncDb.ReadyActivities
                           && VmMain.SyncDb.ReadyClients
                           && !VmMain.SyncDb.ReadyOcf21b;
        }

        private void CmdTransferDoc21cOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyProviders
                           && VmMain.SyncDb.ReadyInsurers
                           && VmMain.SyncDb.ReadyActivities
                           && VmMain.SyncDb.ReadyClients
                           && !VmMain.SyncDb.ReadyOcf21c;
        }

        private void CmdTransferDocForm1OnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyProviders
                           && VmMain.SyncDb.ReadyInsurers
                           && VmMain.SyncDb.ReadyActivities
                           && VmMain.SyncDb.ReadyClients
                           && !VmMain.SyncDb.ReadyForm1;
        }

        private void CmdTransferDocAcsiOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && VmMain.SyncDb.ReadyProviders
                           && VmMain.SyncDb.ReadyInsurers
                           && VmMain.SyncDb.ReadyActivities
                           && VmMain.SyncDb.ReadyClients
                           && VmMain.SyncDb.ReadyForm1
                           && !VmMain.SyncDb.ReadyAcsi;
        }

        private void CmdTransferDocAllOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady
                           && !VmMain.InProcess
                           && VmMain.IsSyncConnectReady
                           && VmMain.IsTargetConnectReady
                           && (!VmMain.SyncDb.ReadyActivities
                               || !VmMain.SyncDb.ReadyClients
                               || !VmMain.SyncDb.ReadyProviders
                               || !VmMain.SyncDb.ReadyInsurers
                               || !VmMain.SyncDb.ReadyOcf18
                               || !VmMain.SyncDb.ReadyOcf23
                               || !VmMain.SyncDb.ReadyOcf21b
                               || !VmMain.SyncDb.ReadyOcf21c
                               || !VmMain.SyncDb.ReadyForm1
                               || !VmMain.SyncDb.ReadyAcsi);
        }

        #endregion

        #region Execute

        #region Service

        private void CmdSrcCheckDbConnOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            if (CheckConnection(VmMain.DbSyncConnectionString))
                ModalDialog.ShowHandlerMessage("Ok!");
        }

        private void CmdDstCheckConnOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            if (CheckConnection(VmMain.DbTargetConnectionString))
                ModalDialog.ShowHandlerMessage("Ok!");
        }


        private void CmdSrcSetConnOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new SqlServerEngineConfigurator();
            if (dlg.ShowDialog(VmMain.DbSyncConnectionString, out var res))
                VmMain.DbSyncConnectionString = res;
        }

        private void CmdDstSetConnOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new SqlServerEngineConfigurator();
            if (dlg.ShowDialog(VmMain.DbTargetConnectionString, out var res))
                VmMain.DbTargetConnectionString = res;
        }
        #endregion


        private void CmdAnalyzeSyncOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            ProgressShow();
            ThreadPool.QueueUserWorkItem(obj =>
            {
                try
                {
                    _transfer ??= new TransferWork(this);

                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        ProgressHide();

                        var dlg = new DocAnalyzeDialog { DataContext = VmMain };
                        dlg.ShowDialog();

                        CommandsRefresh();
                    });
                }
                catch (Exception ex)
                {
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        ProgressHide();
                        CommandsRefresh();
                        MessageBox.Show(Service.InworkException(ex));
                    });
                }
            });
        }

        private void CmdModifySyncOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            ProgressShow();
            ThreadPool.QueueUserWorkItem(obj =>
            {
                try
                {
                    _transfer ??= new TransferWork(this);

                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        ProgressHide();

                        _transfer.PrepareSyncProviders();
                        var dlg = new SyncPrepareWindow { DataContext = VmMain };
                        dlg.ShowDialog();

                        _transfer.CheckReadiness();
                        CommandsRefresh();
                    });
                }
                catch (Exception ex)
                {
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        ProgressHide();
                        CommandsRefresh();
                        MessageBox.Show(Service.InworkException(ex));
                    });
                }
            });
        }

        #region transfer

        private void CmdTrBusinessCenterOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.BusinessCenter);
        }

        private void CmdTrProvidersOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Providers);
        }

        private void CmdTrInsurersOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Insurers);
        }

        private void CmdTrActivitiesOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Activities);
        }

        private void CmdTrClientsOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Clients);
        }

        private void CmdTrOcf18OnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Ocf18);
        }

        private void CmdTrOcf23OnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Ocf23);
        }

        private void CmdTrOcf21BOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Ocf21B);
        }

        private void CmdTrOcf21COnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Ocf21C);
        }

        private void CmdTrForm1OnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Form1);
        }

        private void CmdTrAcsiOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Acsi);
        }


        private void CmdTransferAllOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.All);
        }

        private void CmdTouchupsOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            CmdTransferOnExecute(TransferTypeEnum.Touchups);
        }

        #endregion

        #endregion

        private void CmdTransferOnExecute(TransferTypeEnum transferType)
        {
            if (VmMain.InProcess) return;
            VmMain.ClearLog();

            if (!Service.Confirm($"Are you sure you're ready?{Environment.NewLine}" +
                                 $"Required steps before start:{Environment.NewLine}" +
                                 $" - 'Modify HCAI Data Before Transfer'->Patients->'Start analysis'->list with problems must be empty;{Environment.NewLine}" +
                                 $" - manually merge double items;{Environment.NewLine}" +
                                 $" - manually merge double patients;{Environment.NewLine}" +
                                 $" - manually merge double providers.",
                    "Start transfer"))
            {
                return;
            }

            this.IsCreatePayments = VmMain.CreatePayment;
            VmMain.InProcess = true;
            ProgressShow();
            ThreadPool.QueueUserWorkItem(obj =>
            {
                try
                {
                    if (_transfer is null)
                    {
                        _transfer = new TransferWork(this);
                        ThreadManager.ExecuteInDispatcherThread(() =>
                        {
                            VmMain.InProcess = true;
                        });
                    }

                    _transfer.Run(transferType);

                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        ProgressHide();
                        VmMain.InProcess = false;

                        _transfer.CheckReadiness();
                        _transfer.CheckTargetsReadiness();
                        CommandsRefresh();
                    });
                }
                catch (Exception ex)
                {
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        ProgressHide();
                        VmMain.InProcess = false;
                        CommandsRefresh();
                        Logger.Error("Transfer work canceled on error!", ex);
                    });
                }
            });
        }
    }
}
