using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ExtractHcaiData.Uo.Vm
{
    public class VMBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool IsChanged { get; set; }

        public void SendChangedAll()
        {
            var saveChanged = IsChanged;
            OnPropertyChanged("");
            IsChanged = saveChanged;
        }

        protected void NotifyPropertyChanged([CallerMemberName]string propertyName = null)
        {
            OnPropertyChanged(propertyName);
        }

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            IsChanged = true;
            if (handler != null && !string.IsNullOrEmpty(propertyName))
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
