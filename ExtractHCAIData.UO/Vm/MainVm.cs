﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ExtractHcaiData.Uo.Model.Analized;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Sync.Providers;

namespace ExtractHcaiData.Uo.Vm
{
    public class MainVm : VMBase
    {
        #region private

        private readonly MainWindow _owner;

        private string _dbSyncConnectionString;
        private string _dbTargetConnectionString;
        private int _daysBeforePayment;
        private bool _addingMode;
        private bool _groupByProviderSpecialty;

        private bool _inProcess;
        private int _progressBarMax;
        private int _progressBarValue;
        private string _stepInfo;

        private readonly SyncDbVm _syncDb = new SyncDbVm();
        private readonly TargetDbVm _targetDb = new TargetDbVm();
        
        private string _startDbSyncConnectionString = "";
        private string _startDbTargetConnectionString = "";
        private int _startDaysBeforePayment;
        private bool _startAddingMode;
        private bool _startGroupByProviderSpecialty;

        private Visibility _progressVisibility;
        private bool _isCanceled;
        private string _taskInfo;
        private bool _isSyncConnectReady;
        private bool _isTargetConnectReady;
        private bool _transfer2ExistingDb;
        private int _invoiceStartNumber;
        //private bool _uoHcaiStructure;
        private bool _usePmsKeysForOcfId;
        private bool _useInvoiceStartNumber;
        private bool _createPayment;

        #endregion

        #region Life circle

        public MainVm(MainWindow owner)
        {
            _owner = owner;
        }

        public void Init()
        {
            _startDbSyncConnectionString = DbSyncConnectionString = MainWindow.Configuration.DbSyncConnectionString;
            _startDbTargetConnectionString = DbTargetConnectionString = MainWindow.Configuration.DbUoConnectionString;
            _startDaysBeforePayment = DaysBeforePayment = MainWindow.Configuration.DaysBeforePayment;
            _startAddingMode = AddingMode = MainWindow.Configuration.AddingMode;
            _startGroupByProviderSpecialty = GroupByProviderSpecialty = MainWindow.Configuration.GroupByProviderSpecialty;

            this.UseInvoiceStartNumber = true;
            this.InvoiceStartNumber = 100;
            this.CreatePayment = true;
            this.InProcess = false;
        }

        public void SaveConfig()
        {
            if (!_startDbSyncConnectionString.Equals(DbSyncConnectionString))
                MainWindow.Configuration.DbSyncConnectionString = DbSyncConnectionString;
            if (!_startDbTargetConnectionString.Equals(DbTargetConnectionString))
                MainWindow.Configuration.DbUoConnectionString = DbTargetConnectionString;
            if (!_startDaysBeforePayment.Equals(DaysBeforePayment))
                MainWindow.Configuration.DaysBeforePayment = DaysBeforePayment;
            if (!_startAddingMode.Equals(AddingMode))
                MainWindow.Configuration.AddingMode = AddingMode;
            if (!_startGroupByProviderSpecialty.Equals(GroupByProviderSpecialty))
                MainWindow.Configuration.GroupByProviderSpecialty = GroupByProviderSpecialty;
        }

        #endregion

        #region Config-service

        public bool IsSyncConnectReady
        {
            get => _isSyncConnectReady;
            set
            {
                _isSyncConnectReady = value;
                NotifyPropertyChanged();
                _owner.CommandsRefresh();
            }
        }

        public bool IsTargetConnectReady
        {
            get => _isTargetConnectReady;
            set
            {
                _isTargetConnectReady = value;
                NotifyPropertyChanged();
                _owner.CommandsRefresh();
            }
        }

        public string DbSyncConnectionString
        {
            get => _dbSyncConnectionString;
            set
            {
                if (value != null && _dbSyncConnectionString != null && _dbSyncConnectionString.Equals(value)) return;

                _dbSyncConnectionString = value;
                NotifyPropertyChanged();

                ClearLog();

                IsSyncConnectReady = _owner.CheckConnection(_dbSyncConnectionString, true);
                if (SyncDb.Inited)
                    SyncDb.Close();
                _owner.CommandsRefresh();
            }
        }

        public string DbTargetConnectionString
        {
            get => _dbTargetConnectionString;
            set
            {
                if (value != null && _dbTargetConnectionString != null && _dbTargetConnectionString.Equals(value)) return;

                _dbTargetConnectionString = value;
                NotifyPropertyChanged();

                ClearLog();

                IsTargetConnectReady = _owner.CheckConnection(_dbTargetConnectionString, true);
                if (TargetDb.Inited)
                    TargetDb.Close();
                _owner.CommandsRefresh();
            }
        }

        public int DaysBeforePayment
        {
            get => _daysBeforePayment;
            set
            {
                _daysBeforePayment = value;
                NotifyPropertyChanged();
            }
        }

        public bool AddingMode
        {
            get => _addingMode;
            set
            {
                _addingMode = value;
                NotifyPropertyChanged();
            }
        }

        public bool GroupByProviderSpecialty
        {
            get => _groupByProviderSpecialty;
            set
            {
                _groupByProviderSpecialty = value;
                NotifyPropertyChanged();
            }
        }

        public bool InProcess
        {
            get => _inProcess;
            set
            {
                _inProcess = value;
                NotifyPropertyChanged();
                _owner.CommandsRefresh();

                ProgressVisibility = _inProcess ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        public Visibility ProgressVisibility
        {
            get => _progressVisibility;
            set
            {
                _progressVisibility = value;
                NotifyPropertyChanged();
            }
        }

        public int ProgressBarMax
        {
            get => _progressBarMax;
            set
            {
                _progressBarMax = value;
                NotifyPropertyChanged();
            }
        }

        public int ProgressBarValue
        {
            get => _progressBarValue;
            set
            {
                _progressBarValue = value;
                NotifyPropertyChanged();
            }
        }

        public string TaskInfo
        {
            get => _taskInfo;
            set
            {
                _taskInfo = value;
                NotifyPropertyChanged();
            }
        }

        public string StepInfo
        {
            get => _stepInfo;
            set
            {
                _stepInfo = value;
                NotifyPropertyChanged();
            }
        }

        public bool IsCanceled
        {
            get => _isCanceled;
            set
            {
                _isCanceled = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region log

        public int MaxMsgLines { get; set; } = 50;

        private List<string> _logLines = new();
        private readonly object _lockLog = new();

        public string Log
        {
            get
            {
                lock (_lockLog)
                    return string.Join(Environment.NewLine, _logLines);
            }
        }

        public void ClearLog()
        {
            lock (_lockLog)
            {
                _logLines.Clear();
                NotifyPropertyChanged(nameof(Log));
            }
        }

        public void AddToLog(string message)
        {
            lock (_lockLog)
            {
                _logLines.AddRange(message.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
                if (_logLines.Count > MaxMsgLines)
                    _logLines = _logLines.Skip(_logLines.Count - MaxMsgLines).Take(MaxMsgLines).ToList();
                NotifyPropertyChanged(nameof(Log));
            }
        }

        public string MakeLogCoreName()
        {
            return string.Empty;
        }

        #endregion
        
        #region Db

        public SyncDbVm SyncDb => _syncDb;

        public TargetDbVm TargetDb => _targetDb;

        #endregion

        #region Editor

        public bool Transfer2ExistingDb
        {
            get => _transfer2ExistingDb;
            set
            {
                _transfer2ExistingDb = value;
                NotifyPropertyChanged();
            }
        }

        /*public bool UoHcaiStructure
        {
            get { return _uoHcaiStructure; }
            set
            {
                _uoHcaiStructure = value;
                NotifyPropertyChanged();
            }
        }*/

        public bool UsePmsKeysForOcfId
        {
            get => _usePmsKeysForOcfId;
            set
            {
                _usePmsKeysForOcfId = value;
                NotifyPropertyChanged();
            }
        }

        public bool UseInvoiceStartNumber
        {
            get => _useInvoiceStartNumber;
            set
            {
                _useInvoiceStartNumber = value;
                NotifyPropertyChanged();
            }
        }

        public int InvoiceStartNumber
        {
            get => _invoiceStartNumber;
            set
            {
                _invoiceStartNumber = value;
                NotifyPropertyChanged();
            }
        }

        public bool CreatePayment
        {
            get => _createPayment;
            set
            {
                _createPayment = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        #region DB prepare

        public ObservableCollection<SyncProviderVm> SyncProviders { get; } = new ObservableCollection<SyncProviderVm>();

        public ObservableCollection<SamePatientGroupVm> SamePatientGroups { get; } = new ObservableCollection<SamePatientGroupVm>();

        public SamePatientGroupVm SelectedSamePatientGroup
        {
            get => _selectedSamePatientGroup;
            set
            {
                _selectedSamePatientGroup = value;
                NotifyPropertyChanged();
            }
        }

        public SyncProviderVm EditedProvider
        {
            get => _editedProvider;
            set
            {
                _editedProvider = value;
                NotifyPropertyChanged();
            }
        }

        #region Merge Pat dialog

        public ObservableCollection<SyncPatient> MergedPatients { get; } = new ObservableCollection<SyncPatient>();
        private SyncPatient _selectedPatient;
        private SamePatientGroupVm _selectedSamePatientGroup;
        private SyncProviderVm _editedProvider;

        public SyncPatient SelectedPatient
        {
            get => _selectedPatient;
            set
            {
                _selectedPatient = value;
                NotifyPropertyChanged();
                if (_selectedPatient != null)
                    _selectedPatient.SendChangedAll();
            }
        }

        #endregion

        #region Merge Items

        public ObservableCollection<SyncActivityItem> MergedActivityItems { get; } = new ObservableCollection<SyncActivityItem>();

        #endregion

        #region MergeProviders

        public ObservableCollection<SyncProviderVm> MergedSyncProviders { get; } = new ObservableCollection<SyncProviderVm>();

        #endregion

        #endregion

        #region Analize docs

        public ObservableCollection<DocOcf18Vm> DocOcf18s { get; } = new ObservableCollection<DocOcf18Vm>();
        public ObservableCollection<DocOcf23Vm> DocOcf23s { get; } = new ObservableCollection<DocOcf23Vm>();
        public ObservableCollection<DocOcf21BVm> DocOcf21Bs { get; } = new ObservableCollection<DocOcf21BVm>();
        public ObservableCollection<DocOcf21CVm> DocOcf21Cs { get; } = new ObservableCollection<DocOcf21CVm>();
        public ObservableCollection<DocForm1Vm> DocForm1s { get; } = new ObservableCollection<DocForm1Vm>();

        public ObservableCollection<DocAcsiVm> DocAcsis { get; } = new ObservableCollection<DocAcsiVm>();

        #endregion
    }
}
