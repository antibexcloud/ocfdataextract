﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Sql;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Vm
{
    public class TargetDbVm : VMBase
    {
        public UniversalCPR UoContext;

        public UoDataBaseConnection Connection { get; private set; }

        public bool Inited => UoContext?.Inited ?? false;
        public string Name => UoContext?.Name ?? string.Empty;

        // --
        public ObservableCollection<CprStaff> Staffs { get; } = new();
        public ObservableCollection<CprResource> Resources { get; } = new();

        public ObservableCollection<CprInsurer> Insurers { get; } = new();

        public ObservableCollection<CprActivity> Activities { get; } = new();
        public ObservableCollection<CprProduct> Products { get; } = new();
        public ObservableCollection<CprClient> Clients { get; } = new();

        public ObservableCollection<string> ActivityTypes { get; } = new();
        public ObservableCollection<string> Measures { get; } = new();

        public Business_Center BusinessCenter { get; set; }

        #region invoice number

        public List<int> ReservedInvoiceIds { get; } = new ();

        public double MaxFreeInvoiceId { get; set; }

        public double GetInvoiceId(double invoiceId)
        {
            // get next
            if (invoiceId == 0)
                return MaxFreeInvoiceId++;

            var exist = UoContext.InvoceGeneralInfoes.Any(d => d.InvoiceID == invoiceId);
            return !exist ? invoiceId : MaxFreeInvoiceId++;
        }
        
        #endregion

        public double ReportId { get; set; }
        public decimal BlockId { get; set; }

        public ObservableCollection<InjuryCode> InjuryCodes { get; } = new();

        private string _connectionString;

        #region Readies

        private bool _readyOcf18PlanNo;
        private bool _readyOcf23PlanNo;
        
        public bool ReadyOcf18PlanNo
        {
            get => _readyOcf18PlanNo;
            set
            {
                _readyOcf18PlanNo = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyOcf23PlanNo
        {
            get => _readyOcf23PlanNo;
            set
            {
                _readyOcf23PlanNo = value;
                NotifyPropertyChanged();
            }
        }


        #endregion

        public void Init(string connectString)
        {
            Clear();
            try
            {
                _connectionString = connectString;
                UoContext = UniversalCPR.OpenFromSqlConnection(connectString);
                BusinessCenter = UoContext.Business_Center.FirstOrDefault();

                Connection = new UoDataBaseConnection(connectString);
                Connection.UpdateMetadata();
            }
            catch (Exception ex)
            {
                UoContext = null;
                throw;
            }
        }

        public void Close()
        {
            Clear();

            if (UoContext != null)
            {
                UoContext.Dispose();
                UoContext = null;
            }

            if (Connection != null)
            {
                Connection.Dispose();
                Connection = null;
            }
        }

        public void ReConnect()
        {
            return;
            Close();
            Init(_connectionString);
        }

        private void Clear()
        {
            Staffs.Clear();
            Resources.Clear();
            Insurers.Clear();
            Activities.Clear();
            Products.Clear();
            Clients.Clear();
            InjuryCodes.Clear();

            ReadyOcf18PlanNo = ReadyOcf23PlanNo = false;
        }

        public void InitInjuryCodes()
        {
            InjuryCodes.Clear();
            foreach (var injuryCode in UoContext.InjuryCodes)
                InjuryCodes.Add(injuryCode);
        }

        public void InitStaffAndResources()
        {
            foreach (var staff in UoContext.Staffs.ToArray())
            {
                if (Staffs.Any(s => s.Backend.staff_id == staff.staff_id)) continue;
                Staffs.Add(new CprStaff(staff));
            }

            foreach (var resource in UoContext.Resources.ToArray())
            {
                if (Resources.Any(r => r.Backend.resource_id == resource.resource_id)) continue;
                var staff = Staffs.First(s => s.Backend.staff_id == resource.staff_id);
                Resources.Add(new CprResource(resource, staff));
            }
        }

        public void InitInsurers()
        {
            foreach (var thirdParty in UoContext.Third_Party.Where(t => t.Type != ""))
            {
                if (Insurers.Any(c => c.Backend.ThirdPartyID == thirdParty.ThirdPartyID)) continue;
                Insurers.Add(new CprInsurer(thirdParty));
            }
        }

        public void InitActivities()
        {
            foreach (var item in UoContext.Activities.ToArray())
            {
                if (Activities.Any(a => a.Backend.activity_id == item.activity_id)) continue;
                Activities.Add(new CprActivity(item));
            }

            foreach (var item in UoContext.Products.ToArray())
            {
                if (Products.Any(a => a.Backend.product_id == item.product_id)) continue;
                Products.Add(new CprProduct(item));
            }

            this.ActivityTypes.Clear();
            var types = this.Activities
                .Select(a => a.Backend.activity_type?.Trim())
                .Where(s => !string.IsNullOrEmpty(s))
                .Distinct()
                .ToList();
            types.Add("Product");
            foreach (var type in types.Distinct().OrderBy(s => s).ToList())
            {
                this.ActivityTypes.Add(type);
            }

            var measures = this.Activities
                .Select(a => a.Backend.mvaMeasure?.Trim())
                .Where(s => !string.IsNullOrEmpty(s))
                .Select(s => ConvertHelper.GetHcaiMeasureFromActivityOrProduct(s, s))
                .Distinct()
                .ToList();
            measures.Add("gd");
            this.Measures.Clear();
            foreach (var measure in measures.Distinct().OrderBy(s => s).ToList())
            {
                this.Measures.Add(measure);
            }
        }

        public void InitClients()
        {
            foreach (var client in UoContext.Clients.ToArray())
            {
                if (Clients.Any(c => c.Id == (int) client.client_id)) continue;
                Clients.Add(new CprClient(client));
            }
        }

        #region HCAI
        
        public void SaveHcaiLogStatus(List<HCAIStatusLog> hcaiLogs)
        {
            hcaiLogs.ForEach(statusLog => Connection.AddHcaiStatusLog(statusLog));
        }

        public void CheckInjuryCode(string code)
        {
            if (string.IsNullOrEmpty(code))
            {
                Logger.Debug($"Empty Injury Code!!!");
                return;
            }

            if (InjuryCodes.Any(c => c.Code == code.ToUpper())) return;
            if (UoContext.InjuryCodes.Any(c => c.Code == code.ToUpper())) return;

            var groupLetter = code.Substring(0, 1);
            var group = UoContext.InjuryCodesGroups.ToArray()
                .FirstOrDefault(g => g.Description.StartsWith(groupLetter));
            if (group is null)
            {
                Logger.Debug($"Bad Injury Code group: '{groupLetter}'!");
                return;
            }

            var item = UoContext.InjuryCodes.Add(new InjuryCode
            {
                InjuryID = UoContext.InjuryCodes.Select(t => t.InjuryID).Max() + 1,
                InjuryCodesGroup = group,
                Code = code,
                InjuryGroupID = group.InjuryGroupID,
                Status = 1,
                CodeDescription = code,
            });
            UoContext.SaveChangesWithDetect();
            InjuryCodes.Add(item);

            Logger.Info($" !!! added new Injury Code: '{code}'");
        }

        #endregion
    }
}
