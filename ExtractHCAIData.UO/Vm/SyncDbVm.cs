﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ExtractHcaiData.Uo.Model.Sync.Insurers;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Sync.Providers;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Vm
{
    public class SyncDbVm : VMBase
    {
        public HCAIOCFSyncEntities SyncContext;

        public bool Inited => SyncContext?.Inited ?? false;
        public string Name => SyncContext?.Name ?? string.Empty;

        public int FacilityId { get; private set; }

        public ObservableCollection<SyncProvider> Providers { get; } = new();
        public ObservableCollection<SyncProviderVirtual> ProviderVirtuals { get; } = new();

        public ObservableCollection<SyncInsurer> Insurers { get; } = new();

        public ObservableCollection<SyncActivityItem> ActivityItems { get; } = new();

        public ObservableCollection<SyncPatient> SyncPatients { get; } = new();

        #region Properties

        private bool _readyProviders;
        private bool _readyInsurers;
        private bool _readyActivities;
        private bool _readyClients;
        private bool _readyOcf18;
        private bool _readyOcf23;
        private bool _readyOcf21B;
        private bool _readyOcf21C;
        private bool _readyForm1;
        private bool _readyAcsi;

        public bool ReadyProviders
        {
            get => _readyProviders;
            set
            {
                _readyProviders = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyInsurers
        {
            get => _readyInsurers;
            set
            {
                _readyInsurers = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyActivities
        {
            get => _readyActivities;
            set
            {
                _readyActivities = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyClients
        {
            get => _readyClients;
            set
            {
                _readyClients = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyOcf18
        {
            get => _readyOcf18;
            set
            {
                _readyOcf18 = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyOcf23
        {
            get => _readyOcf23;
            set
            {
                _readyOcf23 = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyOcf21b
        {
            get => _readyOcf21B;
            set
            {
                _readyOcf21B = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyOcf21c
        {
            get => _readyOcf21C;
            set
            {
                _readyOcf21C = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyForm1
        {
            get => _readyForm1;
            set
            {
                _readyForm1 = value;
                NotifyPropertyChanged();
            }
        }

        public bool ReadyAcsi
        {
            get => _readyAcsi;
            set
            {
                _readyAcsi = value;
                NotifyPropertyChanged();
            }
        }

        #endregion

        public void Init(string connectString)
        {
            Close();
            SyncContext = HCAIOCFSyncEntities.OpenFromSqlConnection(connectString);
            SyncContext.UpdateMetadata();
        }

        public void Close()
        {
            if (SyncContext is null) return;

            Clear();

            SyncContext.Dispose();
            SyncContext = null;
        }

        private void Clear()
        {
            Providers.Clear();
            ProviderVirtuals.Clear();

            Insurers.Clear();
            ActivityItems.Clear();
            SyncPatients.Clear();

            ReadyActivities = ReadyClients = ReadyInsurers = ReadyProviders = false;
        }

        public void InitActivities()
        {
            foreach (var activityItem in SyncContext.ActivityItems)
            {
                if (ActivityItems.Any(a => a.Backend.Id == activityItem.Id)) continue;
                ActivityItems.Add(new SyncActivityItem(activityItem));
            }
        }

        public void InitFacility()
        {
            var facility = SyncContext.FacilityLineItems.FirstOrDefault();
            if (facility is null)
                throw new Exception("No facility info!");
            FacilityId = facility.HCAI_Facility_Registry_ID;
        }

        public void InitProviders()
        {
            foreach (var providerLineItem in SyncContext.ProviderLineItems)
            {
                if (Providers.Any(p => p.HCAI_Provider_Registry_ID == providerLineItem.HCAI_Provider_Registry_ID))
                    continue;
                Providers.Add(new SyncProvider(providerLineItem));
            }

            foreach (var providerVirtual in SyncContext.ProviderVirtuals)
            {
                if (ProviderVirtuals.Any(p => p.HCAI_Provider_Registry_ID == providerVirtual.HCAI_Provider_Registry_ID))
                    continue;
                ProviderVirtuals.Add(new SyncProviderVirtual(providerVirtual));
            }
        }

        public void InitInsurers()
        {
            foreach (var insurerLineItem in SyncContext.InsurerLineItems)
            {
                if (Insurers.Any(p => p.Backend.Insurer_ID == insurerLineItem.Insurer_ID)) continue;
                Insurers.Add(new SyncInsurer(insurerLineItem));
            }
        }

        public void InitPatients()
        {
            foreach (var patient in SyncContext.Patients)
            {
                if (SyncPatients.Any(p => p.Backend.Id == patient.Id)) continue;
                SyncPatients.Add(new SyncPatient(patient));
            }
        }
    }
}
