﻿namespace ExtractHcaiData.Uo.Logging
{
    public enum LoggingLevel
    {
        None  = 0,
        Error = 1,
        Info  = 2,
        Debug = 3,
        Trace = 4,
        All = 99
    }
}
