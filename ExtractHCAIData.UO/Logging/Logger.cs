﻿using System;
using System.IO;
using ExtractHcaiData.Uo.ConfigurationManagment;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;

namespace ExtractHcaiData.Uo.Logging
{
    public static class Logger
    {
        public static MainVm Vm;

        #region private data

        private static string MainCoreName = "ExtractHCAIData.UO";

        private static readonly string LogDirectory;
        private static string _logFileName;
        private static string _logNameCore;

        private static readonly object Locker = new object();
        private static StreamWriter _streamWriter;
        private static readonly LoggingLevel Level = LoggingLevel.Info;

        #endregion

        static Logger()
        {
            var file = typeof (Logger).Assembly.Location;
            var configFile = file + ".config";
            var path = Path.GetDirectoryName(file);

            if (File.Exists(configFile))
            {
                var configuration = new ConfigurationBase(file);
                Level = configuration.LogLevel;
            }

            LogDirectory = path ?? Path.GetTempPath();
            if (!Directory.Exists(LogDirectory)) Directory.CreateDirectory(LogDirectory);
            SetLogName();
        }

        public static void Close()
        {
            CloseStream();
        }

        public static void SetLogName(string nameCore = null)
        {
            var newLogNameCore = nameCore ?? MainCoreName;
            var newLogFileName = Path.Combine(LogDirectory, newLogNameCore + ".log");
            if (newLogNameCore != MainCoreName)
            {
                var idx = 1;
                while (File.Exists(newLogFileName))
                {
                    newLogFileName = Path.Combine(LogDirectory, newLogNameCore + $"{idx++}.log");
                }
            }
            if (newLogNameCore == _logNameCore || newLogFileName == _logFileName) return;

            // close if opened
            CloseStream();
            // set
            _logNameCore = newLogNameCore;
            _logFileName = newLogFileName;
            // check path
            var path = Path.GetDirectoryName(_logFileName);
            if (!Directory.Exists(path)) Directory.CreateDirectory(path);
        }


        public static void ReOpenStream()
        {
            if (_streamWriter != null)
                CloseStream();

            if (File.Exists(_logFileName))
                File.Delete(_logFileName);

            OpenStream();
        }

        public static void Info(string message, bool externalCall=false)
        {
            if (!externalCall && Level < LoggingLevel.Info) return;
            WriteToLog($"[{DateTime.Now}] INFO: {message}");
        }

        public static void Error(string message, Exception ex)
        {
            if (Level < LoggingLevel.Error) return;

            var msg = Service.InworkException(ex);
            var fullMsg = $"{message}\r\n{msg}";

            WriteToLog($"[{DateTime.Now}] ERROR: {fullMsg}");
        }

        public static void Debug(string message)
        {
            if (Level < LoggingLevel.Debug) return;
            WriteToLog($"[{DateTime.Now}] DEBUG: {message}");
        }

        public static void Trace(string message)
        {
            if (Level < LoggingLevel.Trace) return;
            WriteToLog($"[{DateTime.Now}] TRACE: {message}");
        }

        #region private methods

        private static void WriteToLog(string message)
        {
            lock (Locker)
            {
                Vm?.AddToLog(message);
                try
                {
                    if (OpenStream())
                        _streamWriter.WriteLine(message);
                }
                catch
                {
                    CloseStream();
                }
            }
        }

        private static bool OpenStream()
        {
            if (_streamWriter != null) return true;
            try
            {
                _streamWriter = File.AppendText(_logFileName);
                _streamWriter.AutoFlush = true;
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static void CloseStream()
        {
            if (_streamWriter is null) return;
            try
            {
                _streamWriter.Flush();
                _streamWriter.Close();
            }
            finally
            {
                _streamWriter = null;
            }
        }

        #endregion
    }
}
