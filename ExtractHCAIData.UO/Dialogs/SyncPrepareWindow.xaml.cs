﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Sync.Providers;
using ExtractHcaiData.Uo.ThreadWork;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Utils;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for SyncPrepareWindow.xaml
    /// </summary>
    public partial class SyncPrepareWindow : Window
    {
        #region Commands

        private static RoutedUICommand CmdProvMerge { get; } = new("ProvMerge", "CmdProvMerge", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdProvDropVirtual { get; } = new("ProvDropVirtual", "CmdProvDropVirtual", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdProvView { get; } = new("ProvView", "CmdProvView", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdPatMerge { get; } = new("PatMerge", "CmdPatMerge", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdPatientView { get; } = new("PatientView", "CmdPatientView", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdPatDropVirtual { get; } = new("PatDropVirtual", "CmdPatDropVirtual", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdItemMerge { get; } = new("ItemMerge", "CmdItemMerge", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdItemDropVirtual { get; } = new("ItemDropVirtual", "CmdItemDropVirtual", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdItemAutoMerge { get; } = new("ItemAutoMerge", "CmdItemAutoMerge", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdItemView { get; } = new("ItemView", "CmdItemView", typeof(SyncPrepareWindow));

        private static RoutedUICommand CmdPatAnalyze { get; } = new("PatAnalyze", "CmdPatAnalyze", typeof(SyncPrepareWindow));

        private void SetCommands()
        {
            CommandBindings.Add(new CommandBinding(CmdProvMerge, CmdProvMergeOnExecute, CmdProvMergeOnCanExecute));
            ProvMergeButton.Command = CmdProvMerge;
            ProvMergeMenuItem.Command = CmdProvMerge;
            CommandBindings.Add(new CommandBinding(CmdProvDropVirtual, CmdProvDropVirtualOnExecute, CmdProvDropVirtualOnCanExecute));
            ProvDropButton.Command = CmdProvDropVirtual;
            ProvDropMenuItem.Command = CmdProvDropVirtual;

            CommandBindings.Add(new CommandBinding(CmdPatMerge, CmdPatMergeOnExecute, CmdPatMergeOnCanExecute));
            PatMergeButton.Command = CmdPatMerge;
            PatMergeMenuItem.Command = CmdPatMerge;
            CommandBindings.Add(new CommandBinding(CmdPatDropVirtual, CmdPatDropVirtualOnExecute, CmdPatDropVirtualOnCanExecute));
            PatDropButton.Command = CmdPatDropVirtual;
            PatDropMenuItem.Command = CmdPatDropVirtual;

            CommandBindings.Add(new CommandBinding(CmdItemMerge, CmdItemMergeOnExecute, CmdItemMergeOnCanExecute));
            ItemMergeButton.Command = CmdItemMerge;
            ItemMergeMenuItem.Command = CmdItemMerge;
            CommandBindings.Add(new CommandBinding(CmdItemDropVirtual, CmdItemDropVirtualOnExecute, CmdItemDropVirtualOnCanExecute));
            ItemDropButton.Command = CmdItemDropVirtual;
            ItemDropMenuItem.Command = CmdItemDropVirtual;

            CommandBindings.Add(new CommandBinding(CmdItemAutoMerge, CmdItemAutoMergeOnExecute, CmdItemAutoMergeOnCanExecute));
            ItemAutoMergeButton.Command = CmdItemAutoMerge;

            CommandBindings.Add(new CommandBinding(CmdPatientView, CmdPatientViewOnExecute, CmdPatientViewOnCanExecute));
            PatViewButton.Command = CmdPatientView;
            PatViewMenuItem.Command = CmdPatientView;

            CommandBindings.Add(new CommandBinding(CmdItemView, CmdItemViewOnExecute, CmdItemViewOnCanExecute));
            ItemViewButton.Command = CmdItemView;
            ItemViewMenuItem.Command = CmdItemView;

            CommandBindings.Add(new CommandBinding(CmdProvView, CmdProvViewOnExecute, CmdProvViewOnCanExecute));
            ProvViewButton.Command = CmdProvView;
            ProvViewMenuItem.Command = CmdProvView;

            CommandBindings.Add(new CommandBinding(CmdPatAnalyze, CmdPatAnalyzeOnExecute, CmdPatAnalyzeOnCanExecute));
            AnalizePatGroupsButton.Command = CmdPatAnalyze;
        }

        #endregion

        #region Life circle

        private bool _inited;
        private readonly MainWindow _owner;
        private MainVm _vmMain;

        private SyncDbVm SourceDb => _vmMain.SyncDb;
        private TargetDbVm TargetDb => _vmMain.TargetDb;

        private HCAIOCFSyncEntities SyncContext => SourceDb.SyncContext;
        private UniversalCPR UoContext => TargetDb.UoContext;

        public SyncPrepareWindow()
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
            _owner = (MainWindow) Owner;
            _vmMain = _owner.VmMain;
            Icon = Owner.Icon;

            SetCommands();
            CommandsRefresh();

            Height = _owner.ActualHeight * 0.9;
            Width = _owner.ActualWidth * 0.9;
        }

        private static void CommandsRefresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        private void SyncPrepareWindowOnLoaded(object sender, RoutedEventArgs e)
        {
            if (_inited) return;
            _vmMain = (MainVm)DataContext;

            if (SourceDb.SyncPatients.All(p => p.CaseOne))
                OneAllCheckBox.IsChecked = true;

            if (MainWindow.Configuration.GroupByProviderSpecialty)
            {
                CaseColumn.Visibility = Visibility.Collapsed;
                //OneColumn.Visibility = Visibility.Collapsed;
            }
            else
            {
                CaseTypesColumn.Visibility = Visibility.Collapsed;
            }

            foreach (var syncActivityItem in SourceDb.ActivityItems.Where(t => !t.IsUo).ToList())
            {
                syncActivityItem.TryLink(TargetDb);
            }

            _inited = true;
            FilterRefresh(PatientsDataGrid);
            FilterRefresh(ItemsDataGrid);
        }

        private void SyncPrepareWindowOnClosing(object sender, CancelEventArgs e)
        {
            try
            {
                _vmMain.SyncDb.SyncContext.SaveChangesWithDetect();
                e.Cancel = false;
            }
            catch (Exception ex)
            {
                e.Cancel = true;
                MessageBox.Show(Service.InworkException(ex));
            }
        }

        private void DataGridOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CommandsRefresh();
        }

        private static void FilterRefresh(DataGrid grid)
        {
            if (grid.ItemsSource is null) return;

            try
            {
                CollectionViewSource.GetDefaultView(grid.ItemsSource).Refresh();
            }
            catch { }
        }

        #endregion

        #region Can exec

        private bool BaseReady => IsLoaded && _vmMain != null;


        private void CmdPatMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = PatientsDataGrid.SelectedItems.Count > 1;
        }

        private void CmdPatDropVirtualOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = PatientsDataGrid.SelectedItems.OfType<SyncPatient>().Any(p => p.IsVirtual);
        }

        private void CmdPatientViewOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = PatientsDataGrid.SelectedItems.Count == 1
                && PatientsDataGrid.SelectedItems.OfType<SyncPatient>().All(p => p.IsVirtual);
        }


        private void CmdItemMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = ItemsDataGrid.SelectedItems.Count > 1
                           && SyncActivityItem.CanBeMerged(ItemsDataGrid.SelectedItems.OfType<SyncActivityItem>().ToArray());
        }

        private void CmdItemDropVirtualOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = ItemsDataGrid.SelectedItems.OfType<SyncActivityItem>().Any(p => p.IsVirtual);
        }

        private void CmdItemViewOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = ItemsDataGrid.SelectedItems.Count == 1
                           && ItemsDataGrid.SelectedItems.OfType<SyncActivityItem>().All(p => p.IsVirtual);
        }

        private void CmdItemAutoMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = SourceDb.ActivityItems.Any(t => !t.IsVirtual);
        }

        private void CmdProvMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            var first = ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>().FirstOrDefault();
            e.CanExecute = ProvidersDataGrid.SelectedItems.Count > 1
                           && first != null
                           && ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>().All(p => p.HCAI_Provider_Registry_ID == first.HCAI_Provider_Registry_ID);
        }

        private void CmdProvViewOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = ProvidersDataGrid.SelectedItems.Count == 1
                           && ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>().All(p => p.IsVirtual);
        }

        private void CmdProvDropVirtualOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = ProvidersDataGrid.SelectedItems.Count > 0
                           && ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>().All(p => p.IsVirtual);
        }

        private void CmdPatAnalyzeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady || e.CanExecute;
        }

        #endregion

        #region Exec

        private void CmdPatMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new MergePatientDialog { DataContext = _vmMain };
            dlg.PrepareData(PatientsDataGrid.SelectedItems.OfType<SyncPatient>().ToArray());

            var res = dlg.ShowDialog();
            if (res != true) return;

            FilterRefresh(PatientsDataGrid);
            if (sender is null)
                _vmMain.SamePatientGroups.Remove(_vmMain.SelectedSamePatientGroup);
        }

        private void CmdPatDropVirtualOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dropped = PatientsDataGrid.SelectedItems.OfType<SyncPatient>()
                .Where(p => p.IsVirtual)
                .ToArray();
            foreach (var syncPatient in dropped)
            {
                foreach (var patient in syncPatient.Backend.Patient1.ToArray())
                {
                    var find = SourceDb.SyncPatients.First(p => p.Id == patient.Id);
                    find.SetVirtualId(null);
                    find.TryLink(TargetDb, MainWindow.Configuration.GroupByProviderSpecialty);
                }

                SyncContext.Patients.Remove(syncPatient.Backend);
                SyncContext.SaveChangesWithDetect();
                SourceDb.SyncPatients.Remove(syncPatient);
            }

            FilterRefresh(PatientsDataGrid);
        }

        private void CmdPatientViewOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = PatientsDataGrid.SelectedItems.OfType<SyncPatient>().FirstOrDefault();
            if (selected is null || !selected.IsVirtual) return;

            var child = _vmMain.SyncDb.SyncPatients
                .Where(t => t.VirtualId == selected.Id)
                .OrderBy(t => t.Name)
                .ToList();

            var dlg = new ViewVirtualPatient(selected, child) { Owner = this };
            dlg.ShowDialog();
        }


        private void CmdItemMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new MergeItemDialog { DataContext = _vmMain };
            dlg.PrepareData(ItemsDataGrid.SelectedItems.OfType<SyncActivityItem>().ToArray());
            var res = dlg.ShowDialog();
            if (res == true)
                FilterRefresh(ItemsDataGrid);
        }

        private void CmdItemDropVirtualOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dropped = ItemsDataGrid.SelectedItems.OfType<SyncActivityItem>()
                .Where(t => t.IsVirtual)
                .ToList();
            foreach (var syncActivityItem in dropped)
            {
                foreach (var activityItem in syncActivityItem.Backend.ActivityItems1)
                {
                    var find = SourceDb.ActivityItems.First(a => a.Id == activityItem.Id);
                    find.SetVirtualId(null);
                    find.TryLink(TargetDb);
                }

                SyncContext.ActivityItems.Remove(syncActivityItem.Backend);
                SyncContext.SaveChangesWithDetect();
                SourceDb.ActivityItems.Remove(syncActivityItem);
            }

            FilterRefresh(ItemsDataGrid);
        }

        private void CmdItemViewOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = ItemsDataGrid.SelectedItems.OfType<SyncActivityItem>().FirstOrDefault();
            if (selected is null || !selected.IsVirtual) return;

            var child = _vmMain.SyncDb.ActivityItems
                .Where(t => t.VirtualId == selected.Id)
                .OrderBy(t => t.ItemId)
                .ToList();

            var dlg = new ViewVirtualItem(selected, child) { Owner = this };
            dlg.ShowDialog();
        }

        private void CmdItemAutoMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            _vmMain.InProcess = true;
            Mouse.OverrideCursor = Cursors.Wait;

            ThreadPool.QueueUserWorkItem(obj =>
            {
                try
                {
                    var grouped = SourceDb.ActivityItems
                        .Where(t => !t.IsInVirtual)
                        .GroupBy(t => $"{t.ServiceCode.ToUpper()}_{SyncService.CheckOnQuotes(t.ItemDescription).Trim().SafetySubstring(50).Trim().ToLower()}{t.Measure.Trim().ToLower()}_{t.IsProduct}")
                        .OrderBy(g => g.Key)
                        .ToList();
                    _vmMain.TaskInfo = "Auto merge items";
                    _vmMain.ProgressBarValue = 0;
                    _vmMain.ProgressBarMax = grouped.Count;
                    foreach (IGrouping<string, SyncActivityItem> syncActivityItems in grouped)
                    {
                        // prepare
                        var items = syncActivityItems.ToList();
                        var first = items.First();
                        var count = items.Count;

                        ++_vmMain.ProgressBarValue;
                        _vmMain.StepInfo = first.ItemDescription;

                        var description = SyncService.CheckOnQuotes(first.ItemDescription).SafetySubstring(500);
                        var itemId = description.SafetySubstring(49).Trim().ToLower();

                        if (count == 1)
                        {
                            if (first.IsVirtual || first.ItemId != first.ServiceCode) continue;

                            ThreadManager.ExecuteInDispatcherThread(() =>
                            {
                                first.ItemId = itemId;
                                first.ItemDescription = description;

                                SyncContext.SaveChangesWithDetect();
                            });
                        }
                        else
                        {
                            var unitItem = items
                                .Select(t => new
                                {
                                    t,
                                    diff = Math.Abs(t.Units - 1.0)
                                })
                                .OrderBy(g => g.diff)
                                .First()
                                .t;
                            var units = unitItem.Units;
                            var fee = unitItem.Fee;
                            var taxId = items.FirstOrDefault(t => !string.IsNullOrEmpty(t.TaxId))?.TaxId ?? string.Empty;
                            var GST = items.Max(t => t.GST);
                            var PST = items.Max(t => t.PST);

                            Logger.Info($"{first.ServiceCode}|{first.ItemDescription}|{first.Measure} - {items.Count} grouping");

                            // check/add virtual
                            var virt = items.FirstOrDefault(t => t.IsVirtual);
                            if (virt is null)
                            {
                                var item = new ActivityItem
                                {
                                    IsProduct = first.IsProduct,
                                    activity_id = itemId,
                                    activity_name = first.Name,
                                    ItemDescription = description,
                                    activity_type = first.Type,
                                    activity_duration = first.Duration ?? string.Empty,

                                    mvaServiceCode = first.ServiceCode,
                                    mvaAttribute = (first.Attribute ?? string.Empty).SafetySubstring(3),
                                    mvaMeasure = (first.Measure ?? string.Empty).SafetySubstring(3),
                                    mvaUnits = units,
                                    mvaTaxID = taxId,
                                    mvaFee = fee,
                                    GST = GST,
                                    PST = PST,
                                };

                                ThreadManager.ExecuteInDispatcherThread(() =>
                                {
                                    SyncContext.ActivityItems.Add(item);
                                    SyncContext.SaveChangesWithDetect();

                                    virt = new SyncActivityItem(item);
                                    SourceDb.ActivityItems.Add(virt);

                                    foreach (var activityItem in items)
                                    {
                                        activityItem.SetVirtualId(virt.Id);
                                    }

                                    SyncContext.SaveChangesWithDetect();
                                });
                            }
                            else
                            {
                                ThreadManager.ExecuteInDispatcherThread(() =>
                                {
                                    // I - self
                                    virt.ItemId = itemId;
                                    virt.ItemDescription = description;
                                    virt.Units = units;
                                    virt.TaxId = taxId;
                                    virt.Fee = fee;
                                    virt.GST = GST;
                                    virt.PST = PST;

                                    // II - other virtual
                                    var virtuals = items
                                        .Where(p => p.IsVirtual && p.Id != virt.Id)
                                        .ToList();
                                    foreach (var virtualProduct in virtuals)
                                    {
                                        var productIds = virtualProduct.Backend.ActivityItems1.Select(p => p.Id).ToList();
                                        foreach (var productId in productIds)
                                        {
                                            var syncProduct = SourceDb.ActivityItems.First(p => p.Id == productId);
                                            syncProduct.SetVirtualId(virt.Id);
                                        }

                                        // delete virtual syncProduct
                                        SyncContext.ActivityItems.Remove(virtualProduct.Backend);
                                        SyncContext.SaveChangesWithDetect();
                                        SourceDb.ActivityItems.Remove(virtualProduct);
                                    }

                                    // III - rest
                                    var rest = items
                                        .Where(t => t != virt && !t.IsVirtual)
                                        .ToList();
                                    foreach (var activityItem in rest)
                                    {
                                        activityItem.SetVirtualId(virt.Id);
                                    }

                                    SyncContext.SaveChangesWithDetect();
                                });
                            }
                        }
                    }

                    // end
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        _owner.ProgressHide();
                        _vmMain.InProcess = false;
                        Mouse.OverrideCursor = null;

                        // end
                        CommandsRefresh();
                        MessageBox.Show("DONE.");
                    });
                }
                catch (Exception ex)
                {
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        _owner.ProgressHide();
                        _vmMain.InProcess = false;
                        Mouse.OverrideCursor = null;
                        CommandsRefresh();
                        MessageBox.Show(Service.InworkException(ex));
                    });
                }
            });
        }

        private void CmdProvMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dlg = new MergeProviderDialog { DataContext = _vmMain };
            dlg.PrepareData(ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>().ToArray());
            var res = dlg.ShowDialog();
        }

        private void CmdProvDropVirtualOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var dropped = ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>()
                .Where(p => p.IsVirtual)
                .ToList();
            foreach (var providerVm in dropped)
            {
                var virt = providerVm.BackendVirtual;

                // add
                foreach (var syncProvider in virt.Backend.ProviderLineItems.ToArray())
                {
                    var find = SourceDb.Providers.FirstOrDefault(p => p.HCAI_Provider_Registry_ID == syncProvider.HCAI_Provider_Registry_ID);
                    if (find is null)
                    {
                        MessageBox.Show("CmdProvDropVirtualOnExecute  - not find provider ???");
                        continue;
                    }
                    find.SetVirtualId(null);
                    find.TryLink(_owner._transfer);
                    _vmMain.SyncProviders.Add(new SyncProviderVm(find, null));
                }

                // clear
                //virt.SyncProviders.Clear();
                SyncContext.ProviderVirtuals.Remove(virt.Backend);
                SyncContext.SaveChangesWithDetect();

                // drop
                _vmMain.SyncProviders.Remove(providerVm);
            }
        }

        private void CmdProvViewOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            var selected = ProvidersDataGrid.SelectedItems.OfType<SyncProviderVm>().FirstOrDefault();
            if (selected is null || !selected.IsVirtual) return;

            var child = _vmMain.SyncProviders
                .Where(t => !t.IsVirtual
                            && t.BackendVirtual != null
                            && t.BackendVirtual.HCAI_Provider_Registry_ID == selected.HCAI_Provider_Registry_ID)
                .OrderBy(t => t.Name)
                .ToList();

            var dlg = new ViewVirtualProvider(selected, child) { Owner = this };
            dlg.ShowDialog();
        }

        private void CmdPatAnalyzeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            _vmMain.SamePatientGroups.Clear();
            _vmMain.InProcess = true;
            Mouse.OverrideCursor = Cursors.Wait;

            ThreadPool.QueueUserWorkItem(obj =>
            {
                try
                {
                    // OCF21B
                    var forAnalyzeOcf21B = SyncContext.OCF21BSubmit
                        .Where(d => !string.IsNullOrEmpty(d.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                    && d.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt")
                        .OrderBy(d => d.HCAI_Document_Number)
                        .ToArray();

                    _vmMain.TaskInfo = "Analyze patients (OCF21B)";
                    _vmMain.ProgressBarValue = 0;
                    _vmMain.ProgressBarMax = forAnalyzeOcf21B.Length;
                    foreach (var ocfSubmit in forAnalyzeOcf21B)
                    {
                        ++_vmMain.ProgressBarValue;
                        _vmMain.StepInfo = ocfSubmit.HCAI_Document_Number;

                        // get current
                        var patient = SyncPatient.GetSyncPatient(SourceDb.SyncPatients, ocfSubmit);
                        // find plan
                        var plan = SyncContext.OCF18Submit.FirstOrDefault(p => p.HCAI_Document_Number == ocfSubmit.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber);
                        if (plan is null)
                        {
                            Logger.Info($"No OCF18-plan {ocfSubmit.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber} for OCF21B {ocfSubmit.HCAI_Document_Number}");
                            continue;
                        }

                        // get plan patient
                        var patientPlan = SyncPatient.GetSyncPatient(SourceDb.SyncPatients, plan);
                        // check
                        if (patient.Equals(patientPlan))
                        {
                            continue;
                        }

                        Logger.Info($"OCF18-plan {ocfSubmit.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber} for OCF21B {ocfSubmit.HCAI_Document_Number}" +
                                    $"; patient: {patient.Name} - in other patient: {patientPlan.Name}!");

                        // find & add
                        var findGroupByPatient = _vmMain.SamePatientGroups
                            .FirstOrDefault(g => g.Patients.Any(p => p.Equals(patient)));

                        var findGroupByPatientPlan = _vmMain.SamePatientGroups
                            .FirstOrDefault(g => g.Patients.Any(p => p.Equals(patientPlan)));

                        if (findGroupByPatient is null && findGroupByPatientPlan is null)
                        {
                            // new
                            ThreadManager.ExecuteInDispatcherThread(() =>
                            {
                                var group = new SamePatientGroupVm();
                                group.Patients.Add(patient);
                                group.Patients.Add(patientPlan);
                                _vmMain.SamePatientGroups.Add(group);
                            });
                        }
                        else if (findGroupByPatient != null)
                        {
                            if (!findGroupByPatient.Patients.Any(p => p.Equals(patientPlan)))
                                ThreadManager.ExecuteInDispatcherThread(() =>
                                {
                                    findGroupByPatient.Patients.Add(patientPlan);
                                });
                        }
                        else //if (findGroupByPatientPlan != null)
                        {
                            if (!findGroupByPatientPlan.Patients.Any(p => p.Equals(patient)))
                                ThreadManager.ExecuteInDispatcherThread(() =>
                                {
                                    findGroupByPatientPlan.Patients.Add(patient);
                                });
                        }
                    }

                    // OCF21C
                    var forAnalyzeOcf21C = SyncContext.OCF21CSubmit
                        .Where(d => !string.IsNullOrEmpty(d.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                    && d.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt")
                        .OrderBy(d => d.HCAI_Document_Number)
                        .ToArray();

                    _vmMain.TaskInfo = "Analyze patients (OCF21C)";
                    _vmMain.ProgressBarValue = 0;
                    _vmMain.ProgressBarMax = forAnalyzeOcf21C.Length;
                    foreach (var ocfSubmit in forAnalyzeOcf21C)
                    {
                        ++_vmMain.ProgressBarValue;
                        _vmMain.StepInfo = ocfSubmit.HCAI_Document_Number;

                        // get current
                        var patient = SyncPatient.GetSyncPatient(SourceDb.SyncPatients, ocfSubmit);
                        // find plan
                        var plan = SyncContext.OCF23Submit.FirstOrDefault(p => p.HCAI_Document_Number == ocfSubmit.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber);
                        if (plan is null)
                        {
                            Logger.Info($"No OCF23-plan {ocfSubmit.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber} for OCF21C {ocfSubmit.HCAI_Document_Number}");
                            continue;
                        }
                        // get plan patient
                        var patientPlan = SyncPatient.GetSyncPatient(SourceDb.SyncPatients, plan);
                        // check
                        if (patient.Equals(patientPlan))
                        {
                            continue;
                        }

                        Logger.Info($"OCF23-plan {ocfSubmit.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber} for OCF21C {ocfSubmit.HCAI_Document_Number}" +
                                    $"; patient: {patient.Name} - in other patient: {patientPlan.Name}!");


                        // find & add
                        var findGroupByPatient = _vmMain.SamePatientGroups
                            .FirstOrDefault(g => g.Patients.Any(p => p.Equals(patient)));

                        var findGroupByPatientPlan = _vmMain.SamePatientGroups
                            .FirstOrDefault(g => g.Patients.Any(p => p.Equals(patientPlan)));

                        if (findGroupByPatient is null && findGroupByPatientPlan is null)
                        {
                            // new
                            ThreadManager.ExecuteInDispatcherThread(() =>
                            {
                                var group = new SamePatientGroupVm();
                                group.Patients.Add(patient);
                                group.Patients.Add(patientPlan);
                                _vmMain.SamePatientGroups.Add(group);
                            });
                        }
                        else if (findGroupByPatient != null)
                        {
                            if (!findGroupByPatient.Patients.Any(p => p.Equals(patientPlan)))
                                ThreadManager.ExecuteInDispatcherThread(() =>
                                {
                                    findGroupByPatient.Patients.Add(patientPlan);
                                });
                        }
                        else //if (findGroupByPatientPlan != null)
                        {
                            if (!findGroupByPatientPlan.Patients.Any(p => p.Equals(patient)))
                                ThreadManager.ExecuteInDispatcherThread(() =>
                                {
                                    findGroupByPatientPlan.Patients.Add(patient);
                                });
                        }
                    }

                    // end
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        _owner.ProgressHide();
                        _vmMain.InProcess = false;
                        Mouse.OverrideCursor = null;

                        NoDataGrid.Visibility = Visibility.Collapsed;
                        NoSameGrid.Visibility = _vmMain.SamePatientGroups.Any() ? Visibility.Collapsed : Visibility.Visible;

                        // log
                        if (_vmMain.SamePatientGroups.Any())
                        {
                            Logger.Info(Environment.NewLine + "********* same patient groups **************");
                            foreach (var patientGroup in _vmMain.SamePatientGroups)
                            {
                                Logger.Info("*) same others:");
                                foreach (var patient in patientGroup.Patients.OrderBy(p => p.Name))
                                {
                                    Logger.Info($"  patient: Lastname='{patient.ResultLastName}', Firstname='{patient.ResultFirstName}'," +
                                                $" MiddleName='{patient.ResultMiddleName}', Gender='{patient.ResultGender}', DOB:{patient.ResultDOB:d}");
                                }
                                Logger.Info("-end group" +Environment.NewLine);
                            }
                            Logger.Info(Environment.NewLine + "***********************");
                        }

                        // end
                        CommandsRefresh();
                        MessageBox.Show("DONE.");
                    });
                }
                catch (Exception ex)
                {
                    ThreadManager.ExecuteInDispatcherThread(() =>
                    {
                        _owner.ProgressHide();
                        _vmMain.InProcess = false;
                        Mouse.OverrideCursor = null;
                        CommandsRefresh();
                        MessageBox.Show(Service.InworkException(ex));
                    });
                }
            });
        }

        private void GroupPatientsDataGridOnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (_vmMain.SelectedSamePatientGroup is null) return;

            PatientsDataGrid.SelectedItems.Clear();

            foreach (var syncPatient in _vmMain.SelectedSamePatientGroup.Patients)
                PatientsDataGrid.SelectedItems.Add(syncPatient);
            PatientsDataGrid.ScrollIntoView(PatientsDataGrid.SelectedItem);

            CmdPatMergeOnExecute(null, null);
        }

        #endregion

        #region Patients Filter

        private void HcaiPatientsSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as SyncPatient)?.IsInVirtual == false;
        }

        #endregion

        #region HcaiItemTypes Filter

        private void HcaiItemTypesSourceOnFilter(object sender, FilterEventArgs e)
        {
            e.Accepted = (e.Item as SyncActivityItem)?.IsInVirtual == false;
        }

        #endregion

        private void OneAllCheckBoxOnClick(object sender, RoutedEventArgs e)
        {
            if (!_inited) return;

            var chBox = sender as CheckBox;
            if (chBox is null) return;
            var val = chBox.IsChecked ?? false;

            foreach (var unit in SourceDb.SyncPatients.Where(u => u.CaseOne != val).ToArray())
                unit.CaseOne = val;
        }

        private void PatientsDataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CmdPatientViewOnExecute(null, null);
        }

        private void ItemsDataGrid_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            CmdItemViewOnExecute(null, null);
        }

        private void ItemsDataGridOnRowEditEnding(object sender, DataGridRowEditEndingEventArgs e)
        {
            var item = e.Row.Item as SyncActivityItem;
            if (item is null) return;

            var idItem = item.Id;
            ThreadPool.QueueUserWorkItem(obj =>
            {
                Thread.Sleep(300);

                ThreadManager.ExecuteInDispatcherThread(() =>
                {
                    var syncItem = SourceDb.ActivityItems.FirstOrDefault(t => t.Id == idItem);
                    if (syncItem != null)
                    {
                        syncItem.TryLink(TargetDb);
                    }
                });
            });
        }
    }
}
