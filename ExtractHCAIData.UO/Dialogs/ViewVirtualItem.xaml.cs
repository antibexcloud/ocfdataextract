﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using ExtractHcaiData.Uo.Model.Sync.Items;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for ViewVirtualItem.xaml
    /// </summary>
    public partial class ViewVirtualItem : Window
    {
        public ViewVirtualItem()
        {
            InitializeComponent();
        }

        public ViewVirtualItem(SyncActivityItem virtualItem, List<SyncActivityItem> items)
        {
            InitializeComponent();

            ChildItems = new ObservableCollection<SyncActivityItem>(items);
            Title = $"View virtual item: {virtualItem.ItemId} | {virtualItem.ServiceCode} | {virtualItem.ItemDescription}";
        }

        private void CloseButtonOnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        #region DP

        public ObservableCollection<SyncActivityItem> ChildItems
        {
            get => (ObservableCollection<SyncActivityItem>)GetValue(ChildItemsProperty);
            set => SetValue(ChildItemsProperty, value);
        }

        public static readonly DependencyProperty ChildItemsProperty =
            DependencyProperty.Register(nameof(ChildItems), typeof(ObservableCollection<SyncActivityItem>), typeof(ViewVirtualItem), null);

        #endregion
    }
}
