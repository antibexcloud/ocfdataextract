﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using ExtractHcaiData.Uo.Model.Sync.Patients;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for ViewVirtualPatient.xaml
    /// </summary>
    public partial class ViewVirtualPatient : Window
    {
        public ViewVirtualPatient()
        {
            InitializeComponent();
        }

        public ViewVirtualPatient(SyncPatient virtualPatient, List<SyncPatient> childPatients)
        {
            InitializeComponent();

            ChildPatients = new ObservableCollection<SyncPatient>(childPatients);
            Title = $"View virtual {virtualPatient.Name}";

            if (MainWindow.Configuration.GroupByProviderSpecialty)
            {
                CaseColumn.Visibility = Visibility.Collapsed;
            }
            else
            {
                CaseTypesColumn.Visibility = Visibility.Collapsed;
            }
        }

        private void CloseButtonOnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        #region DP

        public ObservableCollection<SyncPatient> ChildPatients
        {
            get => (ObservableCollection<SyncPatient>)GetValue(ChildPatientsProperty);
            set => SetValue(ChildPatientsProperty, value);
        }

        public static readonly DependencyProperty ChildPatientsProperty =
            DependencyProperty.Register(nameof(ChildPatients), typeof(ObservableCollection<SyncPatient>), typeof(ViewVirtualPatient), null);

        #endregion
    }
}
