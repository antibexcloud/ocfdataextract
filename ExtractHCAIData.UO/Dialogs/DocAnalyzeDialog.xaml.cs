﻿using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ExtractHcaiData.Uo.Model.Analized;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for DocAnalyzeDialog.xaml
    /// </summary>
    public partial class DocAnalyzeDialog : Window
    {
        #region Commands

        private void SetCommands()
        {
        }

        #endregion

        #region Life circle

        private bool _inited;
        private MainVm _vmMain;

        private SyncDbVm SourceDb => _vmMain.SyncDb;
        private HCAIOCFSyncEntities SyncContext => SourceDb.SyncContext;

        public DocAnalyzeDialog()
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
            var owner = (MainWindow)Owner;
            _vmMain = owner.VmMain;
            Icon = Owner.Icon;

            SetCommands();
            CommandsRefresh();

            Height = owner.ActualHeight * 0.9;
            Width = owner.ActualWidth * 0.8;
        }

        private void WindowOnLoaded(object sender, RoutedEventArgs e)
        {
            if (_inited) return;
            _inited = true;
            _vmMain = (DataContext as MainVm);
        }

        private void WindowOnClosing(object sender, CancelEventArgs e)
        {
            
        }

        private void CommandsRefresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        private void DataGridOnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CommandsRefresh();
        }

        private void FilterRefresh(DataGrid grid)
        {
            if (grid.ItemsSource is null) return;

            try
            {
                CollectionViewSource.GetDefaultView(grid.ItemsSource).Refresh();
            }
            catch { }
        }

        #endregion

        private void Ocf18AnalyzeButtonOnClick(object sender, RoutedEventArgs e)
        {
            _vmMain.DocOcf18s.Clear();
            NoDataOcf18Grid.Visibility = Visibility.Visible;

            foreach (var submit in SyncContext.OCF18Submit.Include("GS18LineItemSubmit"))
            {
                var allSubmit = submit.OCF18_InsurerTotals_SubTotal_Proposed;
                var allItemsSubmit = submit.GS18LineItemSubmit
                    .Select(t => t.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost)
                    .Sum();

                var allApproved = 0.0m;
                var allItemsApproved = 0.0m;

                var docAr = SyncContext.OCF18AR.Include("GS18LineItemAR")
                    .FirstOrDefault(a => a.HCAI_Document_Number == submit.HCAI_Document_Number);
                if (docAr != null)
                {
                    allApproved = docAr.OCF18_InsurerTotals_SubTotal_Approved;
                    allItemsApproved = docAr.GS18LineItemAR
                        .Select(t => t.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost)
                        .Sum();
                }

                if (allSubmit == allItemsSubmit && allApproved == allItemsApproved) continue;

                _vmMain.DocOcf18s.Add(new DocOcf18Vm
                {
                    Number = submit.HCAI_Document_Number,
                    Submitted = allSubmit,
                    ItemsSubmitted = allItemsSubmit,
                    Approved = allApproved,
                    ItemsApproved = allItemsApproved,
                });
            }

            NoDataOcf18Grid.Visibility = Visibility.Collapsed;
            NoProblemOcf18Grid.Visibility = !_vmMain.DocOcf18s.Any() ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Ocf23AnalyzeButtonOnClick(object sender, RoutedEventArgs e)
        {
            _vmMain.DocOcf23s.Clear();
            NoDataOcf23Grid.Visibility = Visibility.Visible;

            foreach (var submit in SyncContext.OCF23Submit.Include("OtherGSLineItemSubmits"))
            {
                var allSubmit = submit.OCF23_InsurerTotals_SubTotalPreApproved_Proposed;
                var allItemsSubmit = submit.OtherGSLineItemSubmits
                    .Select(t => t.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost)
                    .Sum();

                var allApproved = 0.0m;
                var allItemsApproved = 0.0m;

                var docAr = SyncContext.OCF23AR.Include("OtherGSLineItemARs")
                    .FirstOrDefault(a => a.HCAI_Document_Number == submit.HCAI_Document_Number);
                if (docAr != null)
                {
                    allApproved = docAr.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved;
                    allItemsApproved = docAr.OtherGSLineItemARs
                        .Select(t => t.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost)
                        .Sum();
                }

                if (allSubmit == allItemsSubmit && allApproved == allItemsApproved) continue;

                _vmMain.DocOcf23s.Add(new DocOcf23Vm
                {
                    Number = submit.HCAI_Document_Number,
                    Submitted = allSubmit,
                    ItemsSubmitted = allItemsSubmit,
                    Approved = allApproved,
                    ItemsApproved = allItemsApproved,
                });
            }

            NoDataOcf23Grid.Visibility = Visibility.Collapsed;
            NoProblemOcf23Grid.Visibility = !_vmMain.DocOcf23s.Any() ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Ocf21BAnalyzeButtonOnClick(object sender, RoutedEventArgs e)
        {
            _vmMain.DocOcf21Bs.Clear();
            NoDataOcf21BGrid.Visibility = Visibility.Visible;

            foreach (var submit in SyncContext.OCF21BSubmit.Include("GS21BLineItemSubmit"))
            {
                var allSubmit = submit.OCF21B_InsurerTotals_SubTotal_Proposed;
                var allItemsSubmit = submit.GS21BLineItemSubmit
                    .Select(t => t.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                    .Sum();

                var allApproved = 0.0m;
                var allItemsApproved = 0.0m;

                var docAr = SyncContext.OCF21BAR.Include("GS21BLineItemAR")
                    .FirstOrDefault(a => a.HCAI_Document_Number == submit.HCAI_Document_Number);
                if (docAr != null)
                {
                    allApproved = docAr.OCF21B_InsurerTotals_SubTotal_Approved;
                    allItemsApproved = docAr.GS21BLineItemAR
                        .Select(t => t.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost)
                        .Sum();
                }

                if (allSubmit == allItemsSubmit && allApproved == allItemsApproved) continue;

                _vmMain.DocOcf21Bs.Add(new DocOcf21BVm
                {
                    Number = submit.HCAI_Document_Number,
                    Submitted = allSubmit,
                    ItemsSubmitted = allItemsSubmit,
                    Approved = allApproved,
                    ItemsApproved = allItemsApproved,
                });
            }

            NoDataOcf21BGrid.Visibility = Visibility.Collapsed;
            NoProblemOcf21BGrid.Visibility = !_vmMain.DocOcf21Bs.Any() ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Ocf21CAnalyzeButtonOnClick(object sender, RoutedEventArgs e)
        {
            _vmMain.DocOcf21Cs.Clear();
            NoDataOcf21CGrid.Visibility = Visibility.Visible;

            foreach (var submit in SyncContext.OCF21CSubmit.Include("OtherReimbursableLineItemSubmits"))
            {
                var allSubmit = submit.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Proposed;
                var allItemsSubmit = submit.OtherReimbursableLineItemSubmits
                    .Select(t => t.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                    .Sum();

                var allApproved = 0.0m;
                var allItemsApproved = 0.0m;

                var docAr = SyncContext.OCF21CAR.Include("OtherReimbursableLineItemARs")
                    .FirstOrDefault(a => a.HCAI_Document_Number == submit.HCAI_Document_Number);
                if (docAr != null)
                {
                    allApproved = docAr.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved;
                    allItemsApproved = docAr.OtherReimbursableLineItemARs
                        .Select(t => t.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost)
                        .Sum();
                }

                if (allSubmit == allItemsSubmit && allApproved == allItemsApproved) continue;

                _vmMain.DocOcf21Cs.Add(new DocOcf21CVm
                {
                    Number = submit.HCAI_Document_Number,
                    Submitted = allSubmit,
                    ItemsSubmitted = allItemsSubmit,
                    Approved = allApproved,
                    ItemsApproved = allItemsApproved,
                });
            }

            NoDataOcf21CGrid.Visibility = Visibility.Collapsed;
            NoProblemOcf21CGrid.Visibility = !_vmMain.DocOcf21Cs.Any() ? Visibility.Visible : Visibility.Collapsed;
        }

        private void Form1AnalyzeButtonOnClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Not Implemented");
        }

        private void ACSIAnalyzeButtonOnClick(object sender, RoutedEventArgs e)
        {
            _vmMain.DocAcsis.Clear();
            NoDataAcsiGrid.Visibility = Visibility.Visible;

            foreach (var submit in SyncContext.ACSISubmits.Include("GSACSILineItemSubmits"))
            {
                var allSubmit = submit.ACSI_InsurerTotals_SubTotal_Proposed;
                var allItemsSubmit = submit.GSACSILineItemSubmits
                    .Select(t => t.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                    .Sum();

                var allApproved = 0.0m;
                var allItemsApproved = 0.0m;

                var docAr = SyncContext.ACSIARs.Include("GSACSILineItemARs")
                    .FirstOrDefault(a => a.HCAI_Document_Number == submit.HCAI_Document_Number);
                if (docAr != null)
                {
                    allApproved = docAr.ACSI_InsurerTotals_SubTotal_Approved;
                    allItemsApproved = docAr.GSACSILineItemARs
                        .Select(t => t.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost)
                        .Sum();
                }

                if (allSubmit == allItemsSubmit && allApproved == allItemsApproved) continue;

                _vmMain.DocAcsis.Add(new DocAcsiVm
                {
                    Number = submit.HCAI_Document_Number,
                    Submitted = allSubmit,
                    ItemsSubmitted = allItemsSubmit,
                    Approved = allApproved,
                    ItemsApproved = allItemsApproved,
                });
            }

            NoDataAcsiGrid.Visibility = Visibility.Collapsed;
            NoProblemAcsiGrid.Visibility = !_vmMain.DocAcsis.Any() ? Visibility.Visible : Visibility.Collapsed;
        }
    }
}
