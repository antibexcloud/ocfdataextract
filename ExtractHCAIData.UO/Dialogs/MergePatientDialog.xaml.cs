﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Dialogs
{
    public partial class MergePatientDialog : Window
    {
        #region Commands

        public static RoutedUICommand CmdSaveMerge { get; } =
            new RoutedUICommand("SaveMerge", "CmdSaveMerge", typeof(MergePatientDialog));

        private void SetCommands()
        {
            CommandBindings.Add(new CommandBinding(CmdSaveMerge, CmdSaveMergeOnExecute, CmdSaveMergeOnCanExecute));
            SaveMergeButton.Command = CmdSaveMerge;
        }

        #endregion

        #region Life circle

        private readonly MainVm _vmMain;

        private SyncDbVm SourceDb => _vmMain.SyncDb;
        private HCAIOCFSyncEntities SyncContext => SourceDb.SyncContext;

        public MergePatientDialog()
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;
            var owner = (MainWindow)Owner;
            Icon = Owner.Icon;
            _vmMain = owner.VmMain;

            SetCommands();
            CommandsRefresh();
        }

        private static void CommandsRefresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        #endregion

        public void PrepareData(SyncPatient[] merged)
        {
            _vmMain.MergedPatients.Clear();
            foreach (var syncPatient in merged)
            {
                _vmMain.MergedPatients.Add(syncPatient);
            }

            Service.SetCombobox(FirstNameComboBox, merged.Select(p => p.ResultFirstName).Distinct().ToArray(), true);
            Service.SetCombobox(MiddleNameComboBox, merged.Select(p => p.ResultMiddleName).Distinct().ToArray(), true);
            Service.SetCombobox(LastNameComboBox, merged.Select(p => p.ResultLastName).Distinct().ToArray(), true);
            Service.SetCombobox(DobComboBox, merged.Select(p => p.ResultDOB.ToString("MM/dd/yyyy")).Distinct().ToArray(), false);
            Service.SetCombobox(GenderComboBox, merged.Select(p => p.ResultGender).Distinct().ToArray(), false);
            Service.SetCombobox(PmsKeyComboBox, merged.Where(p => !string.IsNullOrEmpty(p.ResultPmsKey)).Select(p => p.ResultPmsKey).Distinct().ToArray(), false);

            var variants = _vmMain.MergedPatients
                .GroupBy(m => $"{m.Backend.DOL:d}{m.Backend.MVAInsurerHCAIID}")
                .ToArray();
            var first = _vmMain.MergedPatients
                .OrderBy(m => m.Backend.DOL).ThenBy(m => m.CaseID)
                .First();
            _vmMain.SelectedPatient = variants.Length == 1 ? first : null;
        }

        private bool IsDataReady()
        {
            return Service.ReadyCombobox(FirstNameComboBox)
                   && Service.ReadyCombobox(MiddleNameComboBox)
                   && Service.ReadyCombobox(LastNameComboBox)
                   && Service.ReadyCombobox(DobComboBox)
                   && Service.ReadyCombobox(GenderComboBox)
                   && Service.ReadyCombobox(PmsKeyComboBox)
                   && _vmMain.SelectedPatient != null;
        }

        private Patient GetResult()
        {
            var item = new Patient
            {
                PMSPatientKey = !string.IsNullOrEmpty(PmsKeyComboBox.Text)
                    ? int.TryParse(PmsKeyComboBox.Text, out var id) ? id : null
                    : null,
                first_name = FirstNameComboBox.Text,
                middle_name = MiddleNameComboBox.Text,
                last_name = LastNameComboBox.Text,
                DOB = DateTime.ParseExact(DobComboBox.Text, "MM/dd/yyyy", null),
                gender = GenderComboBox.Text,

                CaseID = _vmMain.SelectedPatient.CaseID,

                HomeStreet = _vmMain.SelectedPatient.HomeStreet,
                HomeCity = _vmMain.SelectedPatient.HomeCity,
                HomeState = _vmMain.SelectedPatient.HomeState,
                HomeZip = _vmMain.SelectedPatient.HomeZip,
                HomePhone = _vmMain.SelectedPatient.HomePhone,

                DOL = _vmMain.SelectedPatient.DOL,
                MVAInsurerHCAIID = _vmMain.SelectedPatient.Backend.MVAInsurerHCAIID,
                MVABranchHCAIID = _vmMain.SelectedPatient.Backend.MVABranchHCAIID,
                MVAPolicyHolder = _vmMain.SelectedPatient.Backend.MVAPolicyHolder,
                MVALastName = _vmMain.SelectedPatient.Backend.MVALastName,
                MVAFirstName = _vmMain.SelectedPatient.Backend.MVAFirstName,
                MVAClaimNo = _vmMain.SelectedPatient.Backend.MVAClaimNo,
                MVAPolicyNo = _vmMain.SelectedPatient.Backend.MVAPolicyNo,
                MVAAdjFirstName = _vmMain.SelectedPatient.Backend.MVAAdjFirstName,
                MVAAdjLastName = _vmMain.SelectedPatient.Backend.MVAAdjLastName,
                MVAAdjTel = _vmMain.SelectedPatient.Backend.MVAAdjTel,
                MVAAdjFax = _vmMain.SelectedPatient.Backend.MVAAdjFax,
                MVAAdjExt = _vmMain.SelectedPatient.Backend.MVAAdjExt,
            };

            if (string.IsNullOrEmpty(item.HomePhone))
            {
                item.HomePhone = _vmMain.MergedPatients
                    .FirstOrDefault(p => !string.IsNullOrEmpty(p.HomePhone))?.HomePhone ?? string.Empty;
            }

            return item;
        }

        private bool BaseReady => IsLoaded && _vmMain != null;

        private void CmdSaveMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady && IsDataReady();
        }

        private void CmdSaveMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var res = GetResult();

                var specialties = _vmMain.MergedPatients
                    .Where(p => !string.IsNullOrEmpty(p.Backend.SpecialtyCases))
                    .ToList()
                    .SelectMany(p => p.Backend.SpecialtyCases.Split(';'))
                    .Distinct()
                    .ToList();

                var virt = _vmMain.MergedPatients.FirstOrDefault(p => p.IsVirtual);
                if (virt != null)
                {
                    var rest = _vmMain.MergedPatients.Where(p => !p.IsVirtual).ToList();
                    foreach (var syncPatient in rest)
                        syncPatient.SetVirtualId(virt.Id);

                    var virtuals = _vmMain.MergedPatients
                        .Where(p => p.IsVirtual && p.Id != virt.Id)
                        .ToList();
                    foreach (var virtualPatient in virtuals)
                    {
                        var patientIds = virtualPatient.Backend.Patient1.Select(p => p.Id).ToList();
                        foreach (var patientId in patientIds)
                        {
                            var syncPatient = SourceDb.SyncPatients.First(p => p.Id == patientId);
                            syncPatient.SetVirtualId(virt.Id);
                        }

                        // delete virtual Patient
                        SyncContext.Patients.Remove(virtualPatient.Backend);
                        SyncContext.SaveChangesWithDetect();
                        SourceDb.SyncPatients.Remove(virtualPatient);
                    }

                    virt.Backend.PMSPatientKey = res.PMSPatientKey;

                    virt.first_name = res.ResultFirstName;
                    virt.middle_name = res.ResultMiddleName;
                    virt.last_name = res.ResultLastName;
                    virt.gender = res.ResultGender;
                    virt.DOB = res.ResultDOB;

                    virt.HomeStreet = res.HomeStreet;
                    virt.HomeCity = res.HomeCity;
                    virt.HomeState = res.HomeState;
                    virt.HomeZip = res.HomeZip;
                    virt.HomePhone = res.HomePhone;

                    virt.CaseID = res.CaseID;
                    virt.Backend.DOL = res.DOL;
                    virt.Backend.MVAInsurerHCAIID = res.MVAInsurerHCAIID;
                    virt.Backend.MVABranchHCAIID = res.MVABranchHCAIID;
                    virt.Backend.MVAPolicyHolder = res.MVAPolicyHolder;
                    virt.Backend.MVALastName = res.MVALastName;
                    virt.Backend.MVAFirstName = res.MVAFirstName;
                    virt.Backend.MVAClaimNo = res.MVAClaimNo;
                    virt.Backend.MVAPolicyNo = res.MVAPolicyNo;
                    virt.Backend.MVAAdjFirstName = res.MVAAdjFirstName;
                    virt.Backend.MVAAdjLastName = res.MVAAdjLastName;
                    virt.Backend.MVAAdjTel = res.MVAAdjTel;
                    virt.Backend.MVAAdjFax = res.MVAAdjFax;
                    virt.Backend.MVAAdjExt = res.MVAAdjExt;
                    virt.Backend.SpecialtyCases = specialties.Any() ? string.Join(";", specialties) : string.Empty;

                    virt.SetSpecialtyCases();
                }
                else
                {
                    res.SpecialtyCases = specialties.Any() ? string.Join(";", specialties) : string.Empty;
                    SyncContext.Patients.Add(res);
                    SyncContext.SaveChangesWithDetect();
                    res = SyncContext.Patients.OrderByDescending(p => p.Id).First();

                    virt = new SyncPatient(res);
                    SourceDb.SyncPatients.Add(virt);

                    var rest = _vmMain.MergedPatients.ToArray();
                    foreach (var syncPatient in rest)
                        syncPatient.SetVirtualId(virt.Id);
                }

                SyncContext.SaveChangesWithDetect();
                virt.TryLink(_vmMain.TargetDb, MainWindow.Configuration.GroupByProviderSpecialty);

                DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Service.InworkException(ex));
            }
        }

        private void CloseButtonOnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
