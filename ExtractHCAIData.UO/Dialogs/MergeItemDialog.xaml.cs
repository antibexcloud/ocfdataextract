﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Utils;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for MergeItemDialog.xaml
    /// </summary>
    public partial class MergeItemDialog : Window
    {
        #region Commands

        private static readonly RoutedUICommand SaveMergeCommand = new RoutedUICommand("SaveMerge", "CmdSaveMerge", typeof(MergeItemDialog));
        public static RoutedUICommand CmdSaveMerge => SaveMergeCommand;

        private void SetCommands()
        {
            CommandBindings.Add(new CommandBinding(CmdSaveMerge, CmdSaveMergeOnExecute, CmdSaveMergeOnCanExecute));
            SaveMergeButton.Command = CmdSaveMerge;
        }

        #endregion

        #region Life circle

        private readonly MainVm _vmMain;

        private SyncDbVm SourceDb => _vmMain.SyncDb;
        private HCAIOCFSyncEntities SyncContext => SourceDb.SyncContext;

        public MergeItemDialog()
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;
            var owner = (MainWindow)Owner;
            Icon = Owner.Icon;
            _vmMain = owner.VmMain;

            SetCommands();
            CommandsRefresh();
        }

        private static void CommandsRefresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        #endregion

        public void PrepareData(SyncActivityItem[] merged)
        {
            _vmMain.MergedActivityItems.Clear();
            foreach (var syncItem in merged)
                _vmMain.MergedActivityItems.Add(syncItem);

            ServiceCodeTextBox.Text = merged.First().ServiceCode;
            Service.SetCombobox(TypeComboBox, merged.Select(t => t.Type).Distinct().ToArray(), false);
            Service.SetCombobox(ItemIdComboBox, merged.Select(t => t.ItemId).Distinct().ToArray(), true);
            Service.SetCombobox(NameComboBox, merged.Select(t => t.Name).Distinct().ToArray(), true);
            Service.SetCombobox(DurationComboBox, merged.Select(t => t.Duration).Distinct().ToArray(), true);
            Service.SetCombobox(AttributeComboBox, merged.Select(t => t.Attribute).Distinct().ToArray(), true);

            Service.SetCombobox(UnitsComboBox, merged.Select(t => t.Units.ToString("F")).Distinct().ToArray(), true);
            Service.SetCombobox(MeasureComboBox, merged.Select(t => t.Measure).Distinct().ToArray(), true);
            Service.SetCombobox(FeeComboBox, merged.Select(t => t.Fee.ToString("F")).Distinct().ToArray(), true);

            Service.SetCombobox(TaxIdComboBox, merged.Select(t => t.TaxId).Distinct().ToArray(), false);
            Service.SetCombobox(GSTComboBox, merged.Select(t => t.GST.ToString("N0")).Distinct().ToArray(), false);
            Service.SetCombobox(PSTComboBox, merged.Select(t => t.PST.ToString("N0")).Distinct().ToArray(), false);

            Service.SetCombobox(ItemDescriptionComboBox, merged.Select(t => t.ItemDescription).Distinct().ToArray(), true);
            Service.SetCombobox(IsProductComboBox, merged.Select(t => t.IsProduct ? "Yes" : "No").Distinct().ToArray(), false);
        }

        private bool IsDataReady()
        {
            return Service.ReadyCombobox(TypeComboBox)
                   && Service.ReadyCombobox(ItemIdComboBox)
                   && Service.ReadyCombobox(NameComboBox)
                   && Service.ReadyCombobox(DurationComboBox)
                   && Service.ReadyCombobox(AttributeComboBox)
                   
                   && Service.ReadyCombobox(UnitsComboBox)
                   && Service.ReadyCombobox(MeasureComboBox)
                   && Service.ReadyCombobox(FeeComboBox)
                   
                   && Service.ReadyCombobox(TaxIdComboBox)
                   && Service.ReadyCombobox(GSTComboBox)
                   && Service.ReadyCombobox(PSTComboBox)
                   
                   && Service.ReadyCombobox(ItemDescriptionComboBox)
                   && Service.ReadyCombobox(IsProductComboBox);
        }

        private ActivityItem GetResult()
        {
            var item = new ActivityItem
            {
                IsProduct = IsProductComboBox.Text == "Yes",
                activity_id = ItemIdComboBox.Text.SafetySubstring(50),
                activity_name = NameComboBox.Text.SafetySubstring(500),
                activity_type = TypeComboBox.Text,
                activity_duration = DurationComboBox.Text ?? string.Empty,
                
                mvaServiceCode = ServiceCodeTextBox.Text.SafetySubstring(16),
                mvaAttribute = (AttributeComboBox.Text ?? string.Empty).SafetySubstring(3),
                mvaMeasure = MeasureComboBox.Text ?? string.Empty,
                mvaUnits = ServiceConvert.ConvertToDouble(UnitsComboBox.Text),
                mvaTaxID = TaxIdComboBox.Text ?? string.Empty,
                mvaFee = ServiceConvert.ConvertToDouble(FeeComboBox.Text),
                GST = ServiceConvert.ConvertToDouble(GSTComboBox.Text),
                PST = ServiceConvert.ConvertToDouble(PSTComboBox.Text),
                ItemDescription = SyncService.CheckOnQuotes(ItemDescriptionComboBox.Text).SafetySubstring(500),
            };

            return item;
        }

        private bool BaseReady => IsLoaded && _vmMain != null;

        private void CmdSaveMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady;
            if (!e.CanExecute) return;

            e.CanExecute = IsDataReady();
        }

        private void CmdSaveMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var res = GetResult();

                var virt = _vmMain.MergedActivityItems.FirstOrDefault(p => p.IsVirtual);
                if (virt != null)
                {
                    var rest = _vmMain.MergedActivityItems.Where(p => !p.IsVirtual).ToList();
                    foreach (var syncItem in rest)
                        syncItem.SetVirtualId(virt.Id);

                    var virtuals = _vmMain.MergedActivityItems
                        .Where(p => p.IsVirtual && p.Id != virt.Id)
                        .ToList();
                    foreach (var virtualProduct in virtuals)
                    {
                        var productIds = virtualProduct.Backend.ActivityItems1.Select(p => p.Id).ToList();
                        foreach (var productId in productIds)
                        {
                            var syncProduct = SourceDb.ActivityItems.First(p => p.Id == productId);
                            syncProduct.SetVirtualId(virt.Id);
                        }

                        // delete virtual syncProduct
                        SyncContext.ActivityItems.Remove(virtualProduct.Backend);
                        SyncContext.SaveChangesWithDetect();
                        SourceDb.ActivityItems.Remove(virtualProduct);
                    }

                    virt.ItemId = res.activity_id.SafetySubstring(50);
                    virt.Name = res.activity_name.SafetySubstring(500);
                    virt.Type = res.activity_type;
                    virt.ItemDescription = SyncService.CheckOnQuotes(res.ItemDescription).SafetySubstring(500);
                    virt.Duration = res.activity_duration;

                    virt.ServiceCode = res.mvaServiceCode.SafetySubstring(50);
                    virt.Attribute = res.ResultAttribute.SafetySubstring(5);
                    virt.Measure = res.ResultMeasure;
                    virt.Units = res.ResultUnits;
                    virt.Fee = res.mvaFee;
                    virt.TaxId = res.mvaTaxID;
                    virt.Backend.GST = res.GST;
                    virt.Backend.PST = res.PST;

                    SyncContext.SaveChangesWithDetect();
                }
                else
                {
                    SyncContext.ActivityItems.Add(res);
                    SyncContext.SaveChangesWithDetect();
                    res = SyncContext.ActivityItems.OrderByDescending(p => p.Id).First();

                    virt = new SyncActivityItem(res);
                    SourceDb.ActivityItems.Add(virt);

                    var rest = _vmMain.MergedActivityItems.ToArray();
                    foreach (var syncItem in rest)
                        syncItem.SetVirtualId(virt.Id);
                    SyncContext.SaveChangesWithDetect();
                }

                DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Service.InworkException(ex));
            }
        }

        private void CloseButtonOnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
