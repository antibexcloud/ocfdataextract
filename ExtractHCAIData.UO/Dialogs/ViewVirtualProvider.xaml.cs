﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using ExtractHcaiData.Uo.Model.Sync.Providers;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for ViewVirtualProvider.xaml
    /// </summary>
    public partial class ViewVirtualProvider : Window
    {
        public ViewVirtualProvider()
        {
            InitializeComponent();
        }

        public ViewVirtualProvider(SyncProviderVm virtualProvider, List<SyncProviderVm> child)
        {
            InitializeComponent();

            ChildProviders = new ObservableCollection<SyncProviderVm>(child);
            Title = $"View virtual {virtualProvider.Name}";
        }

        private void CloseButtonOnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

        #region DP

        public ObservableCollection<SyncProviderVm> ChildProviders
        {
            get => (ObservableCollection<SyncProviderVm>)GetValue(ChildProvidersProperty);
            set => SetValue(ChildProvidersProperty, value);
        }

        public static readonly DependencyProperty ChildProvidersProperty =
            DependencyProperty.Register(nameof(ChildProviders), typeof(ObservableCollection<SyncProviderVm>), typeof(ViewVirtualProvider), null);

        #endregion
    }
}
