﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ExtractHcaiData.Uo.Model.Sync.Providers;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Dialogs
{
    /// <summary>
    /// Interaction logic for MergeProviderDialog.xaml
    /// </summary>
    public partial class MergeProviderDialog : Window
    {
        #region Commands

        private static readonly RoutedUICommand SaveMergeCommand = new RoutedUICommand("SaveMerge", "CmdSaveMerge", typeof(MergeProviderDialog));
        public static RoutedUICommand CmdSaveMerge => SaveMergeCommand;

        private void SetCommands()
        {
            CommandBindings.Add(new CommandBinding(CmdSaveMerge, CmdSaveMergeOnExecute, CmdSaveMergeOnCanExecute));
            SaveMergeButton.Command = CmdSaveMerge;
        }

        #endregion

        #region Life circle

        private readonly MainVm _vmMain;
        private readonly MainWindow _owner;

        private SyncDbVm SourceDb => _vmMain.SyncDb;
        private HCAIOCFSyncEntities SyncContext => SourceDb.SyncContext;

        public MergeProviderDialog()
        {
            InitializeComponent();

            Owner = Application.Current.MainWindow;
            Icon = Owner.Icon;
            _owner = (MainWindow)Owner;
            _vmMain = _owner.VmMain;

            SetCommands();
            CommandsRefresh();
        }

        private void CommandsRefresh()
        {
            CommandManager.InvalidateRequerySuggested();
        }

        #endregion

        public void PrepareData(SyncProviderVm[] merged)
        {
            _vmMain.MergedSyncProviders.Clear();
            foreach (var syncItem in merged)
                _vmMain.MergedSyncProviders.Add(syncItem);

            var findVirt = merged.FirstOrDefault(p => p.IsVirtual);

            Service.SetCombobox(HCAI_Provider_Registry_IDComboBox,
                merged.Select(p => p.HCAI_Provider_Registry_ID.ToString()).Distinct().ToArray(), findVirt == null);
            if (findVirt != null)
            {
                HCAI_Provider_Registry_IDComboBox.Text = findVirt.HCAI_Provider_Registry_ID.ToString();
                HCAI_Provider_Registry_IDComboBox.IsEnabled = false;
            }
            
            Service.SetCombobox(Provider_Last_NameComboBox, merged.Select(p => p.Provider_Last_Name).Distinct().ToArray(), true);
            Service.SetCombobox(Provider_First_NameComboBox, merged.Select(p => p.Provider_First_Name).Distinct().ToArray(), true);
            Service.SetCombobox(Provider_StatusComboBox, merged.Select(p => p.Provider_Status).Distinct().ToArray(), false);

            Service.SetCombobox(AddressComboBox, merged.Select(p => p.Address).Distinct().ToArray(), true);
            Service.SetCombobox(CityComboBox, merged.Select(p => p.City).Distinct().ToArray(), true);
            Service.SetCombobox(ProvinceComboBox, merged.Select(p => p.Province).Distinct().ToArray(), true);
            Service.SetCombobox(PostalCodeComboBox, merged.Select(p => p.PostalCode).Distinct().ToArray(), true);

            Service.SetCombobox(TelephoneNumberComboBox, merged.Select(p => p.TelephoneNumber).Distinct().ToArray(), true);
            Service.SetCombobox(TelephoneExtensionComboBox, merged.Select(p => p.TelephoneExtension).Distinct().ToArray(), true);
            Service.SetCombobox(FaxNumberComboBox, merged.Select(p => p.FaxNumber).Distinct().ToArray(), true);
            Service.SetCombobox(EmailComboBox, merged.Select(p => p.Email).Distinct().ToArray(), true);
        }

        private bool IsDataReady()
        {
            return Service.ReadyCombobox(HCAI_Provider_Registry_IDComboBox)
                   && Service.ReadyCombobox(Provider_Last_NameComboBox)
                   && Service.ReadyCombobox(Provider_First_NameComboBox)
                   && Service.ReadyCombobox(Provider_First_NameComboBox)
                   
                   && Service.ReadyCombobox(AddressComboBox)
                   && Service.ReadyCombobox(CityComboBox)
                   && Service.ReadyCombobox(ProvinceComboBox)
                   && Service.ReadyCombobox(PostalCodeComboBox)
                   
                   && Service.ReadyCombobox(TelephoneNumberComboBox)
                   && Service.ReadyCombobox(TelephoneExtensionComboBox)
                   && Service.ReadyCombobox(FaxNumberComboBox)
                   && Service.ReadyCombobox(EmailComboBox);
        }

        private ProviderVirtual GetResult()
        {
            var facilityId = _vmMain.MergedSyncProviders.First().HCAI_Facility_Registry_ID;
            return new ProviderVirtual
            {
                HCAI_Provider_Registry_ID = int.TryParse(HCAI_Provider_Registry_IDComboBox.Text, out var providerRegId)
                    ? providerRegId
                    : 0,
                HCAI_Facility_Registry_ID = facilityId,

                Provider_First_Name = Provider_First_NameComboBox.Text ?? string.Empty,
                Provider_Last_Name = Provider_Last_NameComboBox.Text ?? string.Empty,
                Provider_Status = Provider_StatusComboBox.Text,
                
                Address = AddressComboBox.Text ?? string.Empty,
                City = CityComboBox.Text ?? string.Empty,
                PostalCode = PostalCodeComboBox.Text ?? string.Empty,
                Province = ProvinceComboBox.Text ?? string.Empty,

                Email = EmailComboBox.Text ?? string.Empty,
                FaxNumber = FaxNumberComboBox.Text ?? string.Empty,
                TelephoneNumber = TelephoneNumberComboBox.Text ?? string.Empty,
                TelephoneExtension = TelephoneExtensionComboBox.Text ?? string.Empty,
            };
        }

        private bool BaseReady => IsLoaded && (_vmMain != null);

        private void CmdSaveMergeOnCanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = BaseReady && IsDataReady();
        }

        private void CmdSaveMergeOnExecute(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                var res = GetResult();
                var virt = _vmMain.MergedSyncProviders.FirstOrDefault(p => p.IsVirtual);
                
                if (virt != null)
                {
                    var rest = _vmMain.MergedSyncProviders.Where(p => !p.IsVirtual).ToArray();
                    foreach (var syncProviderVm in rest)
                    {
                        syncProviderVm.BackendProvider.SetVirtualId(virt.HCAI_Provider_Registry_ID);
                        _vmMain.SyncProviders.Remove(syncProviderVm);
                    }

                    var virtuals = _vmMain.MergedSyncProviders
                        .Where(p => p.IsVirtual && p.HCAI_Provider_Registry_ID != virt.HCAI_Provider_Registry_ID)
                        .ToList();
                    foreach (var syncProviderVm in virtuals)
                    {
                        // ???
                        var providerIds = syncProviderVm.BackendVirtual.Backend.ProviderLineItems
                            .Where(l => l.virtualId == syncProviderVm.HCAI_Provider_Registry_ID)
                            .Select(l => l.HCAI_Provider_Registry_ID)
                            .ToList();
                        foreach (var providerId in providerIds)
                        {
                            var syncProduct = SourceDb.Providers.First(p => p.HCAI_Provider_Registry_ID == providerId);
                            syncProduct.SetVirtualId(virt.HCAI_Provider_Registry_ID);
                        }

                        // delete virtual syncProvider
                        SyncContext.ProviderVirtuals.Remove(syncProviderVm.BackendVirtual.Backend);
                        SyncContext.SaveChangesWithDetect();
                        _vmMain.SyncProviders.Remove(syncProviderVm);
                    }

                    virt.BackendVirtual.HCAI_Provider_Registry_ID = res.HCAI_Provider_Registry_ID;
                    virt.BackendVirtual.Provider_First_Name = res.ResultFirstName;
                    virt.BackendVirtual.Provider_Last_Name = res.ResultLastName;
                    virt.BackendVirtual.Provider_Status = res.Provider_Status;

                    virt.BackendVirtual.Address = res.Address;
                    virt.BackendVirtual.City = res.City;
                    virt.BackendVirtual.PostalCode = res.PostalCode;
                    virt.BackendVirtual.Province = res.Province;

                    virt.BackendVirtual.Email = res.Email;
                    virt.BackendVirtual.FaxNumber = res.FaxNumber;
                    virt.BackendVirtual.TelephoneNumber = res.TelephoneNumber;
                    virt.BackendVirtual.TelephoneExtension = res.TelephoneExtension;

                    SyncContext.SaveChangesWithDetect();
                }
                else
                {
                    SyncContext.ProviderVirtuals.Add(res);
                    SyncContext.SaveChangesWithDetect();

                    var syncVirt = new SyncProviderVirtual(res);
                    virt = new SyncProviderVm(null, syncVirt);
                    SourceDb.ProviderVirtuals.Add(syncVirt);

                    var rest = _vmMain.MergedSyncProviders.ToArray();
                    foreach (var syncProviderVm in rest)
                    {
                        syncProviderVm.BackendProvider.SetVirtualId(virt.HCAI_Provider_Registry_ID);
                    }
                    SyncContext.SaveChangesWithDetect();

                    foreach (var syncProviderVm in rest)
                    {
                        syncProviderVm.BackendProvider.Refresh(SyncContext);
                        _vmMain.SyncProviders.Remove(syncProviderVm);
                    }

                    syncVirt.TryLink(_owner._transfer);
                    _vmMain.SyncProviders.Add(virt);
                }

                DialogResult = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(Service.InworkException(ex));
            }
        }

        private void CloseButtonOnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
    }
}
