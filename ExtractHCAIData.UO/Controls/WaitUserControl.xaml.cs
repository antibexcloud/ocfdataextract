﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace ExtractHcaiData.Uo.Controls
{
    public partial class WaitUserControl : UserControl
    {
        public WaitUserControl()
        {
            InitializeComponent();
        }

        private void UIElementOnIsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var vis = (bool)e.NewValue;

            var res = this.Resources["KeyStoryboard"];
            var storyboard = res as Storyboard;
            if (storyboard is null) return;

            if (vis)
                storyboard.Begin();
            else
                storyboard.Remove();
        }
    }
}
