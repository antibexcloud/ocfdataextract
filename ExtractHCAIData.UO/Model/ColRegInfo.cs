﻿namespace ExtractHcaiData.Uo.Model
{
    public class ColRegInfo
    {
        public string ColRegType { get; set; }
        public short? ColRegRegulated { get; set; }

        public string ColRegNo { get; set; }
        public string ColRegHrRate { get; set; }
        public string ColRegServiceType { get; set; }
        public string ColRegSignature { get; set; }

        public string StaffOccupation { get; set; }
    }
}
