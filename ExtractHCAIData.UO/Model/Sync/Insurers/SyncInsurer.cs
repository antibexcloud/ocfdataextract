﻿using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Sync.Insurers
{
    public class SyncInsurer : VMBase
    {
        public InsurerLineItem Backend { get; }

        public CprInsurer UoInsurer { get; set; }

        public SyncInsurer(InsurerLineItem backend)
        {
            Backend = backend;
        }

        public bool IsUo => UoInsurer != null;
    }
}
