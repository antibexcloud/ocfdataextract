﻿using System.Linq;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Utils;

namespace ExtractHcaiData.Uo.Model.Sync.Items
{
    public class SyncActivityItem : VMBase
    {
        private bool _isUo;
        public ActivityItem Backend { get; }

        public CprActivity UoActivity { get; set; }
        public CprProduct UoProduct { get; set; }

        public string RealServiceCode => UoActivity?.Backend.mvaServiceCode ?? UoProduct?.Backend.mvaServiceCode ?? this.ServiceCode;
        public string RealName => UoActivity?.Backend.activity_name ?? UoProduct?.Backend.product_name ?? this.Name;

        public SyncActivityItem(ActivityItem backend)
        {
            Backend = backend;
        }

        public void TryLink(TargetDbVm targetDb)
        {
            if (IsProduct)
            {
                if (this.ServiceCode == this.ItemId)
                {
                    var uoProductsByCode = targetDb.Products.Where(a => a.EqualsByCode(this)).ToList();
                    UoProduct = uoProductsByCode.Count == 1 ? uoProductsByCode.First() : null;

                    if (UoProduct != null)
                    {
                        this.ItemId = UoProduct.Backend.product_id.Trim();
                        this.Name = UoProduct.Backend.product_name?.Trim() ?? this.ItemDescription;
                        this.Measure = ConvertHelper.GetHcaiMeasureFromActivityOrProduct(UoProduct.Backend.mvaMeasure, this.Measure);
                        this.Units = UoProduct.Backend.mvaUnits ?? this.Units;
                    }
                }
                else
                {
                    var uoProductsByItemId = targetDb.Products.Where(a => a.EqualsBtItemId(this)).ToList();
                    var uoProductsByDescription = targetDb.Products.Where(a => a.EqualsByDescription(this)).ToList();
                    UoProduct = (uoProductsByItemId.Count == 1 ? uoProductsByItemId.First() : null)
                                ?? uoProductsByDescription.FirstOrDefault();
                }
            }
            else
            {
                if (this.ServiceCode == this.ItemId)
                {
                    var uoActivitiesByCode = targetDb.Activities.Where(a => a.EqualsByCode(this)).ToList();
                    UoActivity = uoActivitiesByCode.Count == 1 ? uoActivitiesByCode.First() : null;

                    if (UoActivity is null && uoActivitiesByCode.Any())
                    {
                        var uoActivitiesByItemId = targetDb.Activities.Where(a => a.EqualsByItemId(this)).ToList();
                        if (uoActivitiesByItemId.Count == 1)
                            UoActivity = uoActivitiesByItemId.First();
                    }

                    if (UoActivity != null)
                    {
                        this.ItemId = UoActivity.Backend.activity_id.Trim();
                        this.Name = UoActivity.Backend.activity_name?.Trim() ?? this.ItemDescription;
                        this.Type = UoActivity.Backend.activity_type?.Trim() ?? this.Type;
                        this.Measure = ConvertHelper.GetHcaiMeasureFromActivityOrProduct(UoActivity.Backend.mvaMeasure, this.Measure);
                        this.Units = UoActivity.Backend.mvaUnits ?? this.Units;
                        this.Attribute = UoActivity.Backend.mvaAttribute ?? string.Empty;
                        this.Duration = UoActivity.Backend.activity_duration ?? string.Empty;
                    }
                }
                else
                {
                    var uoActivitiesByItemId = targetDb.Activities.Where(a => a.EqualsByItemId(this)).ToList();
                    var uoActivitiesByDescription = targetDb.Activities.Where(a => a.EqualsByDescription(this)).ToList();
                    UoActivity = (uoActivitiesByItemId.Count == 1 ? uoActivitiesByItemId.First() : null)
                                 ?? uoActivitiesByDescription.FirstOrDefault();
                }
            }

            SetInUo();
        }

        public void SetVirtualId(int? id)
        {
            Backend.virtualId = id;
            NotifyPropertyChanged(nameof(IsInVirtual));
            if (id is null) return;

            SetInUo();
        }

        public static bool CanBeMerged(SyncActivityItem[] items)
        {
            if (items is null || !items.Any()) return false;

            var first = items.First();
            var res = items.All(t => t.IsProduct == first.IsProduct && t.ServiceCode == first.ServiceCode);
            return res;
        }

        private void SetInUo()
        {
            IsUo = IsProduct ? UoProduct != null : UoActivity != null;
        }

        public bool IsUo
        {
            get => _isUo;
            private set
            {
                if (_isUo == value) return;
                _isUo = value;
                NotifyPropertyChanged();
            }
        }

        public int? VirtualId => Backend.virtualId;

        public bool IsInVirtual => VirtualId != null;

        public bool IsVirtual => Backend.ActivityItems1.Any();

        #region IActivityItem

        public int Id => Backend.Id;

        public bool IsNotProduct => !Backend.IsProduct;

        public bool IsProduct
        {
            get => Backend.IsProduct;
            set
            {
                Backend.IsProduct = value;
                NotifyPropertyChanged();
            }
        }

        public string ItemId
        {
            get => Backend.activity_id;
            set
            {
                if (Backend.activity_id == value) return;
                Backend.activity_id = value;
                NotifyPropertyChanged();
            }
        }

        public string ItemDescription
        {
            get => SyncService.CheckOnQuotes(Backend.ItemDescription);
            set
            {
                if (Backend.ItemDescription == value) return;
                Backend.ItemDescription = value;
                if (Backend.activity_name == Backend.mvaServiceCode)
                {
                    this.Name = value;
                }

                NotifyPropertyChanged();
            }
        }

        public string Name
        {
            get => !string.IsNullOrEmpty(Backend.activity_name) && Backend.activity_name != Backend.mvaServiceCode
                ? Backend.activity_name
                : Backend.ItemDescription;
            set
            {
                if (Backend.activity_name == value) return;
                Backend.activity_name = value;
                NotifyPropertyChanged();
            }
        }

        public string Duration
        {
            get => Backend.activity_duration;
            set
            {
                Backend.activity_duration = value;
                NotifyPropertyChanged();
            }
        }

        public string Type
        {
            get => Backend.activity_type;
            set
            {
                if (Backend.activity_type == value) return;
                Backend.activity_type = value;
                NotifyPropertyChanged();
            }
        }

        public string Attribute
        {
            get => Backend.ResultAttribute;
            set
            {
                if (Backend.ResultAttribute == value) return;
                Backend.ResultAttribute = value;
                NotifyPropertyChanged();
            }
        }

        public string Measure
        {
            get => Backend.ResultMeasure;
            set
            {
                if (Backend.ResultMeasure == value) return;
                Backend.ResultMeasure = value;
                NotifyPropertyChanged();
            }
        }

        public double Units
        {
            get => Backend.ResultUnits;
            set
            {
                if (Backend.ResultUnits == value) return;
                Backend.ResultUnits = value;
                NotifyPropertyChanged();
            }
        }

        public string ServiceCode
        {
            get => Backend.mvaServiceCode;
            set
            {
                if (Backend.mvaServiceCode == value) return;
                Backend.mvaServiceCode = value;
                NotifyPropertyChanged();
            }
        }

        public double Fee
        {
            get => Backend.mvaFee;
            set
            {
                Backend.mvaFee = value;
                NotifyPropertyChanged();
            }
        }

        public string TaxId
        {
            get => Backend.mvaTaxID;
            set
            {
                Backend.mvaTaxID = value;
                NotifyPropertyChanged();
            }
        }

        public double GST
        {
            get => Backend.GST;
            set
            {
                Backend.GST = value;
                NotifyPropertyChanged();
            }
        }

        public double PST
        {
            get => Backend.PST;
            set
            {
                Backend.PST = value;
                NotifyPropertyChanged();
            }
        }

        #endregion
    }
}
