﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using ExtractHcaiData.Uo.Work;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Sync.Patients
{
    public class SyncPatient : VMBase
    {
        private bool _caseOne;

        public Patient Backend { get; }

        public CprClient UoClient { get; set; }
        public Client_Case UoCase { get; set; }

        public List<SpecialtyCaseModel> SpecialtyCases { get; } = new();

        public SyncPatient(Patient backend)
        {
            Backend = backend;
            _caseOne = CaseID.EndsWith("1");

            SetSpecialtyCases();
        }

        public void SetSpecialtyCases()
        {
            var specialties = (Backend.SpecialtyCases ?? string.Empty)
                .Split(';')
                .Where(s => !string.IsNullOrEmpty(s))
                .ToList();
            SpecialtyCases.Clear();
            foreach (var specialty in specialties)
            {
                SpecialtyCases.Add(new SpecialtyCaseModel
                {
                    Specialty = specialty.ToUpper(),
                    CaseId = GenerateBaseCaseId(null, ResultLastName, ResultFirstName, specialty),
                });
            }
        }

        private void AddSpecialty(TransferWork worker, string specialtyAbbr)
        {
            if (string.IsNullOrEmpty(specialtyAbbr)) return;

            var specialties = (Backend.SpecialtyCases ?? string.Empty)
                .Split(';')
                .Where(s => !string.IsNullOrEmpty(s))
                .ToList();
            if (specialties.Contains(specialtyAbbr.ToUpper())) return;

            specialties.Add(specialtyAbbr.ToUpper());
            Backend.SpecialtyCases = string.Join(";", specialties);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            SetSpecialtyCases();
        }

        public void SetVirtualId(int? id)
        {
            Backend.virtualId = id;
            NotifyPropertyChanged(nameof(IsInVirtual));
            if (id is null) return;

            SpecialtyCases.Clear();
            NotifyPropertyChanged(nameof(IsUo));
        }

        public void TryLink(TargetDbVm targetDb, bool groupByProviderSpecialty)
        {
            if (UoClient is null)
            {
                var all = targetDb.Clients.Where(cl => cl.Equals(this))
                    .OrderBy(c => c.Backend.client_id)
                    .ToList();
                if (all.Count <= 1)
                    UoClient = all.FirstOrDefault();
                else
                {
                    UoClient = all.FirstOrDefault(c => c.Backend.Client_Case.Any(cs => cs.CaseType == "MVA"))
                               ?? all.FirstOrDefault(c => c.Backend.Client_Case.Any())
                               ?? all.FirstOrDefault();
                }
            }

            if (groupByProviderSpecialty)
            {
                if (UoClient != null)
                {
                    foreach (var specialtyCase in SpecialtyCases)
                    {
                        if (specialtyCase.UoCase != null) continue;

                        specialtyCase.UoCase = UoClient.Backend.Client_Case
                            .FirstOrDefault(c => c.CaseType == "MVA"
                                                 && c.CaseID.StartsWith($"{specialtyCase.CaseId}_"));
                    }
                }
            }
            else
            {
                if (UoCase is null && UoClient != null)
                {
                    UoCase = UoClient.Backend.Client_Case
                                 .FirstOrDefault(c => c.CaseType == "MVA"
                                                      && c.CaseID.StartsWith($"{CaseID}_"))
                             ?? UoClient.Backend.Client_Case
                                 .FirstOrDefault(c => c.CaseType == "MVA"
                                                      && c.MVAInsurerHCAIID == Backend.MVAInsurerHCAIID
                                                      && c.MVABranchHCAIID == Backend.MVABranchHCAIID);
                    if (UoCase != null)
                    {
                        var baseId = UoCase.PureCaseId;
                        if (CaseID != baseId)
                            CaseID = baseId;
                    }
                }
            }
        }

        public bool IsUo => UoClient != null
                            && (UoCase != null
                                || (SpecialtyCases.Any() && SpecialtyCases.All(c => c.UoCase != null)));

        public int? VirtualId => Backend.virtualId;

        public bool IsInVirtual => Backend.virtualId != null;

        public bool IsVirtual => Backend.Patient1.Any();

        public bool CaseOne
        {
            get => _caseOne;
            set
            {
                _caseOne = value;
                NotifyPropertyChanged();

                if (_caseOne)
                {
                    if (!CaseID.EndsWith("1"))
                        CaseID += "1";
                }
                else
                {
                    if (CaseID.EndsWith("1"))
                        CaseID = CaseID.Substring(0, CaseID.Length-1);
                }

                foreach (var specialtyCase in SpecialtyCases)
                {
                    if (_caseOne)
                    {
                        if (!specialtyCase.CaseId.EndsWith("1"))
                            specialtyCase.CaseId += "1";
                    }
                    else
                    {
                        if (specialtyCase.CaseId.EndsWith("1"))
                            specialtyCase.CaseId = specialtyCase.CaseId.Substring(0, specialtyCase.CaseId.Length - 1);
                    }
                }
            }
        }

        public string Name =>
            Backend.ResultFirstName.Trim() +
            (string.IsNullOrEmpty(Backend.ResultMiddleName) ? "" : " " + Backend.ResultMiddleName.Trim()) +
            " " + Backend.ResultLastName.Trim();

        private bool EqualsCore(SyncPatient other)
        {
            return (Name.ToLower() == other.Name.ToLower()
                    || ((this.Backend.ResultFirstName.ToUpper() == other.Backend.ResultFirstName.ToUpper()
                        || this.Backend.first_name.ToUpper() == other.Backend.first_name.ToUpper()
                        || this.Backend.ResultFirstName.ToUpper() == other.Backend.first_name.ToUpper()
                        || this.Backend.first_name.ToUpper() == other.Backend.ResultFirstName.ToUpper())
                        && (this.Backend.ResultLastName.ToUpper() == other.Backend.ResultLastName.ToUpper()
                            || this.Backend.last_name.ToUpper() == other.Backend.last_name.ToUpper()
                            || this.Backend.ResultLastName.ToUpper() == other.Backend.last_name.ToUpper()
                            || this.Backend.last_name.ToUpper() == other.Backend.ResultLastName.ToUpper()))
                    || ((this.Backend.ResultFirstName.ToUpper() == other.Backend.ResultLastName.ToUpper()
                         || this.Backend.first_name.ToUpper() == other.Backend.last_name.ToUpper()
                         || this.Backend.first_name.ToUpper() == other.Backend.ResultLastName.ToUpper()
                         || this.Backend.ResultFirstName.ToUpper() == other.Backend.last_name.ToUpper())
                        && (this.Backend.ResultLastName.ToUpper() == other.Backend.ResultFirstName.ToUpper()
                            || this.Backend.last_name.ToUpper() == other.Backend.first_name.ToUpper()
                            || this.Backend.ResultLastName.ToUpper() == other.Backend.first_name.ToUpper()
                            || this.Backend.last_name.ToUpper() == other.Backend.ResultFirstName.ToUpper())))
                   // DOB
                   && (Backend.ResultDOB == other.Backend.ResultDOB
                       || Backend.DOB == other.Backend.DOB
                       || Backend.DOB == other.Backend.ResultDOB
                       || Backend.ResultDOB == other.Backend.DOB)
                   // Gender
                   && (string.IsNullOrEmpty(Backend.ResultGender)
                       || string.IsNullOrEmpty(other.Backend.ResultGender)
                       || (Backend.ResultGender == other.Backend.ResultGender
                           || Backend.gender == other.Backend.gender
                           || Backend.gender == other.Backend.ResultGender
                           || Backend.ResultGender == other.Backend.gender));
        }

        public bool Equals(SyncPatient other)
        {
            if (this.Backend.PMSPatientKey > 0
                && other.Backend.PMSPatientKey > 0
                && this.Backend.PMSPatientKey.Value == other.Backend.PMSPatientKey)
                return true;

            return EqualsCore(other)
                   // Case Data
                   && (Backend.DOL is null
                       || other.Backend.DOL is null
                       || Math.Abs((Backend.DOL.Value - other.Backend.DOL.Value).TotalDays) <= 30)
                   /*&& Backend.MVAInsurerHCAIID == other.Backend.MVAInsurerHCAIID
                   && Backend.MVABranchHCAIID == other.Backend.MVABranchHCAIID*/;
        }

        private static string GenerateBaseCaseId(ObservableCollection<SyncPatient> items, string lastName, string firstName, string specialtyAbbr = "")
        {
            var prepared = new string(lastName.ToCharArray().Where(char.IsLetter).Select(ch => ch).ToArray());
            var pCaseId = prepared.Substring(0, Math.Min(3, prepared.Length));

            if (firstName.Length > 1)
                pCaseId += firstName.Substring(0, Math.Min(1, firstName.Length)).ToUpper();

            if (!string.IsNullOrEmpty(specialtyAbbr))
            {
                pCaseId += $"-{specialtyAbbr.ToUpper()}";
            }
            else
            {
                if (items.Any(p => p.CaseID == pCaseId))
                {
                    var index = 1;
                    while (items.Any(p => p.CaseID == $"{pCaseId}{index}"))
                    {
                        ++index;
                    }

                    return $"{pCaseId}{index}";
                }
            }

            return pCaseId;
        }

        public string GenerateCaseId(UniversalCPR uoContext)
        {
            return GenerateCaseId(uoContext, CaseID);
        }

        public string GenerateCaseId(UniversalCPR uoContext, string baseCaseId)
        {
            var patientId = UoClient.Backend.client_id;
            if (string.IsNullOrEmpty(baseCaseId)) return $"{patientId}_{patientId}";

            var caseId = $"{baseCaseId}_{patientId}".Replace(" ", "");
            var idx = 1;
            while (uoContext.Client_Case.Any(c => c.CaseID == caseId))
            {
                if (CaseOne)
                {
                    var baseCase = baseCaseId.Substring(0, baseCaseId.Length - 1);
                    caseId = $"{baseCase}{idx++}_{patientId}";
                }
                else
                {
                    caseId = $"{baseCaseId}{idx++}_{patientId}";
                }
            }

            return caseId;
        }

        public string CaseTypes
        {
            get => Backend.SpecialtyCases;
            set
            {
                Backend.SpecialtyCases = value;
                NotifyPropertyChanged();
            }
        }

        public string CaseID
        {
            get => Backend.CaseID;
            set
            {
                Backend.CaseID = value;
                NotifyPropertyChanged();
            }
        }

        #region IPatient

        public int Id => Backend.Id;

        public string first_name
        {
            get => Backend.ResultFirstName;
            set
            {
                Backend.ResultFirstName = value;
                NotifyPropertyChanged();
            }
        }

        public string last_name
        {
            get => Backend.ResultLastName;
            set
            {
                Backend.ResultLastName = value;
                NotifyPropertyChanged();
            }
        }

        public string middle_name
        {
            get => Backend.ResultMiddleName;
            set
            {
                Backend.ResultMiddleName = value;
                NotifyPropertyChanged();
            }
        }

        public string gender
        {
            get => Backend.ResultGender;
            set
            {
                Backend.ResultGender = value;
                NotifyPropertyChanged();
            }
        }

        public DateTime DOB
        {
            get => Backend.ResultDOB;
            set
            {
                Backend.ResultDOB = value;
                NotifyPropertyChanged();
            }
        }

        public string HomePhone
        {
            get => Backend.HomePhone;
            set
            {
                Backend.HomePhone = value;
                NotifyPropertyChanged();
            }
        }

        public string HomeStreet
        {
            get => Backend.HomeStreet;
            set
            {
                Backend.HomeStreet = value;
                NotifyPropertyChanged();
            }
        }

        public string HomeCity
        {
            get => Backend.HomeCity;
            set
            {
                Backend.HomeCity = value;
                NotifyPropertyChanged();
            }
        }

        public string HomeState
        {
            get => Backend.HomeState;
            set
            {
                Backend.HomeState = value;
                NotifyPropertyChanged();
            }
        }

        public string HomeZip
        {
            get => Backend.HomeZip;
            set
            {
                Backend.HomeZip = value;
                NotifyPropertyChanged();
            }
        }

        public string ResultFirstName
        {
            get => !string.IsNullOrEmpty(Backend.manualFirstName) ? Backend.manualFirstName : Backend.first_name;
            set
            {
                Backend.manualFirstName = value;
                SetSpecialtyCases();
            }
        }

        public string ResultMiddleName
        {
            get => !string.IsNullOrEmpty(Backend.manualMiddleName) ? Backend.manualMiddleName : Backend.middle_name;
            set => Backend.manualMiddleName = value;
        }

        public string ResultLastName
        {
            get => !string.IsNullOrEmpty(Backend.manualLastName) ? Backend.manualLastName : Backend.last_name;
            set
            {
                Backend.manualLastName = value;
                SetSpecialtyCases();
            }
        }

        public string ResultGender
        {
            get => !string.IsNullOrEmpty(Backend.manualGender) ? Backend.manualGender : Backend.gender;
            set => Backend.manualGender = value;
        }

        public string ResultPmsKey
        {
            get => Backend.PMSPatientKey != null && Backend.PMSPatientKey > 0 ? $"{Backend.PMSPatientKey.Value}" : string.Empty;
            set => Backend.PMSPatientKey = !string.IsNullOrEmpty(value)
                ? int.TryParse(value, out var id) ? id : (int?)null
                : (int?)null;
        }

        public DateTime ResultDOB
        {
            get => Backend.manualDOB ?? Backend.DOB;
            set => Backend.manualDOB = value;
        }

        #endregion

        #region MVA

        public DateTime? DOL => Backend.DOL;
        public string MVAInsurerHCAIID => Backend.MVAInsurerHCAIID;
        public string MVABranchHCAIID => Backend.MVABranchHCAIID;
        public string MVAClaimNo => Backend.MVAClaimNo;
        public string MVAPolicyNo => Backend.MVAPolicyNo;
        public bool MVAPolicyHolder => Backend.MVAPolicyHolder;
        public string MVAFirstName => Backend.MVAFirstName;
        public string MVALastName => Backend.MVALastName;
        public string MVAAdjFirstName => Backend.MVAAdjFirstName;
        public string MVAAdjLastName => Backend.MVAAdjLastName;
        public string MVAAdjTel => Backend.MVAAdjTel;
        public string MVAAdjFax => Backend.MVAAdjFax;
        public string MVAAdjExt => Backend.MVAAdjExt;

        #endregion

        private Client_Case GetClientCase(TransferWork worker, string specialtyAbbr, bool mustBeCaseLink)
        {
            var isSpecialtyCase = worker.GroupByProviderSpecialty && !string.IsNullOrEmpty(specialtyAbbr);
            if (!isSpecialtyCase)
            {
                if (mustBeCaseLink && this.UoCase is null)
                    throw new Exception($"No {nameof(Client_Case)} in patient {this.Name}!");
                return this.UoCase;
            }

            var specialtyCase = this.SpecialtyCases.FirstOrDefault(s => s.Specialty == specialtyAbbr.ToUpper());
            if (specialtyCase is null)
            {
                throw new Exception($"No {nameof(SpecialtyCaseModel)} [{specialtyAbbr}] in patient {this.Name}!");
            }

            if (mustBeCaseLink && specialtyCase.UoCase is null)
            {
                throw new Exception($"No {nameof(Client_Case)} [{specialtyAbbr}] in patient {this.Name}!");
            }

            return specialtyCase.UoCase;
        }

        #region Get sync

        public static SyncPatient GetSyncPatient(ObservableCollection<SyncPatient> items, OCF18Submit item)
        {
            var specialtyAbbr = !string.IsNullOrEmpty(item.OCF18_HealthPractitioner_Occupation)
                ? item.OCF18_HealthPractitioner_Occupation
                : item.OCF18_OtherHealthPractitioner_Occupation;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            return FindSyncPatient(items, checkItem);
        }

        public static SyncPatient GetSyncPatient(ObservableCollection<SyncPatient> items, OCF23Submit item)
        {
            var specialtyAbbr = item.OCF23_HealthPractitioner_Occupation;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };
            
            // *** find
            return FindSyncPatient(items, checkItem);
        }

        public static SyncPatient GetSyncPatient(ObservableCollection<SyncPatient> items, OCF21BSubmit item)
        {
            var specialtyAbbr = item.GS21BLineItemSubmit
                                    .FirstOrDefault()?.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation
                                ?? string.Empty;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };
            
            // *** find
            return FindSyncPatient(items, checkItem);
        }

        public static SyncPatient GetSyncPatient(ObservableCollection<SyncPatient> items, OCF21CSubmit item)
        {
            var specialtyAbbr = item.PAFReimbursableLineItemSubmits
                                    .FirstOrDefault()?.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation
                                ?? string.Empty;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };
            
            // *** find
            return FindSyncPatient(items, checkItem);
        }

        public static SyncPatient GetSyncPatient(ObservableCollection<SyncPatient> items, AACNSubm item)
        {
            var specialtyAbbr = item.Assessor_Occupation;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_Telephone)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_Extension) ? "x" + item.Applicant_Extension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                //MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                //MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                //MVAAdjTel = item.Insurer_Adjuster_TelephoneNumber,
                //MVAAdjFax = item.Insurer_Adjuster_FaxNumber,
                //MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };
            
            // *** find
            return FindSyncPatient(items, checkItem);
        }

        #endregion

        #region Converters

        private static SyncPatient FindSyncPatient(ObservableCollection<SyncPatient> items, Patient checkItem)
        {
            var sync = new SyncPatient(checkItem);

            var syncItem = items.FirstOrDefault(p => p.Equals(sync));
            if (syncItem is null)
            {
                return null;
            }

            if (syncItem.IsInVirtual)
                syncItem = items.First(p => p.Id == syncItem.Backend.virtualId);
            return syncItem;
        }

        public static Client_Case MakeHcaiPatientsInfo(TransferWork worker, ObservableCollection<SyncPatient> items, OCF18Submit item, bool checkWithCreate)
        {
            var specialtyAbbr = !string.IsNullOrEmpty(item.OCF18_HealthPractitioner_Occupation)
                ? item.OCF18_HealthPractitioner_Occupation
                : item.OCF18_OtherHealthPractitioner_Occupation;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            var syncItem = FindSyncPatient(items, checkItem);
            if (syncItem != null)
            {
                if (checkWithCreate)
                    syncItem.AddSpecialty(worker, specialtyAbbr);

                return worker.GroupByProviderSpecialty ? syncItem.GetClientCase(worker, specialtyAbbr, !checkWithCreate) : syncItem.UoCase;
            }
            if (!checkWithCreate) return null;

            // create
            var patient = worker.SourceDb.SyncContext.Patients.Add(checkItem);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // *** add
            syncItem = new SyncPatient(patient);
            items.Add(syncItem);

            // *** end
            return syncItem.UoCase;
        }

        public static Client_Case MakeHcaiPatientsInfo(TransferWork worker, ObservableCollection<SyncPatient> items, OCF23Submit item, bool checkWithCreate)
        {
            var specialtyAbbr = item.OCF23_HealthPractitioner_Occupation;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            var syncItem = FindSyncPatient(items, checkItem);
            if (syncItem != null)
            {
                if (checkWithCreate)
                    syncItem.AddSpecialty(worker, specialtyAbbr);

                return worker.GroupByProviderSpecialty ? syncItem.GetClientCase(worker, specialtyAbbr, !checkWithCreate) : syncItem.UoCase;
            }
            if (!checkWithCreate) return null;

            // create
            var patient = worker.SourceDb.SyncContext.Patients.Add(checkItem);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // *** add
            syncItem = new SyncPatient(patient);
            items.Add(syncItem);

            // *** end
            return syncItem.UoCase;
        }

        public static Client_Case MakeHcaiPatientsInfo(TransferWork worker, ObservableCollection<SyncPatient> items, OCF21BSubmit item, bool checkWithCreate)
        {
            var refPlanNumber = !string.IsNullOrEmpty(item.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                      && item.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt"
                ? item.OCF21B_PreviouslyApprovedGoodsAndServices_PlanNumber
                : string.Empty;
            var plan = !string.IsNullOrEmpty(refPlanNumber)
                ? worker.SourceDb.SyncContext.OCF18Submit.FirstOrDefault(p => p.HCAI_Document_Number == refPlanNumber)
                : null;
            var isPlan = plan != null;

            var specialtyAbbr = isPlan
                ? !string.IsNullOrEmpty(plan.OCF18_HealthPractitioner_Occupation)
                    ? plan.OCF18_HealthPractitioner_Occupation
                    : plan.OCF18_OtherHealthPractitioner_Occupation
                : item.GS21BLineItemSubmit
                      .FirstOrDefault()?.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation
                  ?? string.Empty;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            var syncItem = FindSyncPatient(items, checkItem);
            if (syncItem != null)
            {
                if (checkWithCreate)
                    syncItem.AddSpecialty(worker, specialtyAbbr);

                return worker.GroupByProviderSpecialty ? syncItem.GetClientCase(worker, specialtyAbbr, !checkWithCreate) : syncItem.UoCase;
            }
            if (!checkWithCreate) return null;

            // create
            var patient = worker.SourceDb.SyncContext.Patients.Add(checkItem);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // *** add
            syncItem = new SyncPatient(patient);
            items.Add(syncItem);

            // *** end
            return syncItem.UoCase;
        }

        public static Client_Case MakeHcaiPatientsInfo(TransferWork worker, ObservableCollection<SyncPatient> items, OCF21CSubmit item, bool checkWithCreate)
        {
            var refPlanNumber = !string.IsNullOrEmpty(item.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                && item.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt"
                ? item.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber
                : string.Empty;
            var plan = !string.IsNullOrEmpty(refPlanNumber)
                ? worker.SourceDb.SyncContext.OCF23Submit.FirstOrDefault(p => p.HCAI_Document_Number == refPlanNumber)
                : null;
            var isPlan = plan != null;

            var specialtyAbbr = isPlan
                ? plan.OCF23_HealthPractitioner_Occupation
                : item.PAFReimbursableLineItemSubmits
                      .FirstOrDefault()?.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation
                  ?? string.Empty;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            var syncItem = FindSyncPatient(items, checkItem);
            if (syncItem != null)
            {
                if (checkWithCreate)
                    syncItem.AddSpecialty(worker, specialtyAbbr);

                return worker.GroupByProviderSpecialty ? syncItem.GetClientCase(worker, specialtyAbbr, !checkWithCreate) : syncItem.UoCase;
            }
            if (!checkWithCreate) return null;

            // create
            var patient = worker.SourceDb.SyncContext.Patients.Add(checkItem);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // *** add
            syncItem = new SyncPatient(patient);
            items.Add(syncItem);

            // *** end
            return syncItem.UoCase;
        }

        public static Client_Case MakeHcaiPatientsInfo(TransferWork worker, ObservableCollection<SyncPatient> items, AACNSubm item, bool checkWithCreate)
        {
            var specialtyAbbr = item.Assessor_Occupation;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_Telephone)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_Extension) ? "x" + item.Applicant_Extension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                //MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                //MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                //MVAAdjTel = item.Insurer_Adjuster_TelephoneNumber,
                //MVAAdjFax = item.Insurer_Adjuster_FaxNumber,
                //MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            var syncItem = FindSyncPatient(items, checkItem);
            if (syncItem != null)
            {
                if (checkWithCreate)
                    syncItem.AddSpecialty(worker, specialtyAbbr);

                return worker.GroupByProviderSpecialty ? syncItem.GetClientCase(worker, specialtyAbbr, !checkWithCreate) : syncItem.UoCase;
            }
            if (!checkWithCreate) return null;

            // create
            var patient = worker.SourceDb.SyncContext.Patients.Add(checkItem);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // *** add
            syncItem = new SyncPatient(patient);
            items.Add(syncItem);

            // *** end
            return syncItem.UoCase;
        }

        public static Client_Case MakeHcaiPatientsInfo(TransferWork worker, ObservableCollection<SyncPatient> items, ACSISubmit item, bool checkWithCreate)
        {
            var refPlanNumber = !string.IsNullOrEmpty(item.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                      && item.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt"
                ? item.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber
                : string.Empty;
            var plan = !string.IsNullOrEmpty(refPlanNumber)
                ? worker.SourceDb.SyncContext.OCF18Submit.FirstOrDefault(p => p.HCAI_Document_Number == refPlanNumber)
                : null;
            var isPlan = plan != null;

            var specialtyAbbr = isPlan
                ? !string.IsNullOrEmpty(plan.OCF18_HealthPractitioner_Occupation)
                    ? plan.OCF18_HealthPractitioner_Occupation
                    : plan.OCF18_OtherHealthPractitioner_Occupation
                : item.GSACSILineItemSubmits
                      .FirstOrDefault()?.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation
                  ?? string.Empty;

            // *** prepare for find
            var checkItem = new Patient
            {
                PMSPatientKey = int.TryParse(item.PMSFields_PMSPatientKey, out var patientKey) ? patientKey : (int?)null,
                first_name = item.Applicant_Name_FirstName,
                middle_name = item.Applicant_Name_MiddleName,
                last_name = item.Applicant_Name_LastName,
                gender = item.Applicant_Gender,
                DOB = item.Applicant_DateOfBirth,

                HomePhone = $"{ConvertHelper.PhoneFromHcai(item.Applicant_TelephoneNumber)}" +
                            $"{(!string.IsNullOrEmpty(item.Applicant_TelephoneExtension) ? "x" + item.Applicant_TelephoneExtension : "")}",
                HomeStreet = $"{item.Applicant_Address_StreetAddress1}" +
                             $"{(!string.IsNullOrEmpty(item.Applicant_Address_StreetAddress2) ? " " + item.Applicant_Address_StreetAddress2 : string.Empty)}",
                HomeCity = item.Applicant_Address_City,
                HomeState = ConvertHelper.FormatProvince(item.Applicant_Address_Province),
                HomeZip = ConvertHelper.FormatPostalCode(item.Applicant_Address_PostalCode),

                // case data
                CaseID = GenerateBaseCaseId(items, item.Applicant_Name_LastName, item.Applicant_Name_FirstName),
                DOL = item.Header_DateOfAccident,
                MVAInsurerHCAIID = item.Insurer_IBCInsurerID,
                MVABranchHCAIID = item.Insurer_IBCBranchID,
                MVAClaimNo = item.Header_ClaimNumber,
                MVAPolicyNo = item.Header_PolicyNumber,

                MVAPolicyHolder = item.Insurer_PolicyHolder_IsSameAsApplicant,
                MVAFirstName = item.Insurer_PolicyHolder_Name_FirstName,
                MVALastName = item.Insurer_PolicyHolder_Name_LastName,

                MVAAdjFirstName = item.Insurer_Adjuster_Name_FirstName,
                MVAAdjLastName = item.Insurer_Adjuster_Name_LastName,
                MVAAdjTel = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_TelephoneNumber),
                MVAAdjFax = ConvertHelper.PhoneFromHcai(item.Insurer_Adjuster_FaxNumber),
                MVAAdjExt = item.Insurer_Adjuster_TelephoneExtension,

                SpecialtyCases = specialtyAbbr,
            };

            // *** find
            var syncItem = FindSyncPatient(items, checkItem);
            if (syncItem != null)
            {
                if (checkWithCreate)
                    syncItem.AddSpecialty(worker, specialtyAbbr);

                return worker.GroupByProviderSpecialty ? syncItem.GetClientCase(worker, specialtyAbbr, !checkWithCreate) : syncItem.UoCase;
            }
            if (!checkWithCreate) return null;

            // create
            var patient = worker.SourceDb.SyncContext.Patients.Add(checkItem);
            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // *** add
            syncItem = new SyncPatient(patient);
            items.Add(syncItem);

            // *** end
            return syncItem.UoCase;
        }

        #endregion
    }
}
