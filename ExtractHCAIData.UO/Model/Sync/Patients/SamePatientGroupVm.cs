﻿using System.Collections.ObjectModel;
using System.Linq;
using ExtractHcaiData.Uo.Vm;

namespace ExtractHcaiData.Uo.Model.Sync.Patients
{
    public class SamePatientGroupVm : VMBase
    {
        public ObservableCollection<SyncPatient> Patients { get; } = new ObservableCollection<SyncPatient>();

        public string Name
        {
            get
            {
                var first = Patients.FirstOrDefault();
                return first?.Name ?? "???";
            }
        }
    }
}
