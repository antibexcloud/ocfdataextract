﻿using ExtractHcaiData.Uo.UoData;

namespace ExtractHcaiData.Uo.Model.Sync.Patients
{
    public class SpecialtyCaseModel
    {
        public string Specialty { get; set; }

        public string CaseId { get; set; }

        public Client_Case UoCase { get; set; }
    }
}
