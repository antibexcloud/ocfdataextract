﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Vm;
using ExtractHcaiData.Uo.Work;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Sync.Providers
{
    public class SyncProviderVirtual : VMBase, IProvider
    {
        public ProviderVirtual Backend { get; }

        public CprResource UoResource { get; set; }

        public SyncProviderVirtual(ProviderVirtual backend)
        {
            Backend = backend;
        }

        public void TryLink(TransferWork worker)
        {
            UoResource = worker.TargetDb.Resources.FirstOrDefault(this.Equals);
        }

        public string Name => Backend.ResultLastName + ", " + Backend.ResultFirstName;

        public bool Equals(CprResource old)
        {
            return !string.IsNullOrEmpty(old.Backend.HCAIRegNo)
                ? $"{Backend.HCAI_Provider_Registry_ID}" == old.Backend.HCAIRegNo
                : (Backend.ResultFirstName ?? string.Empty).ToLower().Equals((old.FirstName ?? string.Empty).ToLower())
                  && (Backend.ResultLastName ?? string.Empty).ToLower().Equals((old.LastName ?? string.Empty).ToLower());
        }

        public bool IsUo => UoResource != null;

        #region Add data

        public int HCAI_Provider_Registry_ID
        {
            get => Backend.HCAI_Provider_Registry_ID;
            set
            {
                Backend.HCAI_Provider_Registry_ID = value;
                NotifyPropertyChanged();
            }
        }

        public int HCAI_Facility_Registry_ID
        {
            get => Backend.HCAI_Facility_Registry_ID;
            set
            {
                Backend.HCAI_Facility_Registry_ID = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_Last_Name
        {
            get => Backend.ResultLastName;
            set
            {
                Backend.ResultLastName = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_First_Name
        {
            get => Backend.Provider_First_Name;
            set
            {
                Backend.Provider_First_Name = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_Status
        {
            get => Backend.Provider_Status;
            set
            {
                Backend.Provider_Status = value;
                NotifyPropertyChanged();
            }
        }

        public string Address
        {
            get => UoResource?.Backend?.Staff?.HomeStreet ?? Backend.Address;
            set
            {
                Backend.Address = value;
                NotifyPropertyChanged();
            }
        }

        public string City
        {
            get => UoResource?.Backend?.Staff?.HomeCity ?? Backend.City;
            set
            {
                Backend.City = value;
                NotifyPropertyChanged();
            }
        }

        public string Province
        {
            get => UoResource?.Backend?.Staff?.HomeState ?? Backend.Province;
            set
            {
                Backend.Province = value;
                NotifyPropertyChanged();
            }
        }

        public string PostalCode
        {
            get => UoResource?.Backend?.Staff?.HomeZip ?? Backend.PostalCode;
            set
            {
                Backend.PostalCode = value;
                NotifyPropertyChanged();
            }
        }

        public string TelephoneNumber
        {
            get => UoResource?.Backend?.Staff?.HomePhone ?? Backend.TelephoneNumber;
            set
            {
                Backend.TelephoneNumber = value;
                NotifyPropertyChanged();
            }
        }

        public string TelephoneExtension
        {
            get => Backend.TelephoneExtension;
            set
            {
                Backend.TelephoneExtension = value;
                NotifyPropertyChanged();
            }
        }

        public string FaxNumber
        {
            get => UoResource?.Backend?.Staff?.Fax ?? Backend.FaxNumber;
            set
            {
                Backend.FaxNumber = value;
                NotifyPropertyChanged();
            }
        }

        public string Email
        {
            get => UoResource?.Backend?.Staff?.Email ?? Backend.Email;
            set
            {
                Backend.Email = value;
                NotifyPropertyChanged();
            }
        }

        public string ResultLastName
        {
            get => Backend.ResultLastName;
            set
            {
                Backend.ResultLastName = value;
                NotifyPropertyChanged();
            }
        }

        public string ResultFirstName
        {
            get => Backend.ResultFirstName;
            set
            {
                Backend.ResultFirstName = value;
                NotifyPropertyChanged();
            }
        }

        public ICollection<RegistrationLineItem> RegistrationLineItems
        {
            get
            {
                var res = new Collection<RegistrationLineItem>();

                foreach (var syncProvider in Backend.ProviderLineItems.OrderBy(p => p.Provider_Status))
                {
                    foreach (var item in syncProvider.RegistrationLineItems)
                    {
                        if (res.All(r => r.Profession != item.Profession))
                            res.Add(item);
                    }
                }

                return res;
            }
        }

        #endregion
    }
}
