﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Vm;
using ExtractHcaiData.Uo.Work;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Sync.Providers
{
    public class SyncProvider : VMBase, IProvider
    {
        public ProviderLineItem Backend { get; private set; }

        public CprResource UoResource { get; set; }

        public SyncProvider(ProviderLineItem backend)
        {
            Backend = backend;
            Refresh(null);
        }

        public void Refresh(HCAIOCFSyncEntities syncContext)
        {
            if (syncContext != null)
                Backend = syncContext.ProviderLineItems.First(p => p.HCAI_Provider_Registry_ID == Backend.HCAI_Provider_Registry_ID);
            NotifyPropertyChanged(nameof(IsInVirtual));
        }

        public void SetVirtualId(int? id)
        {
            Backend.virtualId = id;
            NotifyPropertyChanged(nameof(IsInVirtual));
            if (id is null) return;

            UoResource = null;
            NotifyPropertyChanged(nameof(IsUo));
        }

        public void TryLink(TransferWork worker)
        {
            UoResource = worker.TargetDb.Resources.FirstOrDefault(this.Equals);
        }

        public string Name => Backend.ResultLastName + ", " + Backend.ResultFirstName;

        public bool Equals(CprResource old)
        {
            return !string.IsNullOrEmpty(old.Backend.HCAIRegNo)
                ? $"{Backend.HCAI_Provider_Registry_ID}" == old.Backend.HCAIRegNo
                : (Backend.ResultFirstName ?? string.Empty).ToLower().Equals((old.FirstName ?? string.Empty).ToLower())
                  && (Backend.ResultLastName ?? string.Empty).ToLower().Equals((old.LastName ?? string.Empty).ToLower());
        }

        public bool IsUo => UoResource != null;


        public int? VirtualId => Backend.virtualId;

        public bool IsInVirtual => Backend.virtualId != null;

        #region Add data

        public int HCAI_Provider_Registry_ID => Backend.HCAI_Provider_Registry_ID;
        public int HCAI_Facility_Registry_ID => Backend.HCAI_Facility_Registry_ID;

        public string Provider_Last_Name
        {
            get => Backend.ResultLastName;
            set
            {
                Backend.ResultLastName = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_First_Name
        {
            get => Backend.ResultFirstName;
            set
            {
                Backend.ResultFirstName = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_Status
        {
            get => Backend.Provider_Status;
            set
            {
                Backend.Provider_Status = value;
                NotifyPropertyChanged();
            }
        }

        public string Address
        {
            get => UoResource?.Backend?.Staff?.HomeStreet ?? Backend.Address;
            set
            {
                Backend.Address = value;
                NotifyPropertyChanged();
            }
        }

        public string City
        {
            get => UoResource?.Backend?.Staff?.HomeCity ?? Backend.City;
            set
            {
                Backend.City = value;
                NotifyPropertyChanged();
            }
        }

        public string Province
        {
            get => UoResource?.Backend?.Staff?.HomeState ?? Backend.Province;
            set
            {
                Backend.Province = value;
                NotifyPropertyChanged();
            }
        }

        public string PostalCode
        {
            get => UoResource?.Backend?.Staff?.HomeZip ?? Backend.PostalCode;
            set
            {
                Backend.PostalCode = value;
                NotifyPropertyChanged();
            }
        }

        public string TelephoneNumber
        {
            get => UoResource?.Backend?.Staff?.HomePhone ?? Backend.TelephoneNumber;
            set
            {
                Backend.TelephoneNumber = value;
                NotifyPropertyChanged();
            }
        }

        public string TelephoneExtension
        {
            get => Backend.TelephoneExtension;
            set
            {
                Backend.TelephoneExtension = value;
                NotifyPropertyChanged();
            }
        }

        public string FaxNumber
        {
            get => UoResource?.Backend?.Staff?.Fax ?? Backend.FaxNumber;
            set
            {
                Backend.FaxNumber = value;
                NotifyPropertyChanged();
            }
        }

        public string Email
        {
            get => UoResource?.Backend?.Staff?.Email ?? Backend.Email;
            set
            {
                Backend.Email = value;
                NotifyPropertyChanged();
            }
        }

        public string ResultLastName
        {
            get => Backend.ResultLastName;
            set
            {
                Backend.ResultLastName = value;
                NotifyPropertyChanged();
            }
        }

        public string ResultFirstName
        {
            get => Backend.ResultFirstName;
            set
            {
                Backend.ResultFirstName = value;
                NotifyPropertyChanged();
            }
        }

        public ICollection<RegistrationLineItem> RegistrationLineItems => Backend.RegistrationLineItems;

        #endregion

        #region Converters

        public static void AddUnknownSyncProvider(TransferWork worker, int code, string occupation)
        {
            var find = worker.SourceDb.SyncContext.ProviderLineItems
                .Include("RegistrationLineItems")
                .FirstOrDefault(p => p.HCAI_Provider_Registry_ID == code);
            var isNew = find is null;
            if (isNew)
            {
                find = new ProviderLineItem
                {
                    HCAI_Provider_Registry_ID = code,
                    FacilityLineItem = null,
                    HCAI_Facility_Registry_ID = worker.SourceDb.FacilityId,
                    Provider_Status = "Expired",
                    Provider_First_Name = "Excluded",
                    Provider_Last_Name = $"Unknown_{code}",
                    Provider_Start_Date = new DateTime(1900, 1, 1),
                };

                if (!string.IsNullOrEmpty(occupation))
                {
                    find.RegistrationLineItems.Add(new RegistrationLineItem
                    {
                        ProviderLineItem = find,
                        HCAI_Provider_Registry_ID = code,
                        Profession = occupation,
                        Registration_Number = "",
                    });
                }

                find = worker.SourceDb.SyncContext.ProviderLineItems.Add(find);

                var syncItemHp = new SyncProvider(find);
                worker.SourceDb.Providers.Add(syncItemHp);
                worker.SourceDb.SyncContext.SaveChangesWithDetect();
            }
            else if (find.RegistrationLineItems.ToArray().All(r => r.Profession.ToUpper() != occupation.ToUpper()))
            {
                find.RegistrationLineItems.Add(new RegistrationLineItem
                {
                    ProviderLineItem = find,
                    HCAI_Provider_Registry_ID = code,
                    Profession = occupation,
                    Registration_Number = "",
                });
                worker.SourceDb.SyncContext.SaveChangesWithDetect();
            }
        }

        public static void CheckSyncProvider(TransferWork worker, OCF23Submit item)
        {
            if (item.OCF23_HealthPractitioner_ProviderRegistryID <= 0
                || worker.SourceDb.FacilityId != item.OCF23_HealthPractitioner_FacilityRegistryID)
                return;

            var syncItemHP = worker.SourceDb.Providers
                .FirstOrDefault(p => p.Backend.HCAI_Provider_Registry_ID == item.OCF23_HealthPractitioner_ProviderRegistryID);
            var isNew = syncItemHP?.UoResource is null;
            if (isNew)
            {
                if (syncItemHP is null)
                {
                    var backend = worker.SourceDb.SyncContext.ProviderLineItems
                        .Add(new ProviderLineItem
                        {
                            HCAI_Provider_Registry_ID = item.OCF23_HealthPractitioner_ProviderRegistryID,
                            FacilityLineItem = null,
                            HCAI_Facility_Registry_ID = item.OCF23_HealthPractitioner_FacilityRegistryID,
                            Provider_Status = "Active",
                            Provider_First_Name = item.OCF23_HealthPractitioner_ProviderFirstName,
                            Provider_Last_Name = item.OCF23_HealthPractitioner_ProviderLastName,
                            Provider_Start_Date = new DateTime(1900, 1, 1),
                        });
                    syncItemHP = new SyncProvider(backend);
                    worker.SourceDb.Providers.Add(syncItemHP);
                }

                //
                if (!string.IsNullOrEmpty(item.OCF23_HealthPractitioner_Occupation) &&
                    syncItemHP.Backend.RegistrationLineItems.All(r => r.Profession != item.OCF23_HealthPractitioner_Occupation))
                {
                    syncItemHP.Backend.RegistrationLineItems.Add(new RegistrationLineItem
                    {
                        ProviderLineItem = syncItemHP.Backend,
                        Profession = item.OCF23_HealthPractitioner_Occupation,
                        HCAI_Provider_Registry_ID = syncItemHP.Backend.HCAI_Provider_Registry_ID,
                        Registration_Number = "",
                    });
                }

                // check/fill
                if (string.IsNullOrEmpty(syncItemHP.Address) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_Address1))
                    syncItemHP.Address = $"{item.OCF23_HealthPractitioner_Address1}" +
                                         $"{(!string.IsNullOrEmpty(item.OCF23_HealthPractitioner_Address2) ? " " + item.OCF23_HealthPractitioner_Address1 : "")}";
                if (string.IsNullOrEmpty(syncItemHP.City) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_City))
                    syncItemHP.City = item.OCF23_HealthPractitioner_City;
                if (string.IsNullOrEmpty(syncItemHP.Province) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_Province))
                    syncItemHP.Province = item.OCF23_HealthPractitioner_Province;
                if (string.IsNullOrEmpty(syncItemHP.PostalCode) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_PostalCode))
                    syncItemHP.PostalCode = item.OCF23_HealthPractitioner_PostalCode;
                if (string.IsNullOrEmpty(syncItemHP.Email) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_Email))
                    syncItemHP.Email = item.OCF23_HealthPractitioner_Email;
                if (string.IsNullOrEmpty(syncItemHP.TelephoneNumber) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_TelephoneNumber))
                {
                    syncItemHP.TelephoneNumber = item.OCF23_HealthPractitioner_TelephoneNumber;
                    syncItemHP.TelephoneExtension = item.OCF23_HealthPractitioner_TelephoneExtension;
                }
                if (string.IsNullOrEmpty(syncItemHP.FaxNumber) && !string.IsNullOrEmpty(item.OCF23_HealthPractitioner_FaxNumber))
                    syncItemHP.FaxNumber = item.OCF23_HealthPractitioner_FaxNumber;
            }

            var changed = false;
            foreach (var itemSubmit in item.OtherGSLineItemSubmits)
            {
                var regId = itemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID;
                if (regId <= 0) continue;

                var syncItem = worker.SourceDb.Providers
                    .FirstOrDefault(p => p.Backend.HCAI_Provider_Registry_ID == regId);
                if (syncItem is null) continue;

                if (!string.IsNullOrEmpty(itemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation) &&
                    syncItem.Backend.RegistrationLineItems.All(r => r.Profession != itemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation))
                {
                    var backend = new RegistrationLineItem
                    {
                        ProviderLineItem = syncItem.Backend,
                        Profession = itemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation,
                        HCAI_Provider_Registry_ID = syncItemHP.Backend.HCAI_Provider_Registry_ID,
                        Registration_Number = "",
                    };
                    syncItem.Backend.RegistrationLineItems.Add(backend);
                    changed = true;
                }
            }

            if (isNew || changed)
                worker.SourceDb.SyncContext.SaveChangesWithDetect();
        }

        public static void CheckSyncProvider(TransferWork worker, OCF18Submit item)
        {
            if (item.OCF18_HealthPractitioner_ProviderRegistryID.HasValue
                && item.OCF18_HealthPractitioner_FacilityRegistryID.HasValue
                && worker.SourceDb.FacilityId == item.OCF18_HealthPractitioner_FacilityRegistryID.Value
                && item.OCF18_HealthPractitioner_ProviderRegistryID.Value > 0)
            {
                var syncItem = worker.SourceDb.Providers
                    .FirstOrDefault(p => p.Backend.HCAI_Provider_Registry_ID == item.OCF18_HealthPractitioner_ProviderRegistryID.Value);

                if (syncItem?.UoResource is null)
                {
                    if (syncItem is null)
                    {
                        var backend = worker.SourceDb.SyncContext.ProviderLineItems
                            .Add(new ProviderLineItem
                            {
                                HCAI_Provider_Registry_ID = item.OCF18_HealthPractitioner_ProviderRegistryID.Value,
                                FacilityLineItem = null,
                                HCAI_Facility_Registry_ID = item.OCF18_HealthPractitioner_FacilityRegistryID.Value,
                                Provider_Status = "Active",
                                Provider_First_Name = "OCF18_HealthPractitioner",
                                Provider_Last_Name = "Unknown",
                                Provider_Start_Date = new DateTime(1900, 1, 1),
                            });
                        syncItem = new SyncProvider(backend);
                        worker.SourceDb.Providers.Add(syncItem);
                    }

                    if (!string.IsNullOrEmpty(item.OCF18_HealthPractitioner_Occupation) &&
                        syncItem.Backend.RegistrationLineItems.All(r => r.Profession != item.OCF18_HealthPractitioner_Occupation))
                    {
                        syncItem.Backend.RegistrationLineItems.Add(new RegistrationLineItem
                        {
                            ProviderLineItem = syncItem.Backend,
                            Profession = item.OCF18_HealthPractitioner_Occupation,
                            HCAI_Provider_Registry_ID = syncItem.Backend.HCAI_Provider_Registry_ID,
                            Registration_Number = $"{syncItem.Backend.HCAI_Provider_Registry_ID}", //???
                        });
                    }

                    worker.SourceDb.SyncContext.SaveChangesWithDetect();
                }
            }

            if (!string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_ProviderRegistryNumber)
                && item.OCF18_OtherHealthPractitioner_FacilityRegistryID.HasValue
                && int.TryParse(item.OCF18_OtherHealthPractitioner_ProviderRegistryNumber, out var id)
                && id > 0
                && worker.SourceDb.FacilityId == item.OCF18_OtherHealthPractitioner_FacilityRegistryID.Value)
            {
                var syncItem = worker.SourceDb.Providers
                    .FirstOrDefault(p => p.Backend.HCAI_Provider_Registry_ID == id);

                if (syncItem?.UoResource is null)
                {
                    if (syncItem is null)
                    {
                        var backend = worker.SourceDb.SyncContext.ProviderLineItems
                            .Add(new ProviderLineItem
                            {
                                HCAI_Provider_Registry_ID = int.TryParse(item.OCF18_OtherHealthPractitioner_ProviderRegistryNumber, out var providerRegId)
                                    ? providerRegId
                                    : 0,
                                FacilityLineItem = null,
                                HCAI_Facility_Registry_ID = item.OCF18_OtherHealthPractitioner_FacilityRegistryID.Value,
                                Provider_Status = "Active",
                                Provider_First_Name = item.OCF18_OtherHealthPractitioner_Name_FirstName,
                                Provider_Last_Name = item.OCF18_OtherHealthPractitioner_Name_LastName,
                                Provider_Start_Date = new DateTime(1900, 1, 1),
                            });
                        syncItem = new SyncProvider(backend);
                        worker.SourceDb.Providers.Add(syncItem);
                    }

                    if (!string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Occupation)
                        && syncItem.Backend.RegistrationLineItems.All(r => r.Profession != item.OCF18_OtherHealthPractitioner_Occupation))
                    {
                        syncItem.Backend.RegistrationLineItems.Add(new RegistrationLineItem
                        {
                            ProviderLineItem = syncItem.Backend,
                            Profession = item.OCF18_OtherHealthPractitioner_Occupation,
                            HCAI_Provider_Registry_ID = syncItem.Backend.HCAI_Provider_Registry_ID,
                            Registration_Number = "",
                        });
                    }

                    // check/fill
                    if (string.IsNullOrEmpty(syncItem.Address) && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Address_StreetAddress1))
                        syncItem.Address = $"{item.OCF18_OtherHealthPractitioner_Address_StreetAddress1}" +
                                           $"{(!string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Address_StreetAddress2) ? " " + item.OCF18_OtherHealthPractitioner_Address_StreetAddress2 : "")}";
                    if (string.IsNullOrEmpty(syncItem.City) && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Address_City))
                        syncItem.City = item.OCF18_OtherHealthPractitioner_Address_City;
                    if (string.IsNullOrEmpty(syncItem.Province) && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Address_Province))
                        syncItem.Province = item.OCF18_OtherHealthPractitioner_Address_Province;
                    if (string.IsNullOrEmpty(syncItem.PostalCode) && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Address_PostalCode))
                        syncItem.PostalCode = item.OCF18_OtherHealthPractitioner_Address_PostalCode;
                    if (string.IsNullOrEmpty(syncItem.Email) && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_Email))
                        syncItem.Email = item.OCF18_OtherHealthPractitioner_Email;
                    if (string.IsNullOrEmpty(syncItem.TelephoneNumber) && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_TelephoneNumber))
                    {
                        syncItem.TelephoneNumber = item.OCF18_OtherHealthPractitioner_TelephoneNumber;
                        syncItem.TelephoneExtension = item.OCF18_OtherHealthPractitioner_TelephoneExtension;
                    }
                    if (string.IsNullOrEmpty(syncItem.FaxNumber)
                        && !string.IsNullOrEmpty(item.OCF18_OtherHealthPractitioner_FaxNumber))
                        syncItem.FaxNumber = item.OCF18_OtherHealthPractitioner_FaxNumber;

                    worker.SourceDb.SyncContext.SaveChangesWithDetect();
                }
            }

            if (item.OCF18_RegulatedHealthProfessional_ProviderRegistryID.HasValue
                && item.OCF18_RegulatedHealthProfessional_FacilityRegistryID.HasValue
                && item.OCF18_RegulatedHealthProfessional_ProviderRegistryID.Value > 0
                && item.OCF18_RegulatedHealthProfessional_FacilityRegistryID.Value > 0
                && worker.SourceDb.FacilityId == item.OCF18_RegulatedHealthProfessional_ProviderRegistryID.Value)
            {
                var syncItem = worker.SourceDb.Providers
                    .FirstOrDefault(p => p.Backend.HCAI_Provider_Registry_ID == item.OCF18_RegulatedHealthProfessional_ProviderRegistryID);

                if (syncItem?.UoResource is null)
                {
                    if (syncItem is null)
                    {
                        var backend = worker.SourceDb.SyncContext.ProviderLineItems
                            .Add(new ProviderLineItem
                            {
                                HCAI_Provider_Registry_ID = item.OCF18_RegulatedHealthProfessional_ProviderRegistryID.Value,
                                FacilityLineItem = null,
                                HCAI_Facility_Registry_ID = item.OCF18_RegulatedHealthProfessional_FacilityRegistryID.Value,
                                Provider_Status = "Active",
                                Provider_First_Name = "RegulatedUnknown",
                                Provider_Last_Name = "RegulatedUnknown",
                                Provider_Start_Date = new DateTime(1900, 1, 1),
                            });
                        syncItem = new SyncProvider(backend);
                        worker.SourceDb.Providers.Add(syncItem);
                    }

                    // check/fill
                    if (!string.IsNullOrEmpty(item.OCF18_RegulatedHealthProfessional_Occupation)
                        && syncItem.Backend.RegistrationLineItems.All(r => r.Profession != item.OCF18_RegulatedHealthProfessional_Occupation))
                    {
                        syncItem.Backend.RegistrationLineItems.Add(new RegistrationLineItem
                        {
                            ProviderLineItem = syncItem.Backend,
                            Profession = item.OCF18_RegulatedHealthProfessional_Occupation,
                            HCAI_Provider_Registry_ID = syncItem.Backend.HCAI_Provider_Registry_ID,
                            Registration_Number = "",
                        });
                    }

                    worker.SourceDb.SyncContext.SaveChangesWithDetect();
                }
            }
        }

        #endregion
    }
}
