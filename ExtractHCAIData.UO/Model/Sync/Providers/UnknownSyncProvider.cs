﻿namespace ExtractHcaiData.Uo.Model.Sync.Providers
{
    public class UnknownSyncProvider
    {
        public int Id { get; set; }

        public string Proff { get; set; }
    }
}
