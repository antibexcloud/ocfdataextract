﻿using System.Collections.Generic;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Sync.Providers
{
    public interface IProvider
    {
        int HCAI_Provider_Registry_ID { get; }
        int HCAI_Facility_Registry_ID { get; }

        string Provider_Last_Name { get; set; }
        string Provider_First_Name { get; set; }

        string Provider_Status { get; }
        string Address { get; }
        string City { get; }
        string Province { get; }
        string PostalCode { get; }
        string TelephoneNumber { get; }
        string TelephoneExtension { get; }
        string FaxNumber { get; }
        string Email { get; }

        string ResultLastName { get; set; }
        string ResultFirstName { get; set; }

        ICollection<RegistrationLineItem> RegistrationLineItems { get; }
    }
}
