﻿using ExtractHcaiData.Uo.Vm;

namespace ExtractHcaiData.Uo.Model.Sync.Providers
{
    public class SyncProviderVm : VMBase
    {
        public SyncProvider BackendProvider { get; }
        public SyncProviderVirtual BackendVirtual { get; }

        public SyncProviderVm(SyncProvider backendProvider, SyncProviderVirtual backendVirtual)
        {
            BackendProvider = backendProvider;
            BackendVirtual = backendVirtual;
            SendChangedAll();
        }

        public string Name =>
            BackendProvider != null
                ? BackendProvider.ResultLastName + ", " + BackendProvider.ResultFirstName
                : BackendVirtual.ResultLastName + ", " + BackendVirtual.ResultFirstName;

        public bool IsUo => BackendProvider?.IsUo ?? BackendVirtual.IsUo;

        public bool IsVirtual => BackendVirtual != null;

        #region Add data

        public int HCAI_Provider_Registry_ID => BackendProvider?.HCAI_Provider_Registry_ID ?? BackendVirtual.HCAI_Provider_Registry_ID;

        public int HCAI_Facility_Registry_ID => BackendProvider?.HCAI_Facility_Registry_ID ?? BackendVirtual.HCAI_Facility_Registry_ID;

        public string Provider_Last_Name
        {
            get => BackendProvider != null ? BackendProvider.ResultLastName : BackendVirtual.ResultLastName;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.ResultLastName = value;
                else if (BackendVirtual != null)
                    BackendVirtual.ResultLastName = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_First_Name
        {
            get => BackendProvider != null ? BackendProvider.ResultFirstName : BackendVirtual.ResultFirstName;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.ResultFirstName = value;
                else if (BackendVirtual != null)
                    BackendVirtual.ResultFirstName = value;
                NotifyPropertyChanged();
            }
        }

        public string Provider_Status
        {
            get => BackendProvider != null ? BackendProvider.Provider_Status : BackendVirtual.Provider_Status;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.Provider_Status = value;
                else
                    BackendVirtual.Provider_Status = value;
                NotifyPropertyChanged();
            }
        }

        public string Address
        {
            get => BackendProvider != null ? BackendProvider.Address : BackendVirtual.Address;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.Address = value;
                else
                    BackendVirtual.Address = value;
                NotifyPropertyChanged();
            }
        }

        public string City
        {
            get => BackendProvider != null ? BackendProvider.City : BackendVirtual.City;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.City = value;
                else
                    BackendVirtual.City = value;
                NotifyPropertyChanged();
            }
        }

        public string Province
        {
            get => BackendProvider != null ? BackendProvider.Province : BackendVirtual.Province;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.Province = value;
                else
                    BackendVirtual.Province = value;
                NotifyPropertyChanged();
            }
        }

        public string PostalCode
        {
            get => BackendProvider != null ? BackendProvider.PostalCode : BackendVirtual.PostalCode;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.PostalCode = value;
                else
                    BackendVirtual.PostalCode = value;
                NotifyPropertyChanged();
            }
        }

        public string TelephoneNumber
        {
            get => BackendProvider != null ? BackendProvider.TelephoneNumber : BackendVirtual.TelephoneNumber;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.TelephoneNumber = value;
                else
                    BackendVirtual.TelephoneNumber = value;
                NotifyPropertyChanged();
            }
        }

        public string TelephoneExtension
        {
            get => BackendProvider != null ? BackendProvider.TelephoneExtension : BackendVirtual.TelephoneExtension;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.TelephoneExtension = value;
                else
                    BackendVirtual.TelephoneExtension = value;
                NotifyPropertyChanged();
            }
        }

        public string FaxNumber
        {
            get => BackendProvider != null ? BackendProvider.FaxNumber : BackendVirtual.FaxNumber;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.FaxNumber = value;
                else
                    BackendVirtual.FaxNumber = value;
                NotifyPropertyChanged();
            }
        }

        public string Email
        {
            get => BackendProvider != null ? BackendProvider.Email : BackendVirtual.Email;
            set
            {
                if (BackendProvider != null)
                    BackendProvider.Email = value;
                else
                    BackendVirtual.Email = value;
                NotifyPropertyChanged();
            }
        }

        #endregion
    }
}
