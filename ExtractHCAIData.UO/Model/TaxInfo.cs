﻿using System;

namespace ExtractHcaiData.Uo.Model
{
    public class TaxInfo
    {
        public double GST { get; private set; }
        public double PST { get; private set; }
        public string ItemTaxID { get; private set; } = "";

        public static TaxInfo Create(DateTime ocfDate, bool gst, bool pst)
        {
            var res = new TaxInfo();

            if (gst && pst)
            {
                res.ItemTaxID = "S";
                res.GST = 7;
                res.PST = 8;
            }
            else if (pst)
            {
                res.ItemTaxID = "P";
                res.PST = 8;
                res.GST = 0;
            }
            else if (gst)
            {
                if (ocfDate.Year < 2011)
                {
                    res.ItemTaxID = "G";
                    res.GST = 7;
                    res.PST = 0;
                }
                else
                {
                    if (ocfDate.Year == 2011)
                    {
                        if (ocfDate.Month < 7)
                        {
                            //gst
                            res.ItemTaxID = "G";
                            res.GST = 7;
                            res.PST = 0;
                        }
                        else
                        {
                            //hst
                            res.ItemTaxID = "H";
                            res.GST = 13;
                            res.PST = 0;
                        }
                    }
                    else
                    {
                        //hst
                        res.ItemTaxID = "H";
                        res.GST = 13;
                        res.PST = 0;
                    }
                }
            }

            return res;
        }

        public bool Equals(TaxInfo other)
        {
            return ItemTaxID == other.ItemTaxID && Math.Abs(GST - other.GST) < 0.01 && Math.Abs(PST - other.GST) < 0.01;
        }
    }
}
