﻿namespace ExtractHcaiData.Uo.Model
{
    public class OCF23XRayStructure
    {
        public string xOrder { get; set; }
        public string xCode { get; set; }
        public string xDescription { get; set; }

        public string[] xViews;

        public double xFees { get; set; }
    }
}
