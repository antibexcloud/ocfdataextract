﻿using ExtractHcaiData.Uo.Vm;

namespace ExtractHcaiData.Uo.Model.Analized
{
    public abstract class DocAbstractVm : VMBase
    {
        public string Number { get; set; }

        public decimal Submitted { get; set; }
        public decimal ItemsSubmitted { get; set; }

        public decimal Approved { get; set; }
        public decimal ItemsApproved { get; set; }
    }
}
