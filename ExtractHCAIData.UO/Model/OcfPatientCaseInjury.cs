﻿namespace ExtractHcaiData.Uo.Model
{
    public class OcfPatientCaseInjury
    {
        public string CaseId { get; set; }
        public string Code { get; set; }
        public string CodeDescription { get; set; }

        public int Order { get; set; }
    }
}
