﻿using System;

namespace ExtractHcaiData.Uo.Model
{
    public class ToSetPlanNo
    {
        public double ReportID { get; set; }
        public string CaseID { get; set; }

        public DateTime? SentDate { get; set; }
        public DateTime? ReplyDate { get; set; }

        public short PlanNo { get; set; }
    }
}
