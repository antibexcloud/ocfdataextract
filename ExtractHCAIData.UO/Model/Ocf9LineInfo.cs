﻿namespace ExtractHcaiData.Uo.Model
{
    public class Ocf9LineInfo
    {
        public double GSAmountClaimed { get; set; }

        public double GSServicessAmountPayable { get; set; }

        public string GSReasonCode { get; set; }

        public string GSReasonCodeDesc { get; set; }

        public string GSReasonCodeDescOther { get; set; }

        public string GSDescription { get; set; }
    }
}
