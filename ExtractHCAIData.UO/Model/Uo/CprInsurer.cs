﻿using ExtractHcaiData.Uo.UoData;

namespace ExtractHcaiData.Uo.Model.Uo
{
    public class CprInsurer
    {
        public Third_Party Backend { get; }

        public bool IsMva => Backend.Type.Contains("MVA");

        public CprInsurer(Third_Party backend)
        {
            Backend = backend;
        }
    }
}
