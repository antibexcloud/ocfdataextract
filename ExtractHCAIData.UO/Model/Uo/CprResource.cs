﻿using System.Linq;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Work;

namespace ExtractHcaiData.Uo.Model.Uo
{
    public class CprResource
    {
        public Resource Backend { get; }
        public CprStaff Staff { get; }

        public CprResource(Resource backend, CprStaff staff)
        {
            Backend = backend;
            Staff = staff;
        }

        public string FirstName => Backend.Staff.first_name;
        public string LastName => Backend.Staff.last_name;

        public string Name => FirstName + " " + LastName;

        public string StaffName => FirstName + ", " + LastName;

        public ColRegInfo GetColRegInfoByCode(TransferWork worker, string code)
        {
            if (Backend == null) return null;

            var staffOccupation = (worker.TargetDb.UoContext.StaffTypes.FirstOrDefault(s => s.MVACode == code)
                                   ?? worker.TargetDb.UoContext.StaffTypes.FirstOrDefault(s => s.MVACode == "OTH"))
                ?.Occupation;

            if (Backend.ColRegType1 == code)
                return new ColRegInfo
                {
                    ColRegType = Backend.ColRegType1,
                    ColRegNo = Backend.ColRegNo1,
                    ColRegServiceType = Backend.ColRegServiceType1,
                    ColRegHrRate = Backend.ColRegHrRate1,
                    ColRegRegulated = Backend.ColRegRegulated1,
                    ColRegSignature = Backend.ColRegSignature1,
                    StaffOccupation = staffOccupation,
                };
            if (Backend.ColRegType2 == code)
                return new ColRegInfo
                {
                    ColRegType = Backend.ColRegType2,
                    ColRegNo = Backend.ColRegNo2,
                    ColRegServiceType = Backend.ColRegServiceType2,
                    ColRegHrRate = Backend.ColRegHrRate2,
                    ColRegRegulated = Backend.ColRegRegulated2,
                    ColRegSignature = Backend.ColRegSignature2,
                    StaffOccupation = staffOccupation,
                };
            if (Backend.ColRegType3 == code)
                return new ColRegInfo
                {
                    ColRegType = Backend.ColRegType3,
                    ColRegNo = Backend.ColRegNo3,
                    ColRegServiceType = Backend.ColRegServiceType3,
                    ColRegHrRate = Backend.ColRegHrRate3,
                    ColRegRegulated = Backend.ColRegRegulated3,
                    ColRegSignature = Backend.ColRegSignature3,
                    StaffOccupation = staffOccupation,
                };

            return null;
        }
    }
}
