﻿using ExtractHcaiData.Uo.UoData;

namespace ExtractHcaiData.Uo.Model.Uo
{
    public class CprStaff
    {
        public Staff Backend { get; }

        public CprStaff(Staff backend)
        {
            Backend = backend;
        }

        private string FirstName => Backend.first_name;
        private string LastName => Backend.last_name;

        public string Name => LastName + ", " + FirstName;
    }
}
