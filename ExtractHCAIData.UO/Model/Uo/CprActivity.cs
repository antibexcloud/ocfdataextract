﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Work;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Utils;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Model.Uo
{
    public class CprActivity
    {
        public Activity Backend { get; }

        public CprActivity(Activity backend)
        {
            Backend = backend;
        }

        public bool EqualsByCode(SyncActivityItem item)
        {
            return ConvertHelper.CodeEquals(Backend.mvaServiceCode, item.ServiceCode)
                   && item.Name.Trim().Equals(Backend.activity_name?.Trim(), InvariantCultureIgnoreCase);
        }

        public bool EqualsByItemId(SyncActivityItem item)
        {
            return Backend.activity_type?.Trim() == item.Type.Trim()
                   && Backend.activity_id.Trim().Equals(item.ItemId.Trim(), InvariantCultureIgnoreCase)
                   && ConvertHelper.CodeEquals(Backend.mvaServiceCode, item.ServiceCode)
                   && Backend.mvaMeasure?.Trim() == ConvertHelper.GetActivityOrProductMsr(item.Measure)
                   && Backend.mvaAttribute?.Trim() == item.Attribute
                   && Math.Abs((Backend.mvaUnits ?? 0.0) - item.Units) < 0.01;
        }

        public bool EqualsByDescription(SyncActivityItem item)
        {
            return Backend.activity_type?.Trim() == item.Type.Trim()
                   && ConvertHelper.CodeEquals(Backend.mvaServiceCode, item.ServiceCode)
                   && Backend.mvaMeasure?.Trim() == ConvertHelper.GetActivityOrProductMsr(item.Measure)
                   && Backend.mvaAttribute?.Trim() == item.Attribute
                   && item.Name.Trim().Equals(Backend.activity_name?.Trim(), InvariantCultureIgnoreCase)
                   && Math.Abs((Backend.mvaUnits ?? 0.0) - item.Units) < 0.01;
        }

        #region Converters

        // ocf18
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            GS18LineItemSubmit item, bool onInit)
        {
            var code = item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code;
            var attribute = item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Description);

            if (description == "Cervical Piiloow")
                description = "Cervical Pillow";

            var tax = TaxInfo.Create(ocfDate,
                item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST,
                item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST);
            var measure = SyncService.CheckOnQuotes2(item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure.ToLower());
            var fee = (double)item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost;
            var units = (double)item.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (string.IsNullOrEmpty(description)
                                             || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                             || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                             || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = isProduct ? "Product" : "Service",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ocf23
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            PAFLineItemSubmit item, bool onInit)
        {
            var code = item.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code.Replace(".", "");
            var attribute = item.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Description);
            var tax = TaxInfo.Create(ocfDate, false, false);
            var measure = "pr";
            var fee = (double)item.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee;
            var units = 1.0;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (string.IsNullOrEmpty(description)
                                             || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                             || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                             || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = /*isProduct ? "Product" :*/ "Block",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ocf23
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            OtherGSLineItemSubmit item, bool onInit)
        {
            var code = item.OCF23_OtherGoodsAndServices_Items_Item_Code;
            var attribute = item.OCF23_OtherGoodsAndServices_Items_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.OCF23_OtherGoodsAndServices_Items_Item_Description);
            var tax = TaxInfo.Create(ocfDate, false, false);
            var measure = SyncService.CheckOnQuotes2(item.OCF23_OtherGoodsAndServices_Items_Item_Measure.ToLower());
            var fee = (double)item.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost;
            var units = (double)item.OCF23_OtherGoodsAndServices_Items_Item_Quantity;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (string.IsNullOrEmpty(description)
                                             || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                             || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                             || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = isProduct ? "Product" : "Service",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ocf21b
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            GS21BLineItemSubmit item, bool onInit)
        {
            var code = item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code;
            var attribute = item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description);
            var tax = TaxInfo.Create(ocfDate, item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_GST,
                item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PST);
            var measure = SyncService.CheckOnQuotes2(item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Measure.ToLower());
            var fee = (double)item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost;
            var quantity = (double)item.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity;
            var units = /*(int)quantity == quantity ? 1 :*/ quantity;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (string.IsNullOrEmpty(description)
                                             || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                             || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                             || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = isProduct ? "Product" : "Service",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ocf21c
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            RenderedGSLineItemSubmit item, bool onInit)
        {
            var code = item.OCF21C_RenderedGoodsAndServices_Items_Item_Code;
            var attribute = item.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.OCF21C_RenderedGoodsAndServices_Items_Item_Description);
            var tax = TaxInfo.Create(ocfDate, false, false);
            var measure = SyncService.CheckOnQuotes2(item.OCF21C_RenderedGoodsAndServices_Items_Item_Measure.ToLower());
            var fee = 0.0;
            var units = (double)item.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = isProduct ? "Product" : "Service",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ocf21c
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            PAFReimbursableLineItemSubmit item, bool onInit)
        {
            var code = item.OCF21C_PAFReimbursableFees_Items_Item_Code;
            var attribute = item.OCF21C_PAFReimbursableFees_Items_Item_Attribute;
            var description = string.Empty;
            var tax = TaxInfo.Create(ocfDate, false, false);
            var measure = "pr";
            var fee = (double)item.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost;
            var units = 1.0;
            var isProduct = ConvertHelper.IsProduct(code);
            var activityType = isProduct ? "Product" : "Block";

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = SyncService.MakeActivityId(code),
                    activity_name = description,
                    activity_type = activityType,
                    activity_duration = "",

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,

                    ItemDescription = description,
                });

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ocf21c
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            OtherReimbursableLineItemSubmit item, bool onInit)
        {
            var code = item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code;
            var attribute = item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description);
            var tax = TaxInfo.Create(ocfDate, item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST,
                item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST);
            var measure = SyncService.CheckOnQuotes2(item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure.ToLower());
            var fee = (double)item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost;
            var quantity = (double)item.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity;
            var units = /*(int)quantity == quantity ? 1 :*/ quantity;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (string.IsNullOrEmpty(description)
                                             || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                             || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                             || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = isProduct ? "Product" : "Service",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        // ACSI
        public static SyncActivityItem MakeHcaiItemType(TransferWork worker, DateTime ocfDate, ObservableCollection<SyncActivityItem> items,
            GSACSILineItemSubmit item, bool onInit)
        {
            var code = item.ACSI_ReimbursableGoodsAndServices_Items_Item_Code;
            var attribute = item.ACSI_ReimbursableGoodsAndServices_Items_Item_Attribute;
            var description = SyncService.CheckOnQuotes(item.ACSI_ReimbursableGoodsAndServices_Items_Item_Description);
            var tax = TaxInfo.Create(ocfDate, item.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_GST,
                item.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PST);
            var measure = SyncService.CheckOnQuotes2(item.ACSI_ReimbursableGoodsAndServices_Items_Item_Measure.ToLower());
            var fee = (double)item.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost;
            var quantity = (double)item.ACSI_ReimbursableGoodsAndServices_Items_Item_Quantity;
            var units = /*(int)quantity == quantity ? 1 :*/ quantity;

            var syncActivity = items
                .FirstOrDefault(t => t.ServiceCode == code
                                     //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                     && (string.IsNullOrEmpty(description)
                                         || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                         || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                         || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                     && (t.Measure.ToLower() == measure
                                         || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                     && Math.Abs(t.Backend.mvaUnits - units) < 0.01
                                     && Math.Abs(t.Fee - fee) < 0.01);
            if (syncActivity is null && !onInit)
                syncActivity = items
                    .FirstOrDefault(t => t.ServiceCode == code
                                         //&& (t.Attribute == attribute || string.IsNullOrEmpty(t.Attribute))
                                         && (string.IsNullOrEmpty(description)
                                             || description.Equals(t.RealName, InvariantCultureIgnoreCase)
                                             || description.Equals(t.Name, InvariantCultureIgnoreCase)
                                             || description.Equals(t.ItemDescription, InvariantCultureIgnoreCase))
                                         && (t.Measure.ToLower() == measure
                                             || ConvertHelper.GetHcaiMeasureFromActivityOrProduct(t.Backend.mvaMeasure, t.Backend.mvaMeasure).ToLower() == measure)
                                         && Math.Abs(t.Backend.mvaUnits - units) < 0.01);

            if (syncActivity != null)
            {
                if (syncActivity.IsInVirtual)
                    syncActivity = worker.SourceDb.ActivityItems
                        .FirstOrDefault(a => !a.IsInVirtual && a.Id == syncActivity.VirtualId);
                else if (onInit)
                    syncActivity.CorrectTax(tax);

                return syncActivity;
            }

            if (!onInit) throw new Exception($"Not found item '{code}'{attribute} ({description}) {units}[{measure}]");

            var isProduct = ConvertHelper.IsProduct(code);

            // create
            var activity = worker.SourceDb.SyncContext.ActivityItems
                .Add(new ActivityItem
                {
                    activity_id = code,
                    activity_name = description,
                    activity_type = isProduct ? "Product" : "Service",
                    activity_duration = "",

                    ItemDescription = description,

                    mvaServiceCode = code,
                    mvaAttribute = attribute,
                    mvaMeasure = measure,
                    mvaFee = fee,
                    mvaUnits = units,
                    mvaTaxID = tax.ItemTaxID,
                    IsProduct = isProduct,
                    GST = tax.GST,
                    PST = tax.PST,
                });

            if (!isProduct && activity.mvaMeasure?.StartsWith("h") == true)
            {
                activity.activity_id = SyncService.MakeActivityId(code, activity.mvaUnits);
                activity.activity_duration = SyncService.MakeDuration(activity.mvaUnits);
            }
            else
                activity.activity_id = SyncService.MakeActivityId(code);

            worker.SourceDb.SyncContext.SaveChangesWithDetect();

            // add
            syncActivity = new SyncActivityItem(activity);
            items.Add(syncActivity);
            // exit
            return syncActivity;
        }

        #endregion

        #region Make helpers

        public static Activity MakeActivityFromHcaiItemType(TransferWork worker, SyncActivityItem item)
        {
            var baseActivityId = item.Backend.activity_id.Trim().SafetySubstring(49).Trim();
            var activityId = baseActivityId;

            var idx = 1;
            while (worker.TargetDb.UoContext.Activities.Any(a => a.activity_id.Equals(activityId, InvariantCultureIgnoreCase)))
                activityId = $"{baseActivityId}_{idx++}";

            if (baseActivityId != activityId)
            {
                item.ItemId = activityId;
                worker.SourceDb.SyncContext.SaveChangesWithDetect();
            }

            var activity = new Activity
            {
                activity_id = activityId,
                activity_name = !string.IsNullOrEmpty(item.Name) && item.Name != item.ServiceCode && item.Name != item.ItemDescription
                    ? item.Name
                    : !string.IsNullOrEmpty(item.ItemDescription)
                        ? SyncService.CheckOnQuotes(item.ItemDescription.Trim()).SafetySubstring(500).Trim()
                        : item.ServiceCode,
                activity_type = item.Backend.activity_type.Trim(),
                active_activity = 1,

                activity_duration = item.Backend.activity_duration,
                color = SyncService.GenerateColors(),

                mvaServiceCode = item.Backend.mvaServiceCode.Trim(),
                mvaAttribute = item.Attribute,
                mvaMeasure = ConvertHelper.GetActivityOrProductMsr(item.Measure),
                mvaUnits = item.Units,
                mvaFee = item.Backend.mvaFee,
                mvaTaxID = item.Backend.mvaTaxID,

                AllowToBookOnline = 0,
                provider_req = 1,
                facility_req = 0,
                equipment_req = 0,
                
                measure = ConvertHelper.GetActivityOrProductMsr(item.Measure),
                activity_cost = item.Fee,
                service_code = item.Backend.mvaServiceCode.Trim(),
                service_description = SyncService.CheckOnQuotes(item.ItemDescription.Trim()).SafetySubstring(200).Trim(),
                units = $"{item.Units:F2}",
            };

            activity.OnlineDisplayName = activity.activity_name.SafetySubstring(100);

            return activity;
        }

        #endregion
    }
}
