﻿using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Model.Uo
{
    public class CprClient
    {
        public Client Backend { get; }

        public CprClient(Client backend)
        {
            Backend = backend;
        }

        public int Id => (int)Backend.client_id;

        private string Name =>
            Backend.first_name.Trim() +
            (string.IsNullOrEmpty(Backend.middle_name) ? "" : " " + Backend.middle_name.Trim()) +
            " " + Backend.last_name.Trim();

        public bool Equals(SyncPatient info)
        {
            return (Name.ToLower() == (info.ResultFirstName.Trim() +
                                      (string.IsNullOrEmpty(info.ResultMiddleName) ? "" : " " + info.ResultMiddleName.Trim()) +
                                      " " + info.ResultLastName.Trim()).ToLower()
                    || (this.Backend.last_name.ToLower() == info.ResultLastName.ToLower()
                        && this.Backend.first_name.ToLower() == info.ResultFirstName.ToLower())
                    || (this.Backend.first_name.ToLower() == info.ResultLastName.ToLower()
                        && this.Backend.last_name.ToLower() == info.ResultFirstName.ToLower())
                   )
                   && (Backend.DOB is null || Backend.DOB == info.ResultDOB)
                   && (string.IsNullOrEmpty(Backend.gender) || string.IsNullOrEmpty(info.ResultGender) || Backend.gender == info.ResultGender.ToGender());
        }
    }
}
