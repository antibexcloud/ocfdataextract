﻿using System;
using System.Linq;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Work;
using HcaiSync.Common.Utils;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Model.Uo
{
    public class CprProduct
    {
        public Product Backend { get; }

        public CprProduct(Product backend)
        {
            Backend = backend;
        }

        public bool EqualsByCode(SyncActivityItem item)
        {
            return Backend.mvaServiceCode?.Trim() == item.ServiceCode.Trim()
                   && item.Name.Trim().Equals(Backend.product_name?.Trim(), InvariantCultureIgnoreCase);
        }

        public bool EqualsBtItemId(SyncActivityItem item)
        {
            return Backend.mvaServiceCode?.Trim() == item.Backend.mvaServiceCode.Trim()
                   && item.ItemId.Equals(Backend.product_id, InvariantCultureIgnoreCase)
                   && ((Backend.mvaUnits ?? 0.0) == 0 || Math.Abs((Backend.mvaUnits ?? 0.0) - item.Units) < 0.01);
        }

        public bool EqualsByDescription(SyncActivityItem item)
        {
            return Backend.mvaServiceCode?.Trim() == item.Backend.mvaServiceCode.Trim()
                   && item.Name.Equals(Backend.product_name?.Trim(), InvariantCultureIgnoreCase)
                   && ((Backend.mvaUnits ?? 0.0) == 0 || Math.Abs((Backend.mvaUnits ?? 0.0) - item.Units) < 0.01);
        }

        #region Make helpers

        public static Product MakeProductFromHcaiItemType(TransferWork worker, SyncActivityItem item)
        {
            var baseProductId = item.Backend.activity_id.Trim().SafetySubstring(49).Trim();
            var productId = baseProductId;

            var idx = 1;
            while (worker.TargetDb.UoContext.Products.Any(a => a.product_id.ToLower().Trim() == productId.ToLower()))
                productId = $"{baseProductId}_{idx++}";

            if (baseProductId != productId)
            {
                item.ItemId = productId;
                worker.SourceDb.SyncContext.SaveChangesWithDetect();
            }

            var product = new Product
            {
                product_id = productId,
                product_name = !string.IsNullOrEmpty(item.Name) && item.Name != item.ServiceCode && item.Name != item.ItemDescription
                    ? item.Name.Trim()
                    : !string.IsNullOrEmpty(item.ItemDescription)
                        ? SyncService.CheckOnQuotes(item.ItemDescription.Trim()).SafetySubstring(500).Trim()
                        : item.ServiceCode.Trim(),
                status = 1,
                measurement_unit = "Each",
                unit = item.Units,

                mvaServiceCode = item.Backend.mvaServiceCode,
                mvaMeasure = "Good",
                mvaUnits = item.Units,
                mvaFee = item.Backend.mvaFee,
                mvaTaxID = item.Backend.mvaTaxID,
            };

            return product;
        }

        #endregion
    }
}
