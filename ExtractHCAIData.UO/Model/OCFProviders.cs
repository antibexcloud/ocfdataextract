﻿using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Work;

namespace ExtractHcaiData.Uo.Model
{
    public class OCFProviders
    {
        public string ProviderID { get; private set; }

        public string ProviderName { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }

        public string Type { get; private set; }
        public short Regulated { get; private set; }
        public string Number { get; private set; }
        public string HourRate { get; private set; }

        public string ProviderOccupation { get; private set; }
        public string StaffOccupation { get; private set; }

        public string ProviderHCAINo { get; set; }
        public string RealProviderHCAINo { get; private set; }
        public string ItemPR { get; private set; }

        public int? StaffId { get; private set; }

        private static CprResource FindCprResource(TransferWork worker, string pHcaiNo, string pOccupation)
        {
            var syncProvider = worker.SourceDb.Providers
                .FirstOrDefault(p => $"{p.Backend.HCAI_Provider_Registry_ID}" == pHcaiNo &&
                                     p.Backend.RegistrationLineItems.Any(r => r.Profession == pOccupation));
            if (syncProvider != null)
            {
                if (syncProvider.IsInVirtual)
                {
                    var virtProvider = worker.SourceDb.ProviderVirtuals
                        .FirstOrDefault(p => p.HCAI_Provider_Registry_ID == syncProvider.Backend.virtualId);

                    if (virtProvider != null)
                        return virtProvider.UoResource;

                    Logger.Info("FindCprResource unreal!");
                }
                else
                    return syncProvider.UoResource;
            }

            var resUo = worker.TargetDb.Resources
                .FirstOrDefault(r => r.Backend.HCAIRegNo == pHcaiNo
                                     && (r.Backend.ColRegType1 == pOccupation
                                         || r.Backend.ColRegType2 == pOccupation
                                         || r.Backend.ColRegType3 == pOccupation));
            return resUo;
        }

        public static OCFProviders GetCprResource(TransferWork worker, string pHcaiNo, string pOccupation)
        {
            var cprResource = FindCprResource(worker, pHcaiNo, pOccupation);
            if (cprResource == null) return null;
            //
            var info = cprResource.GetColRegInfoByCode(worker, pOccupation);
            if (info is null)
            {
                Logger.Info($"GetCprResource.GetColRegInfoByCode bad [HcaiNo='{pHcaiNo}', Occupation='{pOccupation}', cprResource: {cprResource.Name}]!");
                return null;
            }

            return new OCFProviders
            {
                HourRate = "",
                ItemPR = ConvertHelper.Letters[0],
                ProviderID = cprResource.Backend.resource_id,

                FirstName = cprResource.Backend.Staff.first_name,
                LastName = cprResource.Backend.Staff.last_name,
                ProviderName = cprResource.Backend.Staff.last_name + ", " + cprResource.Backend.Staff.first_name,

                RealProviderHCAINo = pHcaiNo,
                ProviderHCAINo = cprResource.Backend.HCAIRegNo,

                Number = info.ColRegNo,
                ProviderOccupation = info.ColRegServiceType,
                StaffOccupation = info.StaffOccupation,
                Regulated = info.ColRegRegulated ?? 0,
                Type = info.ColRegType,

                StaffId = cprResource.Backend.Staff.staff_id,
            };
        }

        public static OCFProviders GetOcfProviderIndex(TransferWork worker, List<OCFProviders> providers,
            string pHcaiNo, string pOccupation, int max = 6)
        {
            var provider = providers
                .FirstOrDefault(p => p.RealProviderHCAINo == pHcaiNo && p.Type == pOccupation);
            if (provider != null) return provider;

            var cprResource = FindCprResource(worker, pHcaiNo, pOccupation);
            if (cprResource == null) return null;

            //IF OCCUPATION IS NOT FOUND TAKE FIRST OCCUPATION
            if (providers.Count < max)
            {
                var info = cprResource.GetColRegInfoByCode(worker, pOccupation);

                if (info != null)
                {
                    provider = new OCFProviders
                    {
                        HourRate = "",
                        ItemPR = ConvertHelper.Letters[providers.Count],
                        ProviderID = cprResource.Backend.resource_id,

                        FirstName = cprResource.Backend.Staff.first_name,
                        LastName = cprResource.Backend.Staff.last_name,
                        ProviderName = cprResource.Backend.Staff.last_name + ", " + cprResource.Backend.Staff.first_name,

                        RealProviderHCAINo = pHcaiNo,
                        ProviderHCAINo = cprResource.Backend.HCAIRegNo,

                        Number = info.ColRegNo,
                        ProviderOccupation = info.ColRegServiceType,
                        StaffOccupation = info.StaffOccupation,
                        Regulated = info.ColRegRegulated ?? 0,
                        Type = info.ColRegType,

                        StaffId = cprResource.Backend.Staff.staff_id,
                    };
                    providers.Add(provider);
                    return provider;
                }

                Logger.Info($"GetOcfProviderIndex.GetColRegInfoByCode bad [HcaiNo='{pHcaiNo}', Occupation='{pOccupation}', cprResource: {cprResource.Name}]!");
            }
            else
                Logger.Info($"OCF has more then {max} PROVIDERS, SO USE FIRST PROVIDER");

            return providers.Any() ? providers[0] : null;
        }
    }
}
