﻿using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public class AcsiModel : OcfCommonInfoModel
    {
        public ACSISubmit Submit { get; set; }
    }
}
