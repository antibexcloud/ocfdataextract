﻿using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public class Ocf21BModel : OcfCommonInfoModel
    {
        public OCF21BSubmit Submit { get; set; }
    }
}
