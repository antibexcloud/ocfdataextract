﻿using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public class Form1Model : OcfCommonInfoModel
    {
        public AACNSubm Submit { get; set; }
    }
}
