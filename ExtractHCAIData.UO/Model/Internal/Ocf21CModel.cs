﻿using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public class Ocf21CModel : OcfCommonInfoModel
    {
        public OCF21CSubmit Submit { get; set; }
    }
}
