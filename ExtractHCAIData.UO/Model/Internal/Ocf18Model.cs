﻿using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public class Ocf18Model : OcfCommonInfoModel
    {
        public OCF18Submit Submit { get; set; }
    }
}
