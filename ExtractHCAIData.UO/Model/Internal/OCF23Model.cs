﻿using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public class Ocf23Model : OcfCommonInfoModel
    {
        public OCF23Submit Submit { get; set; }
    }
}
