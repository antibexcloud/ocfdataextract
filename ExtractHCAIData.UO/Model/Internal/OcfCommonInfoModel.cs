﻿using System;

namespace ExtractHcaiData.Uo.Model.Internal
{
    public abstract class OcfCommonInfoModel
    {
        public DateTime? SubmissionDate { get; set; }

        public DateTime OcfDate { get; set; }
    }
}
