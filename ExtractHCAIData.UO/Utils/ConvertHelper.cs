﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.UoData;
using HcaiSync.Common.Enums;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Utils
{
    public static class ConvertHelper
    {
        public static readonly string[] Letters = {"A", "B", "C", "D", "E", "F"};
        public static readonly OCF23XRayStructure[] Ocf23Xrays = LoadOcf23XrayStructure();

        public static T ToEnumParse<T>(this int code)
            where T : struct
        {
            return Enum.GetValues(typeof(T)).OfType<T>().ToArray()[code];
        }

        public static T ToEnumParse<T>(this int? code)
            where T : struct
        {
            return code != null
                ? Enum.GetValues(typeof(T)).OfType<T>().ToArray()[code.Value]
                : default(T);
        }

        public static string ToGender(this string val)
        {
            if (string.IsNullOrEmpty(val)) return string.Empty;

            return val == GenderEnum.M.ToString()
                ? "Male"
                : val == GenderEnum.F.ToString()
                    ? "Female"
                    : string.Empty;
        }

        public static bool CodeEquals(string code1, string code2)
        {
            return (code1 ?? string.Empty).Trim().Replace(".", "")
                .Equals((code2 ?? string.Empty).Trim().Replace(".", ""), InvariantCultureIgnoreCase);
        }

        public static bool StringEquals(string str1, string str2)
        {
            return (str1 ?? string.Empty).Trim().Equals((str2 ?? string.Empty).Trim(), InvariantCultureIgnoreCase);
        }

        public static string GetActivityOrProductMsr(string iMsr)
        {
            var result = iMsr.Trim().ToLower() switch
            {
                "gd" => "Good",
                "km" => "KM",
                "hr" => "Hour",
                "pr" => "Procedure",
                "sn" => "Session",
                _ => iMsr ?? ""
            };

            return result;
        }

        public static string GetHcaiMeasureFromActivityOrProduct(string iMsr, string original)
        {
            var result = iMsr.Trim().ToLower() switch
            {
                "good" => "gd",
                "km" => "km",
                "hour" => "hr",
                "procedure" => "pr",
                "session" => "sn",
                _ => original ?? ""
            };

            return result;
        }

        private static readonly DateTime Plan2014 = new DateTime(2014, 11, 1);
        private static readonly DateTime Plan2016 = new DateTime(2016, 10, 1);

        // PaperWork: OCF18 / OCF23
        public static string GetPlanVersion(DateTime date)
        {
            if (date >= Plan2016) return "October 1, 2016";
            if (date >= Plan2014) return "November 1, 2014";
            return "November 1, 2012";
        }

        public static string GetRepliedAs(string documentStatus)
        {
            if (string.IsNullOrEmpty(documentStatus)) return string.Empty;

            if (documentStatus.ToLower().Trim() == "approved" || documentStatus.ToLower().Trim() == "responded")
                return "Approved";
            if (documentStatus.ToLower().Trim() == "partially approved")
                return "Partially Approved";
            if (documentStatus.ToLower().Trim() == "declined" || documentStatus.ToLower().Trim() == "not approved")
                return "Not Approved";
            if (documentStatus.ToLower().Trim() == "Review Required".ToLower())
                return "";
            if (documentStatus.ToLower().Trim() == "Submitted".ToLower())
                return "";
            return documentStatus;
        }

        private static OCF23XRayStructure[] LoadOcf23XrayStructure()
        {
            var res = new List<OCF23XRayStructure>();

            for (var i = 0; i <= 3; i++)
            {
                var item = new OCF23XRayStructure
                {
                    xOrder = $"{i + 1}",
                    xCode = "3SC10",
                    xFees = 0
                };
                res.Add(item);

                switch (i)
                {
                    case 0:
                        item.xDescription = "X-Ray of the Cervical Spine";
                        item.xViews = new[] { "2 or fewer (CXA)", "3-4 (CXB)", "5-6 (CXC)", "more then 6 (CXD)" };
                        break;

                    case 1:
                        item.xDescription = "X-Ray of the Thoracic Spine";
                        item.xViews = new[] { "2 or fewer (THA)", "3-4 (THB)" };
                        break;

                    case 2:
                        item.xDescription = "X-Ray of the Lumbar Spinal";
                        item.xViews = new[] { "2 or fewer (LBA)", "3-4 (LBB)", "5-6 (LBC)", "more then 6 (LBD)" };
                        break;

                    case 3:
                        item.xDescription = "X-Ray of the Lumbosacral Spinal";
                        item.xViews = new[] { "2 or fewer (LSA)", "3-4 (LSB)", "5-6 (LSC)", "more then 6 (LSD)" };
                        break;
                }
            }

            return res.ToArray();
        }

        private static DateTime GetMyTimeFormat(DateTime dt)
        {
            return new DateTime(1900, 1, 1, dt.Hour, dt.Minute, dt.Second);
        }

        public static bool IsProduct(string serviceCode)
        {
            return serviceCode.Substring(0, Math.Min(3, serviceCode.Length)).ToLower() == "gxx";
        }

        public static double CalculateTaxAmount(double tax, double itemAmount)
        {
            var val = Math.Round(itemAmount * tax / 100, 2);
            return val;
        }

        public static bool IsNumeric(this string n)
        {
            if (string.IsNullOrWhiteSpace(n)) return false;

            try
            {
                return double.TryParse(n, out _);
            }
            catch
            {
                return false;
            }
        }

        public static string PhoneFromHcai(string phone)
        {
            phone = phone?.Trim() ?? string.Empty;
            if (string.IsNullOrWhiteSpace(phone)) return string.Empty;

            // leading '1'/'0'
            while (phone.Length >= 10 && (phone.StartsWith("0") || phone.StartsWith("1")))
                phone = phone.Substring(1, phone.Length - 1);
            // check on Length >= 10
            if (phone.Length < 10) return string.Empty;

            if (!long.TryParse(phone, out _)) return phone;

            var result = string.Empty;
            if (phone.Length > 10)
            {
                result = phone.Substring(0, phone.Length - 10) + " ";
                phone = phone.Substring(phone.Length - 10, 10);
            }
            result += $"{phone.Substring(0, 3)}-{phone.Substring(3, 3)}-{phone.Substring(6, 4)}";
            return result;
        }

        private static string MyLeft(string text, int length)
        {
            if (length <= 0 || string.IsNullOrEmpty(text))
                return string.Empty;
            if (text.Length <= length)
                return text;
            return text.Substring(0, length);
        }

        public static string FormatProvince(string province)
        {
            if (string.IsNullOrWhiteSpace(province)) return string.Empty;

            return province.Length < 2
                ? province
                : MyLeft(province, 2).ToUpper();
        }

        public static string FormatPostalCode(string postalCode)
        {
            try
            {
                var tPostalCode = postalCode?.Trim() ?? string.Empty;
                tPostalCode = tPostalCode.Replace(" ", "");
                tPostalCode = tPostalCode.ToUpper();

                if (tPostalCode.Length == 5)
                {
                    if (IsNumeric(tPostalCode))
                        return tPostalCode;
                }
                if (tPostalCode.Length < 6) return string.Empty;

                var arr = tPostalCode.ToCharArray(0, tPostalCode.Length);

                if ((arr[0] >= 65 && arr[0] <= 90)
                    && (IsNumeric(arr[1].ToString()))
                    && (arr[2] >= 65 && arr[2] <= 90)
                    && (IsNumeric(arr[3].ToString())) && (IsNumeric(arr[5].ToString()))
                    && (arr[4] >= 65 && arr[4] <= 90))
                {
                    return arr[0].ToString() + arr[1] + arr[2] + " " + arr[3] + arr[4] + arr[5];
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        public static List<HCAIStatusLog> CreateHcaiLog(string ocfType, double reportId, string hcaiDocNumber,
            bool isExists, DateTime createDate, DateTime submitDate, DateTime replyDate,
            string repliedAs, bool isArchived, string error)
        {
            var isReport = ocfType.StartsWith("OCF18") || ocfType.StartsWith("OCF23") || ocfType.StartsWith("FORM1");
            var res = new List<HCAIStatusLog>();

            var isReplied = !string.IsNullOrEmpty(repliedAs);

            //1. set log to created
            var logItem = new HCAIStatusLog
            {
                Archived = false,
                ErrorMsg = "",
                HCAIDocNumber = "",
                LockStatus = 0,
                OCFType = ocfType,
                InvoiceID = isReport ? 0 : reportId,
                ReportID = isReport ? reportId : 0,
                //'' created
                IsCurrentStatus = false,
                OCFStatus = 2, //hcai complient
                StatusDate = createDate.Date,
                StatusOrder = 1,
                StatusTime = GetMyTimeFormat(createDate)
            };

            if (!isExists)
            {
                res.Add(logItem);

                //2. set log to sent
                logItem = new HCAIStatusLog
                {
                    Archived = !isReplied && isArchived,
                    ErrorMsg = !isReplied ? error ?? "" : "",
                    HCAIDocNumber = /*isReplied ? "" : */hcaiDocNumber,
                    LockStatus = 0,
                    OCFType = ocfType,
                    InvoiceID = isReport ? 0 : reportId,
                    ReportID = isReport ? reportId : 0,
                    IsCurrentStatus = !isReplied,
                    OCFStatus = 6, //SuccessfullyDelivered
                    StatusDate = submitDate.Date,
                    StatusOrder = 2,
                    StatusTime = GetMyTimeFormat(submitDate),
                };
                res.Add(logItem);
            }

            //3. set log to reply if there is a reply
            var replyAs = isReplied ? repliedAs.ToLower().Trim() : string.Empty;
            var isRequired = replyAs.Contains("approved");
            if (isReplied && isRequired)
            {
                logItem = new HCAIStatusLog
                {
                    Archived = isArchived,
                    ErrorMsg = error ?? "",
                    HCAIDocNumber = hcaiDocNumber,
                    LockStatus = 0,
                    OCFType = ocfType,
                    InvoiceID = isReport ? 0 : reportId,
                    ReportID = isReport ? reportId : 0,
                    IsCurrentStatus = true,
                    StatusDate = replyDate.Date,
                    StatusOrder = 3,
                    StatusTime = GetMyTimeFormat(replyDate)
                };
                res.Add(logItem);

                switch (replyAs)
                {
                    case "approved":
                    //case "responded":
                        logItem.OCFStatus = 14; //approved
                        break;

                    case "partially approved":
                        logItem.OCFStatus = 13; //partially approved
                        break;

                    default:
                        logItem.OCFStatus = 12; //not approved
                        break;
                }
            }

            return res;
        }


        public static void CorrectTax(this SyncActivityItem syncActivity, TaxInfo tax)
        {
            if ((tax.GST > 0 && Math.Abs(tax.GST - syncActivity.GST) > 0.001)
                || (tax.PST > 0 && Math.Abs(tax.PST - syncActivity.PST) > 0.001)
                || (!string.IsNullOrEmpty(tax.ItemTaxID) && tax.ItemTaxID != syncActivity.TaxId))
            {
                syncActivity.TaxId = tax.ItemTaxID;
                syncActivity.GST = tax.GST;
                syncActivity.PST = tax.PST;
            }
        }
    }
}
