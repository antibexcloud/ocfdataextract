﻿using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace ExtractHcaiData.Uo.Utils
{
    public static class Service
    {
        public static double RoundTo(this double source, int digits = 2, MidpointRounding round = MidpointRounding.AwayFromZero)
        {
            try
            {
                return (double)Decimal.Round((decimal)source, digits, round);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static double? RoundTo(this double? source, int digits = 2, MidpointRounding round = MidpointRounding.AwayFromZero)
        {
            try
            {
                return source != null
                ? (double)Decimal.Round((decimal)source.Value, digits, round)
                : (double?)null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static decimal RoundTo(this decimal source, int digits = 2, MidpointRounding round = MidpointRounding.AwayFromZero)
        {
            try
            {
                return Decimal.Round(source, digits, round);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static decimal? RoundTo(this decimal? source, int digits = 2, MidpointRounding round = MidpointRounding.AwayFromZero)
        {
            try
            {
                return source != null
                ? Decimal.Round(source.Value, digits, round)
                : (decimal?)null;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string InworkException(Exception ex)
        {
            var msg = ex.Message;

            if (ex is DbEntityValidationException)
            {
                var exValid = ex as DbEntityValidationException;
                if (exValid.EntityValidationErrors != null)
                    foreach (var exValidEntityValidationError in exValid.EntityValidationErrors)
                        foreach (var dbValidationError in exValidEntityValidationError.ValidationErrors)
                            msg += Environment.NewLine + dbValidationError.ErrorMessage;
            }
            else
            {
                var inner = ex.InnerException;
                while (inner != null)
                {
                    msg = inner.Message;
                    inner = inner.InnerException;
                }
            }

            return msg;
        }

        public static void SetCombobox(ComboBox cb, string[] variants, bool enableEdit)
        {
            var src = variants/*.Where(s => !string.IsNullOrEmpty(s))*/.ToArray();
            cb.ItemsSource = src;

            if (src.Length == 0)
            {
                cb.IsEnabled = enableEdit;
                cb.IsEditable = enableEdit;
            }
            else if (src.Length == 1)
            {
                cb.SelectedValue = src[0];
                cb.Text = src[0];
                cb.IsEditable = enableEdit;
                cb.IsEnabled = enableEdit;
            }
            else
            {
                cb.IsEnabled = true;
                cb.IsEditable = enableEdit;
            }
        }

        public static bool ReadyCombobox(ComboBox cb)
        {
            return !cb.IsEnabled
                   || !string.IsNullOrEmpty(cb.Text)
                   || !(cb.ItemsSource as string[]).Any(s => !string.IsNullOrEmpty(s))
                   || (cb.ItemsSource as string[]).Any(s => s == cb.Text);
        }

        public static DateTime HcaiNum2Date(string number)
        {
            try
            {
                var year = (DateTime.Now.Year / 100) * 100 + int.Parse(number.Substring(0, 2));
                var month = int.Parse(number.Substring(2, 2));
                var day = int.Parse(number.Substring(4, 2));

                return new DateTime(year, month, day);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool Confirm(string message, string caption = "")
        {
            return MessageBox.Show(message, caption, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No) == MessageBoxResult.Yes;
        }
    }
}

