﻿using System;
using System.Data;
using System.Data.SqlTypes;
using System.Globalization;
using System.Linq;

namespace ExtractHcaiData.Uo.Utils
{
    public static class ServiceConvert
    {
        private static readonly DateTime MinDateValue = (DateTime)SqlDateTime.MinValue;

        public static string FullMessage(this Exception ex)
        {
            if (ex is null) return "";

            var res = ex.Message;
            var innerEx = ex.InnerException;
            while (innerEx != null)
            {
                res += "\r\n **** InnerException:\r\n" + innerEx.Message;
                innerEx = innerEx.InnerException;
            }
            return res;
        }

        public static double RoundMy(double value, int digits)
        {
            return Math.Round(value, digits, MidpointRounding.AwayFromZero);
        }
        
        public static double ConvertToDouble(object data)
        {
            try
            {
                var text = data as string;
                if (text is null) return Convert.ToDouble(data);

                if (text.StartsWith("."))
                    text = "0" + text;
                return Double.Parse(text.Replace(',', '.'), NumberStyles.Float, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static float ConvertToFloat(object data)
        {
            try
            {
                var text = data as string;
                if (text is null) return Convert.ToSingle(data);

                if (text.StartsWith("."))
                    text = "0" + text;
                return float.Parse(text.Replace(',', '.'), NumberStyles.Float, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return 0.0f;
            }
        }

        public static object FixDbNull(object value)
        {
            return value is DBNull ? null : value;
        }

        public static bool GetBool(object aInput, bool defaultValue = false)
        {
            return (FixDbNull(aInput) != null) ? Convert.ToBoolean(aInput) : defaultValue;
        }

        public static string GetString(object aInput, string defaultValue="")
        {
            return (FixDbNull(aInput) != null) ? Convert.ToString(aInput) : defaultValue;
        }

        public static int GetInteger(object aInput, int defaultValue = 0)
        {
            return (FixDbNull(aInput) != null) ? Convert.ToInt32(aInput) : defaultValue;
        }

        public static int? GetIntegerNullable(object aInput, int? defaultValue = null)
        {
            return (FixDbNull(aInput) != null) ? Convert.ToInt32(aInput) : defaultValue;
        }

        public static long GetLong(object aInput, long defaultValue = 0)
        {
            return (FixDbNull(aInput) != null) ? Convert.ToInt64(aInput) : defaultValue;
        }

        public static double GetDouble(object aInput, double defaultValue=0.0)
        {
            return (FixDbNull(aInput) != null) ? Convert.ToDouble(aInput) : defaultValue;
        }

        public static DateTime GetDateTime(object aInput)
        {
            var dt = (FixDbNull(aInput) != null) ? Convert.ToDateTime(aInput) : MinDateValue;
            if (dt.Kind == DateTimeKind.Unspecified)
                dt = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            return dt;
        }

        public static Guid GetGuid(object aInput)
        {
            try
            {
                return (FixDbNull(aInput) != null) ? (Guid)aInput : Guid.Empty;
            }
            catch
            {
                return Guid.Empty;
            }
        }

        public static byte[] GetBytes(object aInput)
        {
            try
            {
                if (FixDbNull(aInput) == null) return null;
                return aInput as byte[];
            }
            catch
            {
                return null;
            }
        }

        public static string SafetySubstring(this string str, int maxLengtth)
        {
            return str != null ? str.Substring(0, Math.Min(str.Length, maxLengtth)) : "";
        }

        public static int? PrepareIntNullable(this int? aVal)
        {
            return aVal ?? (int?)null;
        }

        public static int? PrepareIntKey(this int aVal)
        {
            return (aVal != 0) ? aVal : (int?)null;
        }

        public static long? PrepareLongKey(this long aVal)
        {
            return (aVal != 0) ? aVal : (long?)null;
        }

        public static DateTime? PrepareDateTime(this DateTime aVal, bool strong=true)
        {
            if (aVal <= MinDateValue)
                return null;
            if (aVal.Kind != DateTimeKind.Unspecified)
                return aVal.Kind == DateTimeKind.Local ? aVal : aVal.ToLocalTime();

            if (strong)
                throw new Exception("DateTime.Kind == DateTimeKind.Unspecified!");
            return DateTime.SpecifyKind(aVal, DateTimeKind.Local);
        }

        public static double? PrepareDouble(this double val)
        {
            return (!double.IsNaN(val) && !double.IsInfinity(val)) ? val : (double?)null;
        }

        public static string StringOrNull(this string aVal, int maxLehgth = int.MaxValue)
        {
            return (!string.IsNullOrEmpty(aVal)) ? aVal.SafetySubstring(maxLehgth) : null;
        }

        public static T[] Convert2Array<T>(this DataTable sourcetable, Func<DataRow, T> factory)
        {
            if (sourcetable is null) throw new ArgumentNullException(nameof(sourcetable));
            if (factory is null) throw new ArgumentNullException(nameof(factory));
            return sourcetable.Rows.OfType<DataRow>().Select(factory).ToArray();
        }

        public static int Form1Code2Row(string code)
        {
            switch (code)
            {
                case "1.D.U":
                    return 1;
                case "1.D.L":
                    return 2;
                case "1.U.U":
                    return 4;
                case "1.U.L":
                    return 5;
                case "1.P.A":
                    return 7;
                case "1.P.E":
                    return 8;
                case "1.P.M":
                    return 9;
                case "1.O.D":
                    return 11;
                case "1.G.F":
                    return 13;
                case "1.G.HS":
                    return 14;
                case "1.G.SH":
                    return 15;
                case "1.G.C":
                    return 16;
                case "1.G.HR.B":
                    return 18;
                case "1.G.HR.C":
                    return 19;
                case "1.G.HR.S":
                    return 20;
                case "1.G.FN":
                    return 21;
                case "1.G.T":
                    return 22;
                case "1.F.P":
                    return 24;
                case "1.F.A":
                    return 25;
                case "1.M.SI":
                    return 27;
                case "1.M.W":
                    return 28;
                case "1.M.T":
                    return 29;
                case "1.L.B":
                    return 31;
                case "1.L.O":
                    return 32;

                case "2.H.BA.C":
                    return 2;
                case "2.H.BE.C":
                    return 4;
                case "2.H.BE.S":
                    return 5;
                case "2.H.C.P":
                    return 7;
                case "2.H.C.O":
                    return 8;
                case "2.SU.T":
                    return 10;
                case "2.SU.A":
                    return 11;
                case "2.SU.I":
                    return 12;
                case "2.SU.EC":
                    return 13;
                case "2.C.SC":
                    return 15;

                case "3.G.CA":
                    return 1;
                case "3.G.D":
                    return 2;
                case "3.G.CL":
                    return 3;
                case "3.G.B":
                    return 4;
                case "3.G.ME":
                    return 5;
                case "3.G.MO":
                    return 6;
                case "3.BC.V":
                    return 8;
                case "3.BC.CI":
                    return 9;
                case "3.BC.D":
                    return 10;
                case "3.BC.B":
                    return 11;
                case "3.BC.CL":
                    return 12;
                case "3.T.C":
                    return 14;
                case "3.T.T":
                    return 15;
                case "3.T.SU":
                    return 16;
                case "3.T.M":
                    return 17;
                case "3.V.V":
                    return 19;
                case "3.V.M":
                    return 20;
                case "3.V.T":
                    return 21;
                case "3.V.C":
                    return 22;
                case "3.V.A":
                    return 23;
                case "3.V.R":
                    return 24;
                case "3.E.ST":
                    return 26;
                case "3.E.W":
                    return 27;
                case "3.SK.C":
                    return 29;
                case "3.SK.M":
                    return 30;
                case "3.SK.L":
                    return 31;
                case "3.SK.CH":
                    return 32;
                case "3.SK.T":
                    return 33;
                case "3.ME.O.A":
                    return 36;
                case "3.ME.O.M":
                    return 37;
                case "3.ME.O.C":
                    return 38;
                case "3.ME.I.A":
                    return 40;
                case "3.ME.I.M":
                    return 41;
                case "3.ME.I.C":
                    return 42;
                case "3.ME.IO.A":
                    return 44;
                case "3.ME.IO.C":
                    return 45;
                case "3.ME.IO.M":
                    return 46;
                case "3.BA.SH.T":
                    return 49;
                case "3.BA.SH.B":
                    return 50;
                case "3.BA.SH.S":
                    return 51;
                case "3.BA.B.P":
                    return 53;
                case "3.BA.B.B":
                    return 54;
                case "3.BA.B.S":
                    return 55;
                case "3.BA.B.M":
                    return 56;
                case "3.BA.O.B":
                    return 58;
                case "3.BA.O.M":
                    return 59;
                case "3.BA.O.D":
                    return 60;
                case "3.O.T.P":
                    return 63;
                case "3.O.T.A":
                    return 64;
                case "3.O.D.S":
                    return 66;
                case "3.O.D.M":
                    return 67;
                case "3.MA.E":
                    return 69;
                case "3.MA.SA":
                    return 70;
                case "3.SU.V":
                    return 72;

                default:
                    return 0;
            }
        }
    }
}
