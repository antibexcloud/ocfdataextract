﻿namespace ExtractHcaiData.Uo.Work
{
    public enum TransferTypeEnum
    {
        All = 0,


        BusinessCenter = 1,

        Providers = 2,

        Insurers = 3,

        Activities = 4,

        Clients = 5,


        Ocf18 = 10,

        Ocf23 = 11,

        Ocf21B = 12,

        Ocf21C = 13,

        Form1 = 14,

        Acsi = 15,


        Touchups = 20,
    }
}
