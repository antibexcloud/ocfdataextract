﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskOcf18 : AbstractTask
    {
        public TaskOcf18(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TRANSFERRING OCF18 HCAI DATA TO UO";

        public override void InWorking()
        {
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = SyncContext.OCF18Submit.Count();

            Transfer.InitReportId();

            var count = 0;
            foreach (var doc in SyncContext.OCF18Submit)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = doc.HCAI_Document_Number;

                try
                {
                    var paperWork = UoContext.PaperWorks
                        .FirstOrDefault(ocf => ocf.ReportType == "OCF18" && ocf.HCAIDocumentNumber == doc.HCAI_Document_Number);

                    if (!AddDocument(doc, paperWork, out var dopInfo, out var rollback))
                    {
                        if (rollback)
                        {
                            Logger.Info($"skip OCF18 document '{doc.HCAI_Document_Number}': {dopInfo}");
                        }
                        continue;
                    }

                    Logger.Info($"{(paperWork is null ? "added" : "updated")} OCF18 document '{doc.HCAI_Document_Number}'" +
                                $"{(string.IsNullOrEmpty(dopInfo) ? "" : $": {dopInfo}")}.");
                    ++count;
                    if (count >= 20)
                    {
                        count = 0;
                        TargetDb.ReConnect();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add OCF18 document '{doc.HCAI_Document_Number}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }

        private bool AddDocument(OCF18Submit doc, PaperWork paperWork, out string dopInfo, out bool rollback)
        {
            rollback = false;
            dopInfo = string.Empty;
            var docAr = SyncContext.OCF18AR
                .Include("GS18LineItemAR")
                .FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
            if (docAr is null)
                throw new Exception($" OCF18 document {doc.HCAI_Document_Number} - no AR-information!");

            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, doc, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF18 document {doc.HCAI_Document_Number} - no information about client in UO!");
                return false;
            }

            var datePlan = doc.OCF18_ApplicantSignature_SigningDate ?? docAr.Adjuster_Response_Date;
            var docDate = Service.HcaiNum2Date(doc.HCAI_Document_Number);
            var days = (DateTime.Now.Date - docDate).TotalDays;
            var isActive = days <= 6*30;
            var repliedAs = ConvertHelper.GetRepliedAs(docAr.Document_Status);
            var isDeclined = repliedAs.Equals("Not Approved", InvariantCultureIgnoreCase)
                             || repliedAs.Equals("Declined", InvariantCultureIgnoreCase);

            var isExists = paperWork != null;
            DbContextTransaction transaction = null;
            if (isExists)
            {
                if (paperWork.CaseID != clientCase.CaseID)
                {
                    if (clientCase.client_id == paperWork.Client_Case.client_id)
                    {
                        clientCase = paperWork.Client_Case;
                    }
                    else
                    {
                        Logger.Info($"OCF18 document '{doc.HCAI_Document_Number}' has CaseID={paperWork.CaseID}, but {clientCase.CaseID} founded...");
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(repliedAs)) return false;

                var wasDeclined = !string.IsNullOrEmpty(paperWork.RepliedAs)
                                  && (paperWork.RepliedAs.Equals("Declined", InvariantCultureIgnoreCase)
                                      || paperWork.RepliedAs.Equals("Not Approved", InvariantCultureIgnoreCase));
                if (isDeclined && wasDeclined) return false;

                if (repliedAs.Equals(paperWork.RepliedAs, InvariantCultureIgnoreCase)
                    && (paperWork.ReplyDate is null || paperWork.ReplyDate == docAr.Adjuster_Response_Date.Date))
                    return false;
                dopInfo = $"from status '{paperWork.RepliedAs}' to status '{repliedAs}'";
                transaction = UoContext.Database.BeginTransaction(IsolationLevel.Unspecified);
            }

            //***** PaperWork *****
            var ppWork = paperWork ?? UoContext.PaperWorks.Add(new PaperWork
            {
                RecordCreationDate = datePlan,
                PlanType = "MVA",
                ReportType = "OCF18",
                Version = ConvertHelper.GetPlanVersion(datePlan),
                ReportID = TargetDb.ReportId++,

                HCAIDocumentNumber = doc.HCAI_Document_Number,
                CaseID = clientCase.CaseID,
                CreateDate = datePlan,
                SentDate = datePlan,

                ApplyToAlert = 0, //not sure
                PComments = "", //comments??
                //AccessDate = ,
            });

            // fill
            ppWork.EditDate = datePlan;
            ppWork.ReplyDate = docAr.Adjuster_Response_Date.Date;
            ppWork.ReportStatus = 2;
            ppWork.PlanStatus = (byte)(isActive ? 1 : 0);
            ppWork.RepliedAs = repliedAs;
            ppWork.ApprovedAmount = (double?)docAr.OCF18_InsurerTotals_AutoInsurerTotal_Approved;
            ppWork.DueDate = TargetDb.Connection.AddWorkDays(datePlan, 10, true);
            ppWork.DueTime = new DateTime(1990, 1, 1, 16, 0, 0);

            if (string.IsNullOrEmpty(ppWork.RepliedAs))
            {
                ppWork.ReportStatus = 1;
                ppWork.ReplyDate = null;
            }

            //***** OCF18 *****
            var ocfDoc = UoContext.OCF18.Include("OCFGoodsAndServices")
                             .FirstOrDefault(d => d.ReportID == ppWork.ReportID)
                ?? UoContext.OCF18.Add(new OCF18
                {
                    ReportID = ppWork.ReportID,
                    PlanNo = !string.IsNullOrEmpty(doc.OCF18_Header_PlanNumber)
                             && int.TryParse(doc.OCF18_Header_PlanNumber, out var planNum)
                        ? planNum
                        : 0
                });

            // fill
            ocfDoc.CIFirstName = doc.Applicant_Name_FirstName;
            ocfDoc.CILastName = doc.Applicant_Name_LastName;
            ocfDoc.CIMiddleName = doc.Applicant_Name_MiddleName;
            ocfDoc.CIGender = doc.Applicant_Gender.ToGender();
            ocfDoc.CIDOB = doc.Applicant_DateOfBirth;
            ocfDoc.CIAddress = $"{doc.Applicant_Address_StreetAddress1}" +
                               $"{(!string.IsNullOrEmpty(doc.Applicant_Address_StreetAddress2) ? " " + doc.Applicant_Address_StreetAddress2 : "")}";
            ocfDoc.CIProvince = ConvertHelper.FormatProvince(doc.Applicant_Address_Province);
            ocfDoc.CIPostalCode = ConvertHelper.FormatPostalCode(doc.Applicant_Address_PostalCode);
            ocfDoc.CICity = doc.Applicant_Address_City;

            ocfDoc.CITel = $"{ConvertHelper.PhoneFromHcai(doc.Applicant_TelephoneNumber)}" +
                           $"{(!string.IsNullOrEmpty(doc.Applicant_TelephoneExtension) ? "x" + doc.Applicant_TelephoneExtension : "")}";
            ocfDoc.CISignedBy = doc.OCF18_ApplicantSignature_SigningApplicant_FirstName;
            ocfDoc.CISignedDate = doc.OCF18_ApplicantSignature_SigningDate;
            ocfDoc.chkSignatureOnFile = (byte)(doc.OCF18_ApplicantSignature_IsApplicantSignatureOnFile ? 1 : 0);
            ocfDoc.CISignedByLastName = doc.OCF18_ApplicantSignature_SigningApplicant_LastName;
            ocfDoc.chkPSignedBySame = (byte)(!string.IsNullOrEmpty(doc.OCF18_ApplicantSignature_SigningApplicant_LastName) ? 1 : 0); //not sure
            ocfDoc.CIPolicyNo = doc.Header_PolicyNumber;
            ocfDoc.CIClaimNo = doc.Header_ClaimNumber;
            ocfDoc.CIDOA = doc.Header_DateOfAccident;

            ocfDoc.qOHIP = doc.OCF18_OtherInsurance_MOHAvailable.ToEnumParse<FlagWithNotApplicable>().ToUoString();
            ocfDoc.qEHC = doc.OCF18_OtherInsurance_IsThereOtherInsuranceCoverage == true ? "Yes" : "No";

            ocfDoc.EHC1InsName = doc.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name;
            ocfDoc.EHC1PlanMember =
                !string.IsNullOrEmpty(doc.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName)
                    ? $"{doc.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName}, {doc.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName}"
                    : string.Empty;
            ocfDoc.EHC1PolicyNo = doc.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
            ocfDoc.EHC1IDCertNo = doc.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID;

            ocfDoc.EHC2InsName = doc.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name;
            ocfDoc.EHC2PlanMember =
                !string.IsNullOrEmpty(doc.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName)
                    ? $"{doc.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName}, {doc.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName}"
                    : string.Empty;
            ocfDoc.EHC2PolicyNo = doc.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
            ocfDoc.EHC2IDCertNo = doc.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID;

            ocfDoc.MVAInsurerHCAIID = doc.Insurer_IBCInsurerID;
            ocfDoc.MVABranchHCAIID = doc.Insurer_IBCBranchID;

            ocfDoc.MVAAFirstName = doc.Insurer_Adjuster_Name_FirstName;
            ocfDoc.MVAALastName = doc.Insurer_Adjuster_Name_LastName;
            ocfDoc.MVAATel = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_TelephoneNumber);
            ocfDoc.MVAAExt = doc.Insurer_Adjuster_TelephoneExtension;
            ocfDoc.MVAAFax = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_FaxNumber);
            ocfDoc.MVAPHSame = (short)(doc.Insurer_PolicyHolder_IsSameAsApplicant ? 1 : 0);
            ocfDoc.MVAPHFirstName = doc.Insurer_PolicyHolder_Name_FirstName;
            ocfDoc.MVAPHLastName = doc.Insurer_PolicyHolder_Name_LastName;

            //to do add business info
            ocfDoc.SHPFacilityName = TargetDb.BusinessCenter.BusinessName;
            ocfDoc.SHPAISINo = TargetDb.BusinessCenter.FacilityAISINo;
            ocfDoc.SHPAddress = TargetDb.BusinessCenter.Address;
            ocfDoc.SHPCity = TargetDb.BusinessCenter.City;
            ocfDoc.SHPProvince = ConvertHelper.FormatProvince(TargetDb.BusinessCenter.Province);
            ocfDoc.SHPPostalCode = ConvertHelper.FormatPostalCode(TargetDb.BusinessCenter.PostalCode);
            ocfDoc.SHPTel = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.PNLine1);
            ocfDoc.SHPExt = "";
            ocfDoc.SHPFax = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.Fax);
            ocfDoc.SHPEmail = TargetDb.BusinessCenter.Email;

            ocfDoc.SHPOptConflicts =
                (short)(doc.OCF18_HealthPractitioner_ConflictOfInterest_ConflictExists == true ? 1 : 0);
            ocfDoc.SHPOptDeclare = 0;
            ocfDoc.SHPDeclareText = doc.OCF18_HealthPractitioner_ConflictOfInterest_ConflictDetails;

            ocfDoc.chkIsSHPOther = (short)(doc.OCF18_IsOtherHealthPractitioner == true ? 1 : 0);
            ocfDoc.NameOfAdjusterOnPDF = docAr.NameOfAdjusterOnPDF;
            
            // insurer
            var insurer = SourceDb.Insurers
                .Where(t => t.Backend.Insurer_ID == doc.Insurer_IBCInsurerID && t.UoInsurer != null)
                .Select(t => t.UoInsurer)
                .ToList()
                .FirstOrDefault(t => t.IsMva);
            if (insurer is null)
                insurer = TargetDb.Insurers.FirstOrDefault(t => t.Backend.InsurerHCAIID == doc.Insurer_IBCInsurerID);
            if (insurer != null)
            {
                var branch = insurer.Backend.ThirdPartyContacts
                    .FirstOrDefault(b => b.BranchHCAIID == doc.Insurer_IBCBranchID);

                //ocf23.MVATPID = insurer.Backend.ThirdPartyID;
                ocfDoc.MVAInsName = insurer.Backend.CompanyName;

                if (branch != null)
                {
                    ocfDoc.MVAAddress = branch.Address;
                    ocfDoc.MVACity = branch.City;
                    ocfDoc.MVAProvince = ConvertHelper.FormatProvince(branch.Province);
                    ocfDoc.MVAPostalCode = ConvertHelper.FormatPostalCode(branch.PostalCode);
                }
            }

            var hpProvider = OCFProviders.GetCprResource(Transfer,
                $"{doc.OCF18_HealthPractitioner_ProviderRegistryID}", doc.OCF18_HealthPractitioner_Occupation);
            if (hpProvider != null)
            {
                ocfDoc.staff_id = hpProvider.StaffId;
                ocfDoc.SHPSignedBy = $"{hpProvider.FirstName}, {hpProvider.LastName}";
                ocfDoc.SHPName = ocfDoc.SHPSignedBy;
                ocfDoc.SHPCollegeNo = hpProvider.Number;
                ocfDoc.SHPOccupation = hpProvider.StaffOccupation;
            }
            else
            {
                ocfDoc.SHPSignedBy = string.Empty;
                ocfDoc.staff_id = 0;
            }

            ocfDoc.SHPSignedDate = doc.OCF18_HealthPractitioner_DateSigned;
            ocfDoc.SHPHCAIFacilityID = $"{doc.OCF18_HealthPractitioner_FacilityRegistryID}";
            ocfDoc.SHPHCAIRegNo = $"{doc.OCF18_HealthPractitioner_ProviderRegistryID}";


            //************************************************
            
            //'''''''''
            ocfDoc.SRHPSame = (short)(doc.OCF18_IsRHPSameAsHealthPractitioner ? 1 : 0);
            if (ocfDoc.SRHPSame == 0)
            {
                ocfDoc.SRHPHCAIFacilityID = doc.OCF18_RegulatedHealthProfessional_FacilityRegistryID?.ToString();
                ocfDoc.SRHPFacilityName = ocfDoc.SHPFacilityName;
                ocfDoc.SRHPAISINo = ocfDoc.SHPAISINo;
                ocfDoc.SRHPAddress = ocfDoc.SHPAddress;
                ocfDoc.SRHPCity = ocfDoc.SHPCity;
                ocfDoc.SRHPProvince = ConvertHelper.FormatProvince(ocfDoc.SHPProvince);
                ocfDoc.SRHPPostalCode = ConvertHelper.FormatPostalCode(ocfDoc.SHPPostalCode);
                ocfDoc.SRHPTel = ConvertHelper.PhoneFromHcai(ocfDoc.SHPTel);
                ocfDoc.SRHPExt = ocfDoc.SHPExt;
                ocfDoc.SRHPFax = ConvertHelper.PhoneFromHcai(ocfDoc.SHPFax);
                ocfDoc.SRHPEmail = ocfDoc.SHPEmail;
                ocfDoc.SRHPOptConflicts = (short)(doc.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictExists == true ? 1 : 0);
                ocfDoc.SRHPOptDeclare = 0;
                ocfDoc.SRHPDeclareText = doc.OCF18_RegulatedHealthProfessional_ConflictOfInterest_ConflictDetails;
                ocfDoc.SRHPSignedDate = doc.OCF18_RegulatedHealthProfessional_DateSigned;
                ocfDoc.SRHPHCAIRegNo = doc.OCF18_RegulatedHealthProfessional_ProviderRegistryID?.ToString();

                //'''''''''''''''''
                var regProvider = OCFProviders.GetCprResource(Transfer,
                    $"{doc.OCF18_RegulatedHealthProfessional_ProviderRegistryID}", doc.OCF18_RegulatedHealthProfessional_Occupation);
                if (regProvider != null)
                {
                    ocfDoc.SRHPSignedBy = regProvider.FirstName + ", " + regProvider.LastName;
                    ocfDoc.r_staff_id = regProvider.StaffId;
                    //''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ocfDoc.SRHPName = regProvider.FirstName + ", " + regProvider.LastName;
                    if (ocfDoc.SRHPName.Trim() == ",") ocfDoc.SRHPName = "";

                    ocfDoc.SRHPCollegeNo = regProvider.Number;
                    ocfDoc.SRHPOccupation = regProvider.StaffOccupation;
                }
                else
                {
                    ocfDoc.SRHPSignedBy = "";
                    ocfDoc.r_staff_id = 0;
                    ocfDoc.SRHPCollegeNo = string.Empty;
                    ocfDoc.SRHPOccupation = string.Empty;
                }
            }

            //'''''''''''''
            ocfDoc.Part8A1Opt = doc.OCF18_PriorAndConcurrentConditions_PriorCondition_Response
                .ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.Part8A1Text = doc.OCF18_PriorAndConcurrentConditions_PriorCondition_Explanation;
            ocfDoc.Part8A2Opt = doc.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response
                .ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.Part8A2Text = doc.OCF18_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation;

            ocfDoc.Part8BOpt = doc.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Response
                .ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.Part8BText = doc.OCF18_PriorAndConcurrentConditions_ConcurrentCondition_Explanation;
            ocfDoc.Part8COpt = doc.OCF18_PriorAndConcurrentConditions_IsPAF_Response.ToEnumParse<FlagWithNotApplicable>().ToUoString();
            ocfDoc.Part8CText = doc.OCF18_PriorAndConcurrentConditions_IsPAF_Explanation;

            ocfDoc.Part9A1Opt = doc.OCF18_ActivityLimitations_ToEmployment_Response.ToEnumParse<FlagEmploymentEnum>().ToUoString();
            ocfDoc.Part9A2Opt = doc.OCF18_ActivityLimitations_ToNormalLife_Response.ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.Part9BText = doc.OCF18_ActivityLimitations_ImpactOnAbilities;

            ocfDoc.Part9COpt = doc.OCF18_ActivityLimitations_ModifiedEmployment_Response.ToEnumParse<FlagEmploymentEnum>().ToUoString();
            ocfDoc.Part9CText = doc.OCF18_ActivityLimitations_ModifiedEmployment_Explanation;

            ocfDoc.Part10AIChkPR = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_Goals_PainReduction == true ? 1 : 0);
            ocfDoc.Part10AIChkIRM = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreaseInStrength == true ? 1 : 0);
            ocfDoc.Part10AIChkIS = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_Goals_IncreasedRangeOfMotion == true ? 1 : 0);
            ocfDoc.Part10AIChkO = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_Goals_Other == true ? 1 : 0);
            ocfDoc.Part10AIOText = doc.OCF18_PlanGoalsOutcomesAndMethods_Goals_OtherDescription;
            ocfDoc.Part10AIIChkRANL = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToNormalLiving == true ? 1 : 0);
            ocfDoc.Part10AIIChkRMWA = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToModifiedWorkActivities == true ? 1 : 0);
            ocfDoc.Part10AIIChkRPWA = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_ReturnToPreAccidentWorkActivities == true ? 1 : 0);
            ocfDoc.Part10AIIChkO = (short)(doc.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_Other == true ? 1 : 0);
            ocfDoc.Part10AIIOText = doc.OCF18_PlanGoalsOutcomesAndMethods_FunctionalGoals_OtherDescription;
            ocfDoc.Part10BIText = doc.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_ProgressEvaluation;
            ocfDoc.Part10BIIText = doc.OCF18_PlanGoalsOutcomesAndMethods_Evaluation_PreviousPlanImpact;
            ocfDoc.Part10CIOpt = doc.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Response ? "Yes" : "No";
            ocfDoc.Part10CIText = doc.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Explanation;
            ocfDoc.Part10CIIOpt = doc.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Response ? "Yes" : "No";
            ocfDoc.Part10CIIText = doc.OCF18_PlanGoalsOutcomesAndMethods_BarriersToRecovery_Recommendations_Explanation;
            ocfDoc.Part10DOpt = doc.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Response ? "Yes" : "No";
            ocfDoc.Part10DText = doc.OCF18_PlanGoalsOutcomesAndMethods_ConcurrentTreatment_Explanation;
            ocfDoc.Part10EOpt = doc.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Response ? "Yes" : "No";
            ocfDoc.Part10EText = doc.OCF18_PlanGoalsOutcomesAndMethods_Consistency_Explanation;

            ocfDoc.Visits = doc.OCF18_ProposedGoodsAndServices_AlreadyProvidedTreatmentVisits?.ToString();
            ocfDoc.Weeks = doc.OCF18_ProposedGoodsAndServices_TreatmentPlanDuration.ToString();
            ocfDoc.Comments = doc.OCF18_ProposedGoodsAndServices_GoodsAndServicesComments;

            ocfDoc.Subtotal = (double) doc.OCF18_InsurerTotals_SubTotal_Proposed;
            ocfDoc.OHIPTotal = (double)doc.OCF18_InsurerTotals_MOH_Proposed;
            ocfDoc.EHC12Total = (double)doc.OCF18_InsurerTotals_OtherInsurers_Proposed;
            ocfDoc.GST = (double)doc.OCF18_InsurerTotals_GST_Proposed;
            ocfDoc.PST = (double)doc.OCF18_InsurerTotals_PST_Proposed;
            ocfDoc.Total = (double)doc.OCF18_InsurerTotals_AutoInsurerTotal_Proposed;

            ocfDoc.chkAttachments = (short)(doc.AttachmentsBeingSent ? 1 : 0);

            //''' approval data
            ocfDoc.ASubtotal = (double?)docAr.OCF18_InsurerTotals_SubTotal_Approved;
            ocfDoc.AOHIPTotal = (double?)docAr.OCF18_InsurerTotals_MOH_Approved_LineCost;
            ocfDoc.AEHC12Total = (double?)docAr.OCF18_InsurerTotals_OtherInsurers_Approved_LineCost;
            ocfDoc.AGST = (double?)docAr.OCF18_InsurerTotals_GST_Approved_LineCost;
            ocfDoc.APST = (double?)docAr.OCF18_InsurerTotals_PST_Approved_LineCost;
            ocfDoc.ATotal = (double?)docAr.OCF18_InsurerTotals_AutoInsurerTotal_Approved;
            //'' NOTE NEEDED SECTIONS -- old fields
            //            OCF18Docs(OCF18DocSize).oOCF18Data.ASubtotalCalculations = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.ASubtotalAdjResponse = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.AOHIPTotalCalculations = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.AOHIPTotalAdjResponse = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.AEHC12TotalCalculations = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.AEHC12TotalAdjResponse = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.AGSTCalculations = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.AGSTAdjResponse = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.APSTCalculations = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.APSTAdjResponse = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.ATotalCalculations 0 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.ATotalAdjResponse = "" 'TO DO - NOT SURE IF WE NEED IT
            //            '' not needed
            //            OCF18Docs(OCF18DocSize).oOCF18Data.chkPrintAnotherPlanNo = "" 'TO DO - NOT SURE IF WE NEED IT
            //            OCF18Docs(OCF18DocSize).oOCF18Data.txtPrintAnotherPlanNo = "" 'TO DO - NOT SURE IF WE NEED IT
            
            ocfDoc.MOHTotalApprovedReasonCode = docAr.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCode;
            ocfDoc.MOHTotalApprovedReasonCodeDesc = docAr.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_ReasonCodeDesc;
            ocfDoc.MOHTotalApprovedReasonCodeDescOther = docAr.OCF18_InsurerTotals_MOH_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            ocfDoc.AEHC12TotalApprovedReasonCode = docAr.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCode;
            ocfDoc.AEHC12TotalApprovedReasonCodeDesc = docAr.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_ReasonCodeDesc;
            ocfDoc.AEHC12TotalApprovedReasonCodeDescOther = docAr.OCF18_InsurerTotals_OtherInsurers_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

            ocfDoc.AGSTApprovedReasonCode = docAr.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
            ocfDoc.AGSTApprovedReasonCodeDesc = docAr.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            ocfDoc.AGSTApprovedReasonCodeDescOther = docAr.OCF18_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;
            ocfDoc.APSTApprovedReasonCode = docAr.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
            ocfDoc.APSTApprovedReasonCodeDesc = docAr.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
            ocfDoc.APSTApprovedReasonCodeDescOther = docAr.OCF18_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

            ocfDoc.ATotalCount = docAr.OCF18_InsurerTotals_TotalCount_Approved;
            ocfDoc.SigningAdjuster_FirstName = docAr.SigningAdjuster_FirstName;
            ocfDoc.SigningAdjuster_LastName = docAr.SigningAdjuster_LastName;
            ocfDoc.SigningAdjuster_Date = docAr.Adjuster_Response_Date;


            //************************************************
            //ocf23.PlanNo = "" 'not sure if we need it
            ocfDoc.OCF18AdditionalComments = doc.AdditionalComments;
            ocfDoc.chkWaivedByInsurer = (byte)(doc.OCF18_ApplicantSignature_IsApplicantSignatureWaivedByInsurer == true ? 1 : 0);
            ocfDoc.txtNoOfAttachments = "";

            ocfDoc.aAdjusterResponseExplanation = docAr.OCF18_ProposedGoodsAndServices_AdjusterResponseExplanation;
            ocfDoc.InsurerSignature_ApplicantSignatureWaived = docAr.OCF18_InsurerSignature_ApplicantSignatureWaived;

            //***** OCFGoodsAndServices *****
            var ocfProviders = new List<OCFProviders>();
            var iOrderCounter = 1;
            var orderedSource = !doc.GS18LineItemSubmit
                .Select(gs => int.TryParse(gs.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey, out var k) ? k : 0)
                .GroupBy(n => n)
                .Any(g => g.Count() > 1);
            var orderedOcf = isExists
                             && !ocfDoc.OCFGoodsAndServices
                                 .Select(gs => gs.ItemOrder ?? 0)
                                 .GroupBy(n => n)
                                 .Any(g => g.Count() > 1);
            var equalCounts = isExists && doc.GS18LineItemSubmit.Count == ocfDoc.OCFGoodsAndServices.Count;

            foreach (var gsLineItemSubmit in doc.GS18LineItemSubmit
                .OrderBy(gs => gs.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey))
            {
                var pmsKey = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey;
                var gsLineItemAr = docAr.GS18LineItemAR
                    .FirstOrDefault(gs => gs.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey == pmsKey);

                var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                    SourceDb.ActivityItems, gsLineItemSubmit, false);

                var ocfProvider = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                    $"{gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}",
                    gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation);

                var itemType = hcaiItem.IsProduct
                    ? "Product"
                    : hcaiItem.Type == "Block" ? "Service" : hcaiItem.Type;

                var itemMeasure = !string.IsNullOrEmpty(gsLineItemSubmit
                    .OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure)
                    ? gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Measure.ToLower()
                    : hcaiItem.Measure;

                var count = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_Count;
                var quantity = (double)gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Quantity;
                var rate = (double)gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_LineCost;
                var amount = (double)gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost;
                var gst = hcaiItem.GST > 0 ? hcaiItem.GST : GST;
                var taxId = !string.IsNullOrEmpty(hcaiItem.TaxId) ? hcaiItem.TaxId : "H";
                var itemCategory = hcaiItem.IsProduct ? "Product" : "Activity";
                var attribute = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Attribute;

                var itemBaseUnit = quantity;
                var itemOrder = !string.IsNullOrEmpty(pmsKey) && int.TryParse(pmsKey, out var order) ? order : 0;

                var ocfGs = isExists
                    ? itemOrder > 0 && orderedSource && orderedOcf
                        ? ocfDoc.OCFGoodsAndServices
                            .FirstOrDefault(t => t.ItemVCForShow == 0
                                                 && t.ItemOrder == itemOrder
                                                 //&& t.ItemID == hcaiItem.ItemId
                                                 && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                     || t.ItemServiceCode == hcaiItem.ServiceCode)
                                                 //&& t.ItemAttribute == attribute
                                                 && t.ItemBaseUnit == itemBaseUnit
                                                 && t.ItemRate == rate)
                        : ocfDoc.OCFGoodsAndServices
                            .FirstOrDefault(t => t.ItemVCForShow == 0
                                                 //&& t.ItemType == itemType
                                                 && t.ItemCategory == itemCategory
                                                 && t.ItemID == hcaiItem.ItemId
                                                 //&& t.ItemMeasure == itemMeasure
                                                 && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                     || t.ItemServiceCode == hcaiItem.ServiceCode)
                                                 //&& t.ItemAttribute == attribute
                                                 && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                     || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.Name)
                                                     || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName))
                                                 && t.ItemBaseUnit == itemBaseUnit
                                                 && t.ItemRate == rate)
                    : null;
                var isNewItem = ocfGs is null;
                if (isExists && isNewItem)
                {
                    dopInfo = $"{dopInfo}:{Environment.NewLine}" +
                              $"  No {nameof(OCFGoodsAndService)} {itemOrder}, {hcaiItem.ItemId}, {hcaiItem.RealServiceCode}, {itemBaseUnit}, {rate}!";
                    UoContext.CancelChanges(transaction);
                    rollback = true;
                    return false;
                }

                ocfGs ??= new OCFGoodsAndService
                {
                    ItemVCForShow = 0,
                    ReportID = ppWork.ReportID,
                    OCF18 = ocfDoc,

                    ItemType = itemType,
                    ItemCategory = itemCategory,

                    ItemID = hcaiItem.ItemId,
                    ItemMeasure = itemMeasure,

                    ItemServiceCode = hcaiItem.RealServiceCode /*gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code*/,
                    ItemAttribute = attribute,
                    ItemDescription = hcaiItem.ItemDescription,
                    ItemOccupation = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation
                                     ?? string.Empty,

                    ItemBaseUnit = itemBaseUnit,
                    ItemRate = rate,
                };

                // fill
                ocfGs.ItemQty = count;
                ocfGs.ItemAmount = amount;
                ocfGs.ItemTaxID = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST
                                  || gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST
                    ? taxId
                    : string.Empty;
                ocfGs.GST = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_GST
                    ? gst
                    : 0;
                ocfGs.PST = gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PST
                    ? hcaiItem.PST
                    : 0;

                if (ocfProvider != null)
                {
                    ocfGs.ItemOccupation = ocfProvider.ProviderOccupation;
                    ocfGs.ItemPR = ocfProvider.ItemPR;
                    ocfGs.ItemProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfGs.ItemProviderID = ocfProvider.ProviderID;
                }
                else
                    Logger.Info($"LOAD OCF18 LINE ITEMS ===> PROVIDER IS NOT FOUND ???? HCAI DOCUMENT NUMBER: {gsLineItemSubmit.HCAI_Document_Number}" +
                                $" HCAI REG NO:{gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}, " +
                                $" Profession: {gsLineItemSubmit.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_ProviderReference_Occupation}");

                if (gsLineItemAr != null)
                {
                    ocfGs.aAdjusterReasonCode = gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
                    ocfGs.aAdjusterReasonCodeDesc = gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
                    ocfGs.aAdjusterReasonOtherCodeDesc = gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                    ocfGs.aGST = gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_GST
                        ? gst
                        : 0.0;
                    ocfGs.aPST = gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PST
                        ? hcaiItem.PST
                        : 0.0;

                    ocfGs.aItemQty = gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_Count;
                    ocfGs.aItemRate = ((double)gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_LineCost).RoundTo();
                    ocfGs.aItemAmount = (double)gsLineItemAr.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_TotalLineCost;
                }

                if (isNewItem)
                {
                    itemOrder = itemOrder > 0 ? itemOrder : iOrderCounter++;
                    ocfGs.ItemOrder = itemOrder;
                    ocfDoc.OCFGoodsAndServices.Add(ocfGs);
                }
            }
            
            // paperWork
            ppWork.PlanGST = ocfDoc.GST;
            ppWork.PlanPST = ocfDoc.PST;
            ppWork.PlanSubtotal = ocfDoc.Subtotal;
            ppWork.PlanTotal = ocfDoc.Total;

            // ***** OCF fixes
            foreach (var ocfProvider in ocfProviders)
            {
                if (ocfProvider.ItemPR == ConvertHelper.Letters[0])
                {
                    ocfDoc.AType = ocfProvider.Type;
                    ocfDoc.ANumber = ocfProvider.Number;
                    ocfDoc.AHourRate = ocfProvider.HourRate;
                    ocfDoc.AOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.AProviderID = ocfProvider.ProviderID;
                    ocfDoc.AProviderName = ocfProvider.ProviderName;
                    ocfDoc.AProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.ARegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[1])
                {
                    ocfDoc.BType = ocfProvider.Type;
                    ocfDoc.BNumber = ocfProvider.Number;
                    ocfDoc.BHourRate = ocfProvider.HourRate;
                    ocfDoc.BOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.BProviderID = ocfProvider.ProviderID;
                    ocfDoc.BProviderName = ocfProvider.ProviderName;
                    ocfDoc.BProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.BRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[2])
                {
                    ocfDoc.CType = ocfProvider.Type;
                    ocfDoc.CNumber = ocfProvider.Number;
                    ocfDoc.CHourRate = ocfProvider.HourRate;
                    ocfDoc.COccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.CProviderID = ocfProvider.ProviderID;
                    ocfDoc.CProviderName = ocfProvider.ProviderName;
                    ocfDoc.CProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.CRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[3])
                {
                    ocfDoc.DType = ocfProvider.Type;
                    ocfDoc.DNumber = ocfProvider.Number;
                    ocfDoc.DHourRate = ocfProvider.HourRate;
                    ocfDoc.DOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.DProviderID = ocfProvider.ProviderID;
                    ocfDoc.DProviderName = ocfProvider.ProviderName;
                    ocfDoc.DProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.DRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[4])
                {
                    ocfDoc.EType = ocfProvider.Type;
                    ocfDoc.ENumber = ocfProvider.Number;
                    ocfDoc.EHourRate = ocfProvider.HourRate;
                    ocfDoc.EOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.EProviderID = ocfProvider.ProviderID;
                    ocfDoc.EProviderName = ocfProvider.ProviderName;
                    ocfDoc.EProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.ERegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[5])
                {
                    ocfDoc.Ftype = ocfProvider.Type;
                    ocfDoc.FNumber = ocfProvider.Number;
                    ocfDoc.FHourRate = ocfProvider.HourRate;
                    ocfDoc.FOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.FProviderID = ocfProvider.ProviderID;
                    ocfDoc.FProviderName = ocfProvider.ProviderName;
                    ocfDoc.FProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.FRegulated = ocfProvider.Regulated;
                }
            }

            // save
            UoContext.SaveChangesWithDetect(transaction);

            //***** OCFInjuries *****
            var iOrder = 1;
            foreach (var injury in doc.OCF18InjuryLineItemSubmit)
            {
                var codeDescription = TargetDb.InjuryCodes
                    .FirstOrDefault(c => c.Code == injury.OCF18_InjuriesAndSequelae_Injury_Code)
                    ?.CodeDescription ?? string.Empty;
                TargetDb.CheckInjuryCode(injury.OCF18_InjuriesAndSequelae_Injury_Code);
                TargetDb.Connection.SaveOcfInjuries(ppWork.ReportID, injury.OCF18_InjuriesAndSequelae_Injury_Code, codeDescription, iOrder++);
            }

            // StatusLog
            var isArchived = docAr.Archival_Status == "Yes";
            TargetDb.SaveHcaiLogStatus(ConvertHelper.CreateHcaiLog("OCF18", ppWork.ReportID, ppWork.HCAIDocumentNumber,
                isExists, datePlan, docAr.Submission_Date, docAr.Adjuster_Response_Date,
                ppWork.RepliedAs, isArchived, null));

            // ehc
            Transfer.CheckEhcCoverage(clientCase.CaseID, doc, ppWork.ReportID);

            // end
            return true;
        }
    }
}
