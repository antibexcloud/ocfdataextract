﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskOcf23 : AbstractTask
    {
        public TaskOcf23(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TRANSFERRING OCF23 HCAI DATA TO UO";

        public override void InWorking()
        {
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = SyncContext.OCF23Submit.Count();

            Transfer.InitReportId();

            var count = 0;
            foreach (var doc in SyncContext.OCF23Submit)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = doc.HCAI_Document_Number;

                try
                {
                    var paperWork = UoContext.PaperWorks
                        .FirstOrDefault(ocf => ocf.ReportType == "OCF23" && ocf.HCAIDocumentNumber == doc.HCAI_Document_Number);

                    if (!AddDocument(doc, paperWork, out var dopInfo, out var rollback))
                    {
                        if (rollback)
                        {
                            Logger.Info($"skip OCF23 document '{doc.HCAI_Document_Number}': {dopInfo}");
                        }
                        continue;
                    }

                    Logger.Info($"{(paperWork is null ? "added" : "updated")} OCF23 document '{doc.HCAI_Document_Number}'" +
                                $"{(string.IsNullOrEmpty(dopInfo) ? "" : $": {dopInfo}")}.");
                    ++count;
                    if (count >= 20)
                    {
                        count = 0;
                        TargetDb.ReConnect();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add OCF23 document '{doc.HCAI_Document_Number}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }

        private bool AddDocument(OCF23Submit doc, PaperWork paperWork, out string dopInfo, out bool rollback)
        {
            rollback = false;
            dopInfo = string.Empty;
            var docAr = SyncContext.OCF23AR
                .Include("OtherGSLineItemARs")
                .FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
            if (docAr is null)
                throw new Exception($" OCF23 document {doc.HCAI_Document_Number} - no AR-information!");

            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, doc, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF23 document {doc.HCAI_Document_Number} - no information about client in UO!");
                return false;
            }

            var datePlan = doc.OCF23_ApplicantSignature_SigningDate ?? docAr.Adjuster_Response_Date;
            var docDate = Service.HcaiNum2Date(doc.HCAI_Document_Number);
            var days = (DateTime.Now.Date - docDate).TotalDays;
            var isActive = days <= 6*30;

            var repliedAs = ConvertHelper.GetRepliedAs(docAr.Document_Status);
            var isDeclined = repliedAs.Equals("Not Approved", InvariantCultureIgnoreCase)
                             || repliedAs.Equals("Declined", InvariantCultureIgnoreCase);

            var isExists = paperWork != null;
            DbContextTransaction transaction = null;
            if (isExists)
            {
                if (paperWork.CaseID != clientCase.CaseID)
                {
                    if (clientCase.client_id == paperWork.Client_Case.client_id)
                    {
                        clientCase = paperWork.Client_Case;
                    }
                    else
                    {
                        Logger.Info($"OCF23 document '{doc.HCAI_Document_Number}' has CaseID={paperWork.CaseID}, but {clientCase.CaseID} founded...");
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(repliedAs)) return false;

                var wasDeclined = !string.IsNullOrEmpty(paperWork.RepliedAs)
                                  && (paperWork.RepliedAs.Equals("Declined", InvariantCultureIgnoreCase)
                                      || paperWork.RepliedAs.Equals("Not Approved", InvariantCultureIgnoreCase));
                if (isDeclined && wasDeclined) return false;

                if (repliedAs.Equals(paperWork.RepliedAs, InvariantCultureIgnoreCase)
                    && (paperWork.ReplyDate is null || paperWork.ReplyDate == docAr.Adjuster_Response_Date.Date))
                    return false;
                dopInfo = $"from status '{paperWork.RepliedAs}' to status '{repliedAs}'";
                transaction = UoContext.Database.BeginTransaction(IsolationLevel.Unspecified);
            }
            else
            {
                //if (repliedAs == "Not Approved")
                //{
                //    Logger.Info($"OCF23 plan '{doc.HCAI_Document_Number}' skipped: Not Approved.");
                //    return false;
                //}
            }

            //***** PaperWork *****
            var ppWork = paperWork ?? UoContext.PaperWorks.Add(new PaperWork
            {
                RecordCreationDate = datePlan,
                PlanType = "MVA",
                ReportType = "OCF23",
                Version = ConvertHelper.GetPlanVersion(datePlan),
                ReportID = TargetDb.ReportId++,

                HCAIDocumentNumber = doc.HCAI_Document_Number,
                CaseID = clientCase.CaseID,
                CreateDate = datePlan,
                SentDate = datePlan,

                ApplyToAlert = 0, //not sure
                PComments = "", //comments??
                //AccessDate = ,
            });

            // fill
            ppWork.EditDate = datePlan;
            ppWork.ReplyDate = docAr.Adjuster_Response_Date.Date;
            ppWork.ReportStatus = 2;
            ppWork.PlanStatus = (byte)(isActive ? 1 : 0);
            ppWork.RepliedAs = repliedAs;
            ppWork.ApprovedAmount = (double?)docAr.OCF23_InsurerTotals_AutoInsurerTotal_Approved;
            ppWork.DueDate = TargetDb.Connection.AddWorkDays(datePlan, 10, true);
            ppWork.DueTime = new DateTime(1990, 1, 1, 16, 0, 0);

            if (string.IsNullOrEmpty(ppWork.RepliedAs))
            {
                ppWork.ReportStatus = 1;
                ppWork.ReplyDate = null;
            }

            //***** OCF23 *****
            var ocfDoc = UoContext.OCF23.Include("OCFGoodsAndServices").FirstOrDefault(d => d.ReportID == ppWork.ReportID)
                 ?? UoContext.OCF23.Add(new OCF23 { ReportID = ppWork.ReportID, PlanNo = 0 });


            // fill
            ocfDoc.CIFirstName = doc.Applicant_Name_FirstName;
            ocfDoc.CILastName = doc.Applicant_Name_LastName;
            ocfDoc.CIMiddleName = doc.Applicant_Name_MiddleName;
            ocfDoc.CIGender = doc.Applicant_Gender.ToGender();
            ocfDoc.CIDOB = doc.Applicant_DateOfBirth;
            ocfDoc.CIAddress = $"{doc.Applicant_Address_StreetAddress1}" +
                               $"{(!string.IsNullOrEmpty(doc.Applicant_Address_StreetAddress2) ? " " + doc.Applicant_Address_StreetAddress2 : "")}";
            ocfDoc.CIProvince = ConvertHelper.FormatProvince(doc.Applicant_Address_Province);
            ocfDoc.CIPostalCode = ConvertHelper.FormatPostalCode(doc.Applicant_Address_PostalCode);
            ocfDoc.CICity = doc.Applicant_Address_City;

            ocfDoc.CITel = $"{ConvertHelper.PhoneFromHcai(doc.Applicant_TelephoneNumber)}" +
                           $"{(!string.IsNullOrEmpty(doc.Applicant_TelephoneExtension) ? "x" + doc.Applicant_TelephoneExtension : "")}";

            ocfDoc.CISignedBy = doc.OCF23_ApplicantSignature_SigningApplicant_FirstName;
            ocfDoc.CISignedDate = doc.OCF23_ApplicantSignature_SigningDate;
            ocfDoc.chkSignatureOnFile = (byte)(doc.OCF23_ApplicantSignature_IsApplicantSignatureOnFile ? 1 : 0);
            ocfDoc.CISignedByLastName = doc.OCF23_ApplicantSignature_SigningApplicant_LastName;
            ocfDoc.chkPSignedBySame = (byte)(!string.IsNullOrEmpty(doc.OCF23_ApplicantSignature_SigningApplicant_LastName) ? 1 : 0); //not sure
            ocfDoc.CIPolicyNo = doc.Header_PolicyNumber;
            ocfDoc.CIClaimNo = doc.Header_ClaimNumber;
            ocfDoc.CIDOA = doc.Header_DateOfAccident;

            ocfDoc.qOHIP = doc.OCF23_OtherInsurance_MOHAvailable.ToEnumParse<FlagWithNotApplicable>().ToUoString();
            ocfDoc.qEHC = doc.OCF23_OtherInsurance_IsThereOtherInsuranceCoverage.ToEnumParse<FlagWithNotApplicable>().ToUoString();

            ocfDoc.EHC1InsName = doc.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name;
            ocfDoc.EHC1PlanMember =
                !string.IsNullOrEmpty(doc.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName)
                    ? $"{doc.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName}, {doc.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName}"
                    : string.Empty;
            ocfDoc.EHC1PolicyNo = doc.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
            ocfDoc.EHC1IDCertNo = doc.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID;

            ocfDoc.EHC2InsName = doc.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name;
            ocfDoc.EHC2PlanMember =
                !string.IsNullOrEmpty(doc.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName)
                    ? $"{doc.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName}, {doc.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName}"
                    : string.Empty;
            ocfDoc.EHC2PolicyNo = doc.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
            ocfDoc.EHC2IDCertNo = doc.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID;

            ocfDoc.MVAInsurerHCAIID = doc.Insurer_IBCInsurerID;
            ocfDoc.MVABranchHCAIID = doc.Insurer_IBCBranchID;

            ocfDoc.MVAAFirstName = doc.Insurer_Adjuster_Name_FirstName;
            ocfDoc.MVAALastName = doc.Insurer_Adjuster_Name_LastName;
            ocfDoc.MVAATel = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_TelephoneNumber);
            ocfDoc.MVAAExt = doc.Insurer_Adjuster_TelephoneExtension;
            ocfDoc.MVAAFax = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_FaxNumber);
            ocfDoc.MVAPHSame = (short)(doc.Insurer_PolicyHolder_IsSameAsApplicant ? 1 : 0);
            ocfDoc.MVAPHFirstName = doc.Insurer_PolicyHolder_Name_FirstName;
            ocfDoc.MVAPHLastName = doc.Insurer_PolicyHolder_Name_LastName;

            //to do add business info
            ocfDoc.SHPFacilityName = TargetDb.BusinessCenter.BusinessName;
            ocfDoc.SHPAISINo = TargetDb.BusinessCenter.FacilityAISINo;
            ocfDoc.SHPAddress = TargetDb.BusinessCenter.Address;
            ocfDoc.SHPCity = TargetDb.BusinessCenter.City;
            ocfDoc.SHPProvince = ConvertHelper.FormatProvince(TargetDb.BusinessCenter.Province);
            ocfDoc.SHPPostalCode = ConvertHelper.FormatPostalCode(TargetDb.BusinessCenter.PostalCode);
            ocfDoc.SHPTel = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.PNLine1);
            ocfDoc.SHPExt = "";
            ocfDoc.SHPFax = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.Fax);
            ocfDoc.SHPEmail = TargetDb.BusinessCenter.Email;

            ocfDoc.SHPOptConflicts = (short)(doc.OCF23_HealthPractitioner_ConflictOfInterest_ConflictExists == true ? 1 : 0);
            ocfDoc.SHPOptDeclare = 0;
            ocfDoc.SHPDeclareText = doc.OCF23_HealthPractitioner_ConflictOfInterest_ConflictDetails;
            ocfDoc.NameOfAdjusterOnPDF = docAr.NameOfAdjusterOnPDF;

            // insurer
            var insurer = SourceDb.Insurers
                .Where(t => t.Backend.Insurer_ID == doc.Insurer_IBCInsurerID && t.UoInsurer != null)
                .Select(t => t.UoInsurer)
                .ToList()
                .FirstOrDefault(t => t.IsMva);
            if (insurer is null)
                insurer = TargetDb.Insurers.FirstOrDefault(t => t.Backend.InsurerHCAIID == doc.Insurer_IBCInsurerID);
            if (insurer != null)
            {
                var branch = insurer.Backend.ThirdPartyContacts
                    .FirstOrDefault(b => b.BranchHCAIID == doc.Insurer_IBCBranchID);

                //ocf23.MVATPID = insurer.Backend.ThirdPartyID;
                ocfDoc.MVAInsName = insurer.Backend.CompanyName;

                if (branch != null)
                {
                    ocfDoc.MVAAddress = branch.Address;
                    ocfDoc.MVACity = branch.City;
                    ocfDoc.MVAProvince = ConvertHelper.FormatProvince(branch.Province);
                    ocfDoc.MVAPostalCode = ConvertHelper.FormatPostalCode(branch.PostalCode);
                }
            }

            var hpProvider = OCFProviders.GetCprResource(Transfer, $"{doc.OCF23_HealthPractitioner_ProviderRegistryID}", doc.OCF23_HealthPractitioner_Occupation);
            if (hpProvider != null)
            {
                ocfDoc.staff_id = hpProvider.StaffId;
                ocfDoc.SHPSignedBy = $"{hpProvider.FirstName}, {hpProvider.LastName}";
                ocfDoc.SHPName = ocfDoc.SHPSignedBy;
                ocfDoc.SHPCollegeNo = hpProvider.Number;
                ocfDoc.SHPOccupation = hpProvider.StaffOccupation;
            }
            else
            {
                ocfDoc.SHPSignedBy = string.Empty;
                ocfDoc.staff_id = 0;
            }

            ocfDoc.SHPSignedDate = doc.OCF23_HealthPractitioner_DateSigned;
            ocfDoc.SHPHCAIFacilityID = $"{doc.OCF23_HealthPractitioner_FacilityRegistryID}";
            ocfDoc.SHPHCAIRegNo = $"{doc.OCF23_HealthPractitioner_ProviderRegistryID}";

            ocfDoc.chkNotFirstSHP = (byte) (doc.OCF23_HealthPractitioner_IsFirstInitiatingHealthPractitioner ? 1 : 0);

            //''''''''''''''''''''''''''''''''''''''''''
            ocfDoc.optPart7A = doc.OCF23_PriorAndConcurrentConditions_EmployedAtTimeOfAccident ? "Yes" : "No";
            ocfDoc.optPart7B = doc.OCF23_PriorAndConcurrentConditions_PriorCondition_Response
                .ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.Part7BText = doc.OCF23_PriorAndConcurrentConditions_PriorCondition_Explanation;
            ocfDoc.optPart7C = doc.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Response
                .ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.Part7CText = doc.OCF23_PriorAndConcurrentConditions_PriorCondition_InvestigationOrTreatment_Explanation;
            ocfDoc.optPart8A = doc.OCF23_BarriersToRecovery_Response ? "Yes" : "No";
            ocfDoc.Part8AText = doc.OCF23_BarriersToRecovery_Explanation;

            ocfDoc.chkPart8_DirectPaymentAssignmentResponse = (byte)(doc.OCF23_DirectPaymentAssignment_Response ? 1 : 0);

            //TO DO -- MIGHT NEED TO QUERY FOR THIS RESULTS
            ocfDoc.PAFItem1ID = doc.OCF23_PAFPreApproveServices_PAF_PAFType.SafetySubstring(50);
            ocfDoc.PAFItem1Description = doc.OCF23_PAFPreApproveServices_PAF_PAFType.SafetySubstring(200);
            ocfDoc.PAFItem1MaxFee = doc.OCF23_PAFPreApproveServices_PAF_EstimatedFee.ToString("F");
            ocfDoc.PAFItem1EstimatedFee = doc.OCF23_PAFPreApproveServices_PAF_EstimatedFee.ToString("F");

            ocfDoc.PAFItem2ID = doc.OCF23_PAFPreApproveServices_PAF_PAFType.SafetySubstring(50);
            ocfDoc.PAFItem2Description = doc.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_Description.SafetySubstring(200);
            ocfDoc.PAFItem2MaxFee = doc.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee?.ToString("F") ?? "0.00";
            ocfDoc.PAFItem2EstimatedFee = doc.OCF23_PAFPreApproveServices_SupplementaryGoodsAndServices_EstimatedFee?.ToString("F") ?? "0.00";

            ////' TO DO NOT NEEDED SECTION ---> INSTEAD RECORD PafLineItems
            ocfDoc.PAFItem3ID = "";
            ocfDoc.PAFItem3Description = "";
            ocfDoc.PAFItem3MaxFee = "";
            ocfDoc.PAFItem3EstimatedFee = "";

            //'
            ocfDoc.Comments = doc.OCF23_OtherGoodsAndServices_GoodsAndServicesComments;
            ocfDoc.SubtotalPAFE = doc.OCF23_InsurerTotals_SubTotalPreApproved_Proposed.ToString("F");
            ocfDoc.SubtotalSG = (double) doc.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Proposed;
            ocfDoc.Total = (double) doc.OCF23_InsurerTotals_AutoInsurerTotal_Proposed;
            //ocf23.PlanNo = "" 'not sure if we need it
            ocfDoc.chkAttachments = (short)(doc.AttachmentsBeingSent ? 1 : 0);

            ocfDoc.OCF23AdditionalComments = doc.AdditionalComments;
            ocfDoc.chkWaivedByInsurer = (byte)(doc.OCF23_ApplicantSignature_IsApplicantSignatureWaivedByInsurer ? 1 : 0);
            ocfDoc.txtNoOfAttachments = "";

            //ADJUSTER RESPONSE
            ocfDoc.ASubtotalSG = (double?) docAr.OCF23_InsurerTotals_SubTotalOtherGoodsAndServices_Approved;
            ocfDoc.ATotal = (double?) docAr.OCF23_InsurerTotals_AutoInsurerTotal_Approved;
            //ocf23.ASubtotalSGCalculations = rec_set("")
            //ocf23.ASubtotalSGAdjResponse = rec_set("")
            ocfDoc.ASubtotalPreApproved = (double?) docAr.OCF23_InsurerTotals_SubTotalPreApproved_Approved;

            ocfDoc.SigningAdjuster_Date = docAr.Adjuster_Response_Date;
            ocfDoc.SigningAdjuster_FirstName = docAr.SigningAdjuster_FirstName;
            ocfDoc.SigningAdjuster_LastName = docAr.SigningAdjuster_LastName;

            ocfDoc.InsurerSignature_ApplicantSignatureWaived = docAr.OCF23_InsurerSignature_ApplicantSignatureWaived;
            ocfDoc.InsurerSignature_ResponseForOtherGoodsAndServices = docAr.OCF23_InsurerSignature_ResponseForOtherGoodsAndServices;
            ocfDoc.InsurerSignature_PolicyInForceConfirmation = docAr.OCF23_InsurerSignature_PolicyInForceConfirmation;

            //***** PAFItems_Xray (blocks) *****
            var pCounter = 20;
            var iOrderPAF = 1;
            foreach (var pafLineItemSubmit in doc.PAFLineItemSubmits)
            {
                var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                    SourceDb.ActivityItems, pafLineItemSubmit, false);

                var rate = (double)pafLineItemSubmit.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee;
                var attribute = pafLineItemSubmit.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Attribute;
                var is3SC10 = hcaiItem.RealServiceCode == "3SC10";

                var ocfGs = isExists
                    ? ocfDoc.OCFGoodsAndServices
                        .FirstOrDefault(t => t.ItemVCForShow == 2 && t.ItemOrder == pCounter)
                    : null;
                var isNewItem = ocfGs is null;
                if (isExists && isNewItem)
                {
                    dopInfo = $"{dopInfo}{Environment.NewLine}" +
                              $"  No {nameof(OCFGoodsAndService)} {pCounter}!";
                    rollback = true;
                    UoContext.CancelChanges(transaction);
                    return false;
                }

                ocfGs ??= new OCFGoodsAndService
                {
                    ItemVCForShow = 2,
                    ReportID = ppWork.ReportID,
                    OCF23 = ocfDoc,

                    BlockDuration = hcaiItem.ItemDescription.Contains("weeks") ? 4 : 0,

                    ItemOrder = iOrderPAF++,
                    ItemID = hcaiItem.ItemId,
                    ItemType = "Block",
                    ItemCategory = "Activity",

                    ItemServiceCode = hcaiItem.RealServiceCode,
                    ItemAttribute = attribute,
                    ItemDescription = hcaiItem.ItemDescription,

                    ItemQty = 1,
                    ItemBaseUnit = 0,
                    GST = 0,
                    PST = 0,
                    aGST = 0,
                    aPST = 0,
                };

                if (isNewItem)
                    ocfDoc.OCFGoodsAndServices.Add(ocfGs);

                // fill
                ocfGs.ItemRate = rate;
                ocfGs.ItemAmount = rate;

                ocfGs.aItemQty = ocfGs.ItemQty;
                ocfGs.aItemRate = ocfGs.ItemQty > 0 ? (ocfGs.ItemRate / ocfGs.ItemQty).RoundTo() : ocfGs.ItemRate;
                ocfGs.aItemAmount = ocfGs.ItemAmount;

                // TO check
                if (isNewItem)
                {
                    if (is3SC10)
                    {
                        ocfGs.ItemID = ConvertHelper.Ocf23Xrays[pCounter % 20].xOrder; //numeric order of x-ray in the grid
                        ocfGs.ItemType = ConvertHelper.Ocf23Xrays[pCounter % 20].xViews[0]; //item view - short description
                        ocfGs.ItemDescription = ConvertHelper.Ocf23Xrays[pCounter % 20].xDescription; //item description
                        ocfGs.ItemOrder = (short)pCounter;

                        ++pCounter;
                    }
                    else
                        Logger.Info("WRONG PAF CODE? NOT CLEAR: " + ocfGs.ItemServiceCode);
                }
                else
                    ++pCounter;
            }

            //***** OCFGoodsAndServices (in_block-items) *****
            var iOrderCounter = 1;
            var ocfProviders = new List<OCFProviders>();

            foreach (var gsLineItemSubmit in doc.OtherGSLineItemSubmits
                .OrderBy(gs => gs.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey))
            {
                var gsLineItemAr = docAr.OtherGSLineItemARs
                    .FirstOrDefault(gs => gs.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey == gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey);

                var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                    SourceDb.ActivityItems, gsLineItemSubmit, false);

                var ocfProvider = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                    $"{gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}",
                    gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation, 4);

                var itemBaseUnit = (double)gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Quantity;
                var itemAmount = (double)gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Estimated_LineCost;
                var attribute = gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Attribute;

                var ocfGs = isExists
                    ? ocfDoc.OCFGoodsAndServices
                        .FirstOrDefault(t => t.ItemVCForShow == 0
                                             && t.ItemID == hcaiItem.ItemId
                                             && t.ItemServiceCode == hcaiItem.RealServiceCode
                                             //&& t.ItemAttribute == attribute
                                             && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName)))
                    : null;
                var isNewItem = ocfGs is null;
                if (isExists && isNewItem)
                {
                    dopInfo = $"{dopInfo}:{Environment.NewLine}" +
                              $"  No {nameof(OCFGoodsAndService)} {hcaiItem.ItemId}|{hcaiItem.RealServiceCode}|{hcaiItem.ItemDescription}!";
                    rollback = true;
                    UoContext.CancelChanges(transaction);
                    return false;
                }

                ocfGs ??= new OCFGoodsAndService
                {
                    ItemVCForShow = 0,
                    ReportID = ppWork.ReportID,
                    OCF23 = ocfDoc,

                    ItemID = hcaiItem.ItemId,
                    ItemAttribute = attribute,
                    ItemType = hcaiItem.IsProduct ? "Product" : hcaiItem.Type,
                    ItemCategory = hcaiItem.IsProduct ? "Product" : "Activity",

                    ItemServiceCode = hcaiItem.RealServiceCode /*gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Code*/,
                    ItemDescription = hcaiItem.ItemDescription /*gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Code*/,
                    ItemOrder = !string.IsNullOrEmpty(gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey)
                                && int.TryParse(gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Estimated_PMSGSKey, out var order)
                        ? order
                        : iOrderCounter++,
                    ItemOccupation = !string.IsNullOrEmpty(gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation)
                        ? gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation
                        : string.Empty,

                    ItemBaseUnit = itemBaseUnit,
                    ItemQty = 1,
                    ItemRate = itemBaseUnit != 0
                        ? (itemAmount / itemBaseUnit).RoundTo()
                        : itemAmount,
                    ItemAmount = itemAmount,

                    ItemTaxID = string.Empty,
                    GST = 0,
                    PST = 0,

                    ItemMeasure = !string.IsNullOrEmpty(gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Measure)
                        ? gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_Measure.ToLower()
                        : hcaiItem.Measure,
                };

                if (isNewItem)
                    ocfDoc.OCFGoodsAndServices.Add(ocfGs);

                if (ocfProvider != null)
                {
                    ocfGs.ItemOccupation = ocfProvider.ProviderOccupation;
                    ocfGs.ItemPR = ocfProvider.ItemPR;
                    ocfGs.ItemProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfGs.ItemProviderID = ocfProvider.ProviderID;
                }
                else
                    Logger.Info($"LOAD OCF23 LINE ITEMS ===> PROVIDER IS NOT FOUND ???? HCAI DOCUMENT NUMBER: {gsLineItemSubmit.HCAI_Document_Number}" +
                                $" HCAI REG NO: {gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}, " +
                                $" Proffession: {gsLineItemSubmit.OCF23_OtherGoodsAndServices_Items_Item_ProviderReference_Occupation}");

                if (gsLineItemAr != null)
                {
                    ocfGs.aAdjusterReasonCode = gsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
                    ocfGs.aAdjusterReasonCodeDesc = gsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
                    ocfGs.aAdjusterReasonOtherCodeDesc = gsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                    ocfGs.aGST = 0;
                    ocfGs.aPST = 0;

                    ocfGs.aItemAmount = (double)gsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost;
                    ocfGs.aItemQty = ocfGs.ItemQty;
                    ocfGs.aItemRate = (double)(gsLineItemAr.OCF23_OtherGoodsAndServices_Items_Item_Approved_LineCost /
                                               (ocfGs.ItemQty.HasValue && ocfGs.ItemQty.Value != 0
                                                    ? (ocfGs.ItemQty ?? 1) : 1));
                }
            }

            // paperWork
            ppWork.PlanSubtotal = (ocfDoc.SubtotalPAFM + ocfDoc.SubtotalSG).RoundTo();
            ppWork.PlanTotal = ocfDoc.Total;

            // ***** OCF fixes
            foreach (var ocfProvider in ocfProviders)
            {
                if (ocfProvider.ItemPR == ConvertHelper.Letters[0])
                {
                    ocfDoc.AType = ocfProvider.Type;
                    ocfDoc.ANumber = ocfProvider.Number;
                    ocfDoc.AHourRate = ocfProvider.HourRate;
                    ocfDoc.AOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.AProviderID = ocfProvider.ProviderID;
                    ocfDoc.AProviderName = ocfProvider.ProviderName;
                    ocfDoc.AProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.ARegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[1])
                {
                    ocfDoc.BType = ocfProvider.Type;
                    ocfDoc.BNumber = ocfProvider.Number;
                    ocfDoc.BHourRate = ocfProvider.HourRate;
                    ocfDoc.BOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.BProviderID = ocfProvider.ProviderID;
                    ocfDoc.BProviderName = ocfProvider.ProviderName;
                    ocfDoc.BProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.BRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[2])
                {
                    ocfDoc.CType = ocfProvider.Type;
                    ocfDoc.CNumber = ocfProvider.Number;
                    ocfDoc.CHourRate = ocfProvider.HourRate;
                    ocfDoc.COccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.CProviderID = ocfProvider.ProviderID;
                    ocfDoc.CProviderName = ocfProvider.ProviderName;
                    ocfDoc.CProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.CRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[3])
                {
                    ocfDoc.DType = ocfProvider.Type;
                    ocfDoc.DNumber = ocfProvider.Number;
                    ocfDoc.DHourRate = ocfProvider.HourRate;
                    ocfDoc.DOccupation = ocfProvider.ProviderOccupation;
                    ocfDoc.DProviderID = ocfProvider.ProviderID;
                    ocfDoc.DProviderName = ocfProvider.ProviderName;
                    ocfDoc.DProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfDoc.DRegulated = ocfProvider.Regulated;
                }
            }

            // save
            UoContext.SaveChangesWithDetect(transaction);

            //***** OCFInjuries *****
            var iOrder = 1;
            foreach (var injury in doc.OCF23InjuryLineItemSubmit)
            {
                var codeDescription = TargetDb.InjuryCodes
                    .FirstOrDefault(c => c.Code == injury.OCF23_InjuriesAndSequelae_Injury_Code)
                    ?.CodeDescription ?? string.Empty;
                TargetDb.CheckInjuryCode(injury.OCF23_InjuriesAndSequelae_Injury_Code);
                TargetDb.Connection.SaveOcfInjuries(ppWork.ReportID, injury.OCF23_InjuriesAndSequelae_Injury_Code, codeDescription, iOrder++);
            }

            // StatusLog
            var isArchived = docAr.Archival_Status == "Yes";
            TargetDb.SaveHcaiLogStatus(ConvertHelper.CreateHcaiLog("OCF23", ppWork.ReportID, ppWork.HCAIDocumentNumber,
                isExists, datePlan, docAr.Submission_Date, docAr.Adjuster_Response_Date,
                ppWork.RepliedAs, isArchived, null));

            // ehc
            Transfer.CheckEhcCoverage(clientCase.CaseID, doc, ppWork.ReportID);

            // end
            return true;
        }
    }
}
