﻿using System;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskOcf9 : AbstractTask
    {
        public TaskOcf9(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TRANSFERRING OCF9 HCAI DATA TO UO";

        public override void InWorking()
        {
            var forAdd = SyncContext.OCF9AR
                .ToList()
                .Where(d => !UoContext.OCF9.Any(ocf => ocf.PerantHCAIDocNumber == d.HCAI_Document_Number))
                .ToArray();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = forAdd.Length;

            Transfer.InitReportId();

            var count = 0;
            foreach (var doc in forAdd)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = $"{doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}]";

                try
                {
                    var form1 = SyncContext.AACNSubms.FirstOrDefault(d => d.DocumentNumber == doc.HCAI_Document_Number);
                    var ocf18 = SyncContext.OCF18Submit.FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
                    var ocf23 = SyncContext.OCF23Submit.FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
                    var ocf21B = SyncContext.OCF21BSubmit.FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
                    var ocf21C = SyncContext.OCF21CSubmit.FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
                    var ocfAcsi = SyncContext.ACSISubmits.FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);

                    var res = false;
                    if (form1 != null)
                        res = AddDocument(doc, form1);
                    else if (ocf18 != null)
                        res = AddDocument(doc, ocf18);
                    else if (ocf23 != null)
                        res = AddDocument(doc, ocf23);
                    else if (ocf21B != null)
                        res = AddDocument(doc, ocf21B);
                    else if (ocf21C != null)
                        res = AddDocument(doc, ocf21C);
                    else if (ocfAcsi != null)
                        res = AddDocument(doc, ocfAcsi);

                    if (!res) continue;
                    UoContext.SaveChangesWithDetect();
                    Logger.Info($"added OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}]");

                    ++count;
                    if (count >= 20)
                    {
                        count = 0;
                        TargetDb.ReConnect();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}]", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }

        private bool AddDocument(OCF9AR doc, AACNSubm ocf)
        {
            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, ocf, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}] - no information about client in UO!");
                return false;
            }

            var ocf9 = UoContext.OCF9.Add(new OCF9
            {
                PerantOCFID = long.Parse(doc.OCF9_HCAI_Document_Number),
                IsInvoice = false,
                PerantHCAIDocNumber = doc.HCAI_Document_Number,
                DateRevised = doc.OCF9_Header_DateRevised,

                AdditionalComments = doc.AdditionalComments,
                AttachmentsBeingSent = doc.AttachmentsBeingSent ? "Yes" : "No",
                OCFVersion = doc.OCFVersion,

                PolicyNumber = ocf.Header_PolicyNumber,
                ClaimNumber = ocf.Header_ClaimNumber,
                DOA = ocf.Header_DateOfAccident,

                ApplicantFirstName = ocf.Applicant_Name_FirstName,
                ApplicantLastName = ocf.Applicant_Name_LastName,
                ApplicantMiddleName = ocf.Applicant_Name_MiddleName,
                ApplicantDOB = ocf.Applicant_DateOfBirth,
                ApplicantGender = ocf.Applicant_Gender,
                ApplicantTelephoneNumber = ocf.Applicant_Telephone,
                ApplicantTelephoneNumberExt = ocf.Applicant_Extension,
                ApplicantStreetAddress1 = ocf.Applicant_Address_StreetAddress1,
                ApplicantStreetAddress2 = ocf.Applicant_Address_StreetAddress2,
                ApplicantCity = ocf.Applicant_Address_City,
                ApplicantProvince = ocf.Applicant_Address_Province,
                ApplicantPostalCode = ocf.Applicant_Address_PostalCode,
                ApplicantHomeTelephone = ocf.Applicant_Telephone,

                IBCInsurerID = ocf.Insurer_IBCInsurerID,
                IBCBranchID = ocf.Insurer_IBCBranchID,
            });

            var lines = SyncContext.GS9LineItemAR
                .Where(l => l.OCF9_HCAI_Document_Number == doc.OCF9_HCAI_Document_Number)
                .ToList();
            foreach (var gs9LineItemAr in lines)
            {
                continue;

                Ocf9LineInfo info = null;
                TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
            }

            return true;
        }

        private bool AddDocument(OCF9AR doc, OCF18Submit ocf)
        {
            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, ocf, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}] - no information about client in UO!");
                return false;
            }

            var ocf9 = UoContext.OCF9.Add(new OCF9
            {
                PerantOCFID = long.Parse(doc.OCF9_HCAI_Document_Number),
                IsInvoice = false,
                PerantHCAIDocNumber = doc.HCAI_Document_Number,
                DateRevised = doc.OCF9_Header_DateRevised,

                AdditionalComments = doc.AdditionalComments,
                AttachmentsBeingSent = doc.AttachmentsBeingSent ? "Yes" : "No",
                OCFVersion = doc.OCFVersion,

                PolicyNumber = ocf.Header_PolicyNumber,
                ClaimNumber = ocf.Header_ClaimNumber,
                DOA = ocf.Header_DateOfAccident,

                ApplicantFirstName = ocf.Applicant_Name_FirstName,
                ApplicantLastName = ocf.Applicant_Name_LastName,
                ApplicantMiddleName = ocf.Applicant_Name_MiddleName,
                ApplicantDOB = ocf.Applicant_DateOfBirth,
                ApplicantGender = ocf.Applicant_Gender,
                ApplicantTelephoneNumber = ocf.Applicant_TelephoneNumber,
                ApplicantTelephoneNumberExt = ocf.Applicant_TelephoneExtension,
                ApplicantStreetAddress1 = ocf.Applicant_Address_StreetAddress1,
                ApplicantStreetAddress2 = ocf.Applicant_Address_StreetAddress2,
                ApplicantCity = ocf.Applicant_Address_City,
                ApplicantProvince = ocf.Applicant_Address_Province,
                ApplicantPostalCode = ocf.Applicant_Address_PostalCode,
                ApplicantHomeTelephone = ocf.Applicant_TelephoneNumber,

                IBCInsurerID = ocf.Insurer_IBCInsurerID,
                IBCBranchID = ocf.Insurer_IBCBranchID,
            });

            var lines = SyncContext.GS9LineItemAR
                .Where(l => l.OCF9_HCAI_Document_Number == doc.OCF9_HCAI_Document_Number)
                .ToList();
            foreach (var gs9LineItemAr in lines)
            {
                var submitItem = SyncContext.GS18LineItemSubmit
                    .FirstOrDefault(s => s.HCAI_Document_Number == ocf.HCAI_Document_Number
                        && s.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Code == gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_Code);
                if (submitItem is null)
                {
                    continue;
                }

                var arItem = SyncContext.GS18LineItemAR
                    .FirstOrDefault(r => r.HCAI_Document_Number == ocf.HCAI_Document_Number
                        && r.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_PMSGSKey == submitItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                if (arItem is null)
                {
                    continue;
                }

                var info = new Ocf9LineInfo
                {
                    GSAmountClaimed = ((double)submitItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Estimated_TotalLineCost).RoundTo(),
                    GSServicessAmountPayable = 0,
                    GSDescription = submitItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Description,
                    GSReasonCode = arItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode,
                    GSReasonCodeDesc = arItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc,
                    GSReasonCodeDescOther = arItem.OCF18_ProposedGoodsAndServices_NonSessionGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc,
                };
                TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
            }

            return true;
        }

        private bool AddDocument(OCF9AR doc, OCF23Submit ocf)
        {
            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, ocf, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}] - no information about client in UO!");
                return false;
            }

            var ocf9 = UoContext.OCF9.Add(new OCF9
            {
                PerantOCFID = long.Parse(doc.OCF9_HCAI_Document_Number),
                IsInvoice = false,
                PerantHCAIDocNumber = doc.HCAI_Document_Number,
                DateRevised = doc.OCF9_Header_DateRevised,

                AdditionalComments = doc.AdditionalComments,
                AttachmentsBeingSent = doc.AttachmentsBeingSent ? "Yes" : "No",
                OCFVersion = doc.OCFVersion,

                PolicyNumber = ocf.Header_PolicyNumber,
                ClaimNumber = ocf.Header_ClaimNumber,
                DOA = ocf.Header_DateOfAccident,

                ApplicantFirstName = ocf.Applicant_Name_FirstName,
                ApplicantLastName = ocf.Applicant_Name_LastName,
                ApplicantMiddleName = ocf.Applicant_Name_MiddleName,
                ApplicantDOB = ocf.Applicant_DateOfBirth,
                ApplicantGender = ocf.Applicant_Gender,
                ApplicantTelephoneNumber = ocf.Applicant_TelephoneNumber,
                ApplicantTelephoneNumberExt = ocf.Applicant_TelephoneExtension,
                ApplicantStreetAddress1 = ocf.Applicant_Address_StreetAddress1,
                ApplicantStreetAddress2 = ocf.Applicant_Address_StreetAddress2,
                ApplicantCity = ocf.Applicant_Address_City,
                ApplicantProvince = ocf.Applicant_Address_Province,
                ApplicantPostalCode = ocf.Applicant_Address_PostalCode,
                ApplicantHomeTelephone = ocf.Applicant_TelephoneNumber,

                IBCInsurerID = ocf.Insurer_IBCInsurerID,
                IBCBranchID = ocf.Insurer_IBCBranchID,
            });

            var lines = SyncContext.GS9LineItemAR
                .Where(l => l.OCF9_HCAI_Document_Number == doc.OCF9_HCAI_Document_Number)
                .ToList();
            foreach (var gs9LineItemAr in lines)
            {
                var submitItem = SyncContext.PAFLineItemSubmits
                    .FirstOrDefault(s => s.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && s.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Code == gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_Code);
                if (submitItem is null)
                {
                    continue;
                }

                var arItem = SyncContext.OtherGSLineItemARs
                    .FirstOrDefault(r => r.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && r.OCF23_OtherGoodsAndServices_Items_Item_Approved_PMSGSKey == submitItem.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_PMSGSKey);
                if (arItem is null)
                {
                    continue;
                }

                var info = new Ocf9LineInfo
                {
                    GSAmountClaimed = (double)submitItem.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_EstimatedFee,
                    GSServicessAmountPayable = 0,
                    GSDescription = submitItem.OCF23_PAFPreApproveServices_OtherPreApprovedServices_Item_Description,
                    GSReasonCode = arItem.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode,
                    GSReasonCodeDesc = arItem.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc,
                    GSReasonCodeDescOther = arItem.OCF23_OtherGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc,
                };
                TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
            }

            return true;
        }

        private bool AddDocument(OCF9AR doc, OCF21BSubmit ocf)
        {
            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, ocf, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}] - no information about client in UO!");
                return false;
            }

            var ocf9 = UoContext.OCF9.Add(new OCF9
            {
                PerantOCFID = long.Parse(doc.OCF9_HCAI_Document_Number),
                IsInvoice = true,
                PerantHCAIDocNumber = doc.HCAI_Document_Number,
                DateRevised = doc.OCF9_Header_DateRevised,

                AdditionalComments = doc.AdditionalComments,
                AttachmentsBeingSent = doc.AttachmentsBeingSent ? "Yes" : "No",
                OCFVersion = doc.OCFVersion,

                PolicyNumber = ocf.Header_PolicyNumber,
                ClaimNumber = ocf.Header_ClaimNumber,
                DOA = ocf.Header_DateOfAccident,

                ApplicantFirstName = ocf.Applicant_Name_FirstName,
                ApplicantLastName = ocf.Applicant_Name_LastName,
                ApplicantMiddleName = ocf.Applicant_Name_MiddleName,
                ApplicantDOB = ocf.Applicant_DateOfBirth,
                ApplicantGender = ocf.Applicant_Gender,
                ApplicantTelephoneNumber = ocf.Applicant_TelephoneNumber,
                ApplicantTelephoneNumberExt = ocf.Applicant_TelephoneExtension,
                ApplicantStreetAddress1 = ocf.Applicant_Address_StreetAddress1,
                ApplicantStreetAddress2 = ocf.Applicant_Address_StreetAddress2,
                ApplicantCity = ocf.Applicant_Address_City,
                ApplicantProvince = ocf.Applicant_Address_Province,
                ApplicantPostalCode = ocf.Applicant_Address_PostalCode,
                ApplicantHomeTelephone = ocf.Applicant_TelephoneNumber,

                IBCInsurerID = ocf.Insurer_IBCInsurerID,
                IBCBranchID = ocf.Insurer_IBCBranchID,
            });

            var lines = SyncContext.GS9LineItemAR
                .Where(l => l.OCF9_HCAI_Document_Number == doc.OCF9_HCAI_Document_Number)
                .ToList();
            foreach (var gs9LineItemAr in lines)
            {
                var submitItem = SyncContext.GS21BLineItemSubmit
                    .FirstOrDefault(s => s.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && s.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code == gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_Code);
                if (submitItem is null)
                {
                    continue;
                }

                var arItem = SyncContext.GS21BLineItemAR
                    .FirstOrDefault(r => r.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && r.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                if (arItem is null)
                {
                    continue;
                }

                var info = new Ocf9LineInfo
                {
                    GSAmountClaimed = ((double)submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                                      * ((double)submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity),
                    GSServicessAmountPayable = 0,
                    GSDescription = submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description,
                    GSReasonCode = arItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode,
                    GSReasonCodeDesc = arItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc,
                    GSReasonCodeDescOther = arItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc,
                };
                TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
            }

            return true;
        }

        private bool AddDocument(OCF9AR doc, OCF21CSubmit ocf)
        {
            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, ocf, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}] - no information about client in UO!");
                return false;
            }

            var ocf9 = UoContext.OCF9.Add(new OCF9
            {
                PerantOCFID = long.Parse(doc.OCF9_HCAI_Document_Number),
                IsInvoice = true,
                PerantHCAIDocNumber = doc.HCAI_Document_Number,
                DateRevised = doc.OCF9_Header_DateRevised,

                AdditionalComments = doc.AdditionalComments,
                AttachmentsBeingSent = doc.AttachmentsBeingSent ? "Yes" : "No",
                OCFVersion = doc.OCFVersion,

                PolicyNumber = ocf.Header_PolicyNumber,
                ClaimNumber = ocf.Header_ClaimNumber,
                DOA = ocf.Header_DateOfAccident,

                ApplicantFirstName = ocf.Applicant_Name_FirstName,
                ApplicantLastName = ocf.Applicant_Name_LastName,
                ApplicantMiddleName = ocf.Applicant_Name_MiddleName,
                ApplicantDOB = ocf.Applicant_DateOfBirth,
                ApplicantGender = ocf.Applicant_Gender,
                ApplicantTelephoneNumber = ocf.Applicant_TelephoneNumber,
                ApplicantTelephoneNumberExt = ocf.Applicant_TelephoneExtension,
                ApplicantStreetAddress1 = ocf.Applicant_Address_StreetAddress1,
                ApplicantStreetAddress2 = ocf.Applicant_Address_StreetAddress2,
                ApplicantCity = ocf.Applicant_Address_City,
                ApplicantProvince = ocf.Applicant_Address_Province,
                ApplicantPostalCode = ocf.Applicant_Address_PostalCode,
                ApplicantHomeTelephone = ocf.Applicant_TelephoneNumber,

                IBCInsurerID = ocf.Insurer_IBCInsurerID,
                IBCBranchID = ocf.Insurer_IBCBranchID,
            });

            var lines = SyncContext.GS9LineItemAR
                .Where(l => l.OCF9_HCAI_Document_Number == doc.OCF9_HCAI_Document_Number)
                .ToList();
            foreach (var gs9LineItemAr in lines)
            {
                // I
                var submitOtherReimbursableItem = SyncContext.OtherReimbursableLineItemSubmits
                    .FirstOrDefault(s => s.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && s.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Code == gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_Code);
                if (submitOtherReimbursableItem != null)
                {
                    var pmsKey = submitOtherReimbursableItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey;
                    var arItem = SyncContext.OtherReimbursableLineItemARs
                        .FirstOrDefault(r => r.HCAI_Document_Number == ocf.HCAI_Document_Number
                                             && r.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == pmsKey);
                    if (arItem is null)
                    {
                        continue;
                    }

                    var info = new Ocf9LineInfo
                    {
                        GSAmountClaimed = ((double)submitOtherReimbursableItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                                          * ((double)submitOtherReimbursableItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity),
                        GSServicessAmountPayable = 0,
                        GSDescription = submitOtherReimbursableItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description,
                        GSReasonCode = arItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode,
                        GSReasonCodeDesc = arItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc,
                        GSReasonCodeDescOther = arItem.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc,
                    };
                    TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
                    continue;
                }

                // II
                var submitRenderedItem = SyncContext.PAFReimbursableLineItemSubmits
                    .FirstOrDefault(s => s.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && s.OCF21C_PAFReimbursableFees_Items_Item_Code == gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_Code);
                if (submitRenderedItem != null)
                {
                    var pmsKey = submitRenderedItem.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey;
                    var arItem = SyncContext.PAFReimbursableLineItemARs
                        .FirstOrDefault(r => r.HCAI_Document_Number == ocf.HCAI_Document_Number
                                             && r.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey == pmsKey);
                    if (arItem is null)
                    {
                        continue;
                    }

                    var info = new Ocf9LineInfo
                    {
                        GSAmountClaimed = (double)submitRenderedItem.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost,
                        GSServicessAmountPayable = 0,
                        GSDescription = "",
                        GSReasonCode = arItem.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode,
                        GSReasonCodeDesc = arItem.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc,
                        GSReasonCodeDescOther = arItem.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc,
                    };
                    TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
                    continue;
                }

                // III - ???
            }

            return true;
        }

        private bool AddDocument(OCF9AR doc, ACSISubmit ocf)
        {
            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, ocf, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF9 document {doc.OCF9_HCAI_Document_Number} [{doc.HCAI_Document_Number}] - no information about client in UO!");
                return false;
            }

            var ocf9 = UoContext.OCF9.Add(new OCF9
            {
                PerantOCFID = long.Parse(doc.OCF9_HCAI_Document_Number),
                IsInvoice = true,
                PerantHCAIDocNumber = doc.HCAI_Document_Number,
                DateRevised = doc.OCF9_Header_DateRevised,

                AdditionalComments = doc.AdditionalComments,
                AttachmentsBeingSent = doc.AttachmentsBeingSent ? "Yes" : "No",
                OCFVersion = doc.OCFVersion,

                PolicyNumber = ocf.Header_PolicyNumber,
                ClaimNumber = ocf.Header_ClaimNumber,
                DOA = ocf.Header_DateOfAccident,

                ApplicantFirstName = ocf.Applicant_Name_FirstName,
                ApplicantLastName = ocf.Applicant_Name_LastName,
                ApplicantMiddleName = ocf.Applicant_Name_MiddleName,
                ApplicantDOB = ocf.Applicant_DateOfBirth,
                ApplicantGender = ocf.Applicant_Gender,
                ApplicantTelephoneNumber = ocf.Applicant_TelephoneNumber,
                ApplicantTelephoneNumberExt = ocf.Applicant_TelephoneExtension,
                ApplicantStreetAddress1 = ocf.Applicant_Address_StreetAddress1,
                ApplicantStreetAddress2 = ocf.Applicant_Address_StreetAddress2,
                ApplicantCity = ocf.Applicant_Address_City,
                ApplicantProvince = ocf.Applicant_Address_Province,
                ApplicantPostalCode = ocf.Applicant_Address_PostalCode,
                ApplicantHomeTelephone = ocf.Applicant_TelephoneNumber,

                IBCInsurerID = ocf.Insurer_IBCInsurerID,
                IBCBranchID = ocf.Insurer_IBCBranchID,
            });

            var lines = SyncContext.GS9LineItemAR
                .Where(l => l.OCF9_HCAI_Document_Number == doc.OCF9_HCAI_Document_Number)
                .ToList();
            foreach (var gs9LineItemAr in lines)
            {
                var submitItem = SyncContext.GS21BLineItemSubmit
                    .FirstOrDefault(s => s.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && s.OCF21B_ReimbursableGoodsAndServices_Items_Item_Code == gs9LineItemAr.OCF9_GoodsAndServices_Items_Item_Code);
                if (submitItem is null)
                {
                    continue;
                }

                var arItem = SyncContext.GS21BLineItemAR
                    .FirstOrDefault(r => r.HCAI_Document_Number == ocf.HCAI_Document_Number
                                         && r.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);
                if (arItem is null)
                {
                    continue;
                }

                var info = new Ocf9LineInfo
                {
                    GSAmountClaimed = ((double)submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost)
                                      * ((double)submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Quantity),
                    GSServicessAmountPayable = 0,
                    GSDescription = submitItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Description,
                    GSReasonCode = arItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode,
                    GSReasonCodeDesc = arItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc,
                    GSReasonCodeDescOther = arItem.OCF21B_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc,
                };
                TargetDb.Connection.AddOcf9LineItem(ocf9, gs9LineItemAr, info);
            }

            return true;
        }
    }
}
