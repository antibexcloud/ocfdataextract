﻿namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskTouchupsPlanNo : AbstractTask
    {
        public TaskTouchupsPlanNo(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TouchUps - Plan Numbers";

        public override void InWorking()
        {
            TargetDb.Connection.RunMySqlScripts("ExtractHcaiData.Uo.Work.Sql.TouchUps.sql", GetType().Assembly);
        }
    }
}
