﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskTouchupsOcf23PlanNo : AbstractTask
    {
        public TaskTouchupsOcf23PlanNo(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TouchUps - OCF23 Plan Numbers";

        public override void InWorking()
        {
            var forWork = TargetDb.UoContext.PaperWorks
                .Where(p => p.ReportType == "OCF23")
                .OrderBy(p => p.CaseID).ThenBy(p => p.ReportID).ThenBy(p => p.SentDate)
                .ToArray();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = forWork.Length;

            var tSetPlanNo = new List<ToSetPlanNo>();
            short tPlanNo = 1;
            var lCaseId = string.Empty;
            var lReportId = 0.0;
            foreach (var paperWork in forWork)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;

                var reportId = paperWork.ReportID;
                var ocf23 = TargetDb.UoContext.OCF23.Find(reportId);
                if (ocf23 is null)
                {
                    continue;
                }

                var planNo = ocf23.PlanNo ?? 0;
                if (planNo > 0)
                {
                    var invoices = TargetDb.UoContext.MVAInvoices
                        .Where(m => m.ReportID == reportId && string.IsNullOrEmpty(m.PlanNumber))
                        .ToList();
                    if (invoices.Any())
                    {
                        foreach (var mvaInvoice in invoices)
                        {
                            mvaInvoice.PlanNumber = planNo.ToString();
                            mvaInvoice.PlanType = $"OCF23-{planNo}";
                        }
                        TargetDb.UoContext.SaveChangesWithDetect();
                    }

                    tPlanNo = (short)(planNo + 1);
                    continue;
                }

                if (paperWork.CaseID != lCaseId)
                {
                    tPlanNo = 1;
                    lCaseId = paperWork.CaseID;
                    lReportId = reportId;
                }
                else if (reportId != lReportId)
                {
                    tPlanNo++;
                    lReportId = reportId;
                }
                else
                    Logger.Info("SHOULD NOT BE HERE");

                VmMain.StepInfo = $"CaseID={lCaseId}, ReportID={lReportId}";
                tSetPlanNo.Add(new ToSetPlanNo
                {
                    CaseID = lCaseId,
                    ReportID = lReportId,
                    SentDate = paperWork.SentDate,
                    ReplyDate = paperWork.ReplyDate,
                    PlanNo = tPlanNo,
                });
            }

            // ****
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = tSetPlanNo.Count;

            foreach (var setPlanNo in tSetPlanNo)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = $"CaseID={setPlanNo.CaseID}, ReportID={setPlanNo.ReportID}," +
                                  $" PlanNo={setPlanNo.PlanNo}, Date={setPlanNo.SentDate?.ToString("d")}";
                Logger.Debug($"OCF23 Plan Number: CaseID={setPlanNo.CaseID}, ReportID={setPlanNo.ReportID}," +
                             $" PlanNo={setPlanNo.PlanNo}, Date={setPlanNo.SentDate?.ToString("d")}");

                try
                {
                    // 1
                    foreach (var ocf in TargetDb.UoContext.OCF23.Where(p => p.ReportID == setPlanNo.ReportID))
                        ocf.PlanNo = setPlanNo.PlanNo;

                    // 2
                    foreach (var mvaInvoice in TargetDb.UoContext.MVAInvoices
                                 .Where(m => m.ReportID == setPlanNo.ReportID))
                    {
                        mvaInvoice.PlanNumber = setPlanNo.PlanNo.ToString();
                        mvaInvoice.PlanType = $"OCF23-{setPlanNo.PlanNo}";
                    }
                    TargetDb.UoContext.SaveChangesWithDetect();
                }
                catch (Exception ex)
                {
                    Logger.Error($"OCF23 Set PlanNo '{setPlanNo.PlanNo}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            Transfer.CheckTargetsReadiness();
        }
    }
}
