﻿using System;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskTouchupsInjuries : AbstractTask
    {
        public TaskTouchupsInjuries(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "Touchups - OCF Case Injuries";

        public override void InWorking()
        {
            var allData = TargetDb.Connection.GetOcfPatientCaseInjuries();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = allData.Length;

            foreach (var caseInjury in allData)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = caseInjury.CodeDescription;

                try
                {
                    TargetDb.Connection.SetPatientCaseInjury(caseInjury.CaseId, caseInjury.Code,
                        caseInjury.CodeDescription, caseInjury.Order);
                }
                catch (Exception ex)
                {
                    Logger.Error($"add OCF Case Injury '{caseInjury.CodeDescription}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }
    }
}
