﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskForm1 : AbstractTask
    {
        public TaskForm1(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TRANSFERRING FORM1 HCAI DATA TO UO";

        public override void InWorking()
        {
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = SyncContext.AACNSubms.Count();

            var count = 0;
            foreach (var doc in SyncContext.AACNSubms)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = doc.DocumentNumber;

                try
                {
                    var paperWork = UoContext.PaperWorks
                        .FirstOrDefault(ocf => ocf.ReportType == "FORM1" && ocf.HCAIDocumentNumber == doc.DocumentNumber);

                    if (!AddDocument(doc, paperWork, out var dopInfo)) continue;

                    Logger.Info($"{(paperWork is null ? "added" : "updated")} FORM1 document '{doc.DocumentNumber}'" +
                                $"{(string.IsNullOrEmpty(dopInfo) ? "" : $": {dopInfo}")}.");
                    ++count;
                    if (count >= 20)
                    {
                        count = 0;
                        TargetDb.ReConnect();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add FORM1 document '{doc.DocumentNumber}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }

        private bool AddDocument(AACNSubm doc, PaperWork paperWork, out string dopInfo)
        {
            dopInfo = string.Empty;
            var docAr = SyncContext.AACNARs
                .Include("AACNPart4LineItemAR").Include("AACNServicesLineItemARs")
                .FirstOrDefault(d => d.DocumentNumber == doc.DocumentNumber);
            if (docAr is null)
                throw new Exception($" FORM1 document {doc.DocumentNumber} - no AR-information!");

            // client
            var clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, doc, false);
            if (clientCase is null)
            {
                Logger.Info($"Skipped Form1 document {doc.DocumentNumber} - no information about client in UO!");
                return false;
            }

            var datePlan = doc.Assessor_DateSigned;
            var docDate = Service.HcaiNum2Date(doc.DocumentNumber);
            var days = (DateTime.Now.Date - docDate).TotalDays;
            var isActive = days <= 6 * 30;

            var repliedAs = ConvertHelper.GetRepliedAs(docAr.DocumentState);
            var isDeclined = repliedAs.Equals("Not Approved", InvariantCultureIgnoreCase)
                             || repliedAs.Equals("Declined", InvariantCultureIgnoreCase);

            var isExists = paperWork != null;
            if (isExists)
            {
                if (paperWork.CaseID != clientCase.CaseID)
                {
                    if (clientCase.client_id == paperWork.Client_Case.client_id)
                    {
                        clientCase = paperWork.Client_Case;
                    }
                    else
                    {
                        Logger.Info($"FORM1 document '{doc.DocumentNumber}' has CaseID={paperWork.CaseID}, but {clientCase.CaseID} founded...");
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(repliedAs)) return false;

                var wasDeclined = !string.IsNullOrEmpty(paperWork.RepliedAs)
                                  && (paperWork.RepliedAs.Equals("Declined", InvariantCultureIgnoreCase)
                                      || paperWork.RepliedAs.Equals("Not Approved", InvariantCultureIgnoreCase));
                if (isDeclined && wasDeclined) return false;

                if (repliedAs.Equals(paperWork.RepliedAs, InvariantCultureIgnoreCase)
                    && (paperWork.ReplyDate is null || paperWork.ReplyDate == docAr.AdjusterResponseTime.Date))
                    return false;
                dopInfo = $"from status '{paperWork.RepliedAs}' to status '{repliedAs}'";
            }

            //***** PaperWork *****
            var ppWork = paperWork ?? UoContext.PaperWorks.Add(new PaperWork
            {
                RecordCreationDate = datePlan,
                PlanType = "MVA",
                ReportType = "Form1",
                Version = ConvertHelper.GetPlanVersion(datePlan),
                ReportID = UoContext.PaperWorks.Any()
                    ? UoContext.PaperWorks.Select(r => r.ReportID).Max() + 1
                    : 1,

                HCAIDocumentNumber = doc.DocumentNumber,
                Client_Case = clientCase,
                CaseID = clientCase.CaseID,

                CreateDate = datePlan,
                SentDate = docAr.SubmissionTime,

                ApplyToAlert = 0, //not sure
                PComments = "", //comments??
                AccessDate = doc.AssessmentDate,
            });

            // fill
            ppWork.EditDate = datePlan;
            ppWork.ReplyDate = docAr.AdjusterResponseTime.Date;
            ppWork.ReportStatus = 2;
            ppWork.PlanStatus = (byte)(isActive ? 1 : 0);
            ppWork.RepliedAs = repliedAs;
            if (string.IsNullOrEmpty(ppWork.RepliedAs))
            {
                ppWork.ReportStatus = 1;
                ppWork.ReplyDate = null;
            }
            ppWork.DueDate = TargetDb.Connection.AddWorkDays(datePlan, 10, true);
            ppWork.DueTime = new DateTime(1990, 1, 1, 16, 0, 0);

            //***** FORM1 *****
            var ocfDoc = UoContext.FORM1
                             .FirstOrDefault(d => d.ReportID == ppWork.ReportID)
                 ?? UoContext.FORM1.Add(new FORM1 { ReportID = ppWork.ReportID });

            // fill
            ocfDoc.MVABranchHCAIID = doc.Insurer_IBCBranchID;
            ocfDoc.MVAInsurerHCAIID = doc.Insurer_IBCInsurerID;

            ocfDoc.CIFirstName = doc.Applicant_Name_FirstName;
            ocfDoc.CILastName = doc.Applicant_Name_LastName;
            ocfDoc.CIMiddleName = doc.Applicant_Name_MiddleName;
            ocfDoc.CIGender = doc.Applicant_Gender.ToGender();
            ocfDoc.CIDOB = doc.Applicant_DateOfBirth;
            ocfDoc.CIAddress = $"{doc.Applicant_Address_StreetAddress1}" +
                               $"{(!string.IsNullOrEmpty(doc.Applicant_Address_StreetAddress2) ? " " + doc.Applicant_Address_StreetAddress2 : "")}";
            ocfDoc.CIProvince = ConvertHelper.FormatProvince(doc.Applicant_Address_Province);
            ocfDoc.CIPostalCode = ConvertHelper.FormatPostalCode(doc.Applicant_Address_PostalCode);
            ocfDoc.CICity = doc.Applicant_Address_City;

            ocfDoc.CITel = $"{ConvertHelper.PhoneFromHcai(doc.Applicant_Telephone)}" +
                           $"{(!string.IsNullOrEmpty(doc.Applicant_Extension) ? "x" + doc.Applicant_Extension : "")}";

            ocfDoc.CIPolicyNo = doc.Header_PolicyNumber;
            ocfDoc.CIClaimNo = doc.Header_ClaimNumber;
            ocfDoc.CIDOA = doc.Header_DateOfAccident;

            ocfDoc.AR_SigningAdjusterName_FirstName = doc.Insurer_PolicyHolder_Name_FirstName;
            ocfDoc.AR_SigningAdjusterName_LastName = doc.Insurer_PolicyHolder_Name_LastName;

            //to do add business info
            ocfDoc.SHPFacilityName = TargetDb.BusinessCenter.BusinessName;
            ocfDoc.SHPAISINo = TargetDb.BusinessCenter.FacilityAISINo;
            ocfDoc.SHPAddress = TargetDb.BusinessCenter.Address;
            ocfDoc.SHPCity = TargetDb.BusinessCenter.City;
            ocfDoc.SHPProvince = ConvertHelper.FormatProvince(TargetDb.BusinessCenter.Province);
            ocfDoc.SHPPostalCode = ConvertHelper.FormatPostalCode(TargetDb.BusinessCenter.PostalCode);
            ocfDoc.SHPTel = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.PNLine1);
            ocfDoc.SHPExt = "";
            ocfDoc.SHPFax = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.Fax);

            ocfDoc.AIAssDate = doc.AssessmentDate;
            ocfDoc.AILastAssDate = doc.LastAssessmentDate;
            ocfDoc.AIQuestion = doc.FirstAssessment.ToEnumParse<FlagWithUnknownEnum>().ToUoString();
            ocfDoc.AIMonthlyAllowance = doc.CurrentMonthlyAllowance?.ToString("F");

            ocfDoc.SHPHCAIFacilityID = doc.Assessor_FacilityRegistryID.ToString();
            ocfDoc.SHPHCAIRegNo = doc.Assessor_ProviderRegistryID.ToString();
            ocfDoc.SHPEmail = doc.Assessor_Email;
            ocfDoc.Assessor_IsSignatureOnFile = (byte)(doc.Assessor_IsSignatureOnFile ? 1 : 0);
            ocfDoc.SRHPSignedDate = doc.Assessor_DateSigned;

            ocfDoc.MVAAFirstName = doc.Insurer_PolicyHolder_Name_FirstName;
            ocfDoc.MVAALastName = doc.Insurer_PolicyHolder_Name_LastName;
            ocfDoc.MVAPHSame = (short)(doc.Insurer_PolicyHolder_IsSameAsApplicant ? 1 : 0);

            ocfDoc.AdditionalComments = doc.AdditionalComments;
            ocfDoc.AttachmentsBeingSent = (byte)(doc.AttachmentsBeingSent ? 1 : 0);
            ocfDoc.AttachmentCount = doc.AttachmentCount?.ToString();

            ocfDoc.optInsurerExamination = doc.InsurerExamination == true ? "Yes" : "No";
            ocfDoc.NameOfAdjusterOnPDF = docAr.NameOfAdjusterOnPDF;

            var hpProvider = OCFProviders.GetCprResource(Transfer, $"{doc.Assessor_ProviderRegistryID}", doc.Assessor_Occupation);
            if (hpProvider != null)
            {
                ocfDoc.staff_id = hpProvider.StaffId;
                ocfDoc.SHPName = hpProvider.ProviderName;
                ocfDoc.SHPCollegeNo = hpProvider.Number;
                ocfDoc.SHPOccupation = hpProvider.StaffOccupation;
                ocfDoc.SRHPSignedBy = $"{hpProvider.FirstName}, {hpProvider.LastName}";
            }

            foreach (var part4LineItemSubm in doc.AACNPart4LineItemSubm)
            {
                var arLine = docAr.AACNPart4LineItemAR
                    .FirstOrDefault(a => a.Costs_Approved_Part_ACSItemID == part4LineItemSubm.Costs_Assessed_Part_ACSItemID);

                var num = int.TryParse(part4LineItemSubm.Costs_Assessed_Part_ACSItemID, out var temp)? temp : 0;

                switch (num)
                {
                    case 1:
                        {
                            ocfDoc.Part4_AR_Approved_Part_HourlyRate1 = (double)part4LineItemSubm.Costs_Assessed_Part_HourlyRate;
                            ocfDoc.Part1HourlyRate = ocfDoc.Part4_AR_Approved_Part_HourlyRate1;

                            ocfDoc.Part4_AR_Approved_Part_WeeklyHours1 = (double?)arLine?.Costs_Approved_Part_WeeklyHours;
                            ocfDoc.Part4_AR_Approved_Part_MonthlyHours1 = (double?)arLine?.Costs_Approved_Part_MonthlyHours;
                            ocfDoc.Part4_AR_Approved_Part_Benefit1 = (double?)arLine?.Costs_Approved_Part_Benefit;
                            ocfDoc.Part4_AR_Approved_Part_ReasonCode1 = arLine?.Costs_Approved_Part_ReasonCode;
                            ocfDoc.Part4_AR_Approved_Part_ReasonDescription1 = arLine?.Costs_Approved_Part_ReasonDescription;
                        }
                        break;

                    case 2:
                        {
                            ocfDoc.Part4_AR_Approved_Part_HourlyRate2 = (double)part4LineItemSubm.Costs_Assessed_Part_HourlyRate;
                            ocfDoc.Part2HourlyRate = ocfDoc.Part4_AR_Approved_Part_HourlyRate2;

                            ocfDoc.Part4_AR_Approved_Part_WeeklyHours2 = (double?)arLine?.Costs_Approved_Part_WeeklyHours;
                            ocfDoc.Part4_AR_Approved_Part_MonthlyHours2 = (double?)arLine?.Costs_Approved_Part_MonthlyHours;
                            ocfDoc.Part4_AR_Approved_Part_Benefit2 = (double?)arLine?.Costs_Approved_Part_Benefit;
                            ocfDoc.Part4_AR_Approved_Part_ReasonCode2 = arLine?.Costs_Approved_Part_ReasonCode;
                            ocfDoc.Part4_AR_Approved_Part_ReasonDescription2 = arLine?.Costs_Approved_Part_ReasonDescription;
                        }
                        break;

                    case 3:
                        {
                            ocfDoc.Part4_AR_Approved_Part_HourlyRate3 = (double)part4LineItemSubm.Costs_Assessed_Part_HourlyRate;
                            ocfDoc.Part3HourlyRate = ocfDoc.Part4_AR_Approved_Part_HourlyRate3;

                            ocfDoc.Part4_AR_Approved_Part_WeeklyHours3 = (double?)arLine?.Costs_Approved_Part_WeeklyHours;
                            ocfDoc.Part4_AR_Approved_Part_MonthlyHours3 = (double?)arLine?.Costs_Approved_Part_MonthlyHours;
                            ocfDoc.Part4_AR_Approved_Part_Benefit3 = (double?)arLine?.Costs_Approved_Part_Benefit;
                            ocfDoc.Part4_AR_Approved_Part_ReasonCode3 = arLine?.Costs_Approved_Part_ReasonCode;
                            ocfDoc.Part4_AR_Approved_Part_ReasonDescription3 = arLine?.Costs_Approved_Part_ReasonDescription;
                        }
                        break;
                }
            }

            // approved
            ocfDoc.AR_SubmissionTime = docAr.SubmissionTime;
            ocfDoc.AR_InDispute = docAr.InDispute ? "Yes" : "No";
            ocfDoc.AR_AttachmentsReceivedDate = docAr.AttachmentsReceivedDate;
            ocfDoc.AR_SigningAdjusterName_FirstName = docAr.SigningAdjusterName_FirstName;
            ocfDoc.AR_SigningAdjusterName_LastName = docAr.SigningAdjusterName_LastName;
            ocfDoc.AR_AdjusterResponseTime = docAr.AdjusterResponseTime;
            ocfDoc.AR_ArchivalStatus = docAr.ArchivalStatus;

            ocfDoc.AR_Claimant_Name_FirstName = docAr.Claimant_Name_FirstName;
            ocfDoc.AR_Claimant_Name_MiddleName = docAr.Claimant_Name_MiddleName;
            ocfDoc.AR_Claimant_Name_LastName = docAr.Claimant_Name_LastName;
            ocfDoc.AR_Claimant_DateOfBirth = docAr.Claimant_DateOfBirth;
            ocfDoc.AR_Claimant_Gender = docAr.Claimant_Gender.ToGender();
            ocfDoc.AR_Claimant_Extension = docAr.Claimant_Extension;
            ocfDoc.AR_Claimant_Address_StreetAddress1 = docAr.Claimant_Address_StreetAddress1;
            ocfDoc.AR_Claimant_Address_StreetAddress2 = docAr.Claimant_Address_StreetAddress2;
            ocfDoc.AR_Claimant_Address_City = docAr.Claimant_Address_City;
            ocfDoc.AR_Claimant_Address_Province = ConvertHelper.FormatProvince(docAr.Claimant_Address_Province);
            ocfDoc.AR_Claimant_Address_PostalCode = ConvertHelper.FormatPostalCode(docAr.Claimant_Address_PostalCode);

            ocfDoc.AR_EOB_EOBAdditionalComments = docAr.EOB_EOBAdditionalComments;

            ocfDoc.AR_Costs_Approved_CalculatedBenefit = (double)docAr.Costs_Approved_CalculatedBenefit;
            ocfDoc.AR_Costs_Approved_Benefit = (double)docAr.Costs_Approved_Benefit;
            ocfDoc.AR_Costs_Approved_ReasonCode = docAr.Costs_Approved_ReasonCode;
            ocfDoc.AR_Costs_Approved_ReasonDescription = docAr.Costs_Approved_ReasonDescription;
            ocfDoc.AR_Costs_Approved_AdjusterResponseExplanation = docAr.Costs_Approved_AdjusterResponseExplanation;

            // *** FORM1_Parts123
            var parts123 = new List<FORM1_Parts123>();
            foreach (var lineItemSubm in doc.AACNServicesLineItemSubms)
            {
                var arLine = docAr.AACNServicesLineItemARs
                    .FirstOrDefault(a => a.AttendantCareServices_Item_ACSItemID == lineItemSubm.AttendantCareServices_Item_ACSItemID);

                var item = new FORM1_Parts123
                {
                    ReportID = ppWork.ReportID,
                    ACSItemID = lineItemSubm.AttendantCareServices_Item_ACSItemID,
                    NumberOfMin = (int)lineItemSubm.AttendantCareServices_Item_Assessed_Minutes,
                    TxWeeks = (int)lineItemSubm.AttendantCareServices_Item_Assessed_TimesPerWeek,
                };
                parts123.Add(item);

                item.gPartNo = !string.IsNullOrEmpty(lineItemSubm.AttendantCareServices_Item_ACSItemID)
                    ? byte.TryParse(lineItemSubm.AttendantCareServices_Item_ACSItemID[0].ToString(), out var partNo)
                        ? partNo
                        : (byte)0
                    : (byte)0;


                if (arLine != null)
                {
                    item.AttendantCareServices_Item_Approved_IsItemDeclined =
                        (arLine.AttendantCareServices_Item_Approved_IsItemDeclined is null
                            ? (byte?)null
                            : (byte?)(arLine.AttendantCareServices_Item_Approved_IsItemDeclined.Value ? 1 : 0));
                    item.AttendantCareServices_Item_Approved_Minutes = (double)arLine.AttendantCareServices_Item_Approved_Minutes;
                    item.AttendantCareServices_Item_Approved_TotalMinutes = (double)arLine.AttendantCareServices_Item_Approved_TotalMinutes;
                    item.AttendantCareServices_Item_Approved_TimesPerWeek = (double)arLine.AttendantCareServices_Item_Approved_TimesPerWeek;
                    item.AttendantCareServices_Item_Approved_ReasonCode = arLine.AttendantCareServices_Item_Approved_ReasonCode;
                    item.AttendantCareServices_Item_Approved_ReasonDescription = arLine.AttendantCareServices_Item_Approved_ReasonDescription;
                }

                item.gRowOrder = ServiceConvert.Form1Code2Row(lineItemSubm.AttendantCareServices_Item_ACSItemID);
            }

            // save
            UoContext.SaveChangesWithDetect();

            foreach (var form1Parts123 in parts123)
            {
                Transfer.TargetDb.Connection.SaveForm1Parts123(form1Parts123);
            }

            // StatusLog
            var isArchived = docAr.ArchivalStatus == "Yes";
            TargetDb.SaveHcaiLogStatus(ConvertHelper.CreateHcaiLog("FORM1", ppWork.ReportID, ppWork.HCAIDocumentNumber,
                isExists, doc.Assessor_DateSigned, docAr.SubmissionTime, docAr.AdjusterResponseTime,
                ppWork.RepliedAs, isArchived,
                ocfDoc.AR_Costs_Approved_ReasonDescription));
            return true;
        }
    }
}
