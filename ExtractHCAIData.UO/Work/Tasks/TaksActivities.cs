﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Sync.Items;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.Utils;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaksActivities : AbstractTask
    {
        public TaksActivities(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "Activities & Products";

        private List<string> _existsActivities;
        private List<string> _existsProducts;

        public override void InWorking()
        {
            // I
            var forAddActivity = SourceDb.ActivityItems
                .Where(p => !p.IsProduct && !p.IsUo && !p.IsInVirtual)
                .ToArray();

            var forAddProduct = SourceDb.ActivityItems
                .Where(p => p.IsProduct && !p.IsUo && !p.IsInVirtual)
                .ToArray();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = forAddActivity.Length + forAddProduct.Length;

            _existsActivities = UoContext.Activities.Select(a => a.activity_id).ToList();
            foreach (var syncItem in forAddActivity)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = syncItem.Name;

                var info = $"{(syncItem.IsVirtual ? " virtual" : "")} activity '{syncItem.ItemId}' | '{syncItem.ServiceCode}' | {syncItem.Units} {syncItem.Measure} | '{syncItem.Name}'";
                try
                {
                    //add to Target
                    syncItem.UoActivity = AddActivity(syncItem, info);
                    TargetDb.Activities.Add(syncItem.UoActivity);
                }
                catch (Exception ex)
                {
                    Logger.Error($"add{info}", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            // II
            _existsProducts = UoContext.Products.Select(p => p.product_id).ToList();
            foreach (var syncItem in forAddProduct)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = syncItem.Name;

                var info = $"{(syncItem.IsVirtual ? " virtual" : "")} product '{syncItem.ItemId}' | '{syncItem.ServiceCode}' | {syncItem.Units} {syncItem.Measure} | '{syncItem.Name}'";
                try
                {
                    //add to Target
                    syncItem.UoProduct = AddProduct(syncItem, info);
                    TargetDb.Products.Add(syncItem.UoProduct);
                }
                catch (Exception ex)
                {
                    Logger.Error($"add{info}", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            SourceDb.ReadyActivities = true;
        }

        private CprActivity AddActivity(SyncActivityItem syncItem, string info)
        {
            var activityId = syncItem.Backend.activity_id.Trim();
            if (_existsActivities.Any(a => a.Equals(activityId, InvariantCultureIgnoreCase)))
            {
                var item = UoContext.Activities.Find(activityId);
                return new CprActivity(item);
            }
            else
            {
                var item = CprActivity.MakeActivityFromHcaiItemType(Transfer, syncItem);
                // add
                UoContext.Activities.Add(item);
                UoContext.SaveChangesWithDetect();
                Logger.Info($"added{info}");

                return new CprActivity(item);
            }
        }

        private CprProduct AddProduct(SyncActivityItem syncItem, string info)
        {
            var productId = syncItem.Backend.activity_id.Trim();
            if (_existsProducts.Any(p => p.Equals(productId, InvariantCultureIgnoreCase)))
            {
                var item = UoContext.Products.Find(productId);
                return new CprProduct(item);
            }
            else
            {
                var item = CprProduct.MakeProductFromHcaiItemType(Transfer, syncItem);
                // add
                UoContext.Products.Add(item);
                UoContext.SaveChangesWithDetect();
                Logger.Info($"added{info}");

                return new CprProduct(item);
            }
        }
    }
}
