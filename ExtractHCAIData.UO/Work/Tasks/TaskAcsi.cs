﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Enums;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskAcsi : AbstractTask
    {
        public TaskAcsi(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TRANSFERRING ACSI HCAI DATA TO UO";

        public override void InWorking()
        {
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = SyncContext.ACSISubmits.Count();

            var count = 0;
            foreach (var doc in SyncContext.ACSISubmits)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = doc.HCAI_Document_Number;

                try
                {
                    var mvaInvoice = UoContext.MVAInvoices
                        .FirstOrDefault(ocf => ocf.HCAIDocumentNumber == doc.HCAI_Document_Number);

                    if (!AddDocument(doc, mvaInvoice, out var dopInfo, out var rollback))
                    {
                        if (rollback)
                        {
                            Logger.Info($"skip ACSI document '{doc.HCAI_Document_Number}': {dopInfo}");
                        }
                        continue;
                    }

                    Logger.Info($"{(mvaInvoice is null ? "added" : "updated")} ACSI document '{doc.HCAI_Document_Number}'" +
                                $"{(string.IsNullOrEmpty(dopInfo) ? "" : $": {dopInfo}")}.");
                    ++count;
                    if (count >= 20)
                    {
                        count = 0;
                        TargetDb.ReConnect();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add ACSI document '{doc.HCAI_Document_Number}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }

        private bool AddDocument(ACSISubmit doc, MVAInvoice mvaInvoice, out string dopInfo, out bool rollback)
        {
            rollback = false;
            dopInfo = string.Empty;
            var docAr = SyncContext.ACSIARs
                .Include("GSACSILineItemARs")
                .FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
            if (docAr is null)
                throw new Exception($" ACSI document {doc.HCAI_Document_Number} - no AR-information!");

            // find Report ID by plan number
            var refOfHcaiPlanNumber = !string.IsNullOrEmpty(doc.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                      && doc.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt"
                ? doc.ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber
                : string.Empty;
            var paperWork = !string.IsNullOrEmpty(refOfHcaiPlanNumber)
                ? UoContext.PaperWorks
                    .FirstOrDefault(p => p.ReportType == "FORM1" && p.HCAIDocumentNumber == refOfHcaiPlanNumber)
                : null;

            // client
            Client_Case clientCase;
            if (GroupByProviderSpecialty && paperWork != null)
            {
                clientCase = paperWork.Client_Case;
            }
            else
            {
                clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, doc, false);
            }

            if (clientCase is null)
            {
                Logger.Info($"Skipped ACSI document {doc.HCAI_Document_Number} - no information about client in UO!");
                return false;
            }

            var docDate = Service.HcaiNum2Date(doc.HCAI_Document_Number);
            var days = (DateTime.Now.Date - docDate).TotalDays;
            var isActive = days <= 6 * 30;
            var repliedAs = ConvertHelper.GetRepliedAs(docAr.Document_Status);
            var isDeclined = repliedAs.Equals("Not Approved", InvariantCultureIgnoreCase)
                             || repliedAs.Equals("Declined", InvariantCultureIgnoreCase);

            var iGeneral = mvaInvoice != null
                ? UoContext.InvoceGeneralInfoes.Find(mvaInvoice.InvoiceID)
                : null;

            var isExists = mvaInvoice != null;
            DbContextTransaction transaction = null;
            if (isExists)
            {
                if (iGeneral is null)
                    throw new Exception($"ACSI document '{doc.HCAI_Document_Number}' has no InvoceGeneralInfo!!!");
                if (iGeneral.CaseID != clientCase.CaseID)
                {
                    if (clientCase.client_id == iGeneral.Client_Case.client_id)
                    {
                        clientCase = iGeneral.Client_Case;
                    }
                    else
                    {
                        Logger.Info($"ACSI document '{doc.HCAI_Document_Number}' has CaseID={iGeneral.CaseID}, but {clientCase.CaseID} founded...");
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(repliedAs)) return false;

                var wasDeclined = !string.IsNullOrEmpty(iGeneral.RepliedAs)
                                  && (iGeneral.RepliedAs.Equals("Declined", InvariantCultureIgnoreCase)
                                      || iGeneral.RepliedAs.Equals("Not Approved", InvariantCultureIgnoreCase));
                if (isDeclined && wasDeclined) return false;

                if (repliedAs.Equals(iGeneral.RepliedAs, InvariantCultureIgnoreCase)
                    && (iGeneral.InvoiceReplyDate is null || iGeneral.InvoiceReplyDate.Value.Date == docAr.Adjuster_Response_Date.Date))
                    return false;
                dopInfo = $"from status '{iGeneral.RepliedAs}' to status '{repliedAs}'";
                transaction = UoContext.Database.BeginTransaction(IsolationLevel.Unspecified);
            }

            // invoice number
            var invoiceId = mvaInvoice?.InvoiceID ?? 0;
            if (invoiceId == 0)
            {
                invoiceId = int.TryParse(doc.ACSI_InvoiceInformation_InvoiceNumber, out var num) ? num : 0;
                invoiceId = TargetDb.GetInvoiceId(invoiceId);
            }

            // by plan number
            double? reportId = null;
            if (!string.IsNullOrEmpty(refOfHcaiPlanNumber))
            {
                if (paperWork != null)
                {
                    if (clientCase.CaseID == paperWork.CaseID)
                    {
                        reportId = paperWork.ReportID;
                    }
                    else
                    {
                        Logger.Info($"INVALID ACSI REFERENCE TO OCF18 PLAN FOUND FORM1 HCAI DOC#: {refOfHcaiPlanNumber}" +
                                    $" ACSI InvNo: {invoiceId} (diff CaseID: self='{clientCase.CaseID}', plan='{paperWork.CaseID}')");
                    }
                }
                else
                    Logger.Info($"INVALID ACSI REFERENCE TO FORM1 PLAN: PaperWork with HCAIDocumentNumber='{refOfHcaiPlanNumber}' not founded!" +
                                $" ACSI InvNo: {invoiceId}");
            }

            //***** InvoiceGeneralInfo *****
            iGeneral ??= UoContext.InvoceGeneralInfoes.Add(new InvoceGeneralInfo
            {
                RecordCreationDate = docAr.Submission_Date,
                InvoiceDate = docAr.Submission_Date,
                InvoiceID = invoiceId,
                InvoiceType = "MVA",
                Version = "ACSI",

                CaseID = clientCase.CaseID,
                Client_Case = clientCase,

                InvoiceGST = (double)doc.ACSI_InsurerTotals_GST_Proposed,
                InvoicePST = (double)doc.ACSI_InsurerTotals_PST_Proposed,

                // 2024-09-25
                InvoiceTotal = ((double)doc.ACSI_InsurerTotals_SubTotal_Proposed + (double)doc.ACSI_InsurerTotals_GST_Proposed)
                    .RoundTo(),
                //InvoiceTotal = (double)doc.ACSI_InsurerTotals_AutoInsurerTotal_Proposed,

                MVATotal = (double)doc.ACSI_InsurerTotals_AutoInsurerTotal_Proposed,

                Interest = (double)doc.ACSI_InsurerTotals_Interest_Proposed,

                ToPrint = 0, //do not mark as print -- not sure
                InvoiceStatus = 1, //for reply its also 1

                // not NULL fields
                AcknowledgementErrorCode = "Unknown",
                PaymentRequestUid = "Unknown",
                AdjudicationStatus = "Unknown",
                DeliveryStatus = "Unknown",
                AdjudicatorsInvoiceReferenceId = "Unknown",
                ConfirmationNumber = "Unknown",
            });

            // fill
            iGeneral.ReportID = reportId;
            iGeneral.InvoiceComments = doc.ACSI_InvoiceInformation_InvoiceNumber;
            iGeneral.RepliedAs = repliedAs;
            iGeneral.ApprovedAmt = (double?)docAr.ACSI_InsurerTotals_AutoInsurerTotal_Approved;
            iGeneral.InvoiceReplyDate = !string.IsNullOrEmpty(docAr.Document_Status)
                ? docAr.Adjuster_Response_Date
                : (DateTime?)null;

            // insurer
            var insurer = SourceDb.Insurers
                              .Where(t => t.Backend.Insurer_ID == doc.Insurer_IBCInsurerID && t.UoInsurer != null)
                              .Select(t => t.UoInsurer)
                              .ToList()
                              .FirstOrDefault(t => t.IsMva)
                          ?? TargetDb.Insurers.FirstOrDefault(t => t.Backend.InsurerHCAIID == doc.Insurer_IBCInsurerID);

            //***** MVAInvoice *****
            var iMvaInvoice = mvaInvoice ?? UoContext.MVAInvoices.Add(new MVAInvoice
            {
                InvoiceID = invoiceId,
                InvoiceVersion = "ACSI",

                HCAIDocumentNumber = doc.HCAI_Document_Number,

                staff_id = 0, //TO DO find staff id -- signature of staff id
                FINoConflicts = 0, //no conflicts
                FIDaclare = 0,
                FIDaclareText = "",

                // default values
                FacilityIsPayee = 1, //(short)(doc.ACSI_Payee_FacilityIsPayee ? 1 : 0),
                PrintInjuries = 1,
                PrintProviders = 1,
            });

            // fill
            iMvaInvoice.ReportID = reportId;
            iMvaInvoice.RefOfHCAI_PlanNumber = refOfHcaiPlanNumber;

            iMvaInvoice.FirstInvoice = (short)(doc.ACSI_InvoiceInformation_FirstInvoice ? 1 : 0);
            iMvaInvoice.LastInvoice = (short)(doc.ACSI_InvoiceInformation_LastInvoice ? 1 : 0);
            iMvaInvoice.PlanType = reportId != null
                ? doc.ACSI_PreviouslyApprovedGoodsAndServices_Type
                : "";
            iMvaInvoice.PlanDate = reportId != null
                ? doc.ACSI_PreviouslyApprovedGoodsAndServices_PlanDate
                : null;
            iMvaInvoice.PriorBalance = (double?)doc.ACSI_AccountActivity_PriorBalance;
            iMvaInvoice.PaymentReceived = (double?)doc.ACSI_AccountActivity_PaymentReceivedFromAutoInsurer;
            iMvaInvoice.OverdueAmount = (double?)doc.ACSI_AccountActivity_OverdueAmount;

            iMvaInvoice.MVAInsurerHCAIID = doc.Insurer_IBCInsurerID;
            iMvaInvoice.MVABranchHCAIID = doc.Insurer_IBCBranchID;

            //''' complete provider's info when loading items
            iMvaInvoice.CIFirstName = doc.Applicant_Name_FirstName;
            iMvaInvoice.CILastName = doc.Applicant_Name_LastName;
            iMvaInvoice.CIMiddleName = doc.Applicant_Name_MiddleName;
            iMvaInvoice.CIDOB = doc.Applicant_DateOfBirth;
            iMvaInvoice.CIGender = doc.Applicant_Gender.ToGender();
            iMvaInvoice.CIAddress = $"{doc.Applicant_Address_StreetAddress1}" +
                                    $"{(!string.IsNullOrEmpty(doc.Applicant_Address_StreetAddress2) ? " " + doc.Applicant_Address_StreetAddress2 : "")}";

            iMvaInvoice.CIProvince = ConvertHelper.FormatProvince(doc.Applicant_Address_Province);
            iMvaInvoice.CIPostalCode = ConvertHelper.FormatPostalCode(doc.Applicant_Address_PostalCode);
            iMvaInvoice.CICity = doc.Applicant_Address_City;
            iMvaInvoice.CITel = $"{ConvertHelper.PhoneFromHcai(doc.Applicant_TelephoneNumber)}" +
                                $"{(!string.IsNullOrEmpty(doc.Applicant_TelephoneExtension) ? "x" + doc.Applicant_TelephoneExtension : "")}";
            iMvaInvoice.CIExt = doc.Applicant_TelephoneExtension;
            iMvaInvoice.CIPolicyNo = doc.Header_PolicyNumber;
            iMvaInvoice.CIClaimNo = doc.Header_ClaimNumber;
            iMvaInvoice.CIDOL = doc.Header_DateOfAccident;

            iMvaInvoice.IIAdjFirstName = doc.Insurer_Adjuster_Name_FirstName;
            iMvaInvoice.IIAdjLastName = doc.Insurer_Adjuster_Name_LastName;
            iMvaInvoice.IIAdjTel = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_TelephoneNumber);
            iMvaInvoice.IIAdjExt = doc.Insurer_Adjuster_TelephoneExtension;
            iMvaInvoice.IIAdjFax = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_FaxNumber);
            iMvaInvoice.IIPHolderSame = (short)(doc.Insurer_PolicyHolder_IsSameAsApplicant ? 1 : 0);
            iMvaInvoice.IIPHolderLastName = doc.Insurer_PolicyHolder_Name_LastName;
            iMvaInvoice.IIPHolderFirstName = doc.Insurer_PolicyHolder_Name_FirstName;

            iMvaInvoice.qEHC = (short)(doc.ACSI_OtherInsurance_IsThereOtherInsuranceCoverage ? 1 : 0);
            // N/A - ???
            iMvaInvoice.qOHIP = (short)(doc.ACSI_OtherInsurance_MOHAvailable.ToEnumParse<FlagWithNotApplicable>() == FlagWithNotApplicable.Yes
                ? 1
                : 0);

            iMvaInvoice.EHC1 = doc.ACSI_OtherInsurance_OtherInsurers_Insurer1_Name;
            iMvaInvoice.EHC1PlanMember =
                !string.IsNullOrEmpty(doc.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName)
                    ? $"{doc.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName}, {doc.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName}"
                    : string.Empty;
            iMvaInvoice.EHC1PolicyNo = doc.ACSI_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
            iMvaInvoice.EHC1IDCertNo = doc.ACSI_OtherInsurance_OtherInsurers_Insurer1_ID;

            iMvaInvoice.EHC2 = doc.ACSI_OtherInsurance_OtherInsurers_Insurer2_Name;
            iMvaInvoice.EHC2PlanMember =
                !string.IsNullOrEmpty(doc.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName)
                    ? $"{doc.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName}, {doc.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName}"
                    : string.Empty;
            iMvaInvoice.EHC2PolicyNo = doc.ACSI_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
            iMvaInvoice.EHC2IDCertNo = doc.ACSI_OtherInsurance_OtherInsurers_Insurer2_ID;

            iMvaInvoice.Comments = doc.ACSI_OtherInformation;

            //business center info
            iMvaInvoice.Facility = TargetDb.BusinessCenter.BusinessName;
            iMvaInvoice.FIPayeeFirstName = TargetDb.BusinessCenter.CLastName;
            iMvaInvoice.FIPayeeLastName = TargetDb.BusinessCenter.CFirstName;
            iMvaInvoice.FIAddress = TargetDb.BusinessCenter.Address;
            iMvaInvoice.FICity = TargetDb.BusinessCenter.City;
            iMvaInvoice.FIProvince = ConvertHelper.FormatProvince(TargetDb.BusinessCenter.Province);
            iMvaInvoice.FIPostalCode = ConvertHelper.FormatPostalCode(TargetDb.BusinessCenter.PostalCode);
            iMvaInvoice.FITel = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.PNLine1);
            iMvaInvoice.FIExt = string.Empty;
            iMvaInvoice.FIFax = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.Fax);
            iMvaInvoice.FIAISINo = TargetDb.BusinessCenter.FacilityAISINo;
            iMvaInvoice.FIPayTo = doc.ACSI_Payee_MakeChequePayableTo;
            iMvaInvoice.FIEmail = TargetDb.BusinessCenter.Email;

            //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            iMvaInvoice.OHIPX = (double?)(doc.ACSI_OtherInsuranceAmounts_MOH_OtherService_Proposed ?? 0.0m);
            iMvaInvoice.EHC1X = (double?)(doc.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Proposed ?? 0.0m);
            iMvaInvoice.EHC2X = (double?)(doc.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Proposed ?? 0.0m);

            iMvaInvoice.XService = doc.ACSI_OtherInsuranceAmounts_OtherServiceType;
            iMvaInvoice.chkAttachments = (short)(doc.AttachmentsBeingSent ? 1 : 0);
            iMvaInvoice.AdditionalComments = doc.AdditionalComments;
            iMvaInvoice.ApprovedByOnPDF = docAr.ApprovedByOnPDF;

            iMvaInvoice.OHIP = iMvaInvoice.OHIPX;
            iMvaInvoice.EHC1And2 = iMvaInvoice.EHC1X + iMvaInvoice.EHC2X;

            if (insurer != null)
            {
                var branch = insurer.Backend.ThirdPartyContacts
                    .FirstOrDefault(b => b.BranchHCAIID == doc.Insurer_IBCBranchID);

                iMvaInvoice.IIName = insurer.Backend.CompanyName;

                if (branch != null)
                {
                    iMvaInvoice.IIAddress = branch.Address;
                    iMvaInvoice.IICity = branch.City;
                    iMvaInvoice.IIProvince = ConvertHelper.FormatProvince(branch.Province);
                    iMvaInvoice.IIPostalCode = ConvertHelper.FormatPostalCode(branch.PostalCode);
                }
            }

            //new fields
            iMvaInvoice.IsAmountRefused = (byte)(doc.ACSI_OtherInsuranceAmounts_IsAmountRefused == true ? 1 : 0);
            iMvaInvoice.OHIPX_Debit = (double?)doc.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Proposed;
            //ACSI_OtherInsuranceAmounts_MOHDebits_Total_Proposed - calculated by uo

            iMvaInvoice.EHC1X_Debit = (double?)doc.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Proposed;
            //ACSI_OtherInsuranceAmounts_Insurer1Debits_Total_Proposed - calculated by uo

            iMvaInvoice.EHC2X_Debit = (double?)doc.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Proposed;
            //ACSI_OtherInsuranceAmounts_Insurer2Debits_Total_Proposed - calculated by uo
            //ACSI_OtherInsuranceAmounts_Debits_OtherServiceType - xservice
            //ACSI_InsurerTotals_MOH_Proposed - calculate by uo

            //approval info
            if (!string.IsNullOrEmpty(iGeneral.RepliedAs))
            {
                iMvaInvoice.MOHTotalApprovedLineCost = (double)docAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_LineCost;
                iMvaInvoice.MOHTotalApprovedReasonCode = docAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.MOHTotalApprovedReasonCodeDesc = docAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.MOHTotalApprovedReasonCodeDescOther = docAr.ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.EHC1TotalApprovedLineCost = (double)docAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_LineCost;
                iMvaInvoice.EHC1TotalApprovedReasonCode = docAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.EHC1TotalApprovedReasonCodeDesc = docAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.EHC1TotalApprovedReasonCodeDescOther = docAr.ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.EHC2TotalApprovedLineCost = (double)docAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_LineCost;
                iMvaInvoice.EHC2TotalApprovedReasonCode = docAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.EHC2TotalApprovedReasonCodeDesc = docAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.EHC2TotalApprovedReasonCodeDescOther = docAr.ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.InsurerTotalMOHApproved = (double)docAr.ACSI_InsurerTotals_MOH_Approved;
                iMvaInvoice.InsurerTotalEHC12Approved = (double)docAr.ACSI_InsurerTotals_OtherInsurers_Approved;
                iMvaInvoice.AutoInsurerTotalApproved = (double)docAr.ACSI_InsurerTotals_AutoInsurerTotal_Approved;

                iMvaInvoice.GSTApprovedLineCost = (double)docAr.ACSI_InsurerTotals_GST_Approved_LineCost;
                iMvaInvoice.GSTApprovedReasonCode = docAr.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.GSTApprovedReasonCodeDesc = docAr.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.GSTApprovedReasonCodeDescOther = docAr.ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.PSTApprovedLineCost = (double)docAr.ACSI_InsurerTotals_PST_Approved_LineCost;
                iMvaInvoice.PSTApprovedReasonCode = docAr.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.PSTApprovedReasonCodeDesc = docAr.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.PSTApprovedReasonCodeDescOther = docAr.ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.MOHTotalApprovedLineCost_Debit = (double?)docAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_LineCost;
                iMvaInvoice.MOHTotalApprovedReasonCode_Debit = docAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.MOHTotalApprovedReasonCodeDesc_Debit = docAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.MOHTotalApprovedReasonCodeDescOther_Debit = docAr.ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.EHC1TotalApprovedLineCost_Debit = (double?)docAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_LineCost;
                iMvaInvoice.EHC1TotalApprovedReasonCode_Debit = docAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.EHC1TotalApprovedReasonCodeDesc_Debit = docAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.EHC1TotalApprovedReasonCodeDescOther_Debit = docAr.ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.EHC2TotalApprovedLineCost_Debit = (double?)docAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_LineCost;
                iMvaInvoice.EHC2TotalApprovedReasonCode_Debit = docAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.EHC2TotalApprovedReasonCodeDesc_Debit = docAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.EHC2TotalApprovedReasonCodeDescOther_Debit = docAr.ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.AOtherInsuranceAmounts_AdjusterResponseExplanation = docAr.ACSI_OtherInsuranceAmounts_AdjusterResponseExplanation;
                iMvaInvoice.AReimbursable_AdjusterResponseExplanation = docAr.ACSI_ReimbursableGoodsAndServices_AdjusterResponseExplanation;
                iMvaInvoice.AClaimFormReceived = docAr.ACSI_InsurerSignature_ClaimFormReceived == true ? "Yes" : "No";
                iMvaInvoice.AClaimFormReceivedDate = docAr.ACSI_InsurerSignature_ClaimFormReceivedDate?.ToString("yyyy-MM-dd");

                iMvaInvoice.SubtotalGSApproved = (double)docAr.ACSI_InsurerTotals_SubTotal_Approved;
                iMvaInvoice.TotalInteresedApproved = (double)docAr.ACSI_InsurerTotals_Interest_Approved_LineCost;
                iMvaInvoice.SubTotalOtherGS = 0; //for ocf21c only
            }

            var ocfProviders = new List<OCFProviders>();

            //***** InvoiceItemDetails *****
            var iOrderCounter = 1;
            var orderedSource = !doc.GSACSILineItemSubmits
                .Select(gs => int.TryParse(gs.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey, out var k) ? k : 0)
                .GroupBy(n => n)
                .Any(g => g.Count() > 1);
            var orderedOcf = isExists
                             && !iGeneral.InvoiceItemDetails
                                 .Select(gs => gs.ItemOrder ?? 0)
                                 .GroupBy(n => n)
                                 .Any(g => g.Count() > 1);
            var equalCounts = isExists && doc.GSACSILineItemSubmits.Count == iGeneral.InvoiceItemDetails.Count;

            foreach (var gsLineItemSubmit in doc.GSACSILineItemSubmits)
            {
                var keyValue = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey;
                var gsLineItemAr = docAr.GSACSILineItemARs
                    .FirstOrDefault(gs => gs.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey == keyValue);

                var itemOrder = !string.IsNullOrEmpty(keyValue) && int.TryParse(keyValue, out var key)
                    ? key
                    : 0;

                var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                    SourceDb.ActivityItems, gsLineItemSubmit, false);

                var ocfProvider = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                    $"{gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}",
                    gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation);

                var itemAmount = (double)gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost;
                var units = (double)gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Quantity;
                var gst = hcaiItem.GST > 0 ? hcaiItem.GST : GST;
                var taxId = !string.IsNullOrEmpty(hcaiItem.TaxId) ? hcaiItem.TaxId : "H";

                var itemType = hcaiItem.IsProduct
                    ? "Product"
                    : hcaiItem.Type == "Block" ? "Service" : hcaiItem.Type;
                var itemCategory = hcaiItem.IsProduct ? "Product" : "Activity";
                var attribute = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Attribute;
                var itemMeasure =
                    !string.IsNullOrEmpty(gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Measure)
                        ? gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Measure.ToLower()
                        : hcaiItem.Measure;

                var ocfGs = isExists
                    ? itemOrder > 0 && orderedSource && orderedOcf
                        ? iGeneral.InvoiceItemDetails
                            .FirstOrDefault(t => t.InvoiceID == invoiceId
                                                 && t.ItemVCForShow == 0
                                                 && t.ItemOrder == itemOrder
                                                 && t.ItemID == hcaiItem.ItemId
                                                 && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                     || t.ItemServiceCode == hcaiItem.ServiceCode)
                                                 //&& t.ItemAttribute == attribute
                                                 //&& (t.ItemMeasure == itemMeasure || t.ItemMeasure == hcaiItem.Measure)
                                                 && t.ItemBaseUnit == units
                                                 && t.ItemCategory == itemCategory
                                                 //&& t.ItemType == itemType
                                                 && t.ItemRate == itemAmount
                                                 && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                     || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.Name)
                                                     || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName)))
                        : iGeneral.InvoiceItemDetails
                            .FirstOrDefault(t => t.InvoiceID == invoiceId
                                                 && t.ItemVCForShow == 0
                                                 && t.ItemID == hcaiItem.ItemId
                                                 && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                     || t.ItemServiceCode == hcaiItem.ServiceCode)
                                                 //&& t.ItemAttribute == attribute
                                                 //&& (t.ItemMeasure == itemMeasure || t.ItemMeasure == hcaiItem.Measure)
                                                 && t.ItemBaseUnit == units
                                                 && t.ItemCategory == itemCategory
                                                 //&& t.ItemType == itemType
                                                 && t.ItemRate == itemAmount
                                                 && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                     || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.Name)
                                                     || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName)))
                    : null;

                var isNewItem = ocfGs is null;
                if (isExists && isNewItem)
                {
                    dopInfo = $"{dopInfo}{Environment.NewLine}" +
                              $"  No {nameof(InvoiceItemDetail)} {itemOrder}, {hcaiItem.ItemId}, {units}, {itemMeasure}!";
                    rollback = true;
                    UoContext.CancelChanges(transaction);
                    return false;
                }

                ocfGs ??= new InvoiceItemDetail
                {
                    ItemVCForShow = 0,
                    InvoceGeneralInfo = iGeneral,

                    ItemDate = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_DateOfService,

                    ItemID = hcaiItem.ItemId,
                    ItemAttribute = attribute,
                    ItemType = itemType,
                    ItemCategory = itemCategory,

                    ItemServiceCode = hcaiItem.RealServiceCode,
                    ItemDescription = hcaiItem.ItemDescription,
                    ItemOrder = itemOrder > 0 ? itemOrder : iOrderCounter++,

                    ItemMeasure = itemMeasure,

                    ItemBaseUnit = units,
                    ItemQty = 1,
                    ItemRate = itemAmount,

                    //not NULL fields
                    DeliveryLocationCode = "11",
                    PaymentRequestId = "Unknown",
                    DeliveryStatus = "Unknown",
                    AdjudicationStatus = "Unknown",
                    LineID = "Unknown",
                };
                if (isNewItem)
                    iGeneral.InvoiceItemDetails.Add(ocfGs);

                // fill
                ocfGs.ItemOccupation = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation;
                ocfGs.ItemProviderCode = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation;

                ocfGs.ItemAmount = itemAmount;
                ocfGs.ItemTaxID = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_GST
                                  || gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PST
                    ? taxId
                    : string.Empty;
                ocfGs.GST = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_GST
                    ? gst
                    : 0;
                ocfGs.PST = gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PST
                    ? hcaiItem.PST
                    : 0;

                if (ocfProvider != null)
                {
                    ocfGs.ItemPR = ocfProvider.ItemPR;
                    ocfGs.ItemOccupation = ocfProvider.ProviderOccupation;
                    ocfGs.ItemProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    ocfGs.ItemProviderID = ocfProvider.ProviderID;
                    ocfGs.ItemProviderCode = ocfProvider.Type;
                }
                else
                    Logger.Info($"LOAD ACSI LINE ITEMS ===> PROVIDER IS NOT FOUND ???? HCAI DOCUMENT NUMBER: {gsLineItemSubmit.HCAI_Document_Number}" +
                                $" HCAI REG NO: {gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}," +
                                $" Profession: {gsLineItemSubmit.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation}");

                if (gsLineItemAr != null && !string.IsNullOrEmpty(iGeneral.RepliedAs))
                {
                    ocfGs.aAdjusterReasonCode = gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
                    ocfGs.aAdjusterReasonCodeDesc = gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
                    ocfGs.aAdjusterReasonOtherCodeDesc = gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                    ocfGs.aGST = gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_GST
                        ? gst
                        : 0.0;
                    ocfGs.aPST = gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PST
                        ? hcaiItem.PST
                        : 0.0;

                    ocfGs.aItemQty = 1;
                    ocfGs.aItemRate = (double)gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost;
                    ocfGs.aItemAmount = (double)gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost;

                    if (!string.IsNullOrEmpty(gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey)
                        && int.TryParse(gsLineItemAr.ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey, out var order))
                    {
                        ocfGs.PMSGSKey = order;
                    }
                }
            }

            if (!isExists && doc.GSACSILineItemSubmits.Any())
                iGeneral.InvoiceDate = doc.GSACSILineItemSubmits.Select(s => s.ACSI_ReimbursableGoodsAndServices_Items_Item_DateOfService)
                    .Max();
            iGeneral.InvoiceSentDate = iGeneral.InvoiceDate;
            if (!isExists && iGeneral.InvoiceDate.HasValue)
                iGeneral.HCAIVersion = ConvertHelper.GetPlanVersion(iGeneral.InvoiceDate.Value);

            // ***** OCF fixes
            foreach (var ocfProvider in ocfProviders)
            {
                if (ocfProvider.ItemPR == ConvertHelper.Letters[0])
                {
                    iMvaInvoice.AType = ocfProvider.Type;
                    iMvaInvoice.ANumber = ocfProvider.Number;
                    iMvaInvoice.AHourRate = ocfProvider.HourRate;
                    iMvaInvoice.AOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.AProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.AProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.AProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.ARegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[1])
                {
                    iMvaInvoice.BType = ocfProvider.Type;
                    iMvaInvoice.BNumber = ocfProvider.Number;
                    iMvaInvoice.BHourRate = ocfProvider.HourRate;
                    iMvaInvoice.BOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.BProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.BProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.BProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.BRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[2])
                {
                    iMvaInvoice.CType = ocfProvider.Type;
                    iMvaInvoice.CNumber = ocfProvider.Number;
                    iMvaInvoice.CHourRate = ocfProvider.HourRate;
                    iMvaInvoice.COccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.CProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.CProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.CProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.CRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[3])
                {
                    iMvaInvoice.DType = ocfProvider.Type;
                    iMvaInvoice.DNumber = ocfProvider.Number;
                    iMvaInvoice.DHourRate = ocfProvider.HourRate;
                    iMvaInvoice.DOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.DProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.DProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.DProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.DRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[4])
                {
                    iMvaInvoice.EType = ocfProvider.Type;
                    iMvaInvoice.ENumber = ocfProvider.Number;
                    iMvaInvoice.EHourRate = ocfProvider.HourRate;
                    iMvaInvoice.EOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.EProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.EProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.EProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.ERegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[5])
                {
                    iMvaInvoice.Ftype = ocfProvider.Type;
                    iMvaInvoice.FNumber = ocfProvider.Number;
                    iMvaInvoice.FHourRate = ocfProvider.HourRate;
                    iMvaInvoice.FOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.FProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.FProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.FProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.FRegulated = ocfProvider.Regulated;
                }
            }

            // 2024-10-03
            //iGeneral.InvoiceTotal = iGeneral.InvoiceItemDetails.Sum(d => d.ItemAmount ?? 0).RoundTo();

            // save
            UoContext.SaveChangesWithDetect(transaction);

            // StatusLog
            var isArchived = docAr.Archival_Status == "Yes";
            TargetDb.SaveHcaiLogStatus(ConvertHelper.CreateHcaiLog("ACSI", invoiceId, doc.HCAI_Document_Number,
                isExists, iGeneral.InvoiceDate.Value, docAr.Submission_Date, docAr.Adjuster_Response_Date,
                iGeneral.RepliedAs, isArchived, null));

            // charges
            Transfer.AddInvoiceCharges(iGeneral);

            // payment
            var datePayment = iGeneral.InvoiceReplyDate ?? iGeneral.InvoiceDate;
            //var approved = UoContext.InvoiceItemDetails
            //    .Where(d => d.InvoiceID == invoiceId)
            //    .ToList()
            //    .Sum(d => (d.ItemAmount + d.aGST + d.aPST).RoundTo())
            //    .RoundTo();
            if (!isDeclined
                && datePayment.HasValue
                && iGeneral.ApprovedAmt > 0
                && Transfer.MainWind.IsCreatePayments
                && (TransferWork.DaysBeforePayment == 0 || ((TransferWork.Now - datePayment.Value.Date).TotalDays >= TransferWork.DaysBeforePayment)))
            {
                //if (approved != iGeneral.ApprovedAmt)
                //{

                //}

                Transfer.AddInvoicePayment(clientCase.CaseID, iGeneral, datePayment.Value, iGeneral.ApprovedAmt.Value);
            }

            // ehc
            Transfer.CheckEhcCoverage(clientCase.CaseID, doc, invoiceId);

            // exit
            dopInfo = $"total={iGeneral.InvoiceTotal:C2}";
            return true;
        }
    }
}
