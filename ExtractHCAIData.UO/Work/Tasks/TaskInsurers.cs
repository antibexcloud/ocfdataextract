﻿using System;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskInsurers : AbstractTask
    {
        public TaskInsurers(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "Insurers";

        public override void InWorking()
        {
            if (!SyncContext.InsurerLineItems.Any())
            {
                SourceDb.ReadyInsurers = true;
                Logger.Info($"Not founded records in the InsurerLineItems!");
                return;
            }

            var forAdd = SourceDb.Insurers
                .Where(p => p.UoInsurer == null)
                .ToArray();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = forAdd.Length;

            foreach (var syncInsurer in forAdd)
            {
                if (VmMain.IsCanceled)
                    break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = syncInsurer.Backend.Insurer_Name;

                try
                {
                    //add to Target
                    syncInsurer.UoInsurer = AddInsurer(syncInsurer.Backend);
                    TargetDb.Insurers.Add(syncInsurer.UoInsurer);

                    // end
                    Logger.Info($"added insurer '{syncInsurer.Backend.Insurer_Name}'");
                }
                catch (Exception ex)
                {
                    Logger.Error($"add insurer '{syncInsurer.Backend.Insurer_Name}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            SourceDb.ReadyInsurers = true;
        }

        private CprInsurer AddInsurer(InsurerLineItem insurer)
        {
            var thirdParty = new Third_Party
            {
                ThirdPartyID = UoContext.Third_Party.Any()
                    ? UoContext.Third_Party.Select(t => t.ThirdPartyID).Max() + 1
                    : 1,
                Type = "MVA",
                CompanyName = insurer.Insurer_Name,
                Status = (short) (insurer.Insurer_Status == "Active" ? 1: 0),
                InsurerHCAIID = insurer.Insurer_ID,
                eClaimsIsAvailable = false,
            };
            // add
            UoContext.Third_Party.Add(thirdParty);
            // save
            UoContext.SaveChangesWithDetect();

            // branches
            foreach (var branchLineItem in insurer.BranchLineItems)
            {
                thirdParty.ThirdPartyContacts.Add(new ThirdPartyContact
                {
                    TPContactID = UoContext.ThirdPartyContacts.Any()
                        ? UoContext.ThirdPartyContacts.Select(p => p.TPContactID).Max() + 1
                        : 1,
                    Third_Party = thirdParty,
                    ThirdPartyID = thirdParty.ThirdPartyID,
                    BranchHCAIID = branchLineItem.Branch_ID,
                    Location = branchLineItem.Branch_Name,

                    Province = "ON",
                    Country = "Canada",
                    Address = "Unknown",
                    City = "Unknown",
                    PostalCode = string.Empty,
                });

                UoContext.SaveChangesWithDetect();
            }

            return new CprInsurer(thirdParty);
        }
    }
}
