﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;
using static System.StringComparison;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskOcf21C : AbstractTask
    {
        public TaskOcf21C(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "TRANSFERRING OCF21C HCAI DATA TO UO";

        public override void InWorking()
        {
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = SyncContext.OCF21CSubmit.Count();

            var count = 0;
            foreach (var doc in SyncContext.OCF21CSubmit)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = doc.HCAI_Document_Number;

                try
                {
                    var mvaInvoice = UoContext.MVAInvoices
                        .FirstOrDefault(ocf => ocf.HCAIDocumentNumber == doc.HCAI_Document_Number);

                    if (!AddDocument(doc, mvaInvoice, out var dopInfo, out var rollback))
                    {
                        if (rollback)
                        {
                            Logger.Info($"skip OCF21C document '{doc.HCAI_Document_Number}': {dopInfo}");
                        }
                        continue;
                    }

                    Logger.Info($"{(mvaInvoice is null ? "added" : "updated")} OCF21C document '{doc.HCAI_Document_Number}'" +
                                $"{(string.IsNullOrEmpty(dopInfo) ? "" : $": {dopInfo}")}.");
                    ++count;
                    if (count >= 20)
                    {
                        count = 0;
                        TargetDb.ReConnect();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add OCF21C document '{doc.HCAI_Document_Number}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }
        }

        private bool AddDocument(OCF21CSubmit doc, MVAInvoice mvaInvoice, out string dopInfo, out bool rollback)
        {
            rollback = false;
            dopInfo = string.Empty;
            var docAr = SyncContext.OCF21CAR
                .Include("PAFReimbursableLineItemARs").Include("OtherReimbursableLineItemARs")
                .FirstOrDefault(d => d.HCAI_Document_Number == doc.HCAI_Document_Number);
            if (docAr is null)
                throw new Exception($" OCF21C document {doc.HCAI_Document_Number} - no AR-information!");
            
            //find Report ID by plan number
            double? reportId = null;
            var refOfHcaiPlanNumber = !string.IsNullOrEmpty(doc.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber)
                                      && doc.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber != "exempt"
                ? doc.OCF21C_PreviouslyApprovedGoodsAndServices_PlanNumber
                : string.Empty;
            var ocf23 = !string.IsNullOrEmpty(refOfHcaiPlanNumber)
                ? UoContext.PaperWorks
                    .FirstOrDefault(p => p.ReportType == "OCF23" && p.HCAIDocumentNumber == refOfHcaiPlanNumber)
                : null;

            // client
            Client_Case clientCase;
            if (GroupByProviderSpecialty && ocf23 != null)
            {
                clientCase = ocf23.Client_Case;
            }
            else
            {
                clientCase = SyncPatient.MakeHcaiPatientsInfo(Transfer, SourceDb.SyncPatients, doc, false);
            }

            if (clientCase is null)
            {
                Logger.Info($"Skipped OCF21C document {doc.HCAI_Document_Number} - no information about client in UO!");
                return false;
            }

            var docDate = Service.HcaiNum2Date(doc.HCAI_Document_Number);
            var days = (DateTime.Now.Date - docDate).TotalDays;
            var isActive = days <= 6 * 30;
            var repliedAs = ConvertHelper.GetRepliedAs(docAr.Document_Status);
            var isDeclined = repliedAs.Equals("Not Approved", InvariantCultureIgnoreCase)
                             || repliedAs.Equals("Declined", InvariantCultureIgnoreCase);

            var iGeneral = mvaInvoice != null
                ? UoContext.InvoceGeneralInfoes.Find(mvaInvoice.InvoiceID)
                : null;

            var isExists = mvaInvoice != null;
            DbContextTransaction transaction = null;
            if (isExists)
            {
                if (iGeneral is null)
                    throw new Exception($"OCF21C document '{doc.HCAI_Document_Number}' has no InvoceGeneralInfo!!!");
                if (iGeneral.CaseID != clientCase.CaseID)
                {
                    if (clientCase.client_id == iGeneral.Client_Case.client_id)
                    {
                        clientCase = iGeneral.Client_Case;
                    }
                    else
                    {
                        Logger.Info($"OCF21C document '{doc.HCAI_Document_Number}' has CaseID={iGeneral.CaseID}, but {clientCase.CaseID} founded...");
                        return false;
                    }
                }

                if (string.IsNullOrEmpty(repliedAs)) return false;
                
                var wasDeclined = !string.IsNullOrEmpty(iGeneral.RepliedAs)
                                  && (iGeneral.RepliedAs.Equals("Declined", InvariantCultureIgnoreCase)
                                      || iGeneral.RepliedAs.Equals("Not Approved", InvariantCultureIgnoreCase));
                if (isDeclined && wasDeclined) return false;

                if (repliedAs.Equals(iGeneral.RepliedAs, InvariantCultureIgnoreCase)
                    && (iGeneral.InvoiceReplyDate is null || iGeneral.InvoiceReplyDate.Value.Date == docAr.Adjuster_Response_Date.Date))
                    return false;
                dopInfo = $"from status '{iGeneral.RepliedAs}' to status '{repliedAs}'";
                transaction = UoContext.Database.BeginTransaction(IsolationLevel.Unspecified);
            }
            else
            {
                //if (repliedAs == "Not Approved")
                //{
                //    Logger.Info($"OCF21C document '{doc.HCAI_Document_Number}' skipped: Not Approved.");
                //    return false;
                //}
            }

            // invoice number
            var invoiceId = mvaInvoice?.InvoiceID ?? 0;
            if (invoiceId == 0)
            {
                invoiceId = int.TryParse(doc.OCF21C_InvoiceInformation_InvoiceNumber, out var num) ? num : 0;
                invoiceId = TargetDb.GetInvoiceId(invoiceId);
            }

            //
            if (!string.IsNullOrEmpty(refOfHcaiPlanNumber))
            {
                if (ocf23 != null)
                {
                    if (clientCase.CaseID == ocf23.CaseID)
                    {
                        reportId = ocf23.ReportID;
                    }
                    else
                    {
                        Logger.Info($"INVALID OCF21C REFERENCE TO OCF23 PLAN FOUND OCF23 HCAI DOC#: {refOfHcaiPlanNumber}" +
                                    $" OCF21C InvNo: {invoiceId} (diff CaseID: self='{clientCase.CaseID}', plan='{ocf23.CaseID}')");
                        ocf23 = null;
                    }
                }
                else
                    Logger.Info($"INVALID OCF21C REFERENCE TO OCF23 PLAN: PaperWork with HCAIDocumentNumber='{refOfHcaiPlanNumber}' not founded!" +
                                $" OCF21C InvNo: {invoiceId}");
            }

            var invoiceDate = DateTime.MinValue;
            //***** InvoiceGeneralInfo *****
            iGeneral ??= UoContext.InvoceGeneralInfoes.Add(new InvoceGeneralInfo
            {
                RecordCreationDate = docAr.Submission_Date,
                InvoiceDate = docAr.Submission_Date,
                InvoiceID = invoiceId,
                InvoiceType = "MVA",
                Version = "C",
                ReportID = reportId,

                CaseID = clientCase.CaseID,
                Client_Case = clientCase,

                InvoiceGST = (double) doc.OCF21C_InsurerTotals_GST_Proposed,
                InvoicePST = (double)doc.OCF21C_InsurerTotals_PST_Proposed,

                // 2024-09-25
                InvoiceTotal = ((double)doc.OCF21C_InsurerTotals_SubTotalPreApproved_Proposed + (double)doc.OCF21C_InsurerTotals_GST_Proposed)
                    .RoundTo(),
                //InvoiceTotal = (double)doc.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed,

                MVATotal = (double)doc.OCF21C_InsurerTotals_AutoInsurerTotal_Proposed,
                Interest = (double)doc.OCF21C_InsurerTotals_Interest_Proposed,

                ToPrint = 0, //do not mark as print -- not sure
                InvoiceStatus = 1, //for reply its also 1

                // not NULL fields
                AcknowledgementErrorCode = "Unknown",
                PaymentRequestUid = "Unknown",
                AdjudicationStatus = "Unknown",
                DeliveryStatus = "Unknown",
                AdjudicatorsInvoiceReferenceId = "Unknown",
                ConfirmationNumber = "Unknown",
            });

            // fill
            iGeneral.InvoiceComments = doc.OCF21C_InvoiceInformation_InvoiceNumber;
            iGeneral.RepliedAs = repliedAs;
            iGeneral.ApprovedAmt = (double?)docAr.OCF21C_InsurerTotals_AutoInsurerTotal_Approved;
            iGeneral.InvoiceReplyDate = !string.IsNullOrEmpty(docAr.Document_Status)
                ? docAr.Adjuster_Response_Date
                : (DateTime?)null;

            // insurer
            var insurer = SourceDb.Insurers
                              .Where(t => t.Backend.Insurer_ID == doc.Insurer_IBCInsurerID && t.UoInsurer != null)
                              .Select(t => t.UoInsurer)
                              .ToList()
                              .FirstOrDefault(t => t.IsMva)
                          ?? TargetDb.Insurers.FirstOrDefault(t => t.Backend.InsurerHCAIID == doc.Insurer_IBCInsurerID);

            //
            var isEhcDebit = doc.OCF21C_InsurerTotals_OtherInsurers_Proposed > 0;
            var isMohDebit = doc.OCF21C_InsurerTotals_MOH_Proposed > 0;

            //***** MVAInvoice *****
            var iMvaInvoice = mvaInvoice ?? UoContext.MVAInvoices.Add(new MVAInvoice
            {
                InvoiceID = invoiceId,
                InvoiceVersion = "C",

                ReportID = reportId,
                RefOfHCAI_PlanNumber = refOfHcaiPlanNumber,

                FacilityIsPayee = 1, //(short)(doc.OCF21C_Payee_FacilityIsPayee ? 1 : 0),

                //''' complete provider's info when loading items
                CIFirstName = doc.Applicant_Name_FirstName,
                CILastName = doc.Applicant_Name_LastName,
                CIMiddleName = doc.Applicant_Name_MiddleName,
                CIDOB = doc.Applicant_DateOfBirth,
                CIGender = doc.Applicant_Gender.ToGender(),
                CIAddress = $"{doc.Applicant_Address_StreetAddress1}" +
                            $"{(!string.IsNullOrEmpty(doc.Applicant_Address_StreetAddress2) ? " " + doc.Applicant_Address_StreetAddress2 : "")}",

                CIProvince = ConvertHelper.FormatProvince(doc.Applicant_Address_Province),
                CIPostalCode = ConvertHelper.FormatPostalCode(doc.Applicant_Address_PostalCode),
                CICity = doc.Applicant_Address_City,
                CITel = $"{ConvertHelper.PhoneFromHcai(doc.Applicant_TelephoneNumber)}" +
                        $"{(!string.IsNullOrEmpty(doc.Applicant_TelephoneExtension) ? "x" + doc.Applicant_TelephoneExtension : "")}",
                CIExt = doc.Applicant_TelephoneExtension,
                CIPolicyNo = doc.Header_PolicyNumber,
                CIClaimNo = doc.Header_ClaimNumber,
                CIDOL = doc.Header_DateOfAccident,

                //business center info
                Facility = TargetDb.BusinessCenter.BusinessName,
                FIPayeeFirstName = TargetDb.BusinessCenter.CLastName,
                FIPayeeLastName = TargetDb.BusinessCenter.CFirstName,
                FIAddress = TargetDb.BusinessCenter.Address,
                FICity = TargetDb.BusinessCenter.City,
                FIProvince = ConvertHelper.FormatProvince(TargetDb.BusinessCenter.Province),
                FIPostalCode = ConvertHelper.FormatPostalCode(TargetDb.BusinessCenter.PostalCode),
                FITel = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.PNLine1),
                FIExt = string.Empty,
                FIFax = ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.Fax),
                FIAISINo = TargetDb.BusinessCenter.FacilityAISINo,
                FIPayTo = doc.OCF21C_Payee_MakeChequePayableTo,
                FIEmail = TargetDb.BusinessCenter.Email,

                //NOTE BY DEFAULT NO CONFLICTS
                FINoConflicts = 0, //no conflicts
                FIDaclare = 0,
                FIDaclareText = "",

                staff_id = 0, //TO DO find staff id -- signature of staff id

                // default values
                PrintInjuries = 1,
                PrintProviders = 1,
            });

            // fill
            iMvaInvoice.RefOfHCAI_PlanNumber = refOfHcaiPlanNumber;

            iMvaInvoice.FirstInvoice = (short)(doc.OCF21C_InvoiceInformation_FirstInvoice ? 1 : 0);
            iMvaInvoice.LastInvoice = (short)(doc.OCF21C_InvoiceInformation_LastInvoice ? 1 : 0);
            iMvaInvoice.PlanType = ocf23 != null
                ? doc.OCF21C_PreviouslyApprovedGoodsAndServices_Type
                : "";
            iMvaInvoice.PlanDate = ocf23 != null
                ? doc.OCF21C_PreviouslyApprovedGoodsAndServices_PlanDate
                : null;
            iMvaInvoice.ApprovedAmt = doc.OCF21C_PreviouslyApprovedGoodsAndServices_AmountApproved?.ToString();
            iMvaInvoice.PrevBilled = doc.OCF21C_PreviouslyApprovedGoodsAndServices_PreviouslyBilled?.ToString();
            iMvaInvoice.PriorBalance = (double?)doc.OCF21C_AccountActivity_PriorBalance;
            iMvaInvoice.PaymentReceived = (double?)doc.OCF21C_AccountActivity_PaymentReceivedFromAutoInsurer;
            iMvaInvoice.OverdueAmount = (double?)doc.OCF21C_AccountActivity_OverdueAmount;

            iMvaInvoice.IsAmountRefused = (byte)(doc.OCF21C_OtherInsuranceAmounts_IsAmountRefused == true ? 1 : 0);

            iMvaInvoice.OHIPChiro_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_MOHDebits_Chiropractic;
            iMvaInvoice.OHIPPhysio_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_MOHDebits_Physiotherapy;
            iMvaInvoice.OHIPMassage_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_MOHDebits_MassageTherapy;
            iMvaInvoice.OHIPX_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_MOHDebits_OtherService;

            iMvaInvoice.EHC1Chiro_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Chiropractic;
            iMvaInvoice.EHC1Physio_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Physiotherapy;
            iMvaInvoice.EHC1Massage_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer1Debits_MassageTherapy;
            iMvaInvoice.EHC1X_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer1Debits_OtherService;

            iMvaInvoice.EHC2Chiro_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Chiropractic;
            iMvaInvoice.EHC2Physio_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Physiotherapy;
            iMvaInvoice.EHC2Massage_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer2Debits_MassageTherapy;
            iMvaInvoice.EHC2X_Debit = (double?)doc.OCF21C_OtherInsuranceAmounts_Insurer2Debits_OtherService;

            iMvaInvoice.MOHTotalApprovedLineCost_Debit =
                (double?)docAr.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost;
            iMvaInvoice.MOHTotalApprovedReasonCode_Debit =
                docAr.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode;
            iMvaInvoice.MOHTotalApprovedReasonCodeDesc_Debit =
                docAr.OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            iMvaInvoice.MOHTotalApprovedReasonCodeDescOther_Debit = docAr
                .OCF21C_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

            iMvaInvoice.EHC1TotalApprovedLineCost_Debit =
                (double?)docAr.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost;
            iMvaInvoice.EHC1TotalApprovedReasonCode_Debit =
                docAr.OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode;
            iMvaInvoice.EHC1TotalApprovedReasonCodeDesc_Debit = docAr
                .OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            iMvaInvoice.EHC1TotalApprovedReasonCodeDescOther_Debit = docAr
                .OCF21C_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

            iMvaInvoice.EHC2TotalApprovedLineCost_Debit =
                (double?)docAr.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost;
            iMvaInvoice.EHC2TotalApprovedReasonCode_Debit =
                docAr.OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode;
            iMvaInvoice.EHC2TotalApprovedReasonCodeDesc_Debit = docAr
                .OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
            iMvaInvoice.EHC2TotalApprovedReasonCodeDescOther_Debit = docAr
                .OCF21C_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

            iMvaInvoice.AOtherInsuranceAmounts_AdjusterResponseExplanation =
                docAr.OCF21C_OtherInsuranceAmounts_AdjusterResponseExplanation;
            iMvaInvoice.AReimbursable_AdjusterResponseExplanation =
                docAr.OCF21C_PAFReimbursableFees_AdjusterResponseExplanation;
            iMvaInvoice.AClaimFormReceived = docAr.OCF21C_InsurerSignature_ClaimFormReceived == true ? "Yes" : "No";
            iMvaInvoice.AClaimFormReceivedDate =
                docAr.OCF21C_InsurerSignature_ClaimFormReceivedDate?.ToString("yyyy-MM-dd");
            iMvaInvoice.SubTotalOtherGS =
                (double?)docAr.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved;

            iMvaInvoice.IIAdjFirstName = doc.Insurer_Adjuster_Name_FirstName;
            iMvaInvoice.IIAdjLastName = doc.Insurer_Adjuster_Name_LastName;
            iMvaInvoice.IIAdjTel = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_TelephoneNumber);
            iMvaInvoice.IIAdjExt = doc.Insurer_Adjuster_TelephoneExtension;
            iMvaInvoice.IIAdjFax = ConvertHelper.PhoneFromHcai(doc.Insurer_Adjuster_FaxNumber);
            iMvaInvoice.IIPHolderSame = (short)(doc.Insurer_PolicyHolder_IsSameAsApplicant ? 1 : 0);
            iMvaInvoice.IIPHolderLastName = doc.Insurer_PolicyHolder_Name_LastName;
            iMvaInvoice.IIPHolderFirstName = doc.Insurer_PolicyHolder_Name_FirstName;

            iMvaInvoice.MVAInsurerHCAIID = doc.Insurer_IBCInsurerID;
            iMvaInvoice.MVABranchHCAIID = doc.Insurer_IBCBranchID;

            iMvaInvoice.Comments = doc.OCF21C_OtherInformation;

            //'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            iMvaInvoice.OHIPChiro = (double?)(doc.OCF21C_OtherInsuranceAmounts_MOH_Chiropractic ?? 0.0m);
            iMvaInvoice.OHIPPhysio = (double?)(doc.OCF21C_OtherInsuranceAmounts_MOH_Physiotherapy ?? 0.0m);
            iMvaInvoice.OHIPMassage = (double?)(doc.OCF21C_OtherInsuranceAmounts_MOH_MassageTherapy ?? 0.0m);
            iMvaInvoice.OHIPX = (double?)(doc.OCF21C_OtherInsuranceAmounts_MOH_OtherService ?? 0.0m);

            iMvaInvoice.EHC1Chiro = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer1_Chiropractic ?? 0.0m);
            iMvaInvoice.EHC1Physio = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer1_Physiotherapy ?? 0.0m);
            iMvaInvoice.EHC1Massage = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer1_MassageTherapy ?? 0.0m);
            iMvaInvoice.EHC1X = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer1_OtherService ?? 0.0m);

            iMvaInvoice.EHC2Chiro = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer2_Chiropractic ?? 0.0m);
            iMvaInvoice.EHC2Physio = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer2_Physiotherapy ?? 0.0m);
            iMvaInvoice.EHC2Massage = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer2_MassageTherapy ?? 0.0m);
            iMvaInvoice.EHC2X = (double?)(doc.OCF21C_OtherInsuranceAmounts_Insurer2_OtherService ?? 0.0m);

            iMvaInvoice.XService = doc.OCF21C_OtherInsuranceAmounts_OtherServiceType;
            iMvaInvoice.chkAttachments = (short)(doc.AttachmentsBeingSent ? 1 : 0);
            iMvaInvoice.AdditionalComments = doc.AdditionalComments;
            iMvaInvoice.HCAIDocumentNumber = doc.HCAI_Document_Number;

            iMvaInvoice.ApprovedByOnPDF = docAr.ApprovedByOnPDF;

            iMvaInvoice.OHIP = (iMvaInvoice.OHIPChiro ?? 0)
                               + (iMvaInvoice.OHIPPhysio ?? 0)
                               + (iMvaInvoice.OHIPMassage ?? 0)
                               + (iMvaInvoice.OHIPX ?? 0);
            iMvaInvoice.EHC1And2 = (iMvaInvoice.EHC1Chiro ?? 0)
                                   + (iMvaInvoice.EHC1Physio ?? 0)
                                   + (iMvaInvoice.EHC1Massage ?? 0)
                                   + (iMvaInvoice.EHC1X ?? 0)
                                   + (iMvaInvoice.EHC2Chiro ?? 0)
                                   + (iMvaInvoice.EHC2Physio ?? 0)
                                   + (iMvaInvoice.EHC2Massage ?? 0)
                                   + (iMvaInvoice.EHC2X ?? 0);

            var isEhc = Math.Abs(iMvaInvoice.EHC1And2 ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC1Chiro_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC1Massage_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC1Physio_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC1X_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC2Chiro_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC2Massage_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC2Physio_Debit ?? 0) > 0.01
                        || Math.Abs(iMvaInvoice.EHC2X_Debit ?? 0) > 0.01;
            iMvaInvoice.qEHC = (short)(isEhc ? 1 : 0);
            iMvaInvoice.qOHIP = (short)(Math.Abs(iMvaInvoice.OHIP ?? 0) > 0.01 ? 1 : 0);

            if (insurer != null)
            {
                var branch = insurer.Backend.ThirdPartyContacts
                    .FirstOrDefault(b => b.BranchHCAIID == doc.Insurer_IBCBranchID);

                iMvaInvoice.IIName = insurer.Backend.CompanyName;

                if (branch != null)
                {
                    iMvaInvoice.IIAddress = branch.Address;
                    iMvaInvoice.IICity = branch.City;
                    iMvaInvoice.IIProvince = branch.Province;
                    iMvaInvoice.IIPostalCode = branch.PostalCode;
                }
            }

            //approval info
            if (!string.IsNullOrEmpty(iGeneral.RepliedAs))
            {
                iMvaInvoice.MOHTotalApprovedLineCost = (double) docAr.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_LineCost;
                iMvaInvoice.MOHTotalApprovedReasonCode = docAr.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.MOHTotalApprovedReasonCodeDesc = docAr.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.MOHTotalApprovedReasonCodeDescOther = docAr.OCF21C_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.EHC1TotalApprovedLineCost = (double)docAr.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost;
                iMvaInvoice.EHC1TotalApprovedReasonCode = docAr.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.EHC1TotalApprovedReasonCodeDesc = docAr.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.EHC1TotalApprovedReasonCodeDescOther = docAr.OCF21C_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.EHC2TotalApprovedLineCost = (double)docAr.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost;
                iMvaInvoice.EHC2TotalApprovedReasonCode = docAr.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.EHC2TotalApprovedReasonCodeDesc = docAr.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.EHC2TotalApprovedReasonCodeDescOther = docAr.OCF21C_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.InsurerTotalMOHApproved = (double)docAr.OCF21C_InsurerTotals_MOH_Approved;
                iMvaInvoice.InsurerTotalEHC12Approved = (double)docAr.OCF21C_InsurerTotals_OtherInsurers_Approved;
                iMvaInvoice.AutoInsurerTotalApproved = (double)docAr.OCF21C_InsurerTotals_AutoInsurerTotal_Approved;

                iMvaInvoice.GSTApprovedLineCost = (double)docAr.OCF21C_InsurerTotals_GST_Approved_LineCost;
                iMvaInvoice.GSTApprovedReasonCode = docAr.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.GSTApprovedReasonCodeDesc = docAr.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.GSTApprovedReasonCodeDescOther = docAr.OCF21C_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.PSTApprovedLineCost = (double)docAr.OCF21C_InsurerTotals_PST_Approved_LineCost;
                iMvaInvoice.PSTApprovedReasonCode = docAr.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode;
                iMvaInvoice.PSTApprovedReasonCodeDesc = docAr.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc;
                iMvaInvoice.PSTApprovedReasonCodeDescOther = docAr.OCF21C_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                iMvaInvoice.SubtotalGSApproved = (double)docAr.OCF21C_InsurerTotals_SubTotalPreApproved_Approved;
                iMvaInvoice.TotalInteresedApproved = (double) docAr.OCF21C_InsurerTotals_Interest_Approved_LineCost;
                iMvaInvoice.SubTotalOtherGS = (double)docAr.OCF21C_InsurerTotals_SubTotalOtherReimbursableGoodsAndServices_Approved;
            }

            var ocfProviders = new List<OCFProviders>();

            // ***** PAFReimbursableLineItemSubmit (blocks)
            if (doc.PAFReimbursableLineItemSubmits.Any())
            {
                var maxDet = doc.PAFReimbursableLineItemSubmits
                    .Select(t => t.OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService).Max();
                if (invoiceDate < maxDet) invoiceDate = maxDet;

                var iItemDetailSize = 1;
                var pmsKeyIndex = 300;
                foreach (var lineItemSubmit in doc.PAFReimbursableLineItemSubmits)
                {
                    var pmsKey = lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey;
                    var lineItemAr = docAr.PAFReimbursableLineItemARs
                        .FirstOrDefault(gs => gs.OCF21C_PAFReimbursableFees_Items_Item_Approved_PMSGSKey == pmsKey);

                    var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                        SourceDb.ActivityItems, lineItemSubmit, false);

                    var ocfProvider1 = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                        $"{lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID}",
                        lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation);
                    var ocfProvider2 = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                        $"{lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID}",
                        lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation);
                    var ocfProvider3 = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                        $"{lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_ProviderRegistryID}",
                        lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_Occupation);

                    var fee = (double)lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_EstimatedLineCost;
                    var attribute = lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Attribute;

                    var ocfGs = iGeneral.InvoiceItemDetails
                        .FirstOrDefault(t => t.ItemVCForShow == 2
                                             && t.ItemID == hcaiItem.ItemId
                                             && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                 || t.ItemServiceCode == hcaiItem.ServiceCode)
                                             //&& t.ItemAttribute == attribute
                                             && t.ItemRate == fee
                                             && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.Name)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName)));
                    var isNewItem = ocfGs is null;
                    if (isExists && isNewItem)
                    {
                        dopInfo = $"{dopInfo}{Environment.NewLine}" +
                                  $"  No {nameof(InvoiceItemDetail)}: {hcaiItem.ItemId}, {hcaiItem.RealServiceCode}, {fee}, {hcaiItem.ItemDescription}!";
                        rollback = true;
                        UoContext.CancelChanges(transaction);
                        return false;
                    }

                    ocfGs ??= new InvoiceItemDetail
                    {
                        PMSGSKey = !string.IsNullOrEmpty(pmsKey) && int.TryParse(pmsKey, out var pKey)
                            ? pKey
                            : pmsKeyIndex++,

                        ItemVCForShow = 2,
                        InvoiceID = invoiceId,
                        InvoceGeneralInfo = iGeneral,
                        ReportID = reportId,

                        ItemCategory = "Activity",
                        ItemType = "Block",

                        FirstDateOfService = lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_FirstDateOfService,

                        ItemID = hcaiItem.ItemId,
                        ItemServiceCode = hcaiItem.RealServiceCode,
                        ItemAttribute = attribute,
                        ItemMeasure = string.Empty,
                        ItemDescription = hcaiItem.ItemDescription,
                        ItemOrder = !string.IsNullOrEmpty(lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey)
                                    && int.TryParse(lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_Estimated_PMSGSKey, out var order)
                            ? order
                            : iItemDetailSize++,
                        ItemOccupation = lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation,

                        ItemBaseUnit = 0,
                        ItemQty = 1,
                        ItemRate = fee,
                        ItemAmount = fee,

                        GST = 0,
                        PST = 0,

                        //not NULL fields
                        DeliveryLocationCode = "11",
                        PaymentRequestId = "Unknown",
                        DeliveryStatus = "Unknown",
                        AdjudicationStatus = "Unknown",
                        LineID = "Unknown",
                        aAdjusterReasonCode = string.Empty,
                        aAdjusterReasonCodeDesc = string.Empty,
                        aAdjusterReasonOtherCodeDesc = string.Empty,
                    };
                    if (isNewItem)
                        iGeneral.InvoiceItemDetails.Add(ocfGs);

                    if (ocfProvider1 != null)
                    {
                        ocfGs.ItemPR1 = ocfProvider1.ItemPR;
                        ocfGs.WhichItemPRPrimary = 1;
                    }
                    else if (lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID > 0)
                        Logger.Info($"LOAD OCF21C LINE ITEMS ===> PROVIDER IS NOT FOUND ???? HCAI DOCUMENT NUMBER: {lineItemSubmit.HCAI_Document_Number}" +
                                    $" HCAI REG NO: {lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID}," +
                                    $" Profession: {lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation}");

                    if (lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID > 0)
                    {
                        if (ocfProvider2 != null)
                        {
                            ocfGs.ItemPR2 = ocfProvider2.ItemPR;
                            ocfGs.WhichItemPRPrimary = 2;
                        }
                        else
                            Logger.Info($"LOAD OCF21C LINE ITEMS ===> PROVIDER (second) IS NOT FOUND : HCAI DOCUMENT NUMBER: {doc.HCAI_Document_Number}" +
                                        $" HCAI REG NO: {lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID}," +
                                        $" Proffession: {lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation}");
                    }

                    if (lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_ProviderRegistryID > 0)
                    {
                        if (ocfProvider3 != null)
                        {
                            ocfGs.ItemPR3 = ocfProvider3.ItemPR;
                            ocfGs.WhichItemPRPrimary = 3;
                        }
                        else
                            Logger.Info($"LOAD OCF21C LINE ITEMS ===> PROVIDER (second-two) IS NOT FOUND : HCAI DOCUMENT NUMBER: {doc.HCAI_Document_Number}" +
                                        $" HCAI REG NO: {lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_ProviderRegistryID}," +
                                        $" Proffession: {lineItemSubmit.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_Occupation}");
                    }

                    if (lineItemAr != null && !string.IsNullOrEmpty(iGeneral.RepliedAs))
                    {
                        ocfGs.aAdjusterReasonCode = lineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
                        ocfGs.aAdjusterReasonCodeDesc = lineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
                        ocfGs.aAdjusterReasonOtherCodeDesc = lineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                        ocfGs.aGST = 0.0;
                        ocfGs.aPST = 0.0;
                        ocfGs.aItemAmount = (double)lineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost;
                        ocfGs.aItemQty = 1;
                        ocfGs.aItemRate = (double)lineItemAr.OCF21C_PAFReimbursableFees_Items_Item_Approved_LineCost;
                    }
                }
            }

            // ***** OtherReimbursableLineItemSubmit (empty)
            if (doc.OtherReimbursableLineItemSubmits.Any())
            {
                var maxDet = doc.OtherReimbursableLineItemSubmits
                    .Select(t => t.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService).Max();
                if (invoiceDate < maxDet) invoiceDate = maxDet;

                var iItemDetail2Size = 1;
                foreach (var lineItemSubmit in doc.OtherReimbursableLineItemSubmits)
                {
                    var lineItemAr = docAr.OtherReimbursableLineItemARs
                        .FirstOrDefault(gs => gs.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey ==
                                              lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey);

                    var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                        SourceDb.ActivityItems, lineItemSubmit, false);

                    var ocfProvider = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                        $"{lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}",
                        lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation);

                    var units = (double)lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Quantity;
                    if (units == 0) units = 1;
                    var itemAmount = (double)lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_LineCost;
                    var gst = hcaiItem.GST > 0 ? hcaiItem.GST : GST;
                    var taxId = !string.IsNullOrEmpty(hcaiItem.TaxId) ? hcaiItem.TaxId : "H";
                    var attribute = lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Attribute;
                    var fee = (itemAmount / units).RoundTo();
                    var itemMeasure =
                        !string.IsNullOrEmpty(
                            lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure)
                            ? lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Measure.ToLower()
                            : hcaiItem.Measure;
                    var itemCategory = hcaiItem.IsProduct ? "Product" : "Activity";
                    var itemType = hcaiItem.IsProduct
                        ? "Product"
                        : hcaiItem.Type == "Block"
                            ? "Service"
                            : hcaiItem.Type;

                    var ocfGs = iGeneral.InvoiceItemDetails
                        .FirstOrDefault(t => t.ItemVCForShow == 0
                                             && t.ItemID == hcaiItem.ItemId
                                             && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                 || t.ItemServiceCode == hcaiItem.ServiceCode)
                                             //&& t.ItemAttribute == attribute
                                             //&& t.ItemMeasure == itemMeasure
                                             && t.ItemBaseUnit == units
                                             && t.ItemCategory == itemCategory
                                             //&& t.ItemType == itemType
                                             && t.ItemRate == itemAmount
                                             && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.Name)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName)));
                    var isNewItem = ocfGs is null;
                    if (isExists && isNewItem)
                    {
                        dopInfo = $"{dopInfo}{Environment.NewLine}" +
                                  $"  No {nameof(InvoiceItemDetail)} {hcaiItem.ItemId}, {units}, {itemMeasure}!";
                        rollback = true;
                        UoContext.CancelChanges(transaction);
                        return false;
                    }

                    ocfGs ??= new InvoiceItemDetail
                    {
                        ItemVCForShow = 0,
                        RecordID = (decimal)(reportId ?? 0),
                        InvoiceID = invoiceId,
                        InvoceGeneralInfo = iGeneral,

                        ItemType = itemType,
                        ItemCategory = itemCategory,

                        ItemOrder = !string.IsNullOrEmpty(lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey)
                                    && int.TryParse(lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey, out var order)
                            ? order
                            : iItemDetail2Size++,

                        ItemDate = lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_DateOfService,
                        ItemID = hcaiItem.ItemId,

                        ItemServiceCode = hcaiItem.RealServiceCode,
                        ItemAttribute = attribute,
                        ItemDescription = hcaiItem.ItemDescription /*lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Description*/,
                        ItemOccupation = lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation,

                        ItemBaseUnit = units,
                        ItemQty = 1,
                        ItemRate = fee,
                        ItemAmount = itemAmount,

                        ItemTaxID = lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST
                            ? taxId
                            : string.Empty,
                        GST = lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_GST
                            ? gst
                            : 0,
                        PST = lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Estimated_PST
                            ? hcaiItem.PST
                            : 0,

                        ItemMeasure = itemMeasure,

                        //not NULL fields
                        DeliveryLocationCode = "11",
                        PaymentRequestId = "Unknown",
                        DeliveryStatus = "Unknown",
                        AdjudicationStatus = "Unknown",
                        LineID = "Unknown",
                    };
                    if (isNewItem)
                        iGeneral.InvoiceItemDetails.Add(ocfGs);

                    if (ocfProvider != null)
                    {
                        ocfGs.ItemPR = ocfProvider.ItemPR;
                        ocfGs.ItemOccupation = ocfProvider.ProviderOccupation;
                        ocfGs.ItemProviderHCAINo = ocfProvider.RealProviderHCAINo;
                        ocfGs.ItemProviderID = ocfProvider.ProviderID;
                        ocfGs.ItemProviderCode = "";
                    }
                    else
                        Logger.Info($"LOAD OCF21C LINE ITEMS ===> PROVIDER IS NOT FOUND ???? HCAI DOCUMENT NUMBER: {lineItemSubmit.HCAI_Document_Number}" +
                                    $" HCAI REG NO: {lineItemSubmit.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}");

                    if (lineItemAr != null && !string.IsNullOrEmpty(iGeneral.RepliedAs))
                    {
                        ocfGs.aAdjusterReasonCode = lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode;
                        ocfGs.aAdjusterReasonCodeDesc = lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc;
                        ocfGs.aAdjusterReasonOtherCodeDesc = lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc;

                        ocfGs.aGST = lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_GST
                            ? gst
                            : 0.0;
                        ocfGs.aPST = lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PST
                            ? hcaiItem.PST
                            : 0.0;

                        ocfGs.aItemQty = ocfGs.ItemQty ?? 1;
                        ocfGs.aItemAmount = (double)lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_LineCost;
                        ocfGs.aItemRate = (ocfGs.aItemAmount / ocfGs.aItemQty).RoundTo();

                        if (!string.IsNullOrEmpty(lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey)
                            && int.TryParse(lineItemAr.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey, out var key))
                        {
                            ocfGs.PMSGSKey = key;
                        }
                    }
                }
            }

            //***** RenderedGSLineItemSubmit (in_block-items) *****
            if (doc.RenderedGSLineItemSubmits.Any())
            {
                var maxDet = doc.RenderedGSLineItemSubmits
                    .Select(t => t.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService).Max();
                if (invoiceDate < maxDet) invoiceDate = maxDet;

                var iItemDetail3Size = 1;
                foreach (var lineItemSubmit in doc.RenderedGSLineItemSubmits)
                {
                    var hcaiItem = CprActivity.MakeHcaiItemType(Transfer, docAr.Submission_Date,
                        SourceDb.ActivityItems, lineItemSubmit, false);

                    var ocfProvider = OCFProviders.GetOcfProviderIndex(Transfer, ocfProviders,
                        $"{lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}",
                        lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation);

                    var units = (double)lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Quantity;
                    var attribute = lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Attribute;
                    var itemType = hcaiItem.IsProduct
                        ? "Product"
                        : hcaiItem.Type == "Block"
                            ? "Service"
                            : hcaiItem.Type;
                    var itemMeasure =
                        !string.IsNullOrEmpty(lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Measure)
                            ? lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_Measure.ToLower()
                            : hcaiItem.Measure;

                    var ocfGs = iGeneral.InvoiceItemDetails
                        .FirstOrDefault(t => t.ItemVCForShow == 0
                                             && t.ItemID == hcaiItem.ItemId
                                             && (t.ItemServiceCode == hcaiItem.RealServiceCode
                                                 || t.ItemServiceCode == hcaiItem.ServiceCode)
                                             //&& t.ItemAttribute == attribute
                                             //&& t.ItemMeasure == itemMeasure
                                             && t.ItemBaseUnit == units
                                             && t.ItemType == itemType
                                             && (ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.ItemDescription)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.Name)
                                                 || ConvertHelper.StringEquals(t.ItemDescription, hcaiItem.RealName)));
                    var isNewItem = ocfGs is null;
                    if (isExists && isNewItem)
                    {
                        dopInfo = $"{dopInfo}{Environment.NewLine}" +
                                  $"  No {nameof(InvoiceItemDetail)}: {hcaiItem.ItemId}, {units}, {itemMeasure}!";
                        rollback = true;
                        UoContext.CancelChanges(transaction);
                        return false;
                    }

                    ocfGs ??= new InvoiceItemDetail
                    {
                        RecordID = 0,
                        InvoiceID = invoiceId,
                        InvoceGeneralInfo = iGeneral,
                        ItemVCForShow = 1,

                        ItemDate = lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_DateOfService,

                        ItemID = hcaiItem.ItemId,
                        ItemAttribute = attribute,
                        ItemType = itemType,

                        ItemServiceCode = hcaiItem.RealServiceCode,
                        ItemDescription = hcaiItem.ItemDescription,
                        ItemOrder = !string.IsNullOrEmpty(lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey)
                                    && int.TryParse(lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey, out var order)
                            ? order
                            : iItemDetail3Size++,
                        ItemOccupation = lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation,

                        ItemBaseUnit = units,
                        ItemQty = 1,
                        ItemRate = 0,
                        ItemAmount = 0,

                        ItemTaxID = "",
                        GST = 0,
                        PST = 0,

                        ItemMeasure = itemMeasure,

                        //not NULL fields
                        DeliveryLocationCode = "11",
                        PaymentRequestId = "Unknown",
                        DeliveryStatus = "Unknown",
                        AdjudicationStatus = "Unknown",
                        LineID = "Unknown",
                    };
                    if (isNewItem)
                        iGeneral.InvoiceItemDetails.Add(ocfGs);

                    if (ocfProvider != null)
                    {
                        ocfGs.ItemPR = ocfProvider.ItemPR;
                        ocfGs.ItemOccupation = ocfProvider.ProviderOccupation;
                        ocfGs.ItemProviderHCAINo = ocfProvider.RealProviderHCAINo;
                        ocfGs.ItemProviderID = ocfProvider.ProviderID;
                        ocfGs.ItemProviderCode = "";
                    }
                    else
                        Logger.Info($"LOAD OCF21C LINE ITEMS ===> PROVIDER IS NOT FOUND ???? HCAI DOCUMENT NUMBER: {lineItemSubmit.HCAI_Document_Number}" +
                                    $" HCAI REG NO: {lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID}," +
                                    $" Profession: {lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation}");

                    if (!string.IsNullOrEmpty(lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey)
                        && int.TryParse(lineItemSubmit.OCF21C_RenderedGoodsAndServices_Items_Item_PMSGSKey, out var key))
                    {
                        ocfGs.PMSGSKey = key;
                    }
                    else
                        ocfGs.PMSGSKey = ocfGs.ItemOrder;
                }
            }

            iGeneral.InvoiceSentDate = iGeneral.InvoiceDate = invoiceDate;
            if (iGeneral.InvoiceDate.HasValue)
                iGeneral.HCAIVersion = ConvertHelper.GetPlanVersion(iGeneral.InvoiceDate.Value);

            iMvaInvoice.SignedBy = ""; //TO DO
            iMvaInvoice.iDate = iGeneral.InvoiceSentDate;

            // 2024-10-03
            //iGeneral.InvoiceTotal = iGeneral.InvoiceItemDetails
            //    .Where(d => d.ItemVCForShow == 2)
            //    .Sum(d => d.ItemAmount ?? 0)
            //    .RoundTo();

            // ***** OCF fixes
            foreach (var ocfProvider in ocfProviders)
            {
                if (ocfProvider.ItemPR == ConvertHelper.Letters[0])
                {
                    iMvaInvoice.AType = ocfProvider.Type;
                    iMvaInvoice.ANumber = ocfProvider.Number;
                    iMvaInvoice.AHourRate = ocfProvider.HourRate;
                    iMvaInvoice.AOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.AProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.AProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.AProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.ARegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[1])
                {
                    iMvaInvoice.BType = ocfProvider.Type;
                    iMvaInvoice.BNumber = ocfProvider.Number;
                    iMvaInvoice.BHourRate = ocfProvider.HourRate;
                    iMvaInvoice.BOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.BProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.BProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.BProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.BRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[2])
                {
                    iMvaInvoice.CType = ocfProvider.Type;
                    iMvaInvoice.CNumber = ocfProvider.Number;
                    iMvaInvoice.CHourRate = ocfProvider.HourRate;
                    iMvaInvoice.COccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.CProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.CProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.CProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.CRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[3])
                {
                    iMvaInvoice.DType = ocfProvider.Type;
                    iMvaInvoice.DNumber = ocfProvider.Number;
                    iMvaInvoice.DHourRate = ocfProvider.HourRate;
                    iMvaInvoice.DOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.DProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.DProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.DProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.DRegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[4])
                {
                    iMvaInvoice.EType = ocfProvider.Type;
                    iMvaInvoice.ENumber = ocfProvider.Number;
                    iMvaInvoice.EHourRate = ocfProvider.HourRate;
                    iMvaInvoice.EOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.EProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.EProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.EProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.ERegulated = ocfProvider.Regulated;
                }
                else if (ocfProvider.ItemPR == ConvertHelper.Letters[5])
                {
                    iMvaInvoice.Ftype = ocfProvider.Type;
                    iMvaInvoice.FNumber = ocfProvider.Number;
                    iMvaInvoice.FHourRate = ocfProvider.HourRate;
                    iMvaInvoice.FOccupation = ocfProvider.ProviderOccupation;
                    iMvaInvoice.FProviderID = ocfProvider.ProviderID;
                    iMvaInvoice.FProviderName = ocfProvider.ProviderName;
                    iMvaInvoice.FProviderHCAINo = ocfProvider.RealProviderHCAINo;
                    iMvaInvoice.FRegulated = ocfProvider.Regulated;
                }
            }

            // save
            UoContext.SaveChangesWithDetect(transaction);

            //***** InvoiceInjuries *****
            var iOrder = 1;
            foreach (var injury in doc.OCF21CInjuryLineItemSubmit)
            {
                var codeDescription = TargetDb.InjuryCodes
                    .FirstOrDefault(c => c.Code == injury.OCF21C_InjuriesAndSequelae_Injury_Code)
                    ?.CodeDescription ?? string.Empty;
                TargetDb.CheckInjuryCode(injury.OCF21C_InjuriesAndSequelae_Injury_Code);
                TargetDb.Connection.SaveInvoiceInjuries(invoiceId, injury.OCF21C_InjuriesAndSequelae_Injury_Code, codeDescription, iOrder++);
            }

            // HCAIStatusLog
            var isArchived = docAr.Archival_Status == "Yes";
            TargetDb.SaveHcaiLogStatus(ConvertHelper.CreateHcaiLog("OCF21C", invoiceId, doc.HCAI_Document_Number,
                isExists, iGeneral.InvoiceDate.Value, docAr.Submission_Date, docAr.Adjuster_Response_Date,
                iGeneral.RepliedAs, isArchived, null));

            // charges
            Transfer.AddInvoiceCharges(iGeneral);

            // payment
            var datePayment = iGeneral.InvoiceReplyDate ?? iGeneral.InvoiceDate;
            if (!isDeclined
                && datePayment.HasValue
                && iGeneral.ApprovedAmt > 0
                && Transfer.MainWind.IsCreatePayments
                && (TransferWork.DaysBeforePayment == 0 || ((TransferWork.Now - datePayment.Value.Date).TotalDays >= TransferWork.DaysBeforePayment)))
            {
                Transfer.AddInvoicePayment(clientCase.CaseID, iGeneral, datePayment.Value, iGeneral.ApprovedAmt.Value);
            }

            // exit
            dopInfo = $"total={iGeneral.InvoiceTotal:C2}";
            return true;
        }
    }
}
