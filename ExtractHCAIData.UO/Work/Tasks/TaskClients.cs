﻿using System;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskClients : AbstractTask
    {
        public TaskClients(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "Clients & Cases";

        public override void InWorking()
        {
            var forCheck = SourceDb.SyncPatients
                .Where(p => !p.IsInVirtual)
                .OrderBy(p => p.Name)
                .ToList();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = forCheck.Count;

            foreach (var syncPatient in forCheck)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = syncPatient.Name;

                try
                {
                    if (!syncPatient.IsUo)
                        syncPatient.TryLink(TargetDb, GroupByProviderSpecialty);

                    if (syncPatient.IsUo)
                    {
                        if (!AddMode) continue;

                        #region Check changes

                        var changed = false;
                        var client = syncPatient.UoClient.Backend;
                        if (string.IsNullOrEmpty(client.middle_name) && !string.IsNullOrEmpty(syncPatient.ResultMiddleName))
                        {
                            client.middle_name = syncPatient.ResultMiddleName;
                            changed = true;
                        }

                        if (client.DOB is null)
                        {
                            client.DOB = syncPatient.ResultDOB;
                            changed = true;
                        }
                        //client.gender = syncPatient.ResultGender.ToGender();

                        if (string.IsNullOrEmpty(client.HomePhone) && !string.IsNullOrEmpty(syncPatient.HomePhone))
                        {
                            client.HomePhone = ConvertHelper.PhoneFromHcai(syncPatient.HomePhone);
                            changed = true;
                        }

                        if (string.IsNullOrEmpty(client.HomeStreet) && !string.IsNullOrEmpty(syncPatient.HomeStreet))
                        {
                            client.HomeStreet = syncPatient.HomeStreet;
                            changed = true;
                        }

                        if (string.IsNullOrEmpty(client.HomeCity) && !string.IsNullOrEmpty(syncPatient.HomeCity))
                        {
                            client.HomeCity = syncPatient.HomeCity;
                            changed = true;
                        }

                        if (string.IsNullOrEmpty(client.HomeState) && !string.IsNullOrEmpty(syncPatient.HomeState))
                        {
                            client.HomeState = ConvertHelper.FormatProvince(syncPatient.HomeState);
                            changed = true;
                        }

                        if (string.IsNullOrEmpty(client.HomeZip) && !string.IsNullOrEmpty(syncPatient.HomeZip))
                        {
                            client.HomeZip = ConvertHelper.FormatPostalCode(syncPatient.HomeZip);
                            changed = true;
                        }

                        if (changed)
                        {
                            UoContext.SaveChangesWithDetect();
                            Logger.Info($"updated{(syncPatient.IsVirtual ? " virtual" : "")} patient '{syncPatient.Name}'");
                        }

                        #endregion

                        CheckClientCase(syncPatient);
                    }
                    else
                    {
                        if (syncPatient.UoClient is null)
                        {
                            Logger.Info($"add{(syncPatient.IsVirtual ? " virtual" : "")} patient '{syncPatient.Name}'");
                            AddClient(syncPatient);
                            if (syncPatient.IsVirtual)
                            {
                                foreach (var patient in syncPatient.Backend.Patient1)
                                {
                                    Logger.Info($"  include original patient: Lastname='{patient.ResultLastName}', Firstname='{patient.ResultFirstName}'," +
                                                $" MiddleName='{patient.ResultMiddleName}', Gender='{patient.ResultGender}', DOB:{patient.ResultDOB:d}");
                                }
                            }
                        }
                        else
                        {
                            CheckClientCase(syncPatient);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add{(syncPatient.IsVirtual ? " virtual" : "")} patient '{syncPatient.Name}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            SourceDb.ReadyClients = true;
        }

        private void AddClient(SyncPatient syncPatient)
        {
            var clientId = 0L;
            if (syncPatient.Backend.PMSPatientKey > 0)
            {
                clientId = syncPatient.Backend.PMSPatientKey.Value;
                if (UoContext.Clients.Any(c => c.client_id == clientId))
                    clientId = 0;
            }
            if (clientId == 0)
            {
                clientId = UoContext.Clients.Any()
                    ? (long)UoContext.Clients.Select(c => c.client_id).Max() + 1L
                    : 1L;
            }

            var client = new Client
            {
                client_id = clientId,
                active_client = 1,
                RecordCreationDate = DateTime.Now,

                first_name = syncPatient.ResultFirstName,
                last_name = syncPatient.ResultLastName,
                middle_name = syncPatient.ResultMiddleName,
                DOB = syncPatient.ResultDOB,
                gender = syncPatient.ResultGender.ToGender(),

                HomePhone = ConvertHelper.PhoneFromHcai(syncPatient.HomePhone),
                HomeStreet = syncPatient.HomeStreet,
                HomeCity = syncPatient.HomeCity,
                HomeState = ConvertHelper.FormatProvince(syncPatient.HomeState),
                HomeZip = ConvertHelper.FormatPostalCode(syncPatient.HomeZip),
                HomeCountry = "Canada",
            };

            // other phones
            if (syncPatient.IsVirtual)
            {
                var phones = syncPatient.Backend.Patient1
                    .Where(p => !string.IsNullOrEmpty(p.HomePhone) && p.HomePhone != syncPatient.HomePhone)
                    .Select(p => p.HomePhone)
                    .Distinct()
                    .ToArray();
                if (phones.Any())
                {
                    client.MobilePhone = phones[0];
                    if (phones.Length > 1)
                    {
                        client.OtherPhone = phones[1];
                        if (phones.Length > 2)
                        {
                            client.WorkPhone = phones[2];
                        }
                    }
                }
            }

            // save
            UoContext.Clients.Add(client);
            UoContext.SaveChangesWithDetect();

            // set
            syncPatient.UoClient = new CprClient(client);
            // add
            TargetDb.Clients.Add(syncPatient.UoClient);

            // add case
            CheckClientCase(syncPatient);
        }

        private void CheckClientCase(SyncPatient syncPatient)
        {
            if (GroupByProviderSpecialty)
            {
                syncPatient.SpecialtyCases.ForEach(specialtyCase => CheckClientCase(syncPatient, specialtyCase));
                return;
            }

            CheckClientCase(syncPatient, null);
        }

        private void CheckClientCase(SyncPatient syncPatient, SpecialtyCaseModel specialtyCase)
        {
            var isSpecialtyCase = specialtyCase != null;

            var caseId = isSpecialtyCase
                ? syncPatient.GenerateCaseId(UoContext, specialtyCase.CaseId)
                : syncPatient.UoCase != null
                    ? syncPatient.UoCase.CaseID
                    : syncPatient.GenerateCaseId(UoContext);
            var existCases = syncPatient.UoClient.Backend.Client_Case.ToArray();
            var caseActiveExists = existCases.FirstOrDefault(c => c.CaseType == "MVA" && c.CaseID == caseId)
                ?? existCases.FirstOrDefault(c => c.CaseType == "MVA"
                                                  && syncPatient.DOL != null && c.DOL == syncPatient.DOL);

            var mvaInsurer = !string.IsNullOrEmpty(syncPatient.Backend.MVAInsurerHCAIID)
                ? UoContext.Third_Party
                    .FirstOrDefault(p => p.InsurerHCAIID == syncPatient.Backend.MVAInsurerHCAIID)
                : null;
            var mvaBranch = mvaInsurer != null && !string.IsNullOrEmpty(syncPatient.Backend.MVABranchHCAIID)
                ? mvaInsurer.ThirdPartyContacts
                    .FirstOrDefault(b => b.BranchHCAIID == syncPatient.Backend.MVABranchHCAIID)
                : null;

            var clientCase = caseActiveExists
                          ?? new Client_Case
                          {
                              client_id = syncPatient.UoClient.Backend.client_id,
                              CaseID = caseId,
                              CaseType = "MVA",
                              CaseStatus = 1,
                          };

            // set
            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAInsurerHCAIID) && string.IsNullOrEmpty(clientCase.MVAInsurerHCAIID))
                clientCase.MVAInsurerHCAIID = syncPatient.Backend.MVAInsurerHCAIID;
            if (!string.IsNullOrEmpty(syncPatient.Backend.MVABranchHCAIID) && string.IsNullOrEmpty(clientCase.MVABranchHCAIID))
                clientCase.MVABranchHCAIID = syncPatient.Backend.MVABranchHCAIID;

            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAClaimNo) && string.IsNullOrEmpty(clientCase.MVAClaimNo))
                clientCase.MVAClaimNo = syncPatient.Backend.MVAClaimNo;
            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAPolicyNo) && string.IsNullOrEmpty(clientCase.MVAPolicyNo))
                clientCase.MVAPolicyNo = syncPatient.Backend.MVAPolicyNo;
            clientCase.MVAPolicyHolder = (short)(syncPatient.Backend.MVAPolicyHolder ? 1 : 0);
            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAFirstName) && string.IsNullOrEmpty(clientCase.MVAFirstName))
            {
                clientCase.MVAFirstName = syncPatient.Backend.MVAFirstName;
                clientCase.MVALastName = syncPatient.Backend.MVALastName;
            }

            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAAdjFirstName) && string.IsNullOrEmpty(clientCase.MVAAdjFirstName))
            {
                clientCase.MVAAdjFirstName = syncPatient.Backend.MVAAdjFirstName;
                clientCase.MVAAdjLastName = syncPatient.Backend.MVAAdjLastName;
            }

            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAAdjTel) && string.IsNullOrEmpty(clientCase.MVAAdjTel))
            {
                clientCase.MVAAdjTel = ConvertHelper.PhoneFromHcai(syncPatient.Backend.MVAAdjTel);
                clientCase.MVAAdjExt = syncPatient.Backend.MVAAdjExt;
            }
            if (!string.IsNullOrEmpty(syncPatient.Backend.MVAAdjFax) && string.IsNullOrEmpty(clientCase.MVAAdjFax))
                clientCase.MVAAdjFax = ConvertHelper.PhoneFromHcai(syncPatient.Backend.MVAAdjFax);

            clientCase.DOL ??= syncPatient.Backend.DOL;
            clientCase.OpenDate ??= syncPatient.Backend.DOL;

            clientCase.MVATPID = mvaInsurer != null ? $"{(long)mvaInsurer.ThirdPartyID}" : clientCase.MVATPID;
            if (string.IsNullOrEmpty(clientCase.MVACompanyName))
                clientCase.MVACompanyName = mvaInsurer?.CompanyName ?? clientCase.MVACompanyName;
            if (mvaBranch != null)
            {
                clientCase.MVATPBranchID = mvaBranch.TPContactID;
                clientCase.MVAAddress = mvaBranch.Address;
                clientCase.MVACity = mvaBranch.City;
                clientCase.MVAContactID = mvaBranch.TPContactID;
                clientCase.MVACountry = mvaBranch.Country;
                clientCase.MVAPostalCode = mvaBranch.PostalCode;
                clientCase.MVAProvince = mvaBranch.Province;
            }

            // insurer & branch
            var insurer = SourceDb.Insurers
                              .Where(t => t.Backend.Insurer_ID == syncPatient.Backend.MVAInsurerHCAIID && t.IsUo)
                              .Select(t => t.UoInsurer)
                              .FirstOrDefault() ??
                          TargetDb.Insurers.FirstOrDefault(t =>
                              t.Backend.InsurerHCAIID == syncPatient.Backend.MVAInsurerHCAIID);
            if (insurer != null)
            {
                clientCase.MVATPID = $"{insurer.Backend.ThirdPartyID:F0}";
                clientCase.MVACompanyName = insurer.Backend.CompanyName;
                if (!string.IsNullOrEmpty(clientCase.MVAAdjLastName))
                {
                    clientCase.MVAAdjTPID = mvaInsurer?.ThirdPartyID;
                    clientCase.MVAAdjCompanyName = clientCase.MVACompanyName;
                }

                var branch = insurer.Backend.ThirdPartyContacts
                    .FirstOrDefault(b => b.BranchHCAIID == syncPatient.Backend.MVABranchHCAIID);

                if (branch != null)
                {
                    clientCase.MVAContactID = branch.TPContactID;

                    clientCase.MVAAddress = branch.Address;
                    clientCase.MVACity = branch.City;
                    clientCase.MVAProvince = ConvertHelper.FormatProvince(branch.Province);
                    clientCase.MVAPostalCode = ConvertHelper.FormatPostalCode(branch.PostalCode);
                    clientCase.MVACountry = branch.Country;
                }
            }

            // add
            var isNew = caseActiveExists is null;
            if (isNew)
            {
                UoContext.Client_Case.Add(clientCase);
                Logger.Info($"  added case {clientCase.CaseID} for{(syncPatient.IsVirtual ? " virtual" : "")} patient '{syncPatient.Name}'");
            }

            UoContext.SaveChangesWithDetect();

            // set
            if (isSpecialtyCase)
                specialtyCase.UoCase = clientCase;
            else
                syncPatient.UoCase = clientCase;
        }
    }
}
