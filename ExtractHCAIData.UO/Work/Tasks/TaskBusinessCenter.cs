﻿using System;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskBusinessCenter : AbstractTask
    {
        public TaskBusinessCenter(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "Business Center";

        public override void InWorking()
        {
            var source = SourceDb.SyncContext.FacilityLineItems.FirstOrDefault();
            if (source is null)
            {
                Logger.Info($"Not founded records in the FacilityLineItems!");
                TargetDb.BusinessCenter = UoContext.Business_Center.FirstOrDefault();
                return;
            }

            // get/create
            var target = UoContext.Business_Center.FirstOrDefault() ??
                         UoContext.Business_Center.Add(new Business_Center
                         {
                             BusinessID = 1,
                             Country = "Canada",
                             StartTime = "08:00",
                             EndTime = "20:00",
                             TimeInterval = "00:15",
                             ReleaseDate = new DateTime(2012, 5, 6),
                             ServerVersion = "2014",

                             UOProvinceVersion = "ON",
                             CANO = "",
                             InvoiceElementGroupBranch = "1",
                             EventIdBranch = "1",
                             eClaimsWSIBDeliveryLocationCode = "11",
                             edAppTrigger = 0,
                             eClaimsIsAvailable = false,
                             eClaimsIsAvailableDefaultUser = false,
                             eClaimEventId = 0,
                             eClaimsIsAuditLogRecording = true,
                             eClaimsWsibLastMessageId = 0,
                             eClaimsWsibIsAvailable = false,
                             eClaimsWsibIsAuditLogRecording = true,
                             eClaimsWsibIsAvailableDefaultUser = false,

                             EZ = "BZHSAKJXCYBZCYCY",
                             LZ = "01010101101001010101",
                             UZ = "Vew0pRehI3addwrYONXTpA==",
                             UoServerLicenseKey = "aXQdTH9L7RAoajUOzL/AP/23mj+CH0gTu/i7XLqc/9vUYXMaNQs1cD+vvwGPxILX",
                         });

            // fill
            target.BusinessName = source.Facility_Name;

            target.HCAIID = $"{source.HCAI_Facility_Registry_ID}";
            target.HCAILogin = source.HcaiLogin;
            target.HCAIPassword = source.HcaiPassword;
            target.HCAICheckPayableTo = source.HcaiCheckPayableTo;

            target.Address = $"{source.Billing_Address_Line_1}" +
                             $"{(!string.IsNullOrEmpty(source.Billing_Address_Line_2) ? " " + source.Billing_Address_Line_2 : "")}";
            target.City = source.Billing_City;
            target.Province = ConvertHelper.FormatProvince(source.Billing_Province.StringOrNull() ?? "ON");
            target.PostalCode = ConvertHelper.FormatPostalCode(source.Billing_Postal_Code);
            target.FacilityAISINo = source.AISI_Facility_Number;
            target.PNLine1 = ConvertHelper.PhoneFromHcai(source.Telephone_Number);
            target.Fax = ConvertHelper.PhoneFromHcai(source.Fax_Number);
            target.CFirstName = source.Authorizing_Officer_First_Name;
            target.CLastName = source.Authorizing_Officer_Last_Name;
            target.CSalutation = source.Authorizing_Officer_Title;

            // save
            UoContext.SaveChangesWithDetect();
            TargetDb.BusinessCenter = UoContext.Business_Center.FirstOrDefault();
        }
    }
}
