﻿using System;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Sync.Providers;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.Work.Tasks
{
    public class TaskProviders : AbstractTask
    {
        public TaskProviders(TransferWork owner, int order) : base(owner, order)
        {
        }

        public override string Name => "Providers";

        public override void InWorking()
        {
            var forAddProv = SourceDb.Providers
                .Where(p => (AddMode || !p.IsUo)
                            && !p.IsInVirtual)
                .ToArray();
            var forAddVirt = SourceDb.ProviderVirtuals
                .Where(p => !p.IsUo)
                .ToArray();

            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = forAddProv.Length + forAddVirt.Length;

            foreach (var syncProvider in forAddProv)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = syncProvider.Name;

                try
                {
                    var uo = syncProvider.IsUo ? syncProvider.UoResource : null;
                    var find = TargetDb.Resources.FirstOrDefault(s => syncProvider.Equals(s));

                    //add to Target
                    syncProvider.UoResource = AddProvider(syncProvider, find);
                    if (uo is null)
                        TargetDb.Resources.Add(new CprResource(syncProvider.UoResource.Backend, syncProvider.UoResource.Staff));

                    // end
                    Logger.Info(find != null ? $"updated to provider '{syncProvider.Name}'" : $"added provider '{syncProvider.Name}'");
                }
                catch (Exception ex)
                {
                    Logger.Error($"add provider '{syncProvider.Name}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            foreach (var syncProvider in forAddVirt)
            {
                if (VmMain.IsCanceled) break;

                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = syncProvider.Name;

                try
                {
                    var find = TargetDb.Resources.FirstOrDefault(s => syncProvider.Equals(s));

                    //add to Target
                    syncProvider.UoResource = AddProvider(syncProvider, find);
                    TargetDb.Resources.Add(new CprResource(syncProvider.UoResource.Backend, syncProvider.UoResource.Staff));

                    // end
                    Logger.Info(find != null ? $"updated to virtual provider '{syncProvider.Name}'" : $"added virtual provider '{syncProvider.Name}'");
                    if (find is null)
                    {
                        foreach (var provider in syncProvider.Backend.ProviderLineItems)
                        {
                            Logger.Info($"  include original provider: HCAI_ID={provider.HCAI_Provider_Registry_ID}, " +
                                        $" Lastname='{provider.ResultLastName}', Firstname='{provider.ResultFirstName}'");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"add virtual provider '{syncProvider.Name}'", ex);
                    throw new Exception(Service.InworkException(ex));
                }
            }

            SourceDb.ReadyProviders = true;
        }

        private CprResource AddProvider(IProvider syncProvider, CprResource finded)
        {
            var staff = finded != null
                ? finded.Staff.Backend
                : UoContext.Staffs.Add(new Staff
                {
                    staff_id = UoContext.Staffs.Any() ? UoContext.Staffs.Select(s => s.staff_id).Max() + 1 : 1,
                    first_name = syncProvider.ResultFirstName ?? string.Empty,
                    last_name = syncProvider.ResultLastName ?? string.Empty,
                    status = (short) (syncProvider.Provider_Status.ToLower() == "active" ? 1 : 0),

                    salutation = string.Empty,
                    HomeCountry = "Canada",
                    HomeState = "ON",
                    Provider = 1,

                    Fax = !string.IsNullOrWhiteSpace(syncProvider.FaxNumber)
                        ? ConvertHelper.PhoneFromHcai(syncProvider.FaxNumber)
                        : ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.Fax),
                    Email = syncProvider.Email ?? string.Empty,
                    HomePhone = string.Empty,
                    HomeStreet = syncProvider.Address ?? string.Empty,
                    HomeZip = !string.IsNullOrWhiteSpace(syncProvider.PostalCode)
                        ? ConvertHelper.FormatPostalCode(syncProvider.PostalCode)
                        : ConvertHelper.FormatPostalCode(TargetDb.BusinessCenter.PostalCode),
                    HomeCity = !string.IsNullOrWhiteSpace(syncProvider.City)
                        ? syncProvider.City
                        : TargetDb.BusinessCenter.City ?? string.Empty,

                    WorkPhone = !string.IsNullOrWhiteSpace(syncProvider.TelephoneNumber)
                        ? ConvertHelper.PhoneFromHcai(syncProvider.TelephoneNumber) +
                          (!string.IsNullOrEmpty(syncProvider.TelephoneExtension) ? "x" + syncProvider.TelephoneExtension : "")
                        : ConvertHelper.PhoneFromHcai(TargetDb.BusinessCenter.PNLine1),

                    HourlyWage = string.Empty,
                    HoursPerWeek = string.Empty,
                    AccountNo = string.Empty,
                    Pager = string.Empty,
                    MobilePhone = string.Empty,
                    OtherPhone = string.Empty,
                    SIN = string.Empty,
                    gender = string.Empty,
                    provider_signature = string.Empty,
                    TypeDescription = string.Empty,
                });

            // StaffType
            if (finded is null)
            {
                staff.StaffType = UoContext.StaffTypes
                    .ToList()
                    .FirstOrDefault(st => syncProvider.RegistrationLineItems.Any(r => st.MVACode == r.Profession))
                    ?? UoContext.StaffTypes
                        .ToList()
                        .FirstOrDefault(st => syncProvider.RegistrationLineItems.Any(r => st.MVACode == "OTH"));
                
                if (staff.StaffType is null)
                    throw new Exception($"Unknown StaffType for provider HCAI_ID={syncProvider.HCAI_Provider_Registry_ID}, " +
                                        $" Lastname='{syncProvider.ResultLastName}', Firstname='{syncProvider.ResultFirstName}':{Environment.NewLine}" +
                                        $"{string.Join(";", syncProvider.RegistrationLineItems.Select(p => p.Profession))}");

                staff.TypeDescription = staff.StaffType.Occupation;
                staff.StaffTypeID = staff.StaffType.StaffTypeID;
            }

            var cprStaff = finded?.Staff ?? new CprStaff(staff);
            // save
            UoContext.SaveChangesWithDetect();

            // **** Resource
            var resource = finded != null
                ? finded.Backend
                : UoContext.Resources.Add(new Resource
                {
                    staff_id = staff.staff_id,
                    staff_name = cprStaff.Name,
                    resource_id = !UoContext.Resources.Any(r => r.resource_id == cprStaff.Name)
                        ? cprStaff.Name
                        : $"{staff.last_name.ToUpper()}[{syncProvider.HCAI_Provider_Registry_ID}]",
                    resource_name = staff.first_name + " " + staff.last_name,
                    type = "Provider",
                    ServiceType = staff.StaffType?.ServiceType,
                    status = (short)(syncProvider.Provider_Status.ToLower() == "active" ? 1 : 0),
                    max_occupancy = 1,
                    AllowToOnline = 0,
                    OB = 1,
                    TimeUtilization = true,
                });
            resource.HCAIRegNo = $"{syncProvider.HCAI_Provider_Registry_ID}";

            // professions
            var professions = syncProvider.RegistrationLineItems.OrderBy(r => r.Profession).ToArray();
            for (var i = 0; i < professions.Length; i++)
            {
                var registration = professions[i];
                var staffType = GetStaffType(registration.Profession);

                switch (i)
                {
                    case 0:
                        resource.ColRegType1 = registration.Profession;
                        resource.ColRegRegulated1 = 1;
                        resource.ColRegNo1 = registration.Registration_Number;
                        resource.ColRegServiceType1 = staffType?.ServiceType ?? registration.Profession;
                        break;

                    case 1:
                        resource.ColRegType2 = registration.Profession;
                        resource.ColRegRegulated2 = 1;
                        resource.ColRegNo2 = registration.Registration_Number;
                        resource.ColRegServiceType2 = staffType?.ServiceType ?? registration.Profession;
                        break;

                    case 2:
                        resource.ColRegType3 = registration.Profession;
                        resource.ColRegRegulated3 = 1;
                        resource.ColRegNo3 = registration.Registration_Number;
                        resource.ColRegServiceType3 = staffType?.ServiceType ?? registration.Profession;
                        break;

                    default:
                        Logger.Info($"Provider {resource.resource_name} lost profession {registration.Profession} (more then 3) !!!");
                        break;
                }
            }

            UoContext.SaveChangesWithDetect();

            return finded ?? new CprResource(resource, cprStaff);
        }

        private StaffType GetStaffType(string mvaCode)
        {
            var staffType = UoContext.StaffTypes.FirstOrDefault(st => st.MVACode == mvaCode)
                ?? UoContext.StaffTypes.FirstOrDefault(st => st.MVACode == "OTH");
            return staffType
                ?? throw new Exception($"Unknown StaffType for '{mvaCode}'!");
        }
    }
}
