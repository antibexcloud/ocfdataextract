﻿using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Vm;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Work
{
    public abstract class AbstractTask : ITask
    {
        protected const double GST = 13;

        #region Properties

        private readonly TransferWork _owner;

        public int Order { get; }

        public abstract string Name { get; }

        private MainWindow MainWind => _owner.MainWind;
        protected MainVm VmMain => MainWind.VmMain;

        protected TransferWork Transfer => _owner;

        protected SyncDbVm SourceDb => VmMain.SyncDb;
        protected TargetDbVm TargetDb => VmMain.TargetDb;

        protected HCAIOCFSyncEntities SyncContext
        {
            get
            {
                return SourceDb.SyncContext;
            }
        }

        protected UniversalCPR UoContext
        {
            get
            {
                return TargetDb.UoContext;
            }
        }

        protected bool AddMode => MainWindow.Configuration.AddingMode;
        protected bool GroupByProviderSpecialty => MainWindow.Configuration.GroupByProviderSpecialty;

        #endregion

        #region Live circle

        protected AbstractTask(TransferWork owner, int order)
        {
            Order = order;
            _owner = owner;
        }

        public void Run()
        {
            Logger.Info($"*** Task '{Name}' Start");
            VmMain.TaskInfo = Name;

            InWorking();

            Logger.Info($"*** Task '{Name}' End");
            VmMain.TaskInfo = string.Empty;
        }

        public abstract void InWorking();

        #endregion
    }
}
