﻿set nocount on
declare @tblTypes table (tip nvarchar(16))
insert @tblTypes (tip) values ('OCF18')
insert @tblTypes (tip) values ('OCF23')

declare @planType nvarchar(16);
declare curTypes cursor local for select tip from @tblTypes
open curTypes
fetch next from curTypes into @planType;
while @@FETCH_STATUS = 0 begin
  declare curCase cursor local for
    select distinct CaseID from PaperWork where isnull(PlanNo,0) = 0 and ReportType = @planType order by CaseID

  declare @CaseID nvarchar(105), @ReportID float, @currPlanNo int, @planNo int;
  open curCase;
  fetch next from curCase into @CaseID;
  while @@FETCH_STATUS = 0 begin
    set @planNo = 1;
    -- plans
    declare curPlans cursor local for
	  select Tp.ReportID, isnull(Tp.PlanNo,0) from PaperWork Tp where Tp.CaseID = @CaseID and ReportType = @planType order by Tp.ReportID
    open curPlans;
    fetch next from curPlans into @ReportID, @currPlanNo;
    while @@FETCH_STATUS = 0 begin
	  -- self
      if @currPlanNo <> @planNo
	    update PaperWork set PlanNo = @planNo, PlanName = @planType + '-' + cast(@planNo as nvarchar) where ReportID = @ReportID

      -- next
	  set @planNo = @planNo + 1;
      fetch next from curPlans into @ReportID, @currPlanNo;
    end;
    close curPlans; deallocate curPlans;

    -- next
    fetch next from curCase into @CaseID;
  end;
  close curCase; deallocate curCase;

  -- next
  fetch next from curTypes into @planType;
end;
close curTypes; deallocate curTypes;

-- invoices
update MVAInvoice set PlanNumber = T.numPlan, PlanType = T.namePlan
from (
select Td.InvoiceID as idDoc, cast(Tp.PlanNo as nvarchar) as numPlan, Tp.PlanName as namePlan
  from MVAInvoice Td join
       PaperWork Tp on Tp.ReportID = Td.ReportID
  where Tp.PlanNo is not null
) T
where InvoiceID = T.idDoc

-- OCF18
update OCF18 set PlanNo = T.numPlan
from (
select Tp.ReportID as idDoc, Tp.PlanNo as numPlan
  from PaperWork Tp
  where Tp.PlanNo is not null
) T
where ReportID = T.idDoc and PlanNo <> T.numPlan
select @@ROWCOUNT

-- OCF23
update OCF23 set PlanNo = T.numPlan
from (
select Tp.ReportID as idDoc, Tp.PlanNo as numPlan
  from PaperWork Tp
  where Tp.PlanNo is not null
) T
where ReportID = T.idDoc and PlanNo <> T.numPlan
select @@ROWCOUNT
