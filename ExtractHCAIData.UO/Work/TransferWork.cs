﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Model.Internal;
using ExtractHcaiData.Uo.Model.Sync.Patients;
using ExtractHcaiData.Uo.Model.Sync.Providers;
using ExtractHcaiData.Uo.Model.Uo;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using ExtractHcaiData.Uo.Vm;
using ExtractHcaiData.Uo.Work.Tasks;
using HcaiSync.Common.EfData;

namespace ExtractHcaiData.Uo.Work
{
    public class TransferWork : VMBase
    {
        private readonly List<ITask> _tasks = new List<ITask>();

        public MainWindow MainWind { get; }

        private MainVm VmMain => MainWind.VmMain;

        public SyncDbVm SourceDb => VmMain.SyncDb;
        public TargetDbVm TargetDb => VmMain.TargetDb;

        private HCAIOCFSyncEntities SyncContext => SourceDb.SyncContext;
        private UniversalCPR UoContext => TargetDb.UoContext;

        private readonly bool _inited;

        public bool GroupByProviderSpecialty => MainWindow.Configuration.GroupByProviderSpecialty;

        public static int DaysBeforePayment;
        public static DateTime Now = DateTime.Today;

        public TransferWork(MainWindow owner)
        {
            MainWind = owner;

            if (!TargetDb.Inited)
            {
                TargetDb.Init(VmMain.DbTargetConnectionString);
            }

            if (!SourceDb.Inited)
            {
                SourceDb.Init(VmMain.DbSyncConnectionString);
            }

            DaysBeforePayment = VmMain.DaysBeforePayment;

            var initialPatients = !SourceDb.SyncContext.Patients.Any();
            var initialProviders = !SourceDb.SyncContext.ProviderVirtuals.Any();
            var initialActivities = !SourceDb.SyncContext.ActivityItems.Any();
            var requireInit = initialPatients && initialProviders && initialActivities;
            if (!requireInit && !_inited)
            {
                _inited = true;
                requireInit = Service.Confirm("Check for new data in SYNC database ?", "Sync");
            }

            if (requireInit)
            {
                InitInfo();

                Logger.ReOpenStream();
            }

            SourceDb.InitFacility();
            InitInsurers();
            InitProviders(requireInit);
            InitActivities(requireInit);
            InitClients(requireInit);

            TargetDb.InitInjuryCodes();

            CheckReadiness();
            CheckTargetsReadiness();
        }

        public void Run(TransferTypeEnum transferType)
        {
            if (transferType == TransferTypeEnum.All)
                Logger.Info("Transfer Start");

            _tasks.Clear();
            switch (transferType)
            {
                case TransferTypeEnum.All:
                    _tasks.Add(new TaskBusinessCenter(this, 1));

                    _tasks.Add(new TaskProviders(this, 2));
                    _tasks.Add(new TaskInsurers(this, 3));
                    _tasks.Add(new TaksActivities(this, 4));
                    _tasks.Add(new TaskClients(this, 5));

                    _tasks.Add(new TaskOcf18(this, 10));
                    _tasks.Add(new TaskOcf23(this, 11));
                    _tasks.Add(new TaskOcf21B(this, 12));
                    _tasks.Add(new TaskOcf21C(this, 13));
                    _tasks.Add(new TaskForm1(this, 14));
                    _tasks.Add(new TaskAcsi(this, 15));
                    _tasks.Add(new TaskOcf9(this, 19));
                    break;

                case TransferTypeEnum.BusinessCenter:
                    _tasks.Add(new TaskBusinessCenter(this, (int)transferType));
                    break;

                case TransferTypeEnum.Providers:
                    _tasks.Add(new TaskProviders(this, (int)transferType));
                    break;

                case TransferTypeEnum.Insurers:
                    _tasks.Add(new TaskInsurers(this, (int)transferType));
                    break;

                case TransferTypeEnum.Activities:
                    _tasks.Add(new TaksActivities(this, (int)transferType));
                    break;

                case TransferTypeEnum.Clients:
                    _tasks.Add(new TaskClients(this, (int)transferType));
                    break;

                case TransferTypeEnum.Ocf18:
                    _tasks.Add(new TaskOcf18(this, (int)transferType));
                    break;

                case TransferTypeEnum.Ocf23:
                    _tasks.Add(new TaskOcf23(this, (int)transferType));
                    break;

                case TransferTypeEnum.Ocf21B:
                    _tasks.Add(new TaskOcf21B(this, (int)transferType));
                    break;

                case TransferTypeEnum.Ocf21C:
                    _tasks.Add(new TaskOcf21C(this, (int)transferType));
                    break;

                case TransferTypeEnum.Form1:
                    _tasks.Add(new TaskForm1(this, (int)transferType));
                    break;

                case TransferTypeEnum.Acsi:
                    _tasks.Add(new TaskAcsi(this, (int)transferType));
                    break;

                case TransferTypeEnum.Touchups:
                    _tasks.Add(new TaskTouchupsInjuries(this, 1));
                    _tasks.Add(new TaskTouchupsPlanNo(this, 2));
                    //_tasks.Add(new TaskTouchupsOcf18PlanNo(this, 2));
                    //_tasks.Add(new TaskTouchupsOcf23PlanNo(this, 3));
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(transferType), transferType, null);
            }

            // init
            if (transferType == TransferTypeEnum.All || transferType == TransferTypeEnum.Ocf21B || transferType == TransferTypeEnum.Ocf21C || transferType == TransferTypeEnum.Acsi)
            {
                InitInvoiceId();
            }

            // run
            foreach (var task in _tasks.OrderBy(t => t.Order))
            {
                if (VmMain.IsCanceled) break;
                task.Run();
            }

            if (VmMain.IsCanceled)
                Logger.Info("Canceled by user!");
            else if (transferType == TransferTypeEnum.All || transferType == TransferTypeEnum.Touchups)
                Logger.Info("Transfer End");

            VmMain.IsCanceled = false;
        }

        private InitInfo InitInfo()
        {
            var initInfo = SourceDb.SyncContext.InitInfoes.FirstOrDefault();
            if (initInfo is null)
            {
                SourceDb.SyncContext.InitInfoes.Add(new InitInfo());
                SourceDb.SyncContext.SaveChangesWithDetect();
            }

            return initInfo;
        }

        private void InitProviders(bool init)
        {
            TargetDb.InitStaffAndResources();
            SourceDb.InitProviders();

            if (init)
            {
                VmMain.InProcess = true;

                var initInfo = InitInfo();
                var dateOcf23Start = initInfo.DateOcf23Providers;
                var dateOcf23Max = dateOcf23Start;
                var dateOcf18Start = initInfo.DateOcf18Providers;
                var dateOcf18Max = dateOcf18Start;
                
                // by plans
                VmMain.TaskInfo = "Init providers (from OCF23Submit)";
                var ocf23Set =
                        (from submit in SyncContext.OCF23Submit
                            join arDocRec in SyncContext.OCF23AR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                         from arDoc in arDocs.DefaultIfEmpty()
                         where dateOcf23Start == null || arDoc == null || arDoc.Submission_Date >= dateOcf23Start
                         select new Ocf23Model
                         {
                             SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                             Submit = submit
                         })
                        .ToList();
                VmMain.ProgressBarValue = 0;
                VmMain.ProgressBarMax = ocf23Set.Count;
                foreach (var submitInfo in ocf23Set)
                {
                    ++VmMain.ProgressBarValue;
                    VmMain.StepInfo = submitInfo.Submit.HCAI_Document_Number;
                    SyncProvider.CheckSyncProvider(this, submitInfo.Submit);

                    var date = submitInfo.SubmissionDate ?? Service.HcaiNum2Date(submitInfo.Submit.HCAI_Document_Number);
                    if (dateOcf23Max is null || dateOcf23Max < date)
                        dateOcf23Max = date;
                }

                VmMain.TaskInfo = "Init providers (from OCF18Submit)";
                var ocf18Set =
                    (from submit in SyncContext.OCF18Submit
                        join arDocRec in SyncContext.OCF18AR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                        from arDoc in arDocs.DefaultIfEmpty()
                        where dateOcf18Start == null || arDoc == null || arDoc.Submission_Date >= dateOcf18Start
                        select new Ocf18Model
                        {
                            SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                            Submit = submit
                        })
                    .ToList();
                VmMain.ProgressBarValue = 0;
                VmMain.ProgressBarMax = ocf18Set.Count;
                foreach (var submitInfo in ocf18Set)
                {
                    ++VmMain.ProgressBarValue;
                    VmMain.StepInfo = submitInfo.Submit.HCAI_Document_Number;
                    SyncProvider.CheckSyncProvider(this, submitInfo.Submit);

                    var date = submitInfo.SubmissionDate ?? Service.HcaiNum2Date(submitInfo.Submit.HCAI_Document_Number);
                    if (dateOcf18Max is null || dateOcf18Max < date)
                        dateOcf18Max = date;
                }

                // lost providers
                var lostOcf21B1 = SyncContext.GS21BLineItemSubmit
                    .Where(p => p.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID,
                        Proff = s.OCF21B_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                        .Any(r => r.HCAI_Provider_Registry_ID == p.Id
                                  && r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostOcf21B1.Any())
                {
                    VmMain.TaskInfo = "Init providers (from OCF21B.GS21BLineItemSubmit)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostOcf21B1.Count;
                    foreach (var group in lostOcf21B1)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                var lostOcf21C1 = SyncContext.PAFReimbursableLineItemSubmits
                    .Where(p => p.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_ProviderRegistryID,
                        Proff = s.OCF21C_PAFReimbursableFees_Items_Item_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                        .Any(r => r.HCAI_Provider_Registry_ID == p.Id &&
                                  r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostOcf21C1.Any())
                {
                    VmMain.TaskInfo = "Init providers (from OCF21C.PAFReimbursableLineItemSubmits)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostOcf21C1.Count;
                    foreach (var group in lostOcf21C1)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                var lostOcf21C1S1 = SyncContext.PAFReimbursableLineItemSubmits
                    .Where(p => p.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_ProviderRegistryID.Value,
                        Proff = s.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Providers_Provider_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                        .Any(r => r.HCAI_Provider_Registry_ID == p.Id &&
                                  r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostOcf21C1S1.Any())
                {
                    VmMain.TaskInfo = "Init providers (from OCF21C.PAFReimbursableLineItemSubmits, SecondaryProvider)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostOcf21C1S1.Count;
                    foreach (var group in lostOcf21C1S1)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                var lostOcf21C1S2 = SyncContext.PAFReimbursableLineItemSubmits
                    .Where(p => p.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_ProviderRegistryID.Value,
                        Proff = s.OCF21C_PAFReimbursableFees_Items_Item_SecondaryProviderData_Two_Providers_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                        .Any(r => r.HCAI_Provider_Registry_ID == p.Id &&
                                  r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostOcf21C1S2.Any())
                {
                    VmMain.TaskInfo = "Init providers (from OCF21C.PAFReimbursableLineItemSubmits, SecondaryProvider-Two)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostOcf21C1S2.Count;
                    foreach (var group in lostOcf21C1S2)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                var lostOcf21C2 = SyncContext.OtherReimbursableLineItemSubmits
                    .Where(p => p.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID,
                        Proff = s.OCF21C_OtherReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                            .Any(r => r.HCAI_Provider_Registry_ID == p.Id &&
                                    r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostOcf21C2.Any())
                {
                    VmMain.TaskInfo = "Init providers (from OCF21C.OtherReimbursableLineItemSubmits)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostOcf21C2.Count;
                    foreach (var group in lostOcf21C2)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                var lostOcf21C3 = SyncContext.RenderedGSLineItemSubmits
                    .Where(p => p.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID,
                        Proff = s.OCF21C_RenderedGoodsAndServices_Items_Item_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                        .Any(r => r.HCAI_Provider_Registry_ID == p.Id &&
                                  r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostOcf21C3.Any())
                {
                    VmMain.TaskInfo = "Init providers (from OCF21C.RenderedGSLineItemSubmits)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostOcf21C3.Count;
                    foreach (var group in lostOcf21C3)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                var lostAcsi = SyncContext.GSACSILineItemSubmits
                    .Where(p => p.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID > 0)
                    .Select(s => new UnknownSyncProvider
                    {
                        Id = s.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID,
                        Proff = s.ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation,
                    })
                    .ToList()
                    .Where(p => !SyncContext.ProviderLineItems
                        .Any(r => r.HCAI_Provider_Registry_ID == p.Id
                                  && r.RegistrationLineItems.Any(pr => pr.Profession.ToUpper() == p.Proff.ToUpper())))
                    .GroupBy(p => p.Proff + p.Id)
                    .ToList();
                if (lostAcsi.Any())
                {
                    VmMain.TaskInfo = "Init providers (from ACSI.GSACSILineItemSubmit)";
                    VmMain.ProgressBarValue = 0;
                    VmMain.ProgressBarMax = lostAcsi.Count;
                    foreach (var group in lostAcsi)
                    {
                        var first = group.First();
                        var code = first.Id;
                        var proff = group.First().Proff;
                        ++VmMain.ProgressBarValue;
                        Logger.Info($"added unknown provider with HCAIRegNo={code}");
                        VmMain.StepInfo = code.ToString();
                        SyncProvider.AddUnknownSyncProvider(this, code, proff);
                    }
                }

                // save
                if (dateOcf18Start != dateOcf18Max)
                {
                    initInfo.DateOcf18Providers = dateOcf18Max;
                }
                if (dateOcf23Start != dateOcf23Max)
                {
                    initInfo.DateOcf23Providers = dateOcf23Max;
                }

                SourceDb.SyncContext.SaveChangesWithDetect();

                // end
                VmMain.InProcess = false;
                VmMain.TaskInfo = string.Empty;
            }

            // try link 1
            foreach (var syncProvider in SourceDb.Providers.Where(p => !p.IsInVirtual && !p.IsUo).ToArray())
                syncProvider.UoResource = TargetDb.Resources.FirstOrDefault(p => syncProvider.Equals(p));
            // try link 2
            foreach (var syncProviderVirtual in SourceDb.ProviderVirtuals.Where(p => !p.IsUo).ToArray())
                syncProviderVirtual.UoResource = TargetDb.Resources.FirstOrDefault(p => syncProviderVirtual.Equals(p));

            //
            SourceDb.ReadyProviders = SourceDb.Providers.Where(p => !p.IsInVirtual).All(t => t.IsUo)
                                      && SourceDb.ProviderVirtuals.All(t => t.IsUo);
        }

        private void InitInsurers()
        {
            SourceDb.InitInsurers();
            TargetDb.InitInsurers();

            // try find
            foreach (var syncInsurer in SourceDb.Insurers.Where(p => p.UoInsurer == null).ToArray())
            {
                var find = TargetDb.Insurers
                    .FirstOrDefault(p => syncInsurer.Backend.Insurer_ID == p.Backend.InsurerHCAIID);
                if (find != null)
                    syncInsurer.UoInsurer = find;
            }

            foreach (var syncInsurer in SourceDb.Insurers.Where(p => p.UoInsurer == null).ToArray())
            {
                var find = TargetDb.Insurers
                    .FirstOrDefault(p => syncInsurer.Backend.Insurer_Name.ToLower() == p.Backend.CompanyName.ToLower());
                if (find != null)
                    syncInsurer.UoInsurer = find;
            }

            SourceDb.ReadyInsurers = SourceDb.Insurers.All(t => t.IsUo);
        }

        private void InitActivities(bool init)
        {
            SourceDb.InitActivities();
            TargetDb.InitActivities();

            if (init)
                LoadUniqueItemsFromAllOcfs();

            // link
            foreach (var activityItem in SourceDb.ActivityItems.Where(a => !a.IsUo).ToArray())
                activityItem.TryLink(TargetDb);

            // ready
            SourceDb.ReadyActivities = SourceDb.ActivityItems.Where(t => !t.IsInVirtual).All(t => t.IsUo);
        }

        private void InitClients(bool init)
        {
            SourceDb.InitPatients();
            TargetDb.InitClients();
            if (init || (SourceDb.SyncPatients.Any() && SourceDb.SyncPatients.All(p => string.IsNullOrEmpty(p.Backend.SpecialtyCases))))
                LoadUniquePatientsAndCaseInfo();

            // link
            foreach (var syncPatient in SourceDb.SyncPatients.Where(p => !p.IsUo).ToArray())
                syncPatient.TryLink(TargetDb, GroupByProviderSpecialty);
            SourceDb.SyncContext.SaveChangesWithDetect();

            // check
            SourceDb.ReadyClients = SourceDb.SyncPatients.Where(p => !p.IsInVirtual).All(t => t.IsUo);
        }

        private void InitInvoiceId()
        {
            var maxDbInvoiceId = UoContext.InvoceGeneralInfoes.Any()
                ? UoContext.InvoceGeneralInfoes.Select(d => d.InvoiceID).Max()
                : 0;

            TargetDb.ReservedInvoiceIds.Clear();
            TargetDb.ReservedInvoiceIds.AddRange(SyncContext.OCF21BSubmit
                .Where(d => !string.IsNullOrEmpty(d.OCF21B_InvoiceInformation_InvoiceNumber))
                .ToList()
                .Select(d => int.TryParse(d.OCF21B_InvoiceInformation_InvoiceNumber, out var number) ? number : 0)
                .Where(n => n > 0)
                .ToList());
            TargetDb.ReservedInvoiceIds.AddRange(SyncContext.OCF21CSubmit
                .Where(d => !string.IsNullOrEmpty(d.OCF21C_InvoiceInformation_InvoiceNumber))
                .ToList()
                .Select(d => int.TryParse(d.OCF21C_InvoiceInformation_InvoiceNumber, out var number) ? number : 0)
                .Where(n => n > 0)
                .ToList());
            TargetDb.ReservedInvoiceIds.AddRange(SyncContext.ACSISubmits
                .Where(d => !string.IsNullOrEmpty(d.ACSI_InvoiceInformation_InvoiceNumber))
                .ToList()
                .Select(d => int.TryParse(d.ACSI_InvoiceInformation_InvoiceNumber, out var number) ? number : 0)
                .Where(n => n > 0)
                .ToList());
            var maxReservedId = TargetDb.ReservedInvoiceIds.Any()
                ? TargetDb.ReservedInvoiceIds.Max()
                : 0;

            TargetDb.MaxFreeInvoiceId = Math.Max(maxDbInvoiceId, maxReservedId) + 1;
            if (VmMain.UseInvoiceStartNumber && VmMain.InvoiceStartNumber > 0 && VmMain.InvoiceStartNumber > TargetDb.MaxFreeInvoiceId)
                TargetDb.MaxFreeInvoiceId = VmMain.InvoiceStartNumber;
        }

        
        public void InitReportId()
        {
            TargetDb.ReportId = UoContext.PaperWorks.Any()
                ? UoContext.PaperWorks.Select(r => r.ReportID).Max() + 1
                : 1;
        }

        public void InitBlockId()
        {
            TargetDb.BlockId = UoContext.OCFGoodsAndServices.Any(r => r.BlockID != null)
                ? UoContext.OCFGoodsAndServices
                    .Where(r => r.BlockID != null)
                    .Select(r => r.BlockID ?? 0).Max() + 1
                : 1;
        }

        public void CheckReadiness()
        {
            if (!SyncContext.OCF18Submit.Any())
                SourceDb.ReadyOcf18 = true;
            else
            {
                var srcNumbers = SyncContext.OCF18Submit.Select(d => d.HCAI_Document_Number).ToList();
                var dstNumbers = UoContext.PaperWorks.Where(p => p.ReportType == "OCF18")
                    .Select(p => p.HCAIDocumentNumber)
                    .ToList();
                SourceDb.ReadyOcf18 = srcNumbers.All(s => dstNumbers.Any(d => d == s));
            }

            if (!SyncContext.OCF23AR.Any())
                SourceDb.ReadyOcf23 = true;
            else
            {
                var srcNumbers = SyncContext.OCF23AR
                    .Where(d => d.Document_Status.ToLower() != "declined")
                    .Select(d => d.HCAI_Document_Number).ToList();
                var dstNumbers = UoContext.PaperWorks.Where(p => p.ReportType == "OCF23")
                    .Select(p => p.HCAIDocumentNumber)
                    .ToList();
                SourceDb.ReadyOcf23 = srcNumbers.All(s => dstNumbers.Any(d => d == s));
            }

            if (!SyncContext.AACNSubms.Any())
                SourceDb.ReadyForm1 = true;
            else
            {
                var srcNumbers = SyncContext.AACNSubms.Select(d => d.DocumentNumber).ToList();
                var dstNumbers = UoContext.PaperWorks.Where(p => p.ReportType == "FORM1")
                    .Select(p => p.HCAIDocumentNumber)
                    .ToList();

                SourceDb.ReadyForm1 = srcNumbers.All(s => dstNumbers.Any(d => d == s));
            }

            if (!SyncContext.OCF21BSubmit.Any())
                SourceDb.ReadyOcf21b = true;
            else
            {
                var srcNumbers = SyncContext.OCF21BSubmit.Select(d => d.HCAI_Document_Number).ToList();
                var dstNumbers = UoContext.MVAInvoices.Where(n => n.InvoiceVersion == "B")
                    .Select(n => n.HCAIDocumentNumber)
                    .ToList();

                SourceDb.ReadyOcf21b = srcNumbers.All(s => dstNumbers.Any(d => d == s));
            }

            if (!SyncContext.OCF21CAR.Any())
                SourceDb.ReadyOcf21c = true;
            else
            {
                var srcNumbers = SyncContext.OCF21CAR
                    .Where(d => d.Document_Status.ToLower() != "declined")
                    .Select(d => d.HCAI_Document_Number).ToList();
                var dstNumbers = UoContext.MVAInvoices.Where(n => n.InvoiceVersion == "C")
                    .Select(n => n.HCAIDocumentNumber)
                    .ToList();

                SourceDb.ReadyOcf21c = srcNumbers.All(s => dstNumbers.Any(d => d == s));
            }

            if (!SyncContext.ACSISubmits.Any())
                SourceDb.ReadyAcsi = true;
            else
            {
                var srcNumbers = SyncContext.ACSISubmits.Select(d => d.HCAI_Document_Number).ToList();
                var dstNumbers = UoContext.MVAInvoices.Where(n => n.InvoiceVersion == "ACSI")
                    .Select(n => n.HCAIDocumentNumber)
                    .ToList();

                SourceDb.ReadyAcsi = srcNumbers.All(s => dstNumbers.Any(d => d == s));
            }
        }

        public void CheckTargetsReadiness()
        {
            TargetDb.ReadyOcf18PlanNo = !UoContext.OCF18.Any() || UoContext.OCF18.All(p => p.PlanNo > 0);
            TargetDb.ReadyOcf23PlanNo = !UoContext.OCF23.Any() || UoContext.OCF23.All(p => p.PlanNo > 0);
        }

        public void PrepareSyncProviders()
        {
            VmMain.SyncProviders.Clear();
            foreach (var provider in SourceDb.ProviderVirtuals.ToArray())
                VmMain.SyncProviders.Add(new SyncProviderVm(null, provider));
            foreach (var provider in SourceDb.Providers.Where(p => !p.IsInVirtual).ToArray())
                VmMain.SyncProviders.Add(new SyncProviderVm(provider, null));
        }

        #region Service Activity

        private void LoadUniqueItemsFromAllOcfs()
        {
            VmMain.InProcess = true;

            // 0
            var initInfo = InitInfo();
            var dateOcf18Start = initInfo.ActivityOcf18;
            var dateOcf23Start = initInfo.ActivityOcf23;
            var dateOcf21BStart = initInfo.ActivityOcf21b;
            var dateOcf21CStart = initInfo.ActivityOcf21c;
            var dateAcsiStart = initInfo.ActivityAcsi;
            var dateOcf18Max = dateOcf18Start;
            var dateOcf23Max = dateOcf23Start;
            var dateOcf21BMax = dateOcf21BStart;
            var dateOcf21CMax = dateOcf21CStart;
            var dateAcsiMax = dateAcsiStart;

            // 0 - prepare actual sets
            var ocf18Set =
                (from submit in SyncContext.OCF18Submit
                    join arDocRec in SyncContext.OCF18AR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                    from arDoc in arDocs.DefaultIfEmpty()
                    where dateOcf18Start == null || arDoc == null || arDoc.Submission_Date >= dateOcf18Start
                    select new Ocf18Model
                    {
                        Submit = submit,
                        SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                        OcfDate = arDoc != null
                            ? arDoc.Adjuster_Response_Date
                            : submit.OCF18_ApplicantSignature_SigningDate ?? submit.Header_DateOfAccident,
                    })
                .ToList();
            var ocf23Set =
                (from submit in SyncContext.OCF23Submit
                    join arDocRec in SyncContext.OCF23AR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                    from arDoc in arDocs.DefaultIfEmpty()
                    where dateOcf23Start == null || arDoc == null || arDoc.Submission_Date >= dateOcf23Start
                    select new Ocf23Model
                    {
                        Submit = submit,
                        SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                        OcfDate = arDoc != null
                            ? arDoc.Adjuster_Response_Date
                            : submit.OCF23_ApplicantSignature_SigningDate ?? submit.Header_DateOfAccident,
                    })
                .ToList();

            var ocf21BSet =
                (from submit in SyncContext.OCF21BSubmit
                    join arDocRec in SyncContext.OCF21BAR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                    from arDoc in arDocs.DefaultIfEmpty()
                 where dateOcf21BStart == null || arDoc.Submission_Date >= dateOcf21BStart
                    select new Ocf21BModel
                    {
                        Submit = submit,
                        SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                        OcfDate = arDoc != null ? arDoc.Adjuster_Response_Date : submit.Header_DateOfAccident,
                    })
                .ToList();
            var ocf21CSet =
                (from submit in SyncContext.OCF21CSubmit
                    join arDocRec in SyncContext.OCF21CAR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                    from arDoc in arDocs.DefaultIfEmpty()
                 where dateOcf21CStart == null || arDoc.Submission_Date >= dateOcf21CStart
                    select new Ocf21CModel
                    {
                        Submit = submit,
                        SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                        OcfDate = arDoc != null ? arDoc.Adjuster_Response_Date : submit.Header_DateOfAccident,
                    })
                .ToList();
            var acsiSet =
                (from submit in SyncContext.ACSISubmits
                    join arDocRec in SyncContext.ACSIARs on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                    from arDoc in arDocs.DefaultIfEmpty()
                    where dateAcsiStart == null || arDoc.Submission_Date >= dateAcsiStart
                    select new AcsiModel
                    {
                        Submit = submit,
                        SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                        OcfDate = arDoc != null ? arDoc.Adjuster_Response_Date : submit.Header_DateOfAccident,
                    })
                .ToList();

            //1. select ocf18 distinct items
            VmMain.TaskInfo = "Init HCAIItems (from OCF18Submit/GS18LineItemSubmit)";
            VmMain.ProgressBarValue = 0;
            var gs18Info = ocf18Set
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.GS18LineItemSubmit
                })
                .ToList();
            VmMain.ProgressBarMax = gs18Info.Sum(s => s.GS18LineItemSubmit.Count);
            foreach (var info in gs18Info)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.GS18LineItemSubmit)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf18Max is null || dateOcf18Max < dateActivity)
                {
                    dateOcf18Max = dateActivity;
                }
            }

            //2. select ocf21b distinct items
            VmMain.TaskInfo = "Init HCAIItems (from OCF21BSubmit/GS21BLineItemSubmit)";
            VmMain.ProgressBarValue = 0;
            var gs21BInfo = ocf21BSet
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.GS21BLineItemSubmit
                })
                .ToList();

            VmMain.ProgressBarMax = gs21BInfo.Sum(s => s.GS21BLineItemSubmit.Count);
            foreach (var info in gs21BInfo)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.GS21BLineItemSubmit)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf21BMax is null || dateOcf21BMax < dateActivity)
                {
                    dateOcf21BMax = dateActivity;
                }
            }

            //3. select ACSI distinct items
            VmMain.TaskInfo = "Init HCAIItems (from ACSISubmit/GSACSILineItemSubmit)";
            VmMain.ProgressBarValue = 0;
            var gsAcsiInfo = acsiSet
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.GSACSILineItemSubmits
                })
                .ToList();

            VmMain.ProgressBarMax = gsAcsiInfo.Sum(s => s.GSACSILineItemSubmits.Count);
            foreach (var info in gsAcsiInfo)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.GSACSILineItemSubmits)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateAcsiMax is null || dateAcsiMax < dateActivity)
                {
                    dateAcsiMax = dateActivity;
                }
            }

            //4. select ocf23 distinct items
            //a. load paf WAD + Supplementrary + X-Ray section ===> PAFLineItemSubmit
            VmMain.TaskInfo = "Init HCAIItems (from PAFLineItemSubmits)";
            VmMain.ProgressBarValue = 0;
            var pafLineItemSubmits = ocf23Set
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.PAFLineItemSubmits
                })
                .ToList();
            VmMain.ProgressBarMax = pafLineItemSubmits.Sum(s => s.PAFLineItemSubmits.Count);
            foreach (var info in pafLineItemSubmits)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.PAFLineItemSubmits)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf23Max is null || dateOcf23Max < dateActivity)
                {
                    dateOcf23Max = dateActivity;
                }
            }
            //b. regular items ==> OtherGSLineItemSubmit
            VmMain.TaskInfo = "Init HCAIItems (from OtherGSLineItemSubmits)";
            VmMain.ProgressBarValue = 0;
            var otherGsLineItemSubmits = ocf23Set
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.OtherGSLineItemSubmits
                })
                .ToList();
            VmMain.ProgressBarMax = otherGsLineItemSubmits.Sum(s => s.OtherGSLineItemSubmits.Count);
            foreach (var info in otherGsLineItemSubmits)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.OtherGSLineItemSubmits)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf23Max is null || dateOcf23Max < dateActivity)
                {
                    dateOcf23Max = dateActivity;
                }
            }

            //5. select ocf21c distinct items
            //a. zero dollar charges/page ===> RenderedGSLineItemSubmit
            VmMain.TaskInfo = "Init HCAIItems (from RenderedGSLineItemSubmits)";
            VmMain.ProgressBarValue = 0;
            var renderedGsLineItemSubmits = ocf21CSet
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.RenderedGSLineItemSubmits
                })
                .ToList();
            VmMain.ProgressBarMax = renderedGsLineItemSubmits.Sum(s => s.RenderedGSLineItemSubmits.Count);
            foreach (var info in renderedGsLineItemSubmits)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.RenderedGSLineItemSubmits)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf21CMax is null || dateOcf21CMax < dateActivity)
                {
                    dateOcf21CMax = dateActivity;
                }
            }
            //b. MIG/WAD + Supplementrary + X-Ray section ===> PAFReimbursableLineItemSubmit
            VmMain.TaskInfo = "Init HCAIItems (from PAFReimbursableLineItemSubmits)";
            VmMain.ProgressBarValue = 0;
            var pafReimbursableLineItemSubmits = ocf21CSet
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.PAFReimbursableLineItemSubmits
                })
                .ToList();
            VmMain.ProgressBarMax = pafReimbursableLineItemSubmits.Sum(s => s.PAFReimbursableLineItemSubmits.Count);
            foreach (var info in pafReimbursableLineItemSubmits)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.PAFReimbursableLineItemSubmits)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf21CMax is null || dateOcf21CMax < dateActivity)
                {
                    dateOcf21CMax = dateActivity;
                }
            }
            //c. other items (regular items) ===> OtherReimbursableLineItemSubmit
            VmMain.TaskInfo = "Init HCAIItems (from OtherReimbursableLineItemSubmits)";
            VmMain.ProgressBarValue = 0;
            var otherReimbursableLineItemSubmits = ocf21CSet
                .Select(s => new
                {
                    s.Submit.HCAI_Document_Number,
                    s.SubmissionDate,
                    s.OcfDate,
                    s.Submit.OtherReimbursableLineItemSubmits
                })
                .ToList();
            VmMain.ProgressBarMax = otherReimbursableLineItemSubmits.Sum(s => s.OtherReimbursableLineItemSubmits.Count);
            foreach (var info in otherReimbursableLineItemSubmits)
            {
                VmMain.StepInfo = info.HCAI_Document_Number;
                var date = info.OcfDate;

                foreach (var item in info.OtherReimbursableLineItemSubmits)
                {
                    ++VmMain.ProgressBarValue;
                    CprActivity.MakeHcaiItemType(this, date, SourceDb.ActivityItems, item, true);
                }

                var dateActivity = info.SubmissionDate ?? info.OcfDate;
                if (dateOcf21CMax is null || dateOcf21CMax < dateActivity)
                {
                    dateOcf21CMax = dateActivity;
                }
            }

            // set Description
            VmMain.TaskInfo = "Update descriptions HCAIItems";
            var activityItems = SourceDb.ActivityItems
                .Where(t => string.IsNullOrEmpty(t.Backend.ItemDescription))
                .ToList();
            VmMain.ProgressBarValue = 0;
            VmMain.ProgressBarMax = activityItems.Count;
            foreach (var hcaiItemType in activityItems)
            {
                ++VmMain.ProgressBarValue;
                VmMain.StepInfo = hcaiItemType.RealServiceCode;

                var newDesc = UoContext.ServiceCodes
                    .Where(c => c.ServiceCode1 == hcaiItemType.RealServiceCode)
                    .Select(c => c.Description)
                    .FirstOrDefault();

                switch (hcaiItemType.RealServiceCode)
                {
                    case "AXXCT":
                        hcaiItemType.ItemDescription = "Claimant transportation";
                        break;
                    case "2AZ02":
                        hcaiItemType.ItemDescription = "Assessment, mental health";
                        break;
                    case "3SC10":
                        hcaiItemType.ItemDescription = "X-Ray Spinal Vertebrae";
                        break;
                    default:
                        if (!string.IsNullOrEmpty(newDesc))
                            hcaiItemType.ItemDescription = newDesc;
                        else if (hcaiItemType.ItemDescription is null)
                            hcaiItemType.ItemDescription = string.Empty;
                        break;
                }
            }

            // save
            if (dateOcf18Max != dateOcf18Start)
            {
                initInfo.ActivityOcf18 = dateOcf18Max;
            }
            if (dateOcf23Max != dateOcf23Start)
            {
                initInfo.ActivityOcf23 = dateOcf23Max;
            }
            if (dateOcf21BMax != dateOcf21BStart)
            {
                initInfo.ActivityOcf21b = dateOcf21BMax;
            }
            if (dateOcf21CMax != dateOcf21CStart)
            {
                initInfo.ActivityOcf21c = dateOcf21CMax;
            }
            if (dateAcsiMax != dateAcsiStart)
            {
                initInfo.ActivityAcsi = dateAcsiMax;
            }

            SourceDb.SyncContext.SaveChangesWithDetect();

            // end
            VmMain.InProcess = false;
            VmMain.TaskInfo = string.Empty;
        }

        #endregion
        
        #region Service Clients

        private void LoadUniquePatientsAndCaseInfo()
        {
            VmMain.InProcess = true;

            try
            {
                // prepare dates
                var initInfo = InitInfo();
                var dateOcf18Start = initInfo.PatientsOcf18;
                var dateOcf23Start = initInfo.PatientsOcf23;
                var dateForm1Start = initInfo.PatientsForm1;
                var dateOcf21BStart = initInfo.PatientsOcf21b;
                var dateOcf21CStart = initInfo.PatientsOcf21c;
                var dateAcsiStart = initInfo.PatientsAcsi;
                var dateOcf18Max = dateOcf18Start;
                var dateOcf23Max = dateOcf23Start;
                var dateForm1Max = dateForm1Start;
                var dateOcf21BMax = dateOcf21BStart;
                var dateOcf21CMax = dateOcf21CStart;
                var dateAcsiMax = dateAcsiStart;

                // 0 - prepare actual sets
                var ocf18Set =
                    (from submit in SyncContext.OCF18Submit
                     join arDocRec in SyncContext.OCF18AR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                     from arDoc in arDocs.DefaultIfEmpty()
                     where dateOcf18Start == null || arDoc == null || arDoc.Submission_Date >= dateOcf18Start
                     select new Ocf18Model
                     {
                         Submit = submit,
                         SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                         OcfDate = arDoc != null
                         ? arDoc.Adjuster_Response_Date
                         : submit.OCF18_ApplicantSignature_SigningDate ?? submit.Header_DateOfAccident,
                     })
                    .ToList();
                var ocf23Set =
                    (from submit in SyncContext.OCF23Submit
                     join arDocRec in SyncContext.OCF23AR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                     from arDoc in arDocs.DefaultIfEmpty()
                     where dateOcf23Start == null || arDoc == null || arDoc.Submission_Date >= dateOcf23Start
                     select new Ocf23Model
                     {
                         Submit = submit,
                         SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                         OcfDate = arDoc != null
                         ? arDoc.Adjuster_Response_Date
                         : submit.OCF23_ApplicantSignature_SigningDate ?? submit.Header_DateOfAccident,
                     })
                    .ToList();
                var form1Set =
                    (from submit in SyncContext.AACNSubms
                        join arDocRec in SyncContext.AACNARs on submit.DocumentNumber equals arDocRec.DocumentNumber into arDocs
                        from arDoc in arDocs.DefaultIfEmpty()
                        where dateOcf23Start == null || arDoc == null || arDoc.VersionDate >= dateOcf23Start
                        select new Form1Model
                        {
                            Submit = submit,
                        })
                    .ToList();
                foreach (var model in form1Set)
                {
                    model.SubmissionDate = Service.HcaiNum2Date(model.Submit.DocumentNumber);
                }

                var ocf21BSet =
                    (from submit in SyncContext.OCF21BSubmit
                     join arDocRec in SyncContext.OCF21BAR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                     from arDoc in arDocs.DefaultIfEmpty()
                     where dateOcf21BStart == null || arDoc.Submission_Date >= dateOcf21BStart
                     select new Ocf21BModel
                     {
                         Submit = submit,
                         SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                         OcfDate = arDoc != null ? arDoc.Adjuster_Response_Date : submit.Header_DateOfAccident,
                     })
                    .ToList();
                var ocf21CSet =
                    (from submit in SyncContext.OCF21CSubmit
                     join arDocRec in SyncContext.OCF21CAR on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                     from arDoc in arDocs.DefaultIfEmpty()
                     where dateOcf21CStart == null || arDoc.Submission_Date >= dateOcf21CStart
                     select new Ocf21CModel
                     {
                         Submit = submit,
                         SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                         OcfDate = arDoc != null ? arDoc.Adjuster_Response_Date : submit.Header_DateOfAccident,
                     })
                    .ToList();

                var acsiSet =
                    (from submit in SyncContext.ACSISubmits
                        join arDocRec in SyncContext.ACSIARs on submit.HCAI_Document_Number equals arDocRec.HCAI_Document_Number into arDocs
                        from arDoc in arDocs.DefaultIfEmpty()
                        where dateAcsiStart == null || arDoc.Submission_Date >= dateAcsiStart
                        select new AcsiModel
                        {
                            Submit = submit,
                            SubmissionDate = arDoc != null ? arDoc.Submission_Date : null,
                            OcfDate = arDoc != null ? arDoc.Adjuster_Response_Date : submit.Header_DateOfAccident,
                        })
                    .ToList();

                //
                VmMain.TaskInfo = "Init patients (from OCF18Submit)";
                VmMain.ProgressBarValue = 0;
                var ocf18s = ocf18Set
                    .Where(s => dateOcf18Start == null || s.SubmissionDate == null || s.SubmissionDate >= dateOcf18Start)
                    .ToList();
                VmMain.ProgressBarMax = ocf18s.Count;
                foreach (var info in ocf18s)
                {
                    ++VmMain.ProgressBarValue;
                    VmMain.StepInfo = info.Submit.HCAI_Document_Number;
                    SyncPatient.MakeHcaiPatientsInfo(this, SourceDb.SyncPatients, info.Submit, true);
                    
                    var date = info.SubmissionDate ?? Service.HcaiNum2Date(info.Submit.HCAI_Document_Number);
                    if (dateOcf18Max is null || dateOcf18Max < date)
                    {
                        dateOcf18Max = date;
                    }
                }

                VmMain.TaskInfo = "Init patients (from OCF23Submit)";
                VmMain.ProgressBarValue = 0;
                var ocf23s = ocf23Set
                    .Where(s => dateOcf23Start == null || s.SubmissionDate == null || s.SubmissionDate >= dateOcf23Start)
                    .ToList();
                VmMain.ProgressBarMax = ocf23s.Count;
                foreach (var info in ocf23s)
                {
                    ++VmMain.ProgressBarValue;
                    VmMain.StepInfo = info.Submit.HCAI_Document_Number;
                    SyncPatient.MakeHcaiPatientsInfo(this, SourceDb.SyncPatients, info.Submit, true);

                    var date = info.SubmissionDate ?? Service.HcaiNum2Date(info.Submit.HCAI_Document_Number);
                    if (dateOcf23Max is null || dateOcf23Max < date)
                    {
                        dateOcf23Max = date;
                    }
                }
                
                VmMain.TaskInfo = "Init patients (from AACNSubms)";
                VmMain.ProgressBarValue = 0;
                var form1s = form1Set
                    .Where(s => dateForm1Start == null || s.SubmissionDate >= dateForm1Start)
                    .ToList();
                VmMain.ProgressBarMax = form1s.Count;
                foreach (var info in form1s)
                {
                    ++VmMain.ProgressBarValue;
                    VmMain.StepInfo = info.Submit.DocumentNumber;
                    SyncPatient.MakeHcaiPatientsInfo(this, SourceDb.SyncPatients, info.Submit, true);

                    var date = info.SubmissionDate.Value;
                    if (dateForm1Max is null || dateForm1Max < date)
                    {
                        dateForm1Max = date;
                    }
                }

                VmMain.TaskInfo = "Init patients (from OCF21BSubmit)";
                VmMain.ProgressBarValue = 0;
                var ocf21Bs = ocf21BSet
                    .Where(s => dateOcf21BStart == null || s.SubmissionDate == null || s.SubmissionDate >= dateOcf21BStart)
                    .ToList();
                VmMain.ProgressBarMax = ocf21Bs.Count;
                foreach (var info in ocf21Bs)
                {
                    VmMain.StepInfo = info.Submit.HCAI_Document_Number;
                    SyncPatient.MakeHcaiPatientsInfo(this, SourceDb.SyncPatients, info.Submit, true);

                    var date = info.SubmissionDate ?? Service.HcaiNum2Date(info.Submit.HCAI_Document_Number);
                    if (dateOcf21BMax is null || dateOcf21BMax < date)
                    {
                        dateOcf21BMax = date;
                    }
                }

                VmMain.TaskInfo = "Init patients (from OCF21CSubmit)";
                VmMain.ProgressBarValue = 0;
                var ocf21Cs = ocf21CSet
                    .Where(s => dateOcf21CStart == null || s.SubmissionDate == null || s.SubmissionDate >= dateOcf21CStart)
                    .ToList();
                VmMain.ProgressBarMax = ocf21Cs.Count;
                foreach (var info in ocf21Cs)
                {
                    VmMain.StepInfo = info.Submit.HCAI_Document_Number;
                    SyncPatient.MakeHcaiPatientsInfo(this, SourceDb.SyncPatients, info.Submit, true);

                    var date = info.SubmissionDate ?? Service.HcaiNum2Date(info.Submit.HCAI_Document_Number);
                    if (dateOcf21CMax is null || dateOcf21CMax < date)
                    {
                        dateOcf21CMax = date;
                    }
                }

                VmMain.TaskInfo = "Init patients (from ACSISubmit)";
                VmMain.ProgressBarValue = 0;
                var acsiInfos = acsiSet
                    .Where(s => dateAcsiStart == null || s.SubmissionDate == null || s.SubmissionDate >= dateAcsiStart)
                    .ToList();
                VmMain.ProgressBarMax = acsiInfos.Count;
                foreach (var info in acsiInfos)
                {
                    VmMain.StepInfo = info.Submit.HCAI_Document_Number;
                    SyncPatient.MakeHcaiPatientsInfo(this, SourceDb.SyncPatients, info.Submit, true);

                    var date = info.SubmissionDate ?? Service.HcaiNum2Date(info.Submit.HCAI_Document_Number);
                    if (dateAcsiMax is null || dateAcsiMax < date)
                    {
                        dateAcsiMax = date;
                    }
                }

                // save
                if (dateOcf18Start != dateOcf18Max)
                {
                    initInfo.PatientsOcf18 = dateOcf18Max;
                }
                if (dateOcf23Start != dateOcf23Max)
                {
                    initInfo.PatientsOcf23 = dateOcf23Max;
                }
                if (dateForm1Start != dateForm1Max)
                {
                    initInfo.PatientsForm1 = dateForm1Max;
                }
                if (dateOcf21BStart != dateOcf21BMax)
                {
                    initInfo.PatientsOcf21b = dateOcf21BMax;
                }
                if (dateOcf21CStart != dateOcf21CMax)
                {
                    initInfo.PatientsOcf21c = dateOcf21CMax;
                }
                if (dateAcsiStart != dateAcsiMax)
                {
                    initInfo.PatientsAcsi = dateAcsiMax;
                }

                SourceDb.SyncContext.SaveChangesWithDetect();

                // end
                VmMain.TaskInfo = string.Empty;
            }
            finally
            {
                VmMain.InProcess = false;
            }
        }

        #endregion

        public void AddInvoiceCharges(InvoceGeneralInfo invoice)
        {
            try
            {
                var invoiceId = invoice.InvoiceID;
                var date = invoice.InvoiceDate;
                var caseId = invoice.CaseID;
                var reportId = invoice.ReportID;

                foreach (var invoiceItem in invoice.InvoiceItemDetails)
                {
                    var provider = TargetDb.Resources
                        .FirstOrDefault(r => r.Backend.resource_id == invoiceItem.ItemProviderID);

                    var charge = UoContext.ClientCaseCharges
                        .FirstOrDefault(c => c.InvoiceID == invoiceId
                                             && c.ChargeDate == date
                                             && c.ItemCategory == invoiceItem.ItemCategory
                                             && c.ItemType == invoiceItem.ItemType
                                             && c.ItemID == invoiceItem.ItemID
                                             && c.ItemDescription == invoiceItem.ItemDescription
                                             && c.ItemQty == invoiceItem.ItemQty
                                             && c.ItemRate == invoiceItem.ItemRate
                                             && c.GST == invoiceItem.GST
                                             && c.PST == invoiceItem.PST
                                             && c.ItemTaxID == invoiceItem.ItemTaxID
                                             && c.ItemServiceCode == invoiceItem.ItemServiceCode
                                             && c.ItemServiceCodeDescription == invoiceItem.ItemServiceCodeDescription
                                             && c.ItemMeasure == invoiceItem.ItemMeasure
                                             && c.ItemBaseUnit == invoiceItem.ItemBaseUnit
                                             && c.ItemOccupation == invoiceItem.ItemOccupation
                                             && c.ItemAttribute == invoiceItem.ItemAttribute
                                             && c.ItemProviderID == invoiceItem.ItemProviderID);
                    if (charge != null) continue;

                    UoContext.ClientCaseCharges.Add(new ClientCaseCharge
                    {
                        InvoiceID = invoiceId,
                        BilledToStatus = 1,
                        ReportID = reportId,
                        CaseID = caseId,
                        CaseType = "MVA",
                        ChargeDate = date,

                        ItemCategory = invoiceItem.ItemCategory ?? string.Empty,
                        ItemType = invoiceItem.ItemType ?? string.Empty,
                        ItemID = invoiceItem.ItemID,
                        ItemDescription = invoiceItem.ItemDescription ?? string.Empty,
                        ItemQty = invoiceItem.ItemQty,
                        ItemRate = invoiceItem.ItemRate,
                        GST = invoiceItem.GST,
                        PST = invoiceItem.PST,
                        ItemTaxID = invoiceItem.ItemTaxID,
                        ItemServiceCode = invoiceItem.ItemServiceCode,
                        ItemServiceCodeDescription = invoiceItem.ItemServiceCodeDescription,
                        ItemMeasure = invoiceItem.ItemMeasure,
                        ItemBaseUnit = invoiceItem.ItemBaseUnit,
                        ItemOccupation = invoiceItem.ItemOccupation,
                        ItemAttribute = invoiceItem.ItemAttribute,
                        ItemProviderID = invoiceItem.ItemProviderID,
                        ItemProviderCode = invoiceItem.ItemProviderCode,
                        ItemProviderDisplay = provider != null
                            ? $"{provider.StaffName} - {invoiceItem.ItemProviderCode}"
                            : "",

                        OHIPServiceLocationIndicator = "",
                    });
                }
                
                UoContext.SaveChangesWithDetect();
            }
            catch (Exception ex)
            {
                Logger.Error($"{nameof(AddInvoiceCharges)}", ex);
            }
        }

        public void AddInvoicePayment(string caseId, InvoceGeneralInfo invoice, DateTime date, double amount)
        {
            try
            {
                var invoiceId = invoice.InvoiceID;
                var idPayment = UoContext.PaymentInfoes.Any()
                    ? UoContext.PaymentInfoes.Select(p => p.PaymentID).Max() + 1
                    : 1;

                var paymentRelationInfo = UoContext.PaidInvoiceRelations
                    .Include("PaymentInfo")
                    .FirstOrDefault(p => p.InvoiceID == invoiceId
                                         && p.PaymentInfo.PmtType == "MVA"
                                         && p.PaymentInfo.PmtMethod == "Cheque (MVA)");
                var paymentInfo = paymentRelationInfo?.PaymentInfo;

                // I - add payment
                paymentInfo ??= UoContext.PaymentInfoes.Add(new PaymentInfo
                {
                    PaymentID = idPayment,
                    CaseID = caseId,
                    PmtDate = date.Date,
                    PmtAmount = amount,
                    PmtType = "MVA",
                    PmtMethod = "Cheque (MVA)",
                    PmtMemo = "Cheque (MVA)",
                    RecordCreationDate = DateTime.Now,

                    Client_Case = UoContext.Client_Case.FirstOrDefault(c => c.CaseID == caseId),

                    OhipRemittanceFileName = string.Empty,
                    OhipAccountingNumber = string.Empty,
                });

                // II - add relation
                paymentRelationInfo ??= UoContext.PaidInvoiceRelations.Add(new PaidInvoiceRelation
                {
                    PaymentID = idPayment,
                    InvoiceID = invoiceId,
                    InvoceGeneralInfo = invoice,
                    PaymentInfo = paymentInfo,
                });
                paymentRelationInfo.PaidAmount = amount;

                // III - distribute Payment by Invoice`s items
                if (amount > 0)
                {
                    var items = UoContext.InvoiceItemDetails
                        .Where(t => t.InvoiceID == invoiceId)
                        .OrderBy(t => t.ItemOrder)
                        .ToList();
                    foreach (var itemDetail in items)
                    {
                        var approvedAmount = itemDetail.aItemAmount ?? 0;
                        if (approvedAmount <= 0) continue;

                        var pAmount = amount >= approvedAmount
                            ? approvedAmount
                            : amount;

                        TargetDb.Connection.AddInvoiceItemDistributePayment(invoiceId, idPayment, itemDetail.RecordID, pAmount);

                        amount = (amount - pAmount).RoundTo();
                        if (amount <= 0) break;
                    }
                }

                // save
                UoContext.SaveChangesWithDetect();
            }
            catch (Exception ex)
            {
                Logger.Error($"{nameof(AddInvoicePayment)}", ex);
            }
        }

        #region EHC coverages

        public void CheckEhcCoverage(string caseId, OCF18Submit ocfSubmit, double reportId)
        {
            var ehc1CompanyName = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer1_Name;
            var ehc2CompanyName = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer2_Name;
            var isEhc1 = !string.IsNullOrEmpty(ehc1CompanyName);
            var isEhc2 = !string.IsNullOrEmpty(ehc2CompanyName);
            var dol = ocfSubmit.Header_DateOfAccident;
            var dob = ocfSubmit.Applicant_DateOfBirth;

            if (isEhc1)
            {
                var certNo = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer1_ID;
                var groupNo = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
                var firstName = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName;
                var lastName = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName;
                var isPolicyHolder = (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                                     || (firstName.ToLower() == ocfSubmit.Applicant_Name_FirstName.ToLower()
                                         && lastName.ToLower() == ocfSubmit.Applicant_Name_LastName.ToLower());

                var changed = CheckEhcCoverage(caseId, "OCF18", true, dol, dob,
                    ref ehc1CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.OCF18.Find(reportId);
                    doc.EHC1InsName = ehc1CompanyName;
                    doc.EHC1PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC1PolicyNo = groupNo;
                    doc.EHC1IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }

            if (isEhc2)
            {
                var certNo = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer2_ID;
                var groupNo = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
                var firstName = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName;
                var lastName = ocfSubmit.OCF18_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName;
                var isPolicyHolder = false;

                var changed = CheckEhcCoverage(caseId, "OCF18", false, dol, dob,
                    ref ehc2CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.OCF18.Find(reportId);
                    doc.EHC2InsName = ehc2CompanyName;
                    doc.EHC2PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC2PolicyNo = groupNo;
                    doc.EHC2IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }
        }

        public void CheckEhcCoverage(string caseId, OCF23Submit ocfSubmit, double reportId)
        {
            var ehc1CompanyName = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer1_Name;
            var ehc2CompanyName = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer2_Name;
            var isEhc1 = !string.IsNullOrEmpty(ehc1CompanyName);
            var isEhc2 = !string.IsNullOrEmpty(ehc2CompanyName);
            var dol = ocfSubmit.Header_DateOfAccident;
            var dob = ocfSubmit.Applicant_DateOfBirth;

            if (isEhc1)
            {
                var certNo = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer1_ID;
                var groupNo = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
                var firstName = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName;
                var lastName = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName;
                var isPolicyHolder = (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                                     || (firstName.ToLower() == ocfSubmit.Applicant_Name_FirstName.ToLower()
                                         && lastName.ToLower() == ocfSubmit.Applicant_Name_LastName.ToLower());

                var changed = CheckEhcCoverage(caseId, "OCF23", true, dol, dob,
                    ref ehc1CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.OCF23.Find(reportId);
                    doc.EHC1InsName = ehc1CompanyName;
                    doc.EHC1PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC1PolicyNo = groupNo;
                    doc.EHC1IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }

            if (isEhc2)
            {
                var certNo = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer2_ID;
                var groupNo = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
                var firstName = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName;
                var lastName = ocfSubmit.OCF23_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName;
                var isPolicyHolder = false;

                var changed = CheckEhcCoverage(caseId, "OCF23", false, dol, dob,
                    ref ehc2CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.OCF23.Find(reportId);
                    doc.EHC2InsName = ehc2CompanyName;
                    doc.EHC2PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC2PolicyNo = groupNo;
                    doc.EHC2IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }
        }
        
        public void CheckEhcCoverage(string caseId, OCF21BSubmit ocfSubmit, double invoiceId)
        {
            var ehc1CompanyName = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer1_Name;
            var ehc2CompanyName = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer2_Name;
            var isEhc1 = !string.IsNullOrEmpty(ehc1CompanyName);
            var isEhc2 = !string.IsNullOrEmpty(ehc2CompanyName);
            var dol = ocfSubmit.Header_DateOfAccident;
            var dob = ocfSubmit.Applicant_DateOfBirth;

            if (isEhc1)
            {
                var certNo = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer1_ID;
                var groupNo = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
                var firstName = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName;
                var lastName = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName;
                var isPolicyHolder = (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                                     || (firstName.ToLower() == ocfSubmit.Applicant_Name_FirstName.ToLower()
                                         && lastName.ToLower() == ocfSubmit.Applicant_Name_LastName.ToLower());

                var changed = CheckEhcCoverage(caseId, "OCF21B", true, dol, dob,
                    ref ehc1CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.MVAInvoices.Find(invoiceId);
                    doc.EHC1 = ehc1CompanyName;
                    doc.EHC1PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC1PolicyNo = groupNo;
                    doc.EHC1IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }

            if (isEhc2)
            {
                var certNo = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer2_ID;
                var groupNo = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
                var firstName = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName;
                var lastName = ocfSubmit.OCF21B_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName;
                var isPolicyHolder = false;

                var changed = CheckEhcCoverage(caseId, "OCF21B", false, dol, dob,
                    ref ehc2CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.MVAInvoices.Find(invoiceId);
                    doc.EHC2 = ehc2CompanyName;
                    doc.EHC2PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC2PolicyNo = groupNo;
                    doc.EHC2IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }
        }

        public void CheckEhcCoverage(string caseId, ACSISubmit ocfSubmit, double invoiceId)
        {
            var ehc1CompanyName = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer1_Name;
            var ehc2CompanyName = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer2_Name;
            var isEhc1 = !string.IsNullOrEmpty(ehc1CompanyName);
            var isEhc2 = !string.IsNullOrEmpty(ehc2CompanyName);
            var dol = ocfSubmit.Header_DateOfAccident;
            var dob = ocfSubmit.Applicant_DateOfBirth;

            if (isEhc1)
            {
                var certNo = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer1_ID;
                var groupNo = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber;
                var firstName = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName;
                var lastName = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName;
                var isPolicyHolder = (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName))
                                     || (firstName.ToLower() == ocfSubmit.Applicant_Name_FirstName.ToLower()
                                         && lastName.ToLower() == ocfSubmit.Applicant_Name_LastName.ToLower());

                var changed = CheckEhcCoverage(caseId, "OCF21B", true, dol, dob,
                    ref ehc1CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.MVAInvoices.Find(invoiceId);
                    doc.EHC1 = ehc1CompanyName;
                    doc.EHC1PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC1PolicyNo = groupNo;
                    doc.EHC1IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }

            if (isEhc2)
            {
                var certNo = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer2_ID;
                var groupNo = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber;
                var firstName = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName;
                var lastName = ocfSubmit.ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName;
                var isPolicyHolder = false;

                var changed = CheckEhcCoverage(caseId, "OCF21B", false, dol, dob,
                    ref ehc2CompanyName, ref certNo, ref groupNo, ref firstName, ref lastName, isPolicyHolder);
                if (changed)
                {
                    var doc = UoContext.MVAInvoices.Find(invoiceId);
                    doc.EHC2 = ehc2CompanyName;
                    doc.EHC2PlanMember = !string.IsNullOrEmpty(lastName)
                        ? $"{lastName}, {firstName}"
                        : string.Empty;
                    doc.EHC2PolicyNo = groupNo;
                    doc.EHC2IDCertNo = certNo;
                    UoContext.SaveChangesWithDetect();
                }
            }
        }


        private bool CheckEhcCoverage(string caseId, string byDoc, bool isEhc1, DateTime dol, DateTime dob,
            ref string ehcCompanyName,
            ref string certNo, ref string groupNo,
            ref string firstName, ref string lastName,
            bool isPolicyHolder)
        {
            var yearStart = dol.Year;
            var whichEHC = $"EHC{(isEhc1 ? 1 : 2)}";

            var idCertNo = certNo;
            var policyNo = groupNo;

            // validate
            var isData = !string.IsNullOrEmpty(certNo) || !string.IsNullOrEmpty(groupNo);
            var docChanged = CheckCompanyName(ref ehcCompanyName);
            var companyName = ehcCompanyName;

            try
            {
                var find = UoContext.ClientCaseEHCCoverages
                    .FirstOrDefault(c => c.CaseID == caseId
                                         && c.EHCCompanyName.ToLower() == companyName.ToLower()
                                         && c.WhichEHC == whichEHC
                                         && (!isData
                                             || (c.EHCIDCertNo == idCertNo && c.EHCPGroupNo == policyNo)
                                             || (c.EHCIDCertNo == "" && c.EHCPGroupNo == ""))
                                         );
                if (find != null)
                {
                    var changed = false;
                    if (find.YearStart > yearStart)
                    {
                        find.YearStart = yearStart;
                        changed = true;
                    }

                    if (!string.IsNullOrEmpty(certNo) && string.IsNullOrEmpty(find.EHCIDCertNo))
                    {
                        find.EHCIDCertNo = certNo;
                        changed = true;
                    }
                    else if (string.IsNullOrEmpty(certNo) && !string.IsNullOrEmpty(find.EHCIDCertNo))
                    {
                        certNo = find.EHCIDCertNo;
                        docChanged = true;
                    }

                    if (!string.IsNullOrEmpty(groupNo) && string.IsNullOrEmpty(find.EHCPGroupNo))
                    {
                        find.EHCPGroupNo = groupNo;
                        changed = true;
                    }
                    else if (string.IsNullOrEmpty(groupNo) && !string.IsNullOrEmpty(find.EHCPGroupNo))
                    {
                        groupNo = find.EHCPGroupNo;
                        docChanged = true;
                    }

                    if (string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(find.EHCFirstName))
                    {
                        firstName = find.EHCFirstName;
                        docChanged = true;
                    }
                    if (string.IsNullOrEmpty(lastName) && !string.IsNullOrEmpty(find.EHCLastName))
                    {
                        lastName = find.EHCLastName;
                        docChanged = true;
                    }

                    if (changed)
                    {
                        UoContext.SaveChangesWithDetect();
                        Logger.Debug($"  updated {nameof(ClientCaseEHCCoverage)} {whichEHC} for {caseId} (from {byDoc}): to yearStart={yearStart} (certNo='{certNo}' groupNo='{groupNo}')");
                    }

                    return docChanged;
                }

                var thirdParty = UoContext.Third_Party
                    .FirstOrDefault(c => c.CompanyName == companyName
                        && c.Type.Contains("EHC"));

                find = new ClientCaseEHCCoverage
                {
                    CaseID = caseId,
                    WhichEHC = whichEHC,
                    Status = 1,
                    YearStart = yearStart,
                    EHCCompanyName = ehcCompanyName,
                    EHCIDCertNo = certNo,
                    EHCPGroupNo = groupNo,
                    EHCPolicyHolder = (byte)(isEhc1 && isPolicyHolder ? 1 : 0),
                    EHCFirstName = firstName,
                    EHCLastName = lastName,
                    EHCDOB = dob,

                    EHCTPID = thirdParty?.ThirdPartyID,
                    EHCClaimForm = thirdParty?.eClaimsDisplayCompanyName,

                    EHCTPContactID = null,
                    EHCAddress = null,
                    EHCCity = null,
                    EHCProvince = null,
                    EHCPostalCode = null,
                    EHCCountry = null,
                    EHCTel = null,
                    EHCFax = null,
                };

                UoContext.ClientCaseEHCCoverages.Add(find);
                UoContext.SaveChangesWithDetect();

                Logger.Debug($"  added {nameof(ClientCaseEHCCoverage)} {whichEHC} for {caseId} (from {byDoc}): yearStart={yearStart} certNo='{certNo}' groupNo='{groupNo}'");
                return docChanged;
            }
            catch (Exception ex)
            {
                Logger.Error($"{nameof(CheckEhcCoverage)}", ex);
                return false;
            }
        }

        private bool CheckCompanyName(ref string ehcCompanyName)
        {
            if (ehcCompanyName.ToLower() == "bluecross" || ehcCompanyName.ToLower() == "Blue Cross (Medavie)".ToLower())
            {
                ehcCompanyName = "Blue Cross";
                return true;
            }

            if (ehcCompanyName.ToLower() == "manulife" || ehcCompanyName.ToLower() == "manu life")
            {
                ehcCompanyName = "Manulife Financial";
                return true;
            }

            if (ehcCompanyName.ToLower() == "Sunlife".ToLower()
                || ehcCompanyName.ToLower() == "Sunlife 9 Student Care)".ToLower()
                || ehcCompanyName.ToLower() == "Sun Life ( STUDENT CARE)".ToLower()
                || ehcCompanyName.ToLower() == "Sun Life".ToLower())
            {
                ehcCompanyName = "Sun Life Financial";
                return true;
            }

            if (ehcCompanyName.ToLower() == "Green shield Canada".ToLower()
                || ehcCompanyName.ToLower() == "Green Sheild".ToLower()
                || ehcCompanyName.ToLower() == "Green shield".ToLower()
                || ehcCompanyName.ToLower() == "Green shield/Encon".ToLower()
                || ehcCompanyName.ToLower() == "GreenSheild".ToLower()
                || ehcCompanyName.ToLower() == "Greenshield".ToLower())
            {
                ehcCompanyName = "Green Shield Canada";
                return true;
            }

            if (ehcCompanyName == "Canada life"
                || ehcCompanyName == "canada life"
                || ehcCompanyName.ToLower() == "Canada".ToLower()
                || ehcCompanyName.ToLower() == "canadalife".ToLower()
                || ehcCompanyName.ToLower() == "Canada Life 2".ToLower())
            {
                ehcCompanyName = "Canada Life";
                return true;
            }

            if (ehcCompanyName.ToLower() == "Desjardins Insurance - EHC".ToLower())
            {
                ehcCompanyName = "Desjardins Insurance";
                return true;
            }

            if (ehcCompanyName.ToLower() == "GWL".ToLower() || ehcCompanyName.ToLower() == "Great West Life".ToLower())
            {
                ehcCompanyName = "Great-West Life";
                return true;
            }

            if (ehcCompanyName.ToLower() == "SSQ".ToLower())
            {
                ehcCompanyName = "SSQ Insurance";
                return true;
            }

            if (ehcCompanyName.ToLower() == "equitable life".ToLower())
            {
                ehcCompanyName = "Equitable Life of Canada";
                return true;
            }

            if (ehcCompanyName.ToLower() == "johnston".ToLower())
            {
                ehcCompanyName = "Johnson Inc.";
                return true;
            }

            if (ehcCompanyName.ToLower() == "RwAM".ToLower()
                || ehcCompanyName.ToLower() == "RWAM Insurance Administrators".ToLower()
                || ehcCompanyName.ToLower() == "rwan".ToLower())
            {
                ehcCompanyName = "RWAM Insurance Administrators Inc.";
                return true;
            }

            if (ehcCompanyName.ToLower() == "cooperators".ToLower())
            {
                ehcCompanyName = "The Co-operators";
                return true;
            }

            if (ehcCompanyName.ToLower() == "Advantage maximum benefits".ToLower())
            {
                ehcCompanyName = "Maximum Benefit";
                return true;
            }

            if (ehcCompanyName.ToLower() == "OTHER EHC".ToLower())
            {
                ehcCompanyName = "Other Healthcare Insurance Company";
                return true;
            }

            if (ehcCompanyName.ToLower() == "union".ToLower())
            {
                ehcCompanyName = "Union Benefits";
                return true;
            }

            if (ehcCompanyName.ToLower() == "MDM".ToLower())
            {
                ehcCompanyName = "MDM Insurance Services Inc.";
                return true;
            }

            return false;
        }

        #endregion
    }
}
