﻿namespace ExtractHcaiData.Uo.Work
{
    public interface ITask
    {
        int Order { get; }

        string Name { get; }

        void Run();

        void InWorking();
    }
}
