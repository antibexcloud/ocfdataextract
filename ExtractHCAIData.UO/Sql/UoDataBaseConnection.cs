﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ExtractHcaiData.Uo.Model;
using ExtractHcaiData.Uo.UoData;
using ExtractHcaiData.Uo.Utils;
using HcaiSync.Common.EfData;
using HcaiSync.Common.Sql;

namespace ExtractHcaiData.Uo.Sql
{
    public class UoDataBaseConnection : AbstractDataBaseConnection
    {
        public UoDataBaseConnection(string connectString) : base(connectString)
        {
        }

        public void UpdateMetadata()
        {
            RunMySqlScripts("ExtractHcaiData.Uo.UoData.Sql.updates.sql", typeof(UoDataBaseConnection).Assembly);
        }

        public DateTime AddWorkDays(DateTime d, int w, bool includeHolidays)
        {
            var result = new DateTime(1990, 1, 1);
            if (includeHolidays)
                try
                {
                    var wd = 0;
                    using (var cmd = new SqlCommand("SELECT top 1 *" +
                                                    "  FROM CalendarTable" +
                                                    "  WHERE [Date] = @date", ConnectionDb))
                    {
                        cmd.Parameters.Add(new SqlParameter("@date", SqlDbType.DateTime)).Value = d.PrepareDateTime(false);
                        var res = TryFillDataTable(cmd).Rows.OfType<DataRow>().FirstOrDefault();

                        if (res != null)
                            wd = ServiceConvert.GetInteger(res["WDay"]) + w;
                        else
                            throw new Exception($"No records in [CalendarTable] on {d}!");
                    }

                    //
                    using (var cmd = new SqlCommand("SELECT top 1 *" +
                                                    "  FROM CalendarTable" +
                                                    "  WHERE WDay = @wd" +
                                                    "    AND WeekDay <> 's'" +
                                                    "    AND IsHoliday = 0", ConnectionDb))
                    {
                        cmd.Parameters.Add(new SqlParameter("@wd", SqlDbType.Int)).Value = wd;
                        var res = TryFillDataTable(cmd).Rows.OfType<DataRow>().FirstOrDefault();

                        if (res != null)
                            result = ServiceConvert.GetDateTime(res["Date"]);
                    }

                    return result;
                }
                catch{}

            //
            var dd = DateTime.FromOADate(0);

            dd = d;
            var i = 0;
            while (i != w)
            {
                dd = dd.AddDays(1);
                if (dd.DayOfWeek != DayOfWeek.Sunday)
                    i++;
            }

            result = dd;
            return result;
        }

        public void SaveOcfInjuries(double reportId, string injuryCode, string codeDescription, int iOrder)
        {
            using var cmd = new SqlCommand("if not exists(select ReportID from OCFInjuries where ReportID = @ReportID and Code = @Code)" +
                                           "INSERT OCFInjuries (ReportID, Code, CodeDescription, iOrder)" +
                                           "  values (@ReportID, @Code, @CodeDescription, @iOrder)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@ReportID", SqlDbType.Float)).Value = reportId;
            cmd.Parameters.Add(new SqlParameter("@Code", SqlDbType.NVarChar)).Value = injuryCode;
            cmd.Parameters.Add(new SqlParameter("@CodeDescription", SqlDbType.NVarChar)).Value = codeDescription;
            cmd.Parameters.Add(new SqlParameter("@iOrder", SqlDbType.Int)).Value = iOrder;
            TryExecuteNonQuery(cmd);
        }

        public void SaveInvoiceInjuries(double invoiceId, string injuryCode, string codeDescription, int iOrder)
        {
            using var cmd = new SqlCommand("if not exists(select InvoiceID from InvoiceInjuries where InvoiceID = @InvoiceID and Code = @Code)" +
                                           "INSERT InvoiceInjuries (InvoiceID, Code, Description, iOrder)" +
                                           "  values (@InvoiceID, @Code, @Description, @iOrder)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", SqlDbType.Float)).Value = invoiceId;
            cmd.Parameters.Add(new SqlParameter("@Code", SqlDbType.NVarChar)).Value = injuryCode;
            cmd.Parameters.Add(new SqlParameter("@Description", SqlDbType.NVarChar)).Value = codeDescription;
            cmd.Parameters.Add(new SqlParameter("@iOrder", SqlDbType.Int)).Value = iOrder;
            TryExecuteNonQuery(cmd);
        }

        public void SaveForm1Parts123(FORM1_Parts123 part)
        {
            using var cmd = new SqlCommand("if not exists (select * from dbo.FORM1_Parts123" +
                                           "  where [ReportID] = @ReportID and [gPartNo] = @gPartNo and [ACSItemID] = @ACSItemID)" + Environment.NewLine +
                                           "INSERT dbo.FORM1_Parts123 ([ReportID], [gPartNo], [gRowOrder], [NumberOfMin], [TxWeeks]," +
                                           "                       [ACSItemID], [AttendantCareServices_Item_Approved_Minutes]," +
                                           "                       [AttendantCareServices_Item_Approved_TimesPerWeek]," +
                                           "                       [AttendantCareServices_Item_Approved_TotalMinutes]," +
                                           "                       [AttendantCareServices_Item_Approved_ReasonCode]," +
                                           "                       [AttendantCareServices_Item_Approved_ReasonDescription]," +
                                           "                       [AttendantCareServices_Item_Approved_IsItemDeclined])" +
                                           "  values (@ReportID, @gPartNo, @gRowOrder, @NumberOfMin, @TxWeeks," +
                                           "          @ACSItemID, @AttendantCareServices_Item_Approved_Minutes," +
                                           "          @AttendantCareServices_Item_Approved_TimesPerWeek," +
                                           "          @AttendantCareServices_Item_Approved_TotalMinutes," +
                                           "          @AttendantCareServices_Item_Approved_ReasonCode," +
                                           "          @AttendantCareServices_Item_Approved_ReasonDescription," +
                                           "          @AttendantCareServices_Item_Approved_IsItemDeclined)" + Environment.NewLine +
                                           "else" + Environment.NewLine +
                                           "  update dbo.FORM1_Parts123 set" +
                                           "     [NumberOfMin] = @NumberOfMin, [TxWeeks] = @TxWeeks," +
                                           "     [AttendantCareServices_Item_Approved_Minutes] = @AttendantCareServices_Item_Approved_Minutes," +
                                           "     [AttendantCareServices_Item_Approved_TimesPerWeek] = @AttendantCareServices_Item_Approved_TimesPerWeek," +
                                           "     [AttendantCareServices_Item_Approved_TotalMinutes] = @AttendantCareServices_Item_Approved_TotalMinutes," +
                                           "     [AttendantCareServices_Item_Approved_ReasonCode] = @AttendantCareServices_Item_Approved_ReasonCode," +
                                           "     [AttendantCareServices_Item_Approved_ReasonDescription] = @AttendantCareServices_Item_Approved_ReasonDescription," +
                                           "     [AttendantCareServices_Item_Approved_IsItemDeclined] = @AttendantCareServices_Item_Approved_IsItemDeclined" +
                                           "  where [ReportID] = @ReportID and [gPartNo] = @gPartNo and [ACSItemID] = @ACSItemID", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@ReportID", SqlDbType.Float)).Value = part.ReportID;
            cmd.Parameters.Add(new SqlParameter("@gPartNo", SqlDbType.TinyInt)).Value = part.gPartNo;
            cmd.Parameters.Add(new SqlParameter("@gRowOrder", SqlDbType.Int)).Value = part.gRowOrder;
            cmd.Parameters.Add(new SqlParameter("@NumberOfMin", SqlDbType.Int)).Value = part.NumberOfMin;
            cmd.Parameters.Add(new SqlParameter("@TxWeeks", SqlDbType.Int)).Value = part.TxWeeks;

            cmd.Parameters.Add(new SqlParameter("@ACSItemID", SqlDbType.NVarChar)).Value = part.ACSItemID;

            cmd.Parameters.Add(new SqlParameter("@AttendantCareServices_Item_Approved_Minutes", SqlDbType.Float)).Value =
                part.AttendantCareServices_Item_Approved_Minutes ?? 0;
            cmd.Parameters.Add(new SqlParameter("@AttendantCareServices_Item_Approved_TimesPerWeek", SqlDbType.Float)).Value =
                part.AttendantCareServices_Item_Approved_TimesPerWeek ?? 0;
            cmd.Parameters.Add(new SqlParameter("@AttendantCareServices_Item_Approved_TotalMinutes", SqlDbType.Float)).Value =
                part.AttendantCareServices_Item_Approved_TotalMinutes ?? 0;
            cmd.Parameters.Add(new SqlParameter("@AttendantCareServices_Item_Approved_ReasonCode", SqlDbType.NVarChar)).Value =
                part.AttendantCareServices_Item_Approved_ReasonCode.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@AttendantCareServices_Item_Approved_ReasonDescription", SqlDbType.NVarChar)).Value =
                part.AttendantCareServices_Item_Approved_ReasonDescription.SafetySubstring(300);
            cmd.Parameters.Add(new SqlParameter("@AttendantCareServices_Item_Approved_IsItemDeclined", SqlDbType.TinyInt)).Value =
                part.AttendantCareServices_Item_Approved_IsItemDeclined ?? 0;

            TryExecuteNonQuery(cmd);
        }

        public void AddHcaiStatusLog(HCAIStatusLog log)
        {
            using var cmd = new SqlCommand("INSERT HCAIStatusLog ([StatusOrder], [OCFType], [StatusDate], [IsCurrentStatus]," +
                                           "                      [OCFStatus], [ErrorMsg], [InvoiceID], [ReportID], [StatusTime]," +
                                           "                      [Archived], [HCAIDocNumber], [LockStatus])" +
                                           "  values (@StatusOrder, @OCFType, @StatusDate, @IsCurrentStatus," +
                                           "          @OCFStatus, @ErrorMsg, @InvoiceID, @ReportID, @StatusTime," +
                                           "          @Archived, @HCAIDocNumber, @LockStatus)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@StatusOrder", SqlDbType.Int)).Value = log.StatusOrder;
            cmd.Parameters.Add(new SqlParameter("@OCFType", SqlDbType.NVarChar)).Value = log.OCFType.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@StatusDate", SqlDbType.DateTime)).Value = log.StatusDate;
            cmd.Parameters.Add(new SqlParameter("@IsCurrentStatus", SqlDbType.Bit)).Value = log.IsCurrentStatus;

            cmd.Parameters.Add(new SqlParameter("@OCFStatus", SqlDbType.SmallInt)).Value = log.OCFStatus;
            cmd.Parameters.Add(new SqlParameter("@ErrorMsg", SqlDbType.NText)).Value = log.ErrorMsg.SafetySubstring(512);
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", SqlDbType.Float)).Value = (int?) log.InvoiceID ?? 0;//(object)DBNull.Value;
            cmd.Parameters.Add(new SqlParameter("@ReportID", SqlDbType.Float)).Value = (int?) log.ReportID ?? 0;//(object)DBNull.Value;
            cmd.Parameters.Add(new SqlParameter("@StatusTime", SqlDbType.DateTime)).Value = log.StatusTime;

            cmd.Parameters.Add(new SqlParameter("@Archived", SqlDbType.Bit)).Value = log.Archived ?? false;
            cmd.Parameters.Add(new SqlParameter("@HCAIDocNumber", SqlDbType.NVarChar)).Value = log.HCAIDocNumber.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@LockStatus", SqlDbType.TinyInt)).Value = log.LockStatus;

            TryExecuteNonQuery(cmd);
        }

        public void UpdateHcaiStatusLogInvoice(HCAIStatusLog log)
        {
            using var cmd = new SqlCommand("UPDATE HCAIStatusLog SET IsCurrentStatus = 0 WHERE InvoiceID = @InvoiceID\r\n" +
                                           "INSERT HCAIStatusLog ([StatusOrder], [OCFType], [StatusDate], [IsCurrentStatus]," +
                                           "                      [OCFStatus], [ErrorMsg], [InvoiceID], [StatusTime]," +
                                           "                      [Archived], [HCAIDocNumber], [LockStatus])" +
                                           "  values (@StatusOrder, @OCFType, @StatusDate, @IsCurrentStatus," +
                                           "          @OCFStatus, @ErrorMsg, @InvoiceID, @StatusTime," +
                                           "          @Archived, @HCAIDocNumber, @LockStatus)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@StatusOrder", SqlDbType.Int)).Value = log.StatusOrder;
            cmd.Parameters.Add(new SqlParameter("@OCFType", SqlDbType.NVarChar)).Value = log.OCFType.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@StatusDate", SqlDbType.DateTime)).Value = log.StatusDate;
            cmd.Parameters.Add(new SqlParameter("@IsCurrentStatus", SqlDbType.Bit)).Value = true;

            cmd.Parameters.Add(new SqlParameter("@OCFStatus", SqlDbType.SmallInt)).Value = log.OCFStatus;
            cmd.Parameters.Add(new SqlParameter("@ErrorMsg", SqlDbType.NText)).Value = log.ErrorMsg.SafetySubstring(512);
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", SqlDbType.Float)).Value = log.InvoiceID.Value;
            cmd.Parameters.Add(new SqlParameter("@StatusTime", SqlDbType.DateTime)).Value = log.StatusTime;

            cmd.Parameters.Add(new SqlParameter("@Archived", SqlDbType.Bit)).Value = log.Archived ?? false;
            cmd.Parameters.Add(new SqlParameter("@HCAIDocNumber", SqlDbType.NVarChar)).Value = log.HCAIDocNumber.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@LockStatus", SqlDbType.TinyInt)).Value = log.LockStatus;

            TryExecuteNonQuery(cmd);
        }

        public void UpdateHcaiStatusLogReport(HCAIStatusLog log)
        {
            using var cmd = new SqlCommand("UPDATE HCAIStatusLog SET IsCurrentStatus = 0 WHERE ReportID = @ReportID\r\n" +
                                           "INSERT HCAIStatusLog ([StatusOrder], [OCFType], [StatusDate], [IsCurrentStatus]," +
                                           "                      [OCFStatus], [ErrorMsg], [ReportID], [StatusTime]," +
                                           "                      [Archived], [HCAIDocNumber], [LockStatus])" +
                                           "  values (@StatusOrder, @OCFType, @StatusDate, @IsCurrentStatus," +
                                           "          @OCFStatus, @ErrorMsg, @ReportID, @StatusTime," +
                                           "          @Archived, @HCAIDocNumber, @LockStatus)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@StatusOrder", SqlDbType.Int)).Value = log.StatusOrder;
            cmd.Parameters.Add(new SqlParameter("@OCFType", SqlDbType.NVarChar)).Value = log.OCFType.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@StatusDate", SqlDbType.DateTime)).Value = log.StatusDate;
            cmd.Parameters.Add(new SqlParameter("@IsCurrentStatus", SqlDbType.Bit)).Value = true;

            cmd.Parameters.Add(new SqlParameter("@OCFStatus", SqlDbType.SmallInt)).Value = log.OCFStatus;
            cmd.Parameters.Add(new SqlParameter("@ErrorMsg", SqlDbType.NText)).Value = log.ErrorMsg.SafetySubstring(512);
            cmd.Parameters.Add(new SqlParameter("@ReportID", SqlDbType.Float)).Value = log.ReportID.Value;
            cmd.Parameters.Add(new SqlParameter("@StatusTime", SqlDbType.DateTime)).Value = log.StatusTime;

            cmd.Parameters.Add(new SqlParameter("@Archived", SqlDbType.Bit)).Value = log.Archived ?? false;
            cmd.Parameters.Add(new SqlParameter("@HCAIDocNumber", SqlDbType.NVarChar)).Value = log.HCAIDocNumber.SafetySubstring(50);
            cmd.Parameters.Add(new SqlParameter("@LockStatus", SqlDbType.TinyInt)).Value = log.LockStatus;

            TryExecuteNonQuery(cmd);
        }

        public OcfPatientCaseInjury[] GetOcfPatientCaseInjuries()
        {
            using var cmd = new SqlCommand("select DISTINCT P.CaseID, O.Code, O.CodeDescription, O.iOrder" +
                                           "  from Paperwork P join" +
                                           "       OCFInjuries O on P.ReportID = O.ReportID" +
                                           "  where P.SentDate = (select MAX(P2.SentDate) from Paperwork P2 where P2.CaseID = P.CaseID and P2.ReportID in (select distinct ReportID from OCFInjuries))" +
                                           "  order by P.CaseID, O.iOrder", ConnectionDb);
            return TryFillDataTable(cmd).Rows.OfType<DataRow>()
                .Select(row => new OcfPatientCaseInjury
                {
                    CaseId = ServiceConvert.GetString(row["CaseID"]),
                    Code = ServiceConvert.GetString(row["Code"]),
                    CodeDescription = ServiceConvert.GetString(row["CodeDescription"]),
                    Order = ServiceConvert.GetInteger(row["iOrder"]),
                })
                .ToArray();
        }

        public void SetPatientCaseInjury(string caseId, string code, string codeDescription, int iOrder)
        {
            using var cmd = new SqlCommand("if not exists(select * from Case_Injuries where CaseID = @CaseID and Code = @Code)" +
                                           "  insert Case_Injuries (CaseID, Code, CodeDescription, iOrder, dCode)" +
                                           "  values (@CaseID, @Code, @CodeDescription, @iOrder, @dCode)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@CaseID", SqlDbType.NVarChar)).Value = caseId.SafetySubstring(105);
            cmd.Parameters.Add(new SqlParameter("@Code", SqlDbType.NVarChar)).Value = code.SafetySubstring(20);
            cmd.Parameters.Add(new SqlParameter("@CodeDescription", SqlDbType.NVarChar)).Value = codeDescription.SafetySubstring(300);
            cmd.Parameters.Add(new SqlParameter("@iOrder", SqlDbType.Int)).Value = iOrder;
            cmd.Parameters.Add(new SqlParameter("@dCode", SqlDbType.Int)).Value = 0;

            TryExecuteNonQuery(cmd);
        }

        public void AddOcf9LineItem(OCF9 ocf9, GS9LineItemAR lineItem, Ocf9LineInfo info)
        {
            using var cmd = new SqlCommand("if not exists(select [PerantOCFID] from OCF9LineItems where [PerantOCFID] = @PerantOCFID and [GSCode] = @GSCode)" +
                                           "INSERT OCF9LineItems ([PerantOCFID], [GSCode], [GSAmountClaimed], [GSServicessAmountPayable]," +
                                           "                      [GSInterestPayable], [GSReasonCode], [GSReasonCodeDesc], [GSReasonCodeDescOther]," +
                                           "                      [GSReferenceNumber], [GSDescription])" +
                                           "  values (@PerantOCFID, @GSCode, @GSAmountClaimed, @GSServicessAmountPayable," +
                                           "          @GSInterestPayable, @GSReasonCode, @GSReasonCodeDesc, @GSReasonCodeDescOther," +
                                           "          @GSReferenceNumber, @GSDescription)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@PerantOCFID", SqlDbType.Float)).Value = ocf9.PerantOCFID;
            cmd.Parameters.Add(new SqlParameter("@GSCode", SqlDbType.NVarChar)).Value = lineItem.OCF9_GoodsAndServices_Items_Item_Code;

            var refNumber = lineItem.OCF9_GoodsAndServices_Items_Item_ReferenceNumber.Split(new[] { ';' })[0];
            cmd.Parameters.Add(new SqlParameter("@GSReferenceNumber", SqlDbType.Int)).Value =
                int.TryParse(refNumber, out var number)
                    ? number
                    : 0;
            cmd.Parameters.Add(new SqlParameter("@GSInterestPayable", SqlDbType.Float)).Value = lineItem.OCF9_GoodsAndServices_Items_Item_InterestPayable ?? (object)DBNull.Value;

            cmd.Parameters.Add(new SqlParameter("@GSDescription", SqlDbType.NVarChar)).Value = info.GSDescription ?? string.Empty;
            cmd.Parameters.Add(new SqlParameter("@GSAmountClaimed", SqlDbType.Float)).Value = info.GSAmountClaimed;
            cmd.Parameters.Add(new SqlParameter("@GSServicessAmountPayable", SqlDbType.Float)).Value = info.GSServicessAmountPayable;
            cmd.Parameters.Add(new SqlParameter("@GSReasonCode", SqlDbType.NVarChar)).Value = info.GSReasonCode ?? string.Empty;
            cmd.Parameters.Add(new SqlParameter("@GSReasonCodeDesc", SqlDbType.NVarChar)).Value = info.GSReasonCodeDesc ?? string.Empty;
            cmd.Parameters.Add(new SqlParameter("@GSReasonCodeDescOther", SqlDbType.NVarChar)).Value = info.GSReasonCodeDescOther ?? string.Empty;

            TryExecuteNonQuery(cmd);
        }

        public void AddInvoiceItemDistributePayment(double invoiceId, double idPayment, decimal recordId, double pAmount)
        {
            using var cmd = new SqlCommand("INSERT InvoiceItemDistributePayment (InvoiceID, RecordID, PaymentID, PaidAmt)" +
                                           "  values (@InvoiceID, @RecordID, @PaymentID, @PaidAmt)", ConnectionDb);
            cmd.Parameters.Add(new SqlParameter("@InvoiceID", SqlDbType.Float)).Value = invoiceId;
            cmd.Parameters.Add(new SqlParameter("@RecordID", SqlDbType.Decimal)).Value = recordId;
            cmd.Parameters.Add(new SqlParameter("@PaymentID", SqlDbType.Float)).Value = idPayment;
            cmd.Parameters.Add(new SqlParameter("@PaidAmt", SqlDbType.Float)).Value = pAmount;
            TryExecuteNonQuery(cmd);
        }
    }
}
