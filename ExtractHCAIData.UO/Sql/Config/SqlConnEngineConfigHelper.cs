﻿using System.Collections.Generic;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Linq;

namespace ExtractHcaiData.Uo.Sql.Config
{
    public class SqlConnEngineConfigHelper : ISqlConnEngineConfigHelper
    {
        public IEnumerable<string> GetListOfServers()
        {
            return (from row in SqlDataSourceEnumerator.Instance.GetDataSources().Rows.OfType<DataRow>()
                    select string.IsNullOrEmpty(row[1].ToString())
                               ? row[0].ToString()
                               : $"{row[0]}\\{row[1]}")
                .ToList();
        }

        public IEnumerable<string> GetListOfInitialCatalogs(string connectionString)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                try
                {
                    connection.Open();
                    return (from row in connection.GetSchema(SqlClientMetaDataCollectionNames.Databases).Rows.OfType<DataRow>().Skip(4)
                            select row[0].ToString())
                        .ToList();
                }
                catch
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}