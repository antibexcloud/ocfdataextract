﻿namespace ExtractHcaiData.Uo.Sql.Config
{
    public interface IDataSourceConfigurator
    {
        bool ShowDialog(string connectionString, out string newConnectionString);
    }
}