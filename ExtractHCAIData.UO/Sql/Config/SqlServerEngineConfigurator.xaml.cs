﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ExtractHcaiData.Uo.ThreadWork;

namespace ExtractHcaiData.Uo.Sql.Config
{
    /// <summary>
    /// Interaction logic for SqlServerEngineConfigurator.xaml
    /// </summary>
    public partial class SqlServerEngineConfigurator : IDataSourceConfigurator
    {
        public SqlServerEngineConfigurator()
        {
            InitializeComponent();
            Owner = Application.Current.MainWindow;
        }

        private ISqlConnEngineConfigHelper _helper;

        private SqlConnectionStringBuilder _connectionStringBuilder;

        public bool ShowDialog(string connectionString, out string newConnectionString)
        {
            _connectionStringBuilder = new SqlConnectionStringBuilder(connectionString);
            _helper = new SqlConnEngineConfigHelper();

            UpdateValues();

            ShowDialog();

            UpdateConnectionString();

            newConnectionString = _connectionStringBuilder.ConnectionString;

            return DialogResult == true;
        }

        private void UpdateValues()
        {
            DataSourceCombobox.Text = _connectionStringBuilder.DataSource;

            (_connectionStringBuilder.IntegratedSecurity
                 ? WindowsButton
                 : CredentialsButton).IsChecked = true;

            UserIdBox.Text = _connectionStringBuilder.UserID;

            PasswordBox.Text = _connectionStringBuilder.Password;

            InitialCatalogCombobox.Text = _connectionStringBuilder.InitialCatalog;

            //advancedConnectionProperties.ConnectionBuilder = new ConnectionStringBuilderWrapper(_connectionStringBuilder);
        }

        private void UpdateConnectionString()
        {
            if (!string.IsNullOrEmpty(DataSourceCombobox.Text))
                _connectionStringBuilder.DataSource = DataSourceCombobox.Text;

            _connectionStringBuilder.IntegratedSecurity = WindowsButton.IsChecked == true;

            if (_connectionStringBuilder.IntegratedSecurity)
            {
                _connectionStringBuilder.Remove("User ID");
                _connectionStringBuilder.Remove("Password");
            }
            else
            {
                _connectionStringBuilder.UserID = UserIdBox.Text;
                _connectionStringBuilder.Password = PasswordBox.Text;
                _connectionStringBuilder.Remove("Integrated Security");
            }

            _connectionStringBuilder.InitialCatalog = InitialCatalogCombobox.Text;
        }

        private void OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void RefreshSqlServersListClick(object sender, RoutedEventArgs e)
        {
            RefreshSqlServers();
        }

        private bool _dataSourceIsLoaded;

        private bool _dataSourceLoadingInProgress;

        private void RefreshSqlServers()
        {
            DataSourceCombobox.ItemsSource = null;

            _dataSourceLoadingInProgress = true;

            ThreadManager.ExecuteAction(
                () =>
                    {
                        try
                        {
                            var listOfServers = _helper.GetListOfServers().OrderBy(name => name).ToList();

                            Dispatcher.BeginInvoke(
                                new Action(() =>
                                               {
                                                   DataSourceCombobox.ItemsSource = listOfServers;
                                               }));
                        }
                        catch
                        {
                        }
                        _dataSourceIsLoaded = true;
                        _dataSourceLoadingInProgress = false;
                    });
        }

        private void DataSourceComboboxOpened(object sender, EventArgs e)
        {
            if (_dataSourceIsLoaded || _dataSourceLoadingInProgress) return;

            RefreshSqlServers();
        }

        private bool _initialCatalogsLoaded;

        private bool _initialCatalogsLoading;

        private void RefreshInitialCatalogs()
        {
            InitialCatalogCombobox.ItemsSource = null;

            _initialCatalogsLoading = true;

            var builder = new SqlConnectionStringBuilder
                              {
                                  DataSource = DataSourceCombobox.Text,
                                  IntegratedSecurity = WindowsButton.IsChecked == true
                              };
            if (!builder.IntegratedSecurity)
            {
                builder.UserID = UserIdBox.Text;
                builder.Password = PasswordBox.Text;
            }

            ThreadManager.ExecuteAction(
                () =>
                {
                    var listOfCatalogs = _helper.GetListOfInitialCatalogs(builder.ConnectionString);
                    if (listOfCatalogs != null)
                    {
                        Dispatcher.BeginInvoke(
                            new Action(() =>
                            {
                                InitialCatalogCombobox.ItemsSource = listOfCatalogs.OrderBy(e=>e).ToList();
                            }));
                    }
                    else
                    {
                    }
                    _initialCatalogsLoaded = true;
                    _initialCatalogsLoading = false;
                });
        }

        private void InitialCatalogComboboxOpened(object sender, EventArgs e)
        {
            if (_initialCatalogsLoaded || _initialCatalogsLoading) return;

            RefreshInitialCatalogs();
        }

        private void InitialCatalogReload(object sender, RoutedEventArgs e)
        {
            RefreshInitialCatalogs();
        }

        private void UpdateState()
        {
            _initialCatalogsLoaded = false;
            UpdateButton();
        }

        private void UpdateButton()
        {
            OkButton.IsEnabled = !string.IsNullOrEmpty(DataSourceCombobox.Text) &&
                                 (WindowsButton.IsChecked == true || !string.IsNullOrEmpty(UserIdBox.Text));
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateState();
        }

        private void CheckedChanged(object sender, RoutedEventArgs e)
        {
            UpdateState();
        }
    }
}
