﻿using System.Collections.Generic;

namespace ExtractHcaiData.Uo.Sql.Config
{
    public interface ISqlConnEngineConfigHelper
    {
        IEnumerable<string> GetListOfServers();

        IEnumerable<string> GetListOfInitialCatalogs(string connectionString);
    }
}