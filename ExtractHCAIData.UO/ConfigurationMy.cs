﻿using System.ComponentModel;
using System.Reflection;
using ExtractHcaiData.Uo.ConfigurationManagment;

namespace ExtractHcaiData.Uo
{
    public sealed class ConfigurationMy : ConfigurationDbBase
    {
        public ConfigurationMy(Assembly configFor) : base(configFor)
        {
        }

        [ConfigurationProperty(false)]
        [DefaultValue("")]
        public string DbSyncConnectionString
        {
            get { return GetPropertyValue(() => DbSyncConnectionString); }
            set { SetPropertyValue(() => DbSyncConnectionString, value); }
        }

        [ConfigurationProperty(false)]
        [DefaultValue("")]
        public string DbUoConnectionString
        {
            get { return GetPropertyValue(() => DbUoConnectionString); }
            set { SetPropertyValue(() => DbUoConnectionString, value); }
        }

        [ConfigurationProperty(false)]
        [DefaultValue(0)]
        public int DaysBeforePayment
        {
            get { return GetPropertyValue(() => DaysBeforePayment); }
            set { SetPropertyValue(() => DaysBeforePayment, value); }
        }

        [ConfigurationProperty(false)]
        [DefaultValue(false)]
        public bool AddingMode
        {
            get { return GetPropertyValue(() => AddingMode); }
            set { SetPropertyValue(() => AddingMode, value); }
        }

        [ConfigurationProperty(false)]
        [DefaultValue(false)]
        public bool AvralMode
        {
            get { return GetPropertyValue(() => AvralMode); }
            set { SetPropertyValue(() => AvralMode, value); }
        }

        [ConfigurationProperty(false)]
        [DefaultValue(false)]
        public bool GroupByProviderSpecialty
        {
            get { return GetPropertyValue(() => GroupByProviderSpecialty); }
            set { SetPropertyValue(() => GroupByProviderSpecialty, value); }
        }
    }
}
