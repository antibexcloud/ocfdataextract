﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ExtractHcaiData.Uo.Converters
{
    public class Date2DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is null) return string.Empty;
            var tempDate = (DateTime)value;
            return (tempDate.Year <= 1900) ? "" : tempDate.ToString("yyyy.MM.dd");
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)value;
            try
            {
                return DateTime.Parse(str, CultureInfo.InvariantCulture);

            }
            catch (FormatException)
            {
                return value;
            }
        }
    }
}
