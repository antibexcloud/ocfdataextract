﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ExtractHcaiData.Uo.Converters
{
    public class FloatToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is null) return string.Empty;
            if (parameter != null)
            {
                var format = $"F{parameter}";
                var res = ((double)value).ToString(format);
                return res;
            }
            return System.Convert.ToString(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var str = (string)value;
            try
            {
                return double.Parse(str.Replace(",", CultureInfo.InvariantCulture.NumberFormat.NumberDecimalSeparator), CultureInfo.InvariantCulture);

            }
            catch (FormatException)
            {
                throw new FormatException("not correct!");
            }
        }
    }
}

