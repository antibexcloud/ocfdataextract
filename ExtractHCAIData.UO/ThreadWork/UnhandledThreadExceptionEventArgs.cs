﻿using System;

namespace ExtractHcaiData.Uo.ThreadWork
{
    public class UnhandledThreadExceptionEventArgs : EventArgs
    {
        public UnhandledThreadExceptionEventArgs(Exception exception)
        {
            Exception = exception;
        }

        private Exception Exception { get; }
    }
}