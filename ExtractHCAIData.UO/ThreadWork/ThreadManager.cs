﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Threading;

namespace ExtractHcaiData.Uo.ThreadWork
{
    public static class ThreadManager
    {
        static ThreadManager()
        {
            Application.Current.DispatcherUnhandledException += OnDispatcherUnhandledException;
        }

        private static void OnDispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            OnUnhandledThreadException(e.Exception);
            e.Handled = true;
        }

        public static void ExecuteAction(Action action)
        {
            if (action is null) return;

            ThreadPool.QueueUserWorkItem(
                obj =>
                    {
                        try
                        {
                            action();
                        }
                        catch (Exception e)
                        {
                            OnUnhandledThreadException(e);
                        }
                    });
        }

        public static void ExecuteActionOnDispatcher(Action action)
        {
            Application.Current.Dispatcher.Invoke(action);
        }

        public static event EventHandler<UnhandledThreadExceptionEventArgs> UnhandledThreadException;

        private static void OnUnhandledThreadException(Exception e)
        {
            Application.Current.Dispatcher.Invoke(
                new Action(() =>
                               {
                                   var handler = UnhandledThreadException;
                                   if (handler != null) handler(null, new UnhandledThreadExceptionEventArgs(e));
                               }));
        }

        public static void ExecuteInDispatcherThread(Action action)
        {
            if (Application.Current is null || Application.Current.Dispatcher is null)
                return;
            if (Application.Current.Dispatcher.CheckAccess())
                action();
            else
                Application.Current.Dispatcher.Invoke(action);
        }
    }
}