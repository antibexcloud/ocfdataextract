﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Xml.Linq;

namespace ExtractHcaiData.Uo.ConfigurationManagment
{
    internal class ConfigManager
    {
        private const bool IsSaveUsingXDocument = true;

        static ConfigManager()
        {
            Key = new Guid("57706676-F830-4B05-88A8-B03EE266B64A").ToByteArray();
            Vector = new Guid("32A08CC8-4A19-4CD1-B48C-1E0E61280F92").ToByteArray();
        }

        private static readonly byte[] Key;

        private static readonly byte[] Vector;

        public ConfigManager(Assembly configForAssembly)
        {
            _configLocation = configForAssembly.Location;
        }

        public ConfigManager(string configForAssembly)
        {
            _configLocation = configForAssembly;
        }

        private readonly string _configLocation;

        internal void SetEncryptionFlag(string key, bool isEncrypted)
        {
            if (isEncrypted)
            {
                if (_encryptedKeysList.Contains(key))
                    return;
                _encryptedKeysList.Add(key);
            }
            else
                _encryptedKeysList.Remove(key);
        }

        private readonly IList<string> _encryptedKeysList = new List<string>(); 

        internal string this[string key]
        {
            get
            {
                var configuration = ConfigurationManager.OpenExeConfiguration(_configLocation);
                var settingsPair = configuration.AppSettings.Settings[key];
                if (settingsPair == null)
                    throw new KeyNotFoundException(key);
                var rawData = settingsPair.Value;
                if (!_encryptedKeysList.Contains(key))
                    return rawData;
                if (!StringEncrypted(rawData))
                {
                    if (IsSaveUsingXDocument)
                        SaveUsingXDocument(key, Encrypt(rawData), _configLocation);
                    else
                    {
                        configuration.AppSettings.Settings[key].Value = Encrypt(rawData);
                        configuration.Save();
                    }
                    return rawData;
                }
                return Decrypt(rawData);
            }
            set
            {
                if (IsSaveUsingXDocument)
                    SaveUsingXDocument(key, (!_encryptedKeysList.Contains(key)) ? value : Encrypt(value), _configLocation);
                else
                {
                    var configuration = ConfigurationManager.OpenExeConfiguration(_configLocation);
                    var settingsPair = configuration.AppSettings.Settings[key];
                    if (settingsPair == null)
                        configuration.AppSettings.Settings.Add(key, "");
                    configuration.AppSettings.Settings[key].Value = (!_encryptedKeysList.Contains(key)) ? value : Encrypt(value);
                    configuration.Save(ConfigurationSaveMode.Modified);
                }
            }
        }

        private static void SaveUsingXDocument(string key, string value, string fileName)
        {
            var configFile = fileName + ".config";
            var document = XDocument.Load(configFile);
            if (document.Root == null) return;

            var appSet = document.Root.Element("appSettings");
            if (appSet == null) return;

            var keyNode = appSet.Elements("add").FirstOrDefault(x => x.Attribute("key").Value == key);
            if (keyNode != null)
                keyNode.Attribute("value").Value = value;
            else
                appSet.Add(new XElement("add", new XAttribute("key", key), new XAttribute("value", value)));
            document.Save(configFile);
        }

        private static string Encrypt(string value)
        {
            byte[] data;
            using (var stream = new MemoryStream())
            {
                var writer = new BinaryWriter(stream);
                writer.Write(value);
                data = stream.ToArray();
            }

            Math.DivRem(data.Length, 16, out var rem);
            if (rem != 0)
            {
                var buf = new byte[data.Length + 16 - rem];
                for (var i = 0; i < data.Length; i++)
                    buf[i] = data[i];
                data = buf;
            }
            using (var transformer = new AesCryptoServiceProvider {Padding = PaddingMode.None}.CreateEncryptor(Key, Vector))
            using (var stream = new MemoryStream())
            using (var cryptoStream = new CryptoStream(stream, transformer, CryptoStreamMode.Write))
            {
                cryptoStream.Write(data, 0, data.Length);
                data = stream.ToArray();
            }
            return BitConverter.ToString(data)
                .Split('-')
                .Aggregate("", (str1, str2) => string.Format("{0} 0x{1}", str1, str2)).Trim();
        }

        private static bool StringEncrypted(string data)
        {
            return !string.IsNullOrWhiteSpace(data) &&
                   new Regex(@"^((0x){1}[0-9A-Fa-f]{2} )*((0x){1}[0-9A-Fa-f]{2}){1}$").IsMatch(data);
        }

        private static string Decrypt(string value)
        {
            var data = value.Replace("0x", "")
                    .Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries)
                    .Select(e => Convert.ToByte(e, 16))
                    .ToArray();
            using (var transformer = new AesCryptoServiceProvider { Padding = PaddingMode.None }.CreateDecryptor(Key, Vector))
            using (var memStream = new MemoryStream(data))
            using (var cryptoStream = new CryptoStream(memStream, transformer, CryptoStreamMode.Read))
            {
                var buf = new byte[data.Length];
                cryptoStream.Read(buf, 0, buf.Length);
                data = buf;
            }
            using (var reader = new BinaryReader(new MemoryStream(data)))
                return reader.ReadString();
        }
    }
}
