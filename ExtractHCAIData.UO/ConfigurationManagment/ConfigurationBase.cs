﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using ExtractHcaiData.Uo.Logging;
using ExtractHcaiData.Uo.Utils;

namespace ExtractHcaiData.Uo.ConfigurationManagment
{
    public class ConfigurationBase
    {
        protected ConfigurationBase(Assembly configFor)
        {
            _configManager = new ConfigManager(configFor);
            Init();
        }

        public ConfigurationBase(string configForAssembly)
        {
            _configManager = new ConfigManager(configForAssembly);
            Init();
        }

        private readonly ConfigManager _configManager;

        protected TProperty GetPropertyValue<TProperty>(Expression<Func<TProperty>> projection)
        {
            var mapping = _propertyMappings[((MemberExpression) projection.Body).Member.Name];
            try
            {
                return ConvertTo<TProperty>(_configManager[mapping.Name]);
            }
            catch(KeyNotFoundException)
            {
                if (mapping.DefaultValueOnException)
                    return (TProperty) mapping.DefaultValue;
                throw;
            }
        }

        protected void SetPropertyValue<TProperty>(Expression<Func<TProperty>> projection, object val)
        {
            var mapping = _propertyMappings[((MemberExpression)projection.Body).Member.Name];
            try
            {
                var newValue = val.ToString();
                var oldValue = ConvertTo<TProperty>(_configManager[mapping.Name]).ToString();
                if (!oldValue.Equals(newValue))
                    _configManager[mapping.Name] = newValue;
            }
            catch(Exception ex)
            {
                Logger.Error("SetPropertyValue", ex);
            }
        }

        private static T ConvertTo<T>(string data)
        {
            if (typeof(string) == typeof(T))
                return (T)(object)data;
            if (typeof(byte) == typeof(T))
                return (T)(object)byte.Parse(data);
            if (typeof(short) == typeof(T))
                return (T)(object)short.Parse(data);
            if (typeof(ushort) == typeof(T))
                return (T)(object)ushort.Parse(data);
            if (typeof(int) == typeof(T))
                return (T)(object)(int.Parse(data));
            if (typeof (uint) == typeof (T))
                return (T) (object) uint.Parse(data);
            if (typeof(ulong) == typeof(T))
                return (T)(object)ulong.Parse(data);
            if (typeof(bool) == typeof(T))
                return (T)(object)bool.Parse(data);
            if (typeof (T).IsEnum)
                return (T) Enum.Parse(typeof (T), data);
            if (typeof(double) == typeof(T))
                return (T)(object)ServiceConvert.ConvertToDouble(data);
            if (typeof(float) == typeof(T))
                return (T)(object)ServiceConvert.ConvertToFloat(data);
            if (typeof(DateTime) == typeof(T))
                return (T)(object)Convert.ToDateTime(data);
            throw new NotSupportedException();
        }

        private readonly IDictionary<string, PropertyMapping> _propertyMappings = new Dictionary<string, PropertyMapping>();
        
        private void Init()
        {
            var props = GetType().GetProperties();
            foreach (var info in props)
            {
                var attributes = info.GetCustomAttributes(true);

                var configAttr = attributes.OfType<ConfigurationPropertyAttribute>().FirstOrDefault();
                if (configAttr == null) continue;

                if (configAttr.IsEncrypted)
                    _configManager.SetEncryptionFlag(configAttr.Key ?? info.Name, true);

                var mapping = new PropertyMapping
                {
                    Name = configAttr.Key ?? info.Name,
                    TypeValue = info.PropertyType,
                    IsEncrypt = configAttr.IsEncrypted
                };

                var defValue = attributes.OfType<DefaultValueAttribute>().FirstOrDefault();
                if (defValue != null)
                {
                    mapping.DefaultValue = defValue.Value;
                    mapping.DefaultValueOnException = true;
                }

                var descValue = attributes.OfType<DisplayNameAttribute>().FirstOrDefault();
                if (descValue != null)
                    mapping.Description = descValue.DisplayName;

                _propertyMappings.Add(info.Name, mapping);
            }
        }

        [ConfigurationProperty]
        [DefaultValue(LoggingLevel.Info)]
        [DisplayName("Logging level")]
        public LoggingLevel LogLevel
        {
            get { return GetPropertyValue(() => LogLevel); }
            set { SetPropertyValue(() => LogLevel, value); }
        }
    }
}
