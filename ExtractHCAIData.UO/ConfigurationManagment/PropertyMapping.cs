﻿using System;

namespace ExtractHcaiData.Uo.ConfigurationManagment
{
    public class PropertyMapping
    {
        private object _oldValue;

        public string Name { get; set; }

        public string Description { get; set; }

        public Type TypeValue { get; set; }

        public object DefaultValue { get; set; }

        public bool DefaultValueOnException { get; set; }

        public bool IsEncrypt { get; set; }

        private object OldValue
        {
            get => _oldValue;
            set
            {
                _oldValue = value;
                Value = value;
            }
        }

        private object Value { get; set; }

        public bool IsDirty => OldValue != null && Value != null && !OldValue.ToString().Equals(Value.ToString());
    }
}
