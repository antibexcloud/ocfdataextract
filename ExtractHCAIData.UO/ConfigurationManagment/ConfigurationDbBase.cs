﻿using System.Reflection;

namespace ExtractHcaiData.Uo.ConfigurationManagment
{
    public class ConfigurationDbBase : ConfigurationBase
    {
        protected ConfigurationDbBase(Assembly configFor)
            : base(configFor)
        {
        }

        protected ConfigurationDbBase(string configForAssembly)
            : base(configForAssembly)
        {
        }
    }
}
