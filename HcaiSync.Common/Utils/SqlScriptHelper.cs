﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace HcaiSync.Common.Utils
{
    public static class SqlScriptHelper
    {
        public static string[] GetMySqlScripts(string fullPath, Assembly assembly = null)
        {
            var content = GetTextFromResource(fullPath, assembly);
            if (string.IsNullOrEmpty(content))
            {
                if (content == null)
                    throw new Exception($"File '{fullPath}' not found");
                return new string[0];
            }

            return ParseText2Scripts(content + "\n");
        }

        #region private

        private static string GetTextFromResource(string resName, Assembly assembly = null)
        {
            try
            {
                using (var stream = (assembly ?? typeof(SqlScriptHelper).Assembly).GetManifestResourceStream(resName))
                using (var reader = new StreamReader(stream ?? throw new InvalidOperationException()))
                {
                    return reader.ReadToEnd();
                }
            }
            catch
            {
                return null;
            }
        }

        private static string[] ParseText2Scripts(string textSql)
        {
            var allScripts = textSql.Split(new[] { "\nGO", "\nGo", "\ngO", "\ngo" }, StringSplitOptions.RemoveEmptyEntries);

            var result = new List<string>();
            foreach (var script in allScripts)
            {
                var lines = script.Replace("\r\n", "\n").Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries)
                    .Where(l => !string.IsNullOrEmpty(l) && !l.IsNotSigned())
                    .ToArray();
                if (!lines.Any()) continue;

                var adoptedScript = string.Join(Environment.NewLine, lines);
                result.Add(adoptedScript);
            }

            return result.ToArray();
        }

        private static bool IsNotSigned(this string s)
        {
            return string.IsNullOrEmpty(s) || s.Trim().Length == 0 || s.Trim().StartsWith("--");
        }

        #endregion
    }
}
