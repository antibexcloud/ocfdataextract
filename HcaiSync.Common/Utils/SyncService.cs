﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace HcaiSync.Common.Utils
{
    public static class SyncService
    {
        private const int RGBMAX = 255;

        private static readonly Random Random4Color = new Random();

        private static readonly Dictionary<string, string> ActivityIds = new()
        {
            {"1ZX02", "Exercise"},
            {"1ZX12", "Therapy"},
            {"2AN08", "Brain test"},
            {"2ZZ02", "Assessment (examination)"},
            {"6AA02", "Assessment (mental health)"},
            {"6AA08", "Test (mental health)"},
            {"6AA30", "Therapy (mental health)"},
            {"6DA07", "Facilitation (interpersonal relationships)"},
            {"6KA30", "Therapy (cognition and learning)"},
            {"6VA10", "Counseling (motor and living skills)"},
            {"6VA30", "Therapy (motor and livinig skills)"},
            {"6VA50", "Training (motor and livinig skills)"},
            {"7SE02", "Assessment (environment)"},
            {"7SF12", "Planning service"},
            {"7SF13", "Preparation service "},
            {"7SF15", "Brokerage service"},
            {"7SF19", "Interpretation service"},
            {"7SJ30", "Documentation"},
            {"7SJ30LB", "Documentation (claim form)"},
            {"AXXOT", "Delivery"},
            {"AXXTC", "Claimant transportation"},
            {"AXXTT", "Provider travel time"},
            {"GXX14", "Exercise (equipment)"},
            {"GXX18", "Hot/cold gel pack"},
            {"GXX27", "Pillow (cervical)"},
            {"GXX34", "TENS unit"},
            {"GXX36", "Therapy ball"},
            {"HXXAC", "Form 1 completion "},
            {"HXXCA", "Assessment (CAT)"},
            {"HXXMV", "Assessment (Virtual)"},
        };

        public static string MakeActivityId(string code)
        {
            return ActivityIds.TryGetValue(code, out var id) ? id : code;
        }

        public static string MakeActivityId(string code, double units)
        {
            return ActivityIds.TryGetValue(code, out var id)
                ? id
                : $"{code}[{TimeSpan.FromHours(units).TotalMinutes}]";
        }

        public static string MakeDuration(double mvaUnits)
        {
            var ts = TimeSpan.FromHours(mvaUnits);
            return $"{ts.Hours:D2}:{ts.Minutes:D2}";
        }

        public static string GenerateColors()
        {
            try
            {
                var cr = Random4Color.Next(0, RGBMAX);
                var cg = Random4Color.Next(0, RGBMAX);
                var cb = Random4Color.Next(0, RGBMAX);

                var color = cr * 256 * 256 + cg * 256 + cb;
                return $"{color}";
            }
            catch (Exception ex)
            {
                return "16711680";
            }
        }

        public static string CheckOnQuotes(string original)
        {
            if (string.IsNullOrWhiteSpace(original)) return string.Empty;
            if (original.Trim().StartsWith("\"") && original.Trim().EndsWith("\""))
                return original.Trim().Substring(1, original.Trim().Length - 2);
            return original;
        }

        public static string CheckOnQuotes2(string original)
        {
            if (string.IsNullOrWhiteSpace(original)) return string.Empty;
            if (original.Trim().StartsWith("{") && original.Trim().EndsWith("}"))
                return original.Trim().Substring(1, original.Trim().Length - 2);
            return original;
        }

        public static string ExtendedEfMessage(this Exception ex)
        {
            var msg = string.Empty;

            var exNext = ex;
            while (exNext != null)
            {
                if ((exNext as DbEntityValidationException)?.EntityValidationErrors?.Any() == true)
                {
                    var exValid = exNext as DbEntityValidationException;
                    foreach (var exValidEntityValidationError in exValid.EntityValidationErrors)
                    foreach (var dbValidationError in exValidEntityValidationError.ValidationErrors)
                    {
                        if (msg.Contains(dbValidationError.ErrorMessage)) continue;

                        if (!string.IsNullOrEmpty(msg)) msg += Environment.NewLine;
                        msg += dbValidationError.ErrorMessage;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(msg)) msg += Environment.NewLine;
                    msg += exNext.Message;
                }

                exNext = exNext.InnerException;
            }

            return msg;
        }
    }
}
