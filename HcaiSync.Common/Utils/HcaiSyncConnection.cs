﻿using HcaiSync.Common.Sql;

namespace HcaiSync.Common.Utils
{
    public class HcaiSyncConnection : AbstractDataBaseConnection
    {
        public HcaiSyncConnection(string connectString) : base(connectString)
        {
        }
    }
}
