﻿namespace HcaiSync.Common.Enums
{
    public enum FlagWithNotApplicable
    {
        NotApplicable = 0,

        No = 1,
        
        Yes = 2,
    }

    public static class HelperFlagWithNotApplicable
    {
        public static string ToUoString(this FlagWithNotApplicable value)
        {
            return value == FlagWithNotApplicable.NotApplicable
                ? "N/A"
                : value.ToString();
        }
    }
}
