﻿using System;

namespace HcaiSync.Common.Enums
{
    public enum FlagWithUnknownEnum
    {
        Unknown = 0,

        No = 1,

        Yes = 2
    }

    public static class HelperFlagWithUnknownEnum
    {
        public static string ToUoString(this FlagWithUnknownEnum value)
        {
            var str = value.ToString();
            return str.Substring(0, Math.Min(3, str.Length));
        }
    }
}
