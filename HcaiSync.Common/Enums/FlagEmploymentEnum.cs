﻿using System;

namespace HcaiSync.Common.Enums
{
    public enum FlagEmploymentEnum
    {
        Unknown,

        NotEmployed,

        No,

        Yes,
    }

    public static class HelperFlagEmploymentEnum
    {
        public static string ToUoString(this FlagEmploymentEnum value)
        {
            var str = value.ToString();
            return str.Substring(0, Math.Min(3, str.Length));
        }
    }
}
