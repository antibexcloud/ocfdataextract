﻿namespace HcaiSync.Common.Enums
{
    public enum OcfTypeOfPlanEnum
    {
        OCF18OR22,
        OCF23,
        OCF23WAD,
        OCF23MIG,
        AACN,
    }
}
