﻿namespace HcaiSync.Common.Enums
{
    public enum HcaiValidateStateEnum
    {
        None = 0,

        Ok = 1,

        Bad = 2,
    }
}
