﻿IF not exists (select T0.object_id from sys.tables T0 join sys.schemas T1 on T1.schema_id = T0.schema_id
                 where T0.name='ACSISubmit' and T1.name='dbo')
CREATE TABLE [dbo].[ACSISubmit](
	[HCAI_Document_Number] [nvarchar](11) NOT NULL,

	[PMSFields_PMSSoftware] [nvarchar](20) NOT NULL,
	[PMSFields_PMSVersion] [nvarchar](20) NOT NULL,
	[PMSFields_PMSDocumentKey] [nvarchar](20) NOT NULL,
	[PMSFields_PMSPatientKey] [nvarchar](20) NOT NULL,

	[AttachmentsBeingSent] [bit] NOT NULL,

	[Header_ClaimNumber] [nvarchar](20) NULL,
	[Header_PolicyNumber] [nvarchar](20) NULL,
	[Header_DateOfAccident] [date] NOT NULL,

	[Applicant_Name_FirstName] [nvarchar](50) NOT NULL,
	[Applicant_Name_MiddleName] [nvarchar](50) NULL,
	[Applicant_Name_LastName] [nvarchar](50) NOT NULL,
	[Applicant_DateOfBirth] [date] NOT NULL,
	[Applicant_Gender] [nvarchar](1) NOT NULL,
	[Applicant_TelephoneNumber] [nvarchar](20) NULL,
	[Applicant_TelephoneExtension] [nvarchar](10) NULL,
	[Applicant_Address_StreetAddress1] [nvarchar](50) NOT NULL,
	[Applicant_Address_StreetAddress2] [nvarchar](50) NULL,
	[Applicant_Address_City] [nvarchar](50) NOT NULL,
	[Applicant_Address_Province] [nvarchar](2) NOT NULL,
	[Applicant_Address_PostalCode] [nvarchar](10) NOT NULL,

	[Insurer_IBCInsurerID] [nvarchar](50) NOT NULL,
	[Insurer_IBCBranchID] [nvarchar](50) NOT NULL,

	[Insurer_Adjuster_Name_FirstName] [nvarchar](50) NULL,
	[Insurer_Adjuster_Name_LastName] [nvarchar](50) NULL,
	[Insurer_Adjuster_TelephoneNumber] [nvarchar](20) NULL,
	[Insurer_Adjuster_TelephoneExtension] [nvarchar](10) NULL,
	[Insurer_Adjuster_FaxNumber] [nvarchar](20) NULL,

	[Insurer_PolicyHolder_IsSameAsApplicant] [bit] NOT NULL,
	[Insurer_PolicyHolder_Name_FirstName] [nvarchar](50) NULL,
	[Insurer_PolicyHolder_Name_LastName] [nvarchar](50) NULL,

	[ACSI_InvoiceInformation_InvoiceNumber] [nvarchar](20) NULL,
	[ACSI_InvoiceInformation_FirstInvoice] [bit] NOT NULL,
	[ACSI_InvoiceInformation_LastInvoice] [bit] NOT NULL,

	[ACSI_PreviouslyApprovedGoodsAndServices_Type] [nvarchar](50) NULL,
	[ACSI_PreviouslyApprovedGoodsAndServices_PlanDate] [date] NULL,
	[ACSI_PreviouslyApprovedGoodsAndServices_PlanNumber] [nvarchar](11) NOT NULL,

	[ACSI_Payee_FacilityRegistryID] [int] NOT NULL,
	[ACSI_Payee_FacilityIsPayee] [bit] NOT NULL,
	[ACSI_Payee_MakeChequePayableTo] [nvarchar](200) NULL,

	[ACSI_OtherInsurance_IsThereOtherInsuranceCoverage] [bit] NOT NULL,
	[ACSI_OtherInsurance_MOHAvailable] [int] NULL,

	[ACSI_OtherInsurance_OtherInsurers_Insurer1_ID] [nvarchar](50) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer1_Name] [nvarchar](50) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer1_PolicyNumber] [nvarchar](20) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_FirstName] [nvarchar](50) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer1_PlanMember_LastName] [nvarchar](50) NULL,

	[ACSI_OtherInsurance_OtherInsurers_Insurer2_ID] [nvarchar](50) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer2_Name] [nvarchar](50) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer2_PolicyNumber] [nvarchar](20) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_FirstName] [nvarchar](50) NULL,
	[ACSI_OtherInsurance_OtherInsurers_Insurer2_PlanMember_LastName] [nvarchar](50) NULL,

	[ACSI_OtherInsuranceAmounts_MOH_OtherService_Proposed] [decimal](19, 2) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Proposed] [decimal](19, 2) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Proposed] [decimal](19, 2) NULL,
	[ACSI_OtherInsuranceAmounts_OtherServiceType] [nvarchar](500) NULL,

	[ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Proposed] [decimal](19, 2) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Proposed] [decimal](19, 2) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Proposed] [decimal](19, 2) NULL,
	[ACSI_OtherInsuranceAmounts_IsAmountRefused] [bit] NULL,
	[ACSI_OtherInsuranceAmounts_Debits_OtherServiceType] [nvarchar](500) NULL,

	[ACSI_AccountActivity_PriorBalance] [decimal](19, 2) NULL,
	[ACSI_AccountActivity_PaymentReceivedFromAutoInsurer] [decimal](19, 2) NULL,
	[ACSI_AccountActivity_OverdueAmount] [decimal](19, 2) NULL,
	[ACSI_InsurerTotals_OtherInsurers_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_MOH_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_AutoInsurerTotal_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_GST_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_PST_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_SubTotal_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_Interest_Proposed] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInformation] [nvarchar](500) NULL,
	[AdditionalComments] [nvarchar](max) NULL,
	[OCFVersion] [int] NOT NULL,
 CONSTRAINT [PK_ACSISubmit] PRIMARY KEY CLUSTERED 
(
	[HCAI_Document_Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF not exists (select T0.object_id from sys.tables T0 join sys.schemas T1 on T1.schema_id = T0.schema_id
                 where T0.name='GSACSILineItemSubmit' and T1.name='dbo')
CREATE TABLE [dbo].[GSACSILineItemSubmit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HCAI_Document_Number] [nvarchar](11) NOT NULL,

	[ACSI_ReimbursableGoodsAndServices_Items_Item_ReferenceNumber] int NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PMSGSKey] [nvarchar](10) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Code] [nvarchar](10) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Attribute] [nvarchar](3) NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_ProviderRegistryID] [int] NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_ProviderReference_Occupation] [nvarchar](10) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Quantity] [decimal](19, 6) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Measure] [nvarchar](2) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_DateOfService] [date] NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_GST] [bit] NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_PST] [bit] NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Estimated_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Description] [nvarchar](200) NULL,
	CONSTRAINT [FK_GSACSILineItemSubmit_ACSISubmit] FOREIGN KEY ([HCAI_Document_Number]) REFERENCES dbo.[ACSISubmit] ([HCAI_Document_Number]),
 CONSTRAINT [PK_GSACSILineItemSubmit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF not exists (select T0.object_id from sys.tables T0 join sys.schemas T1 on T1.schema_id = T0.schema_id
                 where T0.name='ACSIAR' and T1.name='dbo')
CREATE TABLE [dbo].[ACSIAR](
	[HCAI_Document_Number] [nvarchar](11) NOT NULL,
	[PMSFields_PMSDocumentKey] [nvarchar](20) NOT NULL,
	[Submission_Date] [datetime] NOT NULL,
	[Submission_Source] [nvarchar](3) NOT NULL,
	[Adjuster_Response_Date] [datetime] NOT NULL,
	[In_Dispute] [bit] NOT NULL,
	[Document_Type] [nvarchar](20) NOT NULL,
	[Document_Status] [nvarchar](20) NOT NULL,
	[Attachments_Received_Date] [date] NULL,

	[Claimant_First_Name] [nvarchar](50) NOT NULL,
	[Claimant_Middle_Name] [nvarchar](50) NULL,
	[Claimant_Last_Name] [nvarchar](50) NOT NULL,
	[Claimant_Date_Of_Birth] [date] NOT NULL,
	[Claimant_Gender] [nvarchar](1) NOT NULL,
	[Claimant_Telephone] [nvarchar](20) NULL,
	[Claimant_Extension] [nvarchar](10) NULL,
	[Claimant_Address1] [nvarchar](50) NOT NULL,
	[Claimant_Address2] [nvarchar](50) NULL,
	[Claimant_City] [nvarchar](50) NOT NULL,
	[Claimant_Province] [nvarchar](2) NOT NULL,
	[Claimant_Postal_Code] [nvarchar](10) NOT NULL,

	[Insurer_IBCInsurerID] [nvarchar](50) NOT NULL,
	[Insurer_IBCBranchID] [nvarchar](50) NOT NULL,

	[Status_Code] [nvarchar](2) NOT NULL,
	[Messages] [nvarchar](200) NOT NULL,

	[ACSI_ReimbursableGoodsAndServices_AdjusterResponseExplanation] [nvarchar](500) NULL,

	[ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_MOHDebits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2Debits_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_MOH_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer1_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_OtherInsuranceAmounts_Insurer2_OtherService_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_OtherInsuranceAmounts_AdjusterResponseExplanation] [nvarchar](500) NULL,

	[ACSI_InsurerSignature_ClaimFormReceived] [bit] NULL,
	[ACSI_InsurerSignature_ClaimFormReceivedDate] [date] NULL,

	[ACSI_InsurerTotals_MOH_Approved] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_OtherInsurers_Approved] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_AutoInsurerTotal_Approved] [decimal](19, 2) NOT NULL,
	
	[ACSI_InsurerTotals_GST_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,
	
	[ACSI_InsurerTotals_PST_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[ACSI_InsurerTotals_SubTotal_Approved] [decimal](19, 2) NOT NULL,
	
	[ACSI_InsurerTotals_Interest_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](200) NULL,
	[ACSI_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](200) NULL,

	[SigningAdjuster_FirstName] [nvarchar](50) NOT NULL,
	[SigningAdjuster_LastName] [nvarchar](50) NOT NULL,
	[ApprovedByOnPDF] [nvarchar](250) NULL,
	[Archival_Status] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ACSIAR] PRIMARY KEY CLUSTERED 
(
	[HCAI_Document_Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF not exists (select T0.object_id from sys.tables T0 join sys.schemas T1 on T1.schema_id = T0.schema_id
                 where T0.name='GSACSILineItemAR' and T1.name='dbo')
CREATE TABLE [dbo].[GSACSILineItemAR](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HCAI_Document_Number] [nvarchar](11) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PMSGSKey] [nvarchar](10) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_GST] [bit] NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_PST] [bit] NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_LineCost] [decimal](19, 2) NOT NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCode] [nvarchar](20) NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_ReasonCodeDesc] [nvarchar](255) NULL,
	[ACSI_ReimbursableGoodsAndServices_Items_Item_Approved_ReasonCodeGroup_OtherReasonCodeDesc] [nvarchar](255) NULL,
	CONSTRAINT [FK_GSACSILineItemAR_ACSIAR] FOREIGN KEY ([HCAI_Document_Number]) REFERENCES dbo.[ACSIAR] ([HCAI_Document_Number]),
 CONSTRAINT [PK_GSACSILineItemAR] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
