﻿IF not exists (select T0.object_id from sys.tables T0 join sys.schemas T1 on T1.schema_id = T0.schema_id
                 where T0.name='InitInfo' and T1.name='dbo')
CREATE TABLE [dbo].[InitInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DateOcf18Providers] [datetime] NULL,
	[DateOcf23Providers] [datetime] NULL,
	[ActivityOcf18] [datetime] NULL,
	[ActivityOcf23] [datetime] NULL,
	[ActivityOcf21b] [datetime] NULL,
	[ActivityOcf21c] [datetime] NULL,
	[ActivityAcsi] [datetime] NULL,
	[PatientsOcf18] [datetime] NULL,
	[PatientsOcf23] [datetime] NULL,
	[PatientsOcf21b] [datetime] NULL,
	[PatientsOcf21c] [datetime] NULL,
	[PatientsForm1] [datetime] NULL,
	[PatientsAcsi] [datetime] NULL,
 CONSTRAINT [PK_InitInfo] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'InitInfo' AND TABLE_SCHEMA='dbo' AND COLUMN_NAME = 'ActivityAcsi')
alter table [dbo].[InitInfo] add
    [ActivityAcsi] [datetime] NULL,
	[PatientsAcsi] [datetime] NULL
GO