﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using HcaiSync.Common.Utils;

namespace HcaiSync.Common.Sql
{
    public abstract class AbstractDataBaseConnection : IDisposable
    {
        public static bool CheckConnection(string connectString, out string error)
        {
            error = string.Empty;
            if (string.IsNullOrEmpty(connectString))
            {
                error = "Connection string empty!";
                return false;
            }

            try
            {
                using (var connection = new SqlConnection(connectString))
                {
                    connection.Open();
                    connection.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }

        #region life circle

        protected SqlConnection ConnectionDb { get; }

        protected string ConnectionString { get; }

        protected AbstractDataBaseConnection(string connectString)
        {
            ConnectionString = connectString;
            ConnectionDb = new SqlConnection(connectString);
            ConnectionDb.Open();
        }

        public void Dispose()
        {
            ConnectionDb.Close();
            ConnectionDb.Dispose();
        }

        public string DataSource()
        {
            var builder = new SqlConnectionStringBuilder(ConnectionString);
            return builder.DataSource;
        }

        public string Basename()
        {
            var builder = new SqlConnectionStringBuilder(ConnectionString);
            return builder.InitialCatalog;
        }

        #endregion

        #region Sql

        public DataRow[] ExecDataQuery(string sql, int commandTimeout = 180)
        {
            using var cmd = new SqlCommand(sql, ConnectionDb);
            try
            {
                cmd.CommandTimeout = commandTimeout;
                var table = new DataTable();
                using (var adapter = new SqlDataAdapter(cmd))
                    adapter.Fill(table);
                return table.Rows.OfType<DataRow>().ToArray();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int ExecNoDataQuery(string sql, bool strong, int commandTimeout = 180)
        {
            using var cmd = new SqlCommand(sql, ConnectionDb);
            try
            {
                if (ConnectionDb.State == ConnectionState.Closed)
                    ConnectionDb.Open();
                cmd.CommandTimeout = commandTimeout;
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                if (strong)
                    throw;
                return -1;
            }
        }

        public int ExecNoDataQueryWithParams(string sql, List<(SqlParameter parameter, object value)> sqlParams, int commandTimeout = 180)
        {
            using var cmd = new SqlCommand(sql, ConnectionDb);
            try
            {
                if (sqlParams != null)
                {
                    foreach (var sqlParameter in sqlParams)
                    {
                        cmd.Parameters.Add(sqlParameter.parameter).Value = sqlParameter.value;
                    }
                }
                if (ConnectionDb.State == ConnectionState.Closed)
                    ConnectionDb.Open();
                cmd.CommandTimeout = commandTimeout;
                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static int TryExecuteNonQuery(SqlCommand cmd)
        {
            try
            {
                if (cmd.Connection.State != ConnectionState.Open)
                    cmd.Connection.Open();
                var res = cmd.ExecuteNonQuery();
                return res;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public static DataTable TryFillDataTable(SqlCommand cmd)
        {
            var table = new DataTable();
            using var adapter = new SqlDataAdapter(cmd);
            adapter.Fill(table);
            return table;
        }

        public DataTable ExecQueryData(string strSql, int timeout = 180)
        {
            using var command = new SqlCommand(strSql, ConnectionDb);
            command.CommandTimeout = timeout;
            return TryFillDataTable(command);
        }

        public DataTable ExecQueryDataWithParams(string strSql, List<(SqlParameter parameter, object value)> sqlParams, int timeout = 180)
        {
            using var command = new SqlCommand(strSql, ConnectionDb);
            if (sqlParams != null)
            {
                foreach (var sqlParameter in sqlParams)
                {
                    command.Parameters.Add(sqlParameter.parameter).Value = sqlParameter.value;
                }
            }
            command.CommandTimeout = timeout;
            return TryFillDataTable(command);
        }

        public DataTable ReadTable(string query)
        {
            return ExecQueryData(query);
        }

        #endregion

        #region run scripts

        public bool RunMySqlScripts(string fileName, Assembly assembly = null, bool strong = true)
        {
            try
            {
                var scripts = SqlScriptHelper.GetMySqlScripts(fileName, assembly);
                return RunSqlScripts(scripts, strong);
            }
            catch (Exception ex)
            {
                if (strong)
                    throw;
                return false;
            }
        }

        private bool RunSqlScripts(string[] scripts, bool strong = true)
        {
            if (!scripts.Any()) return true;

            var dictTimes = new Dictionary<string, int>();
            foreach (var script in scripts)
            {
                var sql = script;

                var dtStart = DateTime.Now;
                ExecNoDataQuery(sql, strong);

                var msec = (int)(DateTime.Now - dtStart).TotalMilliseconds;
                try
                {
                    if (!dictTimes.ContainsKey(sql))
                        dictTimes.Add(sql, msec);
                }
                catch (Exception ex)
                {
                    msec = 0;
                }
            }

            var ordered = dictTimes.OrderByDescending(d => d.Value).FirstOrDefault();
            if (ordered.Value > 3000)
            {
                ordered = new KeyValuePair<string, int>();
            }

            return true;
        }

        #endregion

        #region sql to model reading

        private static object ChangeType(object value, Type conversion)
        {
            var t = conversion;

            if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                if (value is null) return null;
                t = Nullable.GetUnderlyingType(t);
            }

            return Convert.ChangeType(value, t);
        }

        private static List<T> DataTableToList<T>(DataTable table) where T : class, new()
        {
            try
            {
                var list = new List<T>();

                var ignoredNames = typeof(T).GetProperties()
                    .Where(info => info.GetCustomAttributes(true).Any())
                    .Select(info => info.Name)
                    .ToList();

                foreach (DataRow row in table.Rows)
                {
                    var obj = new T();

                    foreach (var prop in obj.GetType().GetProperties().Where(p => ignoredNames.All(n => n != p.Name)))
                    {
                        var columnName = prop.Name;

                        try
                        {
                            if (prop.PropertyType.FullName?.StartsWith("Uo.") == true)
                                continue;
                            var value = row[columnName];
                            if (value is DBNull) continue;

                            var typedValue = ChangeType(value, prop.PropertyType);
                            prop.SetValue(obj, typedValue, null);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

        public List<T> ReadTable<T>(string query, List<(SqlParameter parameter, object value)> sqlParams = null)
            where T : class, new()
        {
            try
            {
                return DataTableToList<T>(ExecQueryDataWithParams(query, sqlParams));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion
    }
}
