//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HcaiSync.Common.EfData
{
    using System;
    using System.Collections.Generic;
    
    public partial class OCF21BAR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OCF21BAR()
        {
            this.GS21BLineItemAR = new HashSet<GS21BLineItemAR>();
        }
    
        public string HCAI_Document_Number { get; set; }
        public string PMSFields_PMSDocumentKey { get; set; }
        public System.DateTime Submission_Date { get; set; }
        public string Submission_Source { get; set; }
        public System.DateTime Adjuster_Response_Date { get; set; }
        public bool In_Dispute { get; set; }
        public string Document_Type { get; set; }
        public string Document_Status { get; set; }
        public Nullable<System.DateTime> Attachments_Received_Date { get; set; }
        public string Claimant_First_Name { get; set; }
        public string Claimant_Middle_Name { get; set; }
        public string Claimant_Last_Name { get; set; }
        public System.DateTime Claimant_Date_Of_Birth { get; set; }
        public string Claimant_Gender { get; set; }
        public string Claimant_Telephone { get; set; }
        public string Claimant_Extension { get; set; }
        public string Claimant_Address1 { get; set; }
        public string Claimant_Address2 { get; set; }
        public string Claimant_City { get; set; }
        public string Claimant_Province { get; set; }
        public string Claimant_Postal_Code { get; set; }
        public string Insurer_IBCInsurerID { get; set; }
        public string Insurer_IBCBranchID { get; set; }
        public string Status_Code { get; set; }
        public string Messages { get; set; }
        public string OCF21B_ReimbursableGoodsAndServices_AdjusterResponseExplanation { get; set; }
        public decimal OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOHDebits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2Debits_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_MOH_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer1_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_LineCost { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_Insurer2_Total_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public string OCF21B_OtherInsuranceAmounts_AdjusterResponseExplanation { get; set; }
        public Nullable<bool> OCF21B_InsurerSignature_ClaimFormReceived { get; set; }
        public Nullable<System.DateTime> OCF21B_InsurerSignature_ClaimFormReceivedDate { get; set; }
        public decimal OCF21B_InsurerTotals_MOH_Approved { get; set; }
        public decimal OCF21B_InsurerTotals_OtherInsurers_Approved { get; set; }
        public decimal OCF21B_InsurerTotals_AutoInsurerTotal_Approved { get; set; }
        public decimal OCF21B_InsurerTotals_GST_Approved_LineCost { get; set; }
        public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_InsurerTotals_GST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_InsurerTotals_PST_Approved_LineCost { get; set; }
        public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_InsurerTotals_PST_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public decimal OCF21B_InsurerTotals_SubTotal_Approved { get; set; }
        public decimal OCF21B_InsurerTotals_Interest_Approved_LineCost { get; set; }
        public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCode { get; set; }
        public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_ReasonCodeDesc { get; set; }
        public string OCF21B_InsurerTotals_Interest_Approved_ReasonCodeGroup_OtherReasonCodeDesc { get; set; }
        public string SigningAdjuster_FirstName { get; set; }
        public string SigningAdjuster_LastName { get; set; }
        public string ApprovedByOnPDF { get; set; }
        public string Archival_Status { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GS21BLineItemAR> GS21BLineItemAR { get; set; }
    }
}
