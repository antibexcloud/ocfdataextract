﻿using System;
using System.Data.Entity;
using System.Data.SqlClient;
using HcaiSync.Common.Utils;

namespace HcaiSync.Common.EfData
{
    public partial class HCAIOCFSyncEntities : DbContext
    {
        private const bool AutoDetectChanges = false;

        private string ConnectString { get; set; }
        public bool Inited { get; }
        public string Name { get; private set; }

        private HCAIOCFSyncEntities(string connectionString) : base(connectionString)
        {
            try
            {
                Configuration.AutoDetectChangesEnabled = AutoDetectChanges;
                Inited = true;
            }
            catch (Exception ex)
            {
                Inited = false;
                throw;
            }
        }

        private static HCAIOCFSyncEntities Open(string serverName, string dbName, bool integratedSecurity, string login, string password)
        {
            var entityBuilder = new System.Data.Entity.Core.EntityClient.EntityConnectionStringBuilder
            {
                ProviderConnectionString = $"data source={serverName};initial catalog={dbName};MultipleActiveResultSets=True;App=EntityFramework;",
                Provider = "System.Data.SqlClient",
                Metadata = @"res://*/EfData.SyncModel.csdl|res://*/EfData.SyncModel.ssdl|res://*/EfData.SyncModel.msl"
            };

            entityBuilder.ProviderConnectionString +=
                (integratedSecurity
                    ? "Integrated Security=True;Persist Security Info=False"
                    : $"Persist security info=True;User id={login};Password={password};");

            var connectionString = entityBuilder.ConnectionString;

            return new HCAIOCFSyncEntities(connectionString) { Name = dbName };
        }

        public static HCAIOCFSyncEntities OpenFromSqlConnection(string connectString)
        {
            try
            {
                // prepare data
                var builder = new SqlConnectionStringBuilder(connectString);

                var name = builder.InitialCatalog;
                var serverName = builder.DataSource;
                var login = builder.UserID;
                var password = builder.Password;
                var integratedSecurity = builder.IntegratedSecurity;

                // open
                var dbContext = Open(serverName, name, integratedSecurity, login, password);
                dbContext.ConnectString = connectString;
                return dbContext;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void SaveChangesWithDetect()
        {
            if (!AutoDetectChanges)
            {
                ChangeTracker.DetectChanges();
            }

            SaveChanges();
        }

        public void UpdateMetadata()
        {
            var sqlHelper = new HcaiSyncConnection(ConnectString);
            sqlHelper.RunMySqlScripts("HcaiSync.Common.SqlScripts.InitInfo.sql");
            sqlHelper.RunMySqlScripts("HcaiSync.Common.SqlScripts.ACSI.sql");
        }
    }
}
