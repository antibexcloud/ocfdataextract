﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace HcaiSync.Common.EfData
{
    public partial class Patient
    {
        [NotMapped]
        public string ResultFirstName
        {
            get => !string.IsNullOrEmpty(this.manualFirstName) ? this.manualFirstName : this.first_name;
            set => this.manualFirstName = value;
        }

        [NotMapped]
        public string ResultMiddleName
        {
            get => !string.IsNullOrEmpty(this.manualMiddleName) ? this.manualMiddleName : this.middle_name;
            set => this.manualMiddleName = value;
        }

        [NotMapped]
        public string ResultLastName
        {
            get => !string.IsNullOrEmpty(this.manualLastName) ? this.manualLastName : this.last_name;
            set => this.manualLastName = value;
        }

        [NotMapped]
        public string ResultGender
        {
            get => !string.IsNullOrEmpty(this.manualGender) ? this.manualGender : this.gender;
            set => this.manualGender = value;
        }

        [NotMapped]
        public DateTime ResultDOB
        {
            get => this.manualDOB ?? this.DOB;
            set => this.manualDOB = value;
        }
    }
}
