﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HcaiSync.Common.EfData
{
    public partial class ProviderLineItem
    {
        [NotMapped]
        public string ResultFirstName
        {
            get => !string.IsNullOrEmpty(this.manualFirstName) ? this.manualFirstName : this.Provider_First_Name;
            set => this.manualFirstName = value;
        }

        [NotMapped]
        public string ResultLastName
        {
            get => !string.IsNullOrEmpty(this.manualLastName) ? this.manualLastName : this.Provider_Last_Name;
            set => this.manualLastName = value;
        }
    }
}
