﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HcaiSync.Common.EfData
{
    public partial class ProviderVirtual
    {
        [NotMapped]
        public string ResultFirstName
        {
            get => this.Provider_First_Name;
            set => this.Provider_First_Name = value;
        }

        [NotMapped]
        public string ResultLastName
        {
            get => this.Provider_Last_Name;
            set => this.Provider_Last_Name = value;
        }
    }
}
