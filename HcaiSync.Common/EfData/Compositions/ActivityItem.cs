﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HcaiSync.Common.EfData
{
    public partial class ActivityItem
    {
        [NotMapped]
        public string ResultMeasure
        {
            get => !string.IsNullOrEmpty(this.manualMeasure) ? this.manualMeasure : this.mvaMeasure;
            set => this.manualMeasure = value;
        }

        [NotMapped]
        public double ResultUnits
        {
            get => this.manualUnits ?? this.mvaUnits;
            set => this.manualUnits = value;
        }

        [NotMapped]
        public string ResultAttribute
        {
            get => !string.IsNullOrEmpty(this.manualAttribute) ? this.manualAttribute : this.mvaAttribute;
            set => this.manualAttribute = value;
        }
    }
}
